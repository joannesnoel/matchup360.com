var BOSH_SERVICE;
var connection = null;
var xmpp_user = null;
var xmpp_pass = null;
var localStream = null;
var RTC = null;
var publisher = null;
var session = null;
var subscriber = null;
var vChatStatus = {status: "ready", peer_id: null};

	
function connectXMPP(){
	if(window.location.protocol == 'https:')
		BOSH_SERVICE = 'https://www.matchup360.com:5281/http-bind';
	else
		BOSH_SERVICE = 'http://www.matchup360.com:5280/http-bind';
	connection = new Strophe.Connection(BOSH_SERVICE);
	
	// Strophe debugging
	//connection.rawInput = function(a) { console.log("in:", a);};
	//connection.rawOutput = function(a) {console.log("out:", a);};
	
	connection.connect(xmpp_user,
							xmpp_pass,
							onConnect);
}

function onConnect(status)
{
	
	if (status === Strophe.Status.CONNECTED) { 
		connection.addHandler(receiveChat, null,'message' ,'chat');		
		connection.addHandler(receiveMsg, null,'message' ,'msg');
		connection.addHandler(receiveHeadline, null,'message' ,'headline');
		connection.addHandler(presenceHandler, null, 'presence');
		connection.addHandler(notificationHandler, null, 'message' , 'notification');
		connection.send($pres().c('show').t('chat').up().c('priority').t('1').tree());
		connection.sendIQ($iq({type: "get"}).c("query", {xmlns: 
							Strophe.NS.ROSTER}), manageRoster);							
	} else if (status === Strophe.Status.DISCONNECTED) {
		console.log("XMPP/Strophe Disconnected.");
		window.location = "http://matchup360.com/welcome/logout";					
	}
}
function logout(){
	connection.disconnect();
}
function AnswerCall(jid,peer_id){
	if(session == null){
		var stanza = $msg({'to': jid, 'type': 'headline', 'command':'video-session-accepted', 'from_id':uid, 'from_fullname':my_fullname, 'from_photo':my_profile_pic});
		connection.send(stanza);
		vChatStatus = {'status':'connecting','peer_id':peer_id};
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax/getVChatConnection',
			type: "post",
			data: "to_uid="+peer_id+"&from_uid="+uid,
			success: function(response){
				vChatConnect(response.sessionId,response.tokenId,peer_id);
			}
		});
	}else{
		setTimeout(function(){
			AnswerCall(jid,peer_id);
		},200)
	}
}
function notificationHandler(stanza){
	var jid = $(stanza).attr('from');
	var type = $(stanza).attr('notification_type');
	var from_fullname = $(stanza).attr('from_fullname');
	var photo = $(stanza).attr('from_photo');
	var notification = $(stanza).attr('notification_string');
	
	var unread_notification_count = parseInt($('#activity_notification').html());
	if(xmpp_user != jid){
		if($('#activities').hasClass('active')){
			switch(type){
				case 'photo_like':
					$('#notification-list').prepend('<div class="read notification-box">'+notification+'</div>');
					$('.photo_notification').colorbox({
						onComplete: function(){
							photo_colorbox_oncomplete()
						}
					});
				break;
				case 'follow':
					$('#notification-list').prepend('<div class="read notification-box">'+notification+'</div>');
				break;
				case 'comment':
					$('#notification-list').prepend('<div class="read notification-box">'+notification+'</div>');
					$('.photo_notification').colorbox({
						onComplete: function(){
							photo_colorbox_oncomplete()
						}
					}); 
				break;
				case 'interested':
					$('#notification-list').prepend('<div class="read notification-box">'+notification+'</div>');
					$('#notification-list .read').first().find('.mCard').click(function(){
						var notification = $(this);
						$.ajax({
							url: './../ajax/revealAdmirer',
							data: 'notification_id='+notification.attr('data-id'),
							type: 'post',
							success: function(data){
								if(data.status == 1){
									notification.html(''+data.notification_string);
									$('#matchup_coins i').html(data.new_points);
								}else
									$.colorbox({href:'/widgets/get_credits'});
							}
						});
					});
				break;
			}
		}
		else{
			$('#activity_notification').html(unread_notification_count + 1);
			$('#activity_notification').show();
		}
	}
	return true;
}
function receiveHeadline(stanza){
	var jid = $(stanza).attr('from');
	var peer_id = $(stanza).attr('from_id');
	var command = $(stanza).attr('command');
	var from_fullname = $(stanza).attr('from_fullname');
	var profile_pic = $(stanza).attr('from_photo');
	
	switch(command){
		case 'video-session-invite':
			playSound("./assets/sounds/telephone-ring-3.mp3",true);
			if($('#call_to_'+peer_id).length > 0){
				$("#soundContainer audio").stop().remove();
				vChatStatus = {'status':'connecting','peer_id':peer_id};
				$.ajax({
					url: window.location.protocol + '//' + window.location.hostname + '/ajax/getVChatConnection',
					type: "post",
					data: "to_uid="+peer_id+"&from_uid="+uid,
					success: function(response){
						vChatConnect(response.sessionId,response.tokenId,peer_id);
					}
				});
				$('#call_to_'+peer_id).remove();
			}else{
				var stanza = $msg({'to': jid, 'type': 'headline', 'command':'video-session-ringing', 'from_id':uid, 'from_fullname':my_fullname, 'from_photo':my_profile_pic});
				connection.send(stanza);
				$('body').append('<div id="got_call_'+peer_id+'"><img src="'+profile_pic+'" height="60" width="60" />'+from_fullname+' is video calling you.</div>');
				$('#got_call_'+peer_id).dialog({resizable: false,
												  height:200,
												  modal: true,
												  buttons: {
													Answer: function() {
															$("#soundContainer audio").stop().remove();
															if(session != null){
																session.disconnect();
															}
															AnswerCall(jid,peer_id);															
															$( this ).dialog( "close" );
														},
													Reject: function(){
														var stanza = $msg({'to': jid, 'type': 'headline', 'command':'video-session-reject', 'from_id':uid, 'from_fullname':my_fullname, 'from_photo':my_profile_pic});
														connection.send(stanza);
														$("#soundContainer audio").stop().remove();
														$( this ).dialog( "close" );
													}
												  },
												  close: function( event, ui ) {
													$(this).dialog('destroy');
													$(this).remove();
												  }
												});
			}
		break;
		case 'video-session-ringing':
			playSound("./assets/sounds/telephone-ring-3.mp3",true);
		break;
		case 'video-session-cancel':
			$("#soundContainer audio").stop().remove();
			$('#got_call_'+peer_id).dialog("close");
			chatWindow = $('#'+peer_id+'.chat-window');
			
			chatWindow.find('.video-call').hide();
			chatWindow.find('.hang-up').hide();
			vChatStatus = {status: "ready", peer_id: null};
		break;
		case 'video-session-reject':
			$("#soundContainer audio").stop().remove();
			$('#call_to_'+peer_id).dialog("close");
			chatWindow = $('#'+peer_id+'.chat-window');
			
			chatWindow.find('.video-call').hide();
			chatWindow.find('.hang-up').hide();
			chatWindow.find('.messages').append('<div class="msg notice">Call rejected by '+from_fullname+'.</div>');
			chatWindow.find('.messages').scrollTo(chatWindow.find('.msg').last());
			vChatStatus = {status: "ready", peer_id: null};
		break;
		case 'video-session-accept':
			$.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/ajax/getVChatConnection',
				type: "post",
				data: "to_uid="+peer_id+"&from_uid="+uid,
				success: function(response){
					vChatConnect(response.sessionId,response.tokenId,peer_id);
				}
			});
		break;
		case 'video-session-accepted':
			$("#soundContainer audio").stop().remove();
			$('#call_to_'+peer_id).dialog("close");
			$.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/ajax/getVChatConnection',
				type: "post",
				data: "to_uid="+peer_id+"&from_uid="+uid,
				success: function(response){
					vChatConnect(response.sessionId,response.tokenId,peer_id);
				}
			});
		break;
	}
	return true;
}
function presenceHandler(data){
	var jid = $(data).attr('from');
	var type = $(data).attr('type');
	jid = jid.split("/");
	jid = jid[0];
	var xmpp_user = jid.split("@");
	xmpp_user = xmpp_user[0];
	if(jid){
		if(type == 'unavailable'){
			$('[data-jid="'+jid+'"]').removeClass('online');
			$('.mCard[data-xmpp-user="'+xmpp_user+'"]').removeClass('online');
		}else{
			$('[data-jid="'+jid+'"]').addClass('online');
			$('.mCard[data-xmpp-user="'+xmpp_user+'"]').addClass('online');
		}
	}
	sortContactList();
	return true;
}
function manageRoster(msg){
	var elems = msg.getElementsByTagName('item');
	for (var i = 0; i < elems.length; i++) {
	  subscription = $(elems[i]).attr('subscription');
	  jid = $(elems[i]).attr('jid');
	  if(subscription == 'to' || subscription == 'both')
		connection.send($pres({type:'probe',to:jid,from:xmpp_user}).tree());
	}
}
function receiveMsg(msg){
	var count = parseInt($("#message_notification").html());
	$("#message_notification").html(count+1);
	$("#message_notification").show();
	return true;
}
function receiveChat(msg){
	var elems = msg.getElementsByTagName('body');
	var body = elems[0];
	var decoded = $("<div/>").html(body.textContent).text();
	var message = eval('('+decoded+')');
	from_id = message.from_uid;
	fullname = message.from_fullname;
	profile_pic = message.from_profile_pic;
	message_id = message.msg_id;
	
	var threadsList = $('#threadsList');
	var messagesBox = $('#messagesBar #messagesBox');
	if($('#messagesBar').css('left') != '0px'){
		count = parseInt($("#message_notification").html());
		$("#message_notification").html(count+1);
		$("#message_notification").show();
		playSound("./assets/sounds/chat-sound.mp3",false);
	}
	if(threadsList.find('.thread').first().attr('data-peerid') != from_id){
		playSound("./assets/sounds/chat-sound.mp3",false);
		$(threadsList.find('[data-peerid="'+from_id+'"]')[0]).remove();
		thread = '<div class="thread unread" data-peerid="'+from_id+'">\
							<img src="'+profile_pic+'" width="50">\
							<div class="name">'+fullname+'</div>\
							<div class="message">'+message.body+'</div>\
						</div>';
		thread = $(thread).click(loadMessageThread);
		threadsList.prepend(thread);
	}else{
		threadsList.find('.thread[data-peerid="'+from_id+'"] .message').html(message.body);
	}
	if(messagesBox.attr('data-peerid') == from_id){
		if(messagesBox.find('.messages').length == 0)
			messagesBox.prepend('<div class="messages"></div>');
		if(messagesBox.find('.messages .line').last().hasClass('recieved')){
			messagesBox.find('.messages .line').last().append('<p>'+message.body+'</p>');
		}else{
			messagesBox.find('.messages').append('<div class="line recieved" data-from-uid="'+from_id+'"><p>'+message.body+'</p></div>');
		}
	}
	$('#messagesBar #messagesBox .messages').scrollTo($('#messagesBar #messagesBox .messages p').last());
	
	$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax/messageReceived',
			type: 'post',
			data: "msg_id="+message_id,
			success: function(data){
				
			}
	});
	return true;
}

function openChatWindow(peer_id,fullname,profile_pic, address, message){
	if($('#'+peer_id+'.chat-window').length > 0)
		return;
	var bar = '<div class="chat-window" id="'+peer_id+'" data-ot-session="" style="display:none"> \
					<div class="peer_info"><img width="50" height="50" src="'+profile_pic+'" id="peer-id"/>\
					<div class="peer-info-box"><span class="peer-name">'+fullname+'</span><br /><span class="peer-address">'+address+'</span></div>\
					<button class="video-call" style="display:none">Video Call</button><button style="display:none" class="hang-up">End Call</button></div>\
					<div style="clear:both"></div> \
					<div class="video" style="display:none">\
						<div class="peer_video"></div> \
						<div class="my_video"></div> \
					</div> \
					<div class="messaging"> \
						<div class="messages"></div>	\
						<div class="typing-area">\
						<input type="text" class="message-to-send" /><button class="send" type="button">Send</button>\
					</div> \
					</div> \
					</div>';
		$('body').append(bar);
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax/getMessageThread/'+peer_id,
			type: 'post',
			success: function(response){
				var start = 0;
				for(var i=start; i<messages.length; i++){
					received = '';
					if(messages[i].from_uid == peer_id)
						received = ' received';
					message_line = '<div class="msg'+received+'" id="msg_'+messages[i].msg_id+'"><div class="sender">'+messages[i].fullname+': </div><div class="message">'+messages[i].message+'</div><div style="clear:both"></div></div>';
					$('#'+peer_id+'.chat-window').find('.messages').append(message_line);
				}
				$('#'+peer_id+'.chat-window').find('.messages').scrollTo($('#'+peer_id+'.chat-window').find('.msg').last());
			}
		});
		var nOfDialogs = $('.ui-dialog:not(.minimized-bar)').length;
		lastDialog = $('.ui-dialog')[nOfDialogs-1];
		$('#'+peer_id+'.chat-window').dialog({ 
				title: 'Chat - '+fullname,
				position: { my: "center+20 center+20", at: "center center", of: lastDialog },
				width: 348,
				resizable: false,
				beforeClose: function(event,ui){
					if(vChatStatus.status == 'connecting'){
						alert('Video session in progress. Cannot close.');
						return false;
					}
					if(session != null)
						session.disconnect();
				},
				close: function( event, ui ) {
					$(this).dialog('destroy');
					$(this).remove();
				},
				focus: function( event, ui ){
					$('.chat-window').parents('.ui-dialog').removeClass('focused');
					$(event.target).parents('.ui-dialog').addClass('focused');
					if($(event.target).parents('.ui-dialog').hasClass('minimized-bar')){
						var otop = $(event.target).parents('.ui-dialog').offset().top;
						var oleft = $(event.target).parents('.ui-dialog').offset().left;
						$(event.target).parents('.ui-dialog').css({position:'absolute',top:otop,left:oleft});
						$(event.target).parents('.ui-dialog').appendTo('body');
						$(event.target).parents('.ui-dialog').removeClass('minimized-bar');
						$(event.target).parents('.ui-dialog').animate({
							height: $(event.target).parents('.ui-dialog').attr('data-old-height'),
							width: $(event.target).parents('.ui-dialog').attr('data-old-width'),
							top: $(event.target).parents('.ui-dialog').attr('data-old-top'),
							left: $(event.target).parents('.ui-dialog').attr('data-old-left')
						}, 200,'swing',function(){
								$($(event.target).parents('.ui-dialog').find('.chat-window')[0]).show();
							});
						$($(event.target).parents('.ui-dialog').find('.ui-dialog-titlebar-minimize')[0]).show();
					}
				},
				open: function( event, ui ) {
					$('#peer-id').click(function(){loadPage('view_profile/'+peer_id);});
					$($(this).parent().find('.ui-dialog-titlebar')[0]).append('<a class="ui-dialog-titlebar-minimize ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span></a>');
					$($(this).parent().find('.ui-dialog-titlebar')[0]).click(function(event){
						if($(this).parents('.ui-dialog').hasClass('minimized-bar') && $(event.target).hasClass('ui-dialog-title')){
							var otop = $(this).parents('.ui-dialog').offset().top;
							var oleft = $(this).parents('.ui-dialog').offset().left;
							$(this).parents('.ui-dialog').css({position:'absolute',top:otop,left:oleft});
							
							$(this).parents('.ui-dialog').appendTo('body');
							$(this).parents('.ui-dialog').removeClass('minimized-bar');
							$(this).parents('.ui-dialog').animate({
								height: $(this).parents('.ui-dialog').attr('data-old-height'),
								width: $(this).parents('.ui-dialog').attr('data-old-width'),
								top: $(this).parents('.ui-dialog').attr('data-old-top'),
								left: $(this).parents('.ui-dialog').attr('data-old-left')
							}, 200,'swing',function(){
								$($(this).find('.chat-window')[0]).show();
							});
							$($(this).find('.ui-dialog-titlebar-minimize')[0]).show();
						}
					});
					$($(this).parent().find('.ui-dialog-titlebar-minimize')).click(function(){
						var noOfMinimizedBars = $('.minimized-bar').length;
						$(this).parents('.ui-dialog').addClass('minimized-bar');
						var oldWidth = $(this).parents('.ui-dialog').css('width');
						var oldHeight = $(this).parents('.ui-dialog').css('height');
						var oldTopPos = $(this).parents('.ui-dialog').css('top');
						var oldLeftPos = $(this).parents('.ui-dialog').css('left');
						$(this).parents('.ui-dialog').attr('data-old-width',oldWidth);
						$(this).parents('.ui-dialog').attr('data-old-height',oldHeight);
						$(this).parents('.ui-dialog').attr('data-old-top',oldTopPos);
						$(this).parents('.ui-dialog').attr('data-old-left',oldLeftPos);
						minimizeBtn = $(this);
						$(this).parents('.ui-dialog').animate({
							height: '28px',
							width: '130px',
							left: 140*noOfMinimizedBars+4+'px',
							top: $(window).height() - 50
						}, 200,'swing',function(){
							$(minimizeBtn).parents('.ui-dialog').css({position:'relative',top: '0px', left: '0px', display:'inline-block'});
							$(minimizeBtn).parents('.ui-dialog').appendTo('#bottom-bar');
							$(minimizeBtn).hide();
							$($(minimizeBtn).parents('.ui-dialog').find('.chat-window')[0]).css('top','0px');
						});
					});
					
					var chatWindow = $('#'+peer_id+'.chat-window');
					if(chatWindow.find('.msg').length != 0)
						$(chatWindow.find('.messages')[0]).scrollTo(chatWindow.find('.msg').last());
					$('#'+peer_id+'.chat-window .typing-area .send').click(sendChat);
					$('#'+peer_id+'.chat-window .typing-area .message-to-send').bind('keypress', function(e) {
																							var code = (e.keyCode ? e.keyCode : e.which);
																						    if(code == 13) { //Enter keycode
																						      sendChat(this);
																						    }
																						});
					$('#'+peer_id+'.chat-window .video-call').click(function(){
						if($('#got_call_'+peer_id).length>0){
							vChatStatus = {'status':'connecting','peer_id':peer_id};
							$.ajax({
								url: window.location.protocol + '//' + window.location.hostname + '/ajax/getVChatConnection',
								type: "post",
								data: "to_uid="+peer_id+"&from_uid="+uid,
								success: function(response){
									vChatConnect(response.sessionId,response.tokenId,peer_id);
								}
							});
						}else{
							$.ajax({
								url: window.location.protocol + '//' + window.location.hostname + '/ajax/getUserData',
								type: "post",
								data: "uid="+peer_id,
								success: function(response){
									var stanza = $msg({'to': response.xmpp_user+'@matchup360.com', 'type': 'headline', 'command':'video-session-invite', 'from_id':uid, 'from_fullname':my_fullname, 'from_photo':my_profile_pic});
									connection.send(stanza);
									vChatStatus = {'status':'connecting','peer_id':peer_id};
									$('body').append('<div id="call_to_'+response.uid+'">Video calling '+response.firstname+' '+response.lastname+'</div>');
									$('#call_to_'+response.uid).dialog({resizable: false,
																	  height:140,
																	  modal: true,
																	  buttons: {
																		Cancel: function() {
																		  var stanza = $msg({'to': response.xmpp_user+'@matchup360.com', 'type': 'headline', 'command':'video-session-cancel', 'from_id':uid, 'from_fullname':my_fullname, 'from_photo':my_profile_pic});
																		  connection.send(stanza);
																		  $( this ).dialog( "close" );
																		  
																		  $('#'+peer_id+'.chat-window .video-call').hide();
																		  $('#'+peer_id+'.chat-window .hang-up').hide();
																		  $("#soundContainer audio").stop().remove();
																		  vChatStatus = {status: "ready", peer_id: null};
																		}
																	  },
																	  close: function( event, ui ) {
																		$(this).dialog('destroy');
																		$(this).remove();
																	  }
																	});
								}
							});
						}
						$(this).hide();
					});
					$('#'+peer_id+'.chat-window .hang-up').click(function(){
						session.disconnect();
					})
				},
				resize: function(event, ui){
					var window = $(event.target).parent();
					$(window.find('.message-to-send')[0]).width(ui.size.width - 88);
					$(window.find('.messages')[0]).height(ui.size.height - 126);
				}
		});

}
function sendMessage(event){
	if(typeof(event.currentTarget) != 'undefined')
		event = event.currentTarget;
	peer_id = $(event).parents('#messagesBox').attr('data-peerid');
	message = $(event).val();
	$(event).val('');
	$.ajax({
		url: window.location.protocol + '//' + window.location.hostname + '/ajax/sendMessage',
		type: 'post',
		data: "to_uid="+peer_id+"&message="+message,
		success: function(data){
			if(data.status=='success'){
				messagesBox = $('#messagesBox');
				if(messagesBox.attr('data-peerid') == peer_id){
					if(messagesBox.find('.messages').length == 0){
						messagesBox.prepend('<div class="messages"><div class="line" data-from-uid="'+peer_id+'"></></div>');
						messagesBox.find('.messages .line').last().append('<p>'+message+'</p>');
					}else{
						if(messagesBox.find('.messages .line').last().hasClass('recieved')){
							messagesBox.find('.messages').append('<div class="line" data-from-uid="'+peer_id+'"><p>'+message+'</p></div>');
						}else{
							if(messagesBox.find('.messages .line').length>0)
								messagesBox.find('.messages .line').last().append('<p>'+message+'</p>');
							else
								messagesBox.find('.messages').last().append('<div class="line" data-from-uid="'+peer_id+'"><p>'+message+'</p></div>');
						}
					}
					$('#messagesBar #messagesBox .messages').scrollTo($('#messagesBar #messagesBox .messages p').last());
					$('#messagesBox .messages').perfectScrollbar();
				}
				var stanza = $msg({'to': data.to_jid, 'type': 'chat'}).c('body').t(data.message);
				connection.send(stanza);
			}
		}
	});
}
function sendChat(event){
	if(typeof(event.currentTarget) != 'undefined')
		event = event.currentTarget;
	var typingArea = $(event).parent();
	var chatWindow = $(event).parent().parent().parent();
	var message = $(typingArea.find('.message-to-send')[0]).val();
	var peer_id = chatWindow.attr('id');
	if(message == '')
		return true;
	var fullname = $(chatWindow.find('.fullname')[0]).html();
	$(chatWindow.find('.message-to-send')[0]).val('');
	$.ajax({
		url: window.location.protocol + '//' + window.location.hostname + '/ajax/sendMessage',
		type: 'post',
		data: "to_uid="+peer_id+"&message="+message,
		success: function(data){
			if(data.status=='success'){
				$(chatWindow.find('.messages')[0]).append('<div class="msg" id="msg_'+data.msg_id+'"><div class="sender">'+my_fullname+': </div><div class="message">'+message+'</div><div style="clear:both"></div></div>');
				if(chatWindow.find('.msg').length != 0)
					$(chatWindow.find('.messages')[0]).scrollTo(chatWindow.find('.msg').last());
				var stanza = $msg({'to': data.to_jid, 'type': 'chat'}).c('body').t(data.message);
				connection.send(stanza);
			}
		}
	});
}
function vChatConnect(sessionId,token,peer_id){
	session = TB.initSession(sessionId);
	session.connect('33127442', token);
	session.addEventListener('sessionConnected', sessionConnectedHandler);
	session.addEventListener('sessionDisconnected', sessionDisconnectedHandler);
	session.addEventListener('connectionCreated', connectionCreatedHandler);
	session.addEventListener('connectionDestroyed', connectionDestroyedHandler);
	session.addEventListener('streamCreated', streamCreatedHandler);
	session.addEventListener('streamDestroyed', streamDestroyedHandler);
}

function sessionConnectedHandler(event) {
	$.ajax({
		url: window.location.protocol + '//' + window.location.hostname + '/ajax/getUserData',
		type: "post",
		data: "uid="+vChatStatus.peer_id,
		success: function(response){
			var chatWindow = $('#'+response.uid+'.chat-window');
			if(chatWindow.length == 0)
				openChatWindow(response.uid,response.firstname+' '+response.lastname,response.profile_pic);
			chatWindow = $('#'+response.uid+'.chat-window');
			$(chatWindow).find('.video-call').hide();
			$(chatWindow).find('.hang-up').show();
			$(chatWindow).find('.video').show();
			chatWindow.dialog( "option", "width", 753);
			
			var div = document.createElement('div');
			div.setAttribute('id', 'publisher');
			$(chatWindow).find('.video .my_video').append(div);
			var publisherProperties = {width: 400, height:300, name:my_fullname};
			publisher = TB.initPublisher('33127442', 'publisher', publisherProperties);
			session.publish(publisher);
		}
	});
	for (var i = 0; i < event.streams.length; i++) {
		addStream(event.streams[i]);
    }
}

function streamCreatedHandler(event) {
  
  for (var i = 0; i < event.streams.length; i++) {
	addStream(event.streams[i]);
  }
}

function streamDestroyedHandler(event) {
  
  for (var i = 0; i < event.streams.length; i++) {
	removeStream(event.streams[i]);
  }
}

function sessionDisconnectedHandler(event) {
  
  if (event.streams) {
	for (var i = 0; i < event.streams.length; i++) {
	  console.log("sessionDisconnectedHandler");
	  console.log(event.streams[i]);
	  removeStream(event.streams[i],true);
	}
  }
  
	session.removeEventListener('sessionConnected', sessionConnectedHandler);
	session.removeEventListener('sessionDisconnected', sessionDisconnectedHandler);
	session.removeEventListener('connectionCreated', connectionCreatedHandler);
	session.removeEventListener('connectionDestroyed', connectionDestroyedHandler);
	session.removeEventListener('streamCreated', streamCreatedHandler);
	session.removeEventListener('streamDestroyed', streamDestroyedHandler);
  
  var chatWindow = $('#'+vChatStatus.peer_id+'.chat-window');
  chatWindow.find('.video').hide();
  
  chatWindow.find('.video-call').hide();
  chatWindow.find('.hang-up').hide();
  chatWindow.dialog( "option", "width", 348);
  chatWindow.find('.peer_video').empty();
  chatWindow.find('.my_video').empty();
  vChatStatus = {'status':'ready','peer_id':null};
  session = null;
  publisher = null;
  
}

function connectionDestroyedHandler(event) {
  
  var user_data = JSON.parse(event.connections[0].data);
  var chatWindow = $('#'+user_data.uid+'.chat-window');
  chatWindow.find('.peer_video').empty();
  chatWindow.find('.my_video').empty();
  chatWindow.find('.video').hide();
  
  chatWindow.find('.video-call').hide();
  chatWindow.find('.hang-up').hide();
  chatWindow.dialog( "option", "width", 348);
  session.disconnect();
  vChatStatus = {'status':'ready','peer_id':null};
}

function connectionCreatedHandler(event) {
  
  console.log('new connection');
  for(var i=0; i<event.connections.length; i++){
	peer_data = JSON.parse(event.connections[i].data);
	if(event.connections[i].connectionId == session.connection.connectionId)
		continue;
	else{
		var chatWindow = $('#'+peer_data.uid+'.chat-window');
		if(chatWindow.length == 0)
			openChatWindow(peer_data.uid,peer_data.firstname+' '+peer_data.lastname,peer_data.profile_pic);
		chatWindow = $('#'+peer_data.uid+'.chat-window');
		chatWindow.find('.video-call').hide();
		chatWindow.find('.hang-up').show();
		chatWindow.find('.video').show();
		chatWindow.dialog( "option", "width", 753);
	}
  }
}


function exceptionHandler(event) {
  console.log("Exception: " + event.code + "::" + event.message);
}



function addStream(stream) {
  
  if (stream.connection.connectionId == session.connection.connectionId) {
	return;
  }
  var user_data = JSON.parse(stream.connection.data);
	var div = document.createElement('div');
	div.setAttribute('id', 'stream' + stream.streamId);
  var chatWindow = $('#'+user_data.uid+'.chat-window');
  if(chatWindow.length == 0){
	openChatWindow(user_data.uid,user_data.firstname+' '+user_data.lastname,user_data.profile_pic);
	var chatWindow = $('#'+user_data.uid+'.chat-window');
	chatWindow.find('.video-call').hide();
	chatWindow.find('.hang-up').show();
  }
	chatWindow.dialog( "option", "width", 753);
	chatWindow.find('.peer_video').show().append(div);
	var divProps = {width: 400, height:300};
	subscriber = session.subscribe(stream, 'stream' + stream.streamId, divProps);
	var pub = document.getElementById(publisher.id);
    pub.style.width = "80px";
    pub.style.height = "60px";
	vChatStatus = {'status':'connected','peer_id':user_data.uid};
}

function removeStream(stream,all) {
  var user_data = JSON.parse(stream.connection.data);
  if (stream.connection.connectionId == session.connection.connectionId && all==false) {
	return;
  }
  var pub = document.getElementById(publisher.id);
  if(pub != null){
	  pub.style.width = "400px";
	  pub.style.height = "300px";
	  session.unsubscribe(stream);
  }
  subscriber = null;
}