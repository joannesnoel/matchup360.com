function loadConversation(e){
	conversation_id = $(this).attr('data-conversation-id');
	$('#messenger .conversation .messages').load(window.location.protocol + '//' + window.location.hostname + '/rest/get_conversation/'+conversation_id,function(){
		$('#messenger .conversation').show().attr('data-conversation-id',conversation_id);
		$('#messenger .threads .thread').removeClass('active');
		$('#messenger .threads .thread[data-conversation-id="'+conversation_id+'"]').addClass('active');
	});
}
function sendMessageDialog(uid){
	conversation = $('#messenger [data-conversation-id="'+uid+'"]');
	if(conversation.length==0){
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/rest/new_message_dialog/'+uid,
			success: function(dialog){
				if(dialog.redirect)
					window.location = dialog.redirect;
				$('body').append(dialog);
			}
		});
	}else{
		conversation.click();
		$('#messenger .ticker').click();
	}
}
function sendMessage(message,id,name,photo){
	$.ajax({
		url: window.location.protocol + '//' + window.location.hostname + '/rest/send_message',
		type: 'post',
		data: 'to_uid='+id+'&message='+message,
		dataType: 'json',
		success: function(response){
			if(response.redirect)
				window.location = response.redirect;
			if(response.status == 'success'){
				connection.send($msg({'to': response.to_jid, 'type': 'chat', 'from_uid': response.from_uid, 'from_name': response.from_name, 'photo': response.from_photo}).c('body').t(message));
				if($('#messenger .thread[data-conversation-id="'+id+'"]').length == 0){
					$('#messenger .threads').prepend('<div class="thread" data-conversation-id="'+id+'">\
														<img src="'+photo+'" alt="'+name+'">\
														<div class="fullname">'+name+'</div>\
														<div class="message">'+message+'</div>\
													</div>');
					$('#messenger [data-conversation-id="'+id+'"]').click(loadConversation).click();
					$('#messenger .threads .nothread').remove();
				}
				if($('#messenger .conversation[data-conversation-id="'+id+'"]').length != 0){
					if($('#messenger .conversation .bubble').last().hasClass('me') == false){
						$('#messenger .conversation .messages').append('<div style="clear:both"/>\
																		<div class="bubble me">\
																			<div class="text">\
																				<p>'+message+'</p>\
																			</div>\
																		</div>');
					}else{
						$('#messenger .conversation .bubble .text').last().append('<p>'+message+'</p>');
					}
					$('#messenger .messages').scrollTop($('#messenger .messages')[0].scrollHeight);
				}
			}
		}
	});
}
function receiveMessage(stanza){
	from_uid = stanza.getAttribute('from_uid');
	from_name = stanza.getAttribute('from_name');
	photo = stanza.getAttribute('photo');
	elems = stanza.getElementsByTagName('body');
	body = elems[0];
	message = Strophe.getText(body);
	conversation = $('#messenger .thread[data-conversation-id="'+from_uid+'"]');
	if($('#messenger').offset().left != 0){
		counter = parseInt($('#messenger #counter').html());
		$('#messenger #counter').html(counter+1);
		$('#messenger #counter').show();
	}
	if($('#messenger .conversation[data-conversation-id="'+from_uid+'"]').length != 0){
		if($('#messenger .conversation .bubble').last().hasClass('me')){
			$('#messenger .conversation .messages').append('<div style="clear:both"/>\
															<div class="bubble">\
																<div class="text">\
																	<p>'+message+'</p>\
																</div>\
															</div>');
		}else{
			$('#messenger .conversation .bubble .text').last().append('<p>'+message+'</p>');
		}
		$('#messenger .messages').scrollTop($('#messenger .messages')[0].scrollHeight);
	}
	if(conversation.length==0){
		$('#messenger .threads').prepend('<div class="thread" data-conversation-id="'+from_uid+'">\
											<img src="'+photo+'" alt="'+from_name+'">\
											<div class="fullname">'+from_name+'</div>\
											<div class="message">'+message+'</div>\
										</div>');
		$('#messenger [data-conversation-id="'+from_uid+'"]').click(loadConversation);
		$('#messenger .threads .nothread').remove();
		if($('#messenger .threads .thread').length == 1)
			$('#messenger [data-conversation-id="'+from_uid+'"]').click();
	}else{
		conversation.html('<img src="'+photo+'" alt="'+from_name+'">\
							<div class="fullname">'+from_name+'</div>\
							<div class="message">'+message+'</div>');
	}
	return true;
}

connection.addHandler(receiveMessage, null,'message' ,'chat');

$('<link>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: window.location.protocol + '//' + window.location.hostname + '/assets/css/module.messenger.css'
        }).appendTo('head');
$('body').prepend('<div id="messenger">\
						<i id="counter">0</i>\
						<div class="ticker">\
							<i/>\
						</div>\
						<div class="threads">\
							<p class="nothread">Your messages will be shown here.</p>\
						</div>\
						<div class="conversation" data-conversation-id="">\
							<div class="messages"></div>\
							<textarea id="chat_text" placeholder="Type your message..."></textarea>\
						</div>\
					</div>');
$('#cboxOverlay').click(function(){ 
	if($('#messenger .ticker').parent().offset().left == 0){
		$('#messenger .ticker').parent().animate({left:'-608px'},400);
		$('#messenger #counter').html('0').hide();
		$('#cboxOverlay').hide().css('opacity',1);
	}
});
$('#messenger #chat_text').bind('keypress', function(e) {
									var code = (e.keyCode ? e.keyCode : e.which);
									if(code == 13) {
										sendMessage($(this).val(),$(this).parent().attr('data-conversation-id'));
										$(this).val('');
										e.preventDefault();
									}
								});
$('#messenger .ticker').click(function(){
	if($(this).parent().offset().left == 0){
		$(this).parent().animate({left:'-608px'},400);
		$('#messenger #counter').html('0').hide();
		$('#cboxOverlay').hide().css('opacity',1);
	}else{
		$(this).parent().animate({left:'0px'},400);
		$('#messenger #counter').html('0').hide();
		$('#cboxOverlay').show().css('opacity',0.5);
	}
});
$.ajax({
	url: window.location.protocol + '//' + window.location.hostname + '/rest/get_message_threads',
	success: function(response){
		if(response.redirect)
			window.location = response.redirect;
		if(response.length){
			$('.threads').empty().html(response);
			$('.threads .thread').click(loadConversation);
			$('.threads .thread').first().click();
		}
	}
});