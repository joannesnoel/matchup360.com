$('<link>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: window.location.protocol + '//' + window.location.hostname + '/assets/css/module.chatroom.css'
        }).appendTo('head');
$('<link>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: window.location.protocol + '//' + window.location.hostname + '/assets/chat/res/default.css'
        }).appendTo('head');
$('body').prepend('<div id="chatroom">\
						<div class="ticker">\
							<i/>\
						</div>\
						<div id="candy">\
						</div>\
					</div>');
jQuery.getScript( window.location.protocol + '//' + window.location.hostname + '/assets/chat/libs/libs.min.js', function(){
	jQuery.getScript( window.location.protocol + '//' + window.location.hostname + '/assets/chat/candy.bundle.js', function(){
			Candy.init(BOSH_SERVICE, {
				core: { debug: true, autojoin: ['world@conference.matchup360.com'] },
				view: { resources: '../../assets/chat/res/', language: 'en' }
			});
			Candy.Core.connect();
	});
});