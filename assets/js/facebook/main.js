var BOSH_SERVICE;
function loadBrowser(path){
	$('div#stage').load(window.location.protocol + '//' + window.location.hostname + '/'+path,function(){
		console.log('page '+path+' loaded.');
	});
}
function startApp(){
	connectJabber();
	jQuery.getScript( window.location.protocol + '//' + window.location.hostname + '/assets/js/apps/shouts.js' );
}
function connectJabber(){
	$.ajax({
		url: window.location.protocol + '//' + window.location.hostname + '/rest/get_user_data',
		dataType: 'json',
		success: function(response){
			if(response.redirect)
				window.location = response.redirect;
			if(window.location.protocol == 'https:')
				BOSH_SERVICE = 'https://www.matchup360.com:5281/http-bind';
			else
				BOSH_SERVICE = 'http://www.matchup360.com:5280/http-bind';
			connection = new Strophe.Connection(BOSH_SERVICE);
			
			// Strophe debugging
			connection.rawInput = function(a) { console.log("in:", a);};
			connection.rawOutput = function(a) {console.log("out:", a);};
			connection.connect(response.xmpp_user+"@matchup360.com/"+makeid(),
									response.xmpp_password,
									onConnect);
		}
	});
}
function onConnect(status){
	if (status === Strophe.Status.CONNECTED) {
		jQuery.getScript( window.location.protocol + '//' + window.location.hostname + '/assets/js/module.messenger.js', function(){
			$('#messenger').delay(1000).fadeIn();
		});
		//jQuery.getScript( window.location.protocol + '//' + window.location.hostname + '/assets/js/module.chatroom.js' );
		//connection.addHandler(receiveChat, null,'message' ,'chat');		
		//connection.addHandler(receiveMsg, null,'message' ,'msg');
		connection.addHandler(receiveSignal, null,'message' ,'headline');
		//connection.addHandler(presenceHandler, null, 'presence');
		//connection.addHandler(notificationHandler, null, 'message' , 'notification');
		connection.send($pres().c('show').t('chat').up().c('priority').t('1').tree());
		//connection.sendIQ($iq({type: "get"}).c("query", {xmlns: 
		//					Strophe.NS.ROSTER}), manageRoster);							
	} else if (status === Strophe.Status.DISCONNECTED) {
		console.log("XMPP/Strophe Disconnected.");			
	}
}
function receiveSignal(stanza){
	var elems = stanza.getElementsByTagName('body');
	var body = elems[0];
	var json_data = eval('('+Strophe.getText(body)+')');
	if(json_data.type=='newpost'){
		$(json_data.post).hide().prependTo(".stream").fadeIn()
		.find('.remove').popover({html:true,placement:'left',title:'Delete this post?',content:'<button id="confirmed-delete" class="btn btn-danger btn-small"><i class="icon-white icon-remove"></i> Yes, delete</button> <button id="close-popover" class="btn btn-small">Cancel</button>'});
	}
	if(json_data.type=='newcomment'){
		$(json_data.post).hide().appendTo('.stream .entry[data-post-id="'+json_data.parent_id+'"] .comments').fadeIn().find('.remove')
		.popover({html:true,placement:'left',title:'Delete this post?',content:'<button id="confirmed-delete" class="btn btn-danger btn-small"><i class="icon-white icon-remove"></i> Yes, delete</button> <button id="close-popover" class="btn btn-small">Cancel</button>'});
	}
	return true;
}
function makeid(){
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for( var i=0; i < 5; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

function reloadAds(){
	$('.advertisements iframe')[0].src = $('.advertisements iframe')[0].src;
	setTimeOut(reloadAds,Math.random() * (1000*60*5 - 1000*60*3) + 1000*60*3);
}

function handlePath(path){
	if(path == '' || path == 'feeds'){
		loadBrowser('page/shouts');
		$('#main-menu li').removeClass('active');
		$('#main-menu li#feeds').addClass('active');
		if ( history.pushState && path == 'feeds' ) 
			history.pushState( null, null, '/feeds' );
	}else if(path == 'members'){
		loadBrowser('page/members');
		if ( history.pushState ) 
			history.pushState( null, null, '/members' );
	}else{
		loadBrowser('page/member/'+path);
		if ( history.pushState ) 
			history.pushState( null, null, '/'+path );
	}
	_gaq.push(['_trackPageview', '/'+path]);
}

$(document).ready(function(){
	$('#main-menu li a').click(function(e){
		page = $(this).parent().attr('id');
		handlePath(page);
		e.preventDefault();
	});	
	window.addEventListener('popstate', function(event) {
		path = document.location.pathname.substr(1);
		if(path == '' || path == 'feeds'){
			$('#main-menu li').removeClass('active');
			loadBrowser('page/shouts');
			$('#main-menu li#feeds').addClass('active');
			_gaq.push(['_trackPageview', '/feeds']);
		}else if(path == 'members'){
			loadBrowser('page/members');
			_gaq.push(['_trackPageview', '/members']);
		}else{
			loadBrowser('page/member/'+path);
			_gaq.push(['_trackPageview', '/'+path]);
		}
	});
	//global delegates
	$('body').delegate('.popover-content button#close-popover', 'click', function(e) {
		$(e.currentTarget).parents('.popover-parent').find('.remove').popover('hide');
		e.preventDefault();
		e.stopPropagation();
	})
	.delegate('.popover-content button#confirmed-delete', 'click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		entry = $(e.currentTarget).parents('.popover-parent').first();
		entry.find('.remove').popover('hide');
		if(entry.attr('data-post-id')){
			$.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/rest/delete_post',
				type: 'post',
				data: 'post_id='+entry.attr('data-post-id'),
				dataType: 'json',
				success: function(response){
					if(response.redirect)
						window.location = response.redirect;
					if(response.status == 'success'){
						entry.fadeOut();
					}
				}
			});
		}else if(entry.attr('data-photo-reference')){
			$.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/rest/delete_photo',
				type: 'post',
				data: 'photo_id='+entry.attr('data-photo-reference'),
				dataType: 'json',
				success: function(response){
					if(response.redirect)
						window.location = response.redirect;
					if(response.status == 'success'){
						entry.fadeOut();
					}
				}
			});
		}
	});
});