<?php
class Search_model extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	
	function search_nearby_people($from_location, $km_radius,$interested_in = null, $age_range = null){
		$query_str = "SELECT DISTINCT(loc.uid), 
						( 6371 * acos( cos( radians(".$from_location['lat'].") ) * 
						cos( radians( loc.lat ) ) * 
						cos( radians( loc.lng ) - 
						radians(".$from_location['lng'].") ) + 
						sin( radians(".$from_location['lat'].") ) * 
						sin( radians( loc.lat ) ) ) ) 
						AS distance FROM user_location loc, user u, user_info ui, user_photo up
						WHERE loc.uid = u.uid
						AND ui.uid = u.uid
						AND up.uid = u.uid ";
		
		if(!is_null($interested_in))
			$query_str .= " AND u.sex IN('".implode('\',\'',$interested_in)."') ";

		if(!is_null($age_range))
			$query_str .= " AND YEAR(NOW()) - YEAR(birthdate) BETWEEN ". (int)$age_range[0] ." AND " . (int)$age_range[1] . " ";
		
		$query_str .= "HAVING distance < $km_radius ORDER BY distance ASC";
		
		die($query_str);
	}    
	
	function members($criteria = null){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userfollow_model');
		$from_location['lat'] = $this->session->userdata('current_lattitude');
		$from_location['lng'] = $this->session->userdata('current_longitude');
		
		$q_str = "SELECT u.uid, u.firstname, u.lastname, u.profile_pic, u.sex, u.xmpp_user, city.name AS city, state.name AS state, country.name AS country,
						DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(u.birthdate, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(u.birthdate, '00-%m-%d')) AS age,
						( 6371 * acos( cos( radians(".$from_location['lat'].") ) * 
						cos( radians( u.current_lattitude ) ) * 
						cos( radians( u.current_longitude ) - 
						radians(".$from_location['lng'].") ) + 
						sin( radians(".$from_location['lat'].") ) * 
						sin( radians( u.current_lattitude ) ) ) ) 
						AS distance FROM user u
						LEFT JOIN user_location city ON u.city = city.locid
						LEFT JOIN user_location state ON u.state = state.locid
						LEFT JOIN user_location country ON u.country = country.locid
						WHERE u.profile_pic <> '' AND u.uid <> ".$uid."
						ORDER BY u.last_seen DESC
						";
		$q = $this->db->query($q_str);
		$response = array();
		$members = $q->result_array();
		foreach($members as $member){
			$check_follow = $this->Userfollow_model->retrieve(array('from'=>$uid ,'to'=>$member['uid']));
			if(!empty($member['profile_pic']))
				$member['profile_pic'] = config_item('s3_bucket_url').$member['uid'].'/photos/'.$member['profile_pic'];
			else{
				if($member['sex'] == 'm')
					$member['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile.jpg';
				else
					$member['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile_female.jpg';
			}
			if($check_follow)
				$member['followed'] = true;
			else
				$member['followed'] = false;
			
			$response[] = $member;
		}
		return $response;
	}

}
?>