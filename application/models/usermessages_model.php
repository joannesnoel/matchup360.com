<?php
class Usermessages_model extends CI_Model {
    var $table_name   = 'user_messages';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	function update($data, $where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
		return $this->db->insert_id();
	}
	function getMessageThreads($uid){
		$str = "select * from (select u.uid, u.firstname, u.lastname, u.sex, u.xmpp_user, u.profile_pic, m.message, m.sent_date FROM user_messages m
			INNER JOIN user u ON IF(m.from_uid=$uid, m.to_uid, m.from_uid) = u.uid
			WHERE m.from_uid = $uid OR m.to_uid = $uid order by m.sent_date desc) t group by uid order by sent_date desc";
		$result = $this->db->query($str)->result_array();
		if(count($result)>0){
			for($i=0; $i<count($result); $i++){
				if(!empty($result[$i]['profile_pic']))
					$result[$i]['profile_pic'] = config_item('s3_bucket_url').$result[$i]['uid'].'/photos/'.$result[$i]['profile_pic'];
				else{
					if($result[$i]['sex'] == 'm')
						$result[$i]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile.jpg';
					else
						$result[$i]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile_female.jpg';
				}
			}		
			return $result;
		}
		return $result;
	} 
	function getThread($from_uid, $to_uid){
		$fields = "m.mid AS msg_id, m.from_uid, m.message, m.sent_date, u.profile_pic, CONCAT(u.firstname,' ',u.lastname) AS fullname, u.sex";
		$str = "select ".$fields." from user_messages m, user u where u.uid = m.from_uid and m.to_uid = ".$to_uid."	and m.from_uid = ".$from_uid." union
				select ".$fields." from user_messages m, user u where u.uid = m.from_uid and from_uid =  ".$to_uid." and to_uid = ".$from_uid."
					order by sent_date asc";
		$result = $this->db->query($str)->result_array();
		$this->db->query("UPDATE user_messages SET read_date ='".date('Y-m-d H:i:s')."' WHERE read_date IS NULL AND (from_uid = $from_uid AND to_uid = $to_uid) OR (to_uid = $from_uid AND from_uid = $to_uid)");
		if(count($result)>0){
			for($i=0; $i<count($result); $i++){
				if(!empty($result[$i]['profile_pic']))
					$result[$i]['profile_pic'] = config_item('s3_bucket_url').$result[$i]['from_uid'].'/photos/'.$result[$i]['profile_pic'];
				else{
					if($result[$i]['sex'] == 'm')
						$result[$i]['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile.jpg';
					else
						$result[$i]['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile_female.jpg';
				}
			}
			return $result;
		}
		return $result;
	}
}
?>