<?php
class Vchatsessions_model extends CI_Model {

    var $table_name   = 'vchat_sessions';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		require_once '/var/www/OT-SDK/OpenTokSDK.php';
		require_once '/var/www/OT-SDK/OpenTokArchive.php';
		require_once '/var/www/OT-SDK/OpenTokSession.php';
    }
	
	function getSessionId($peer1,$peer2){
		$q_str = $this->db->query("SELECT sessionId FROM ".$this->table_name." WHERE ((peer_uid1 = $peer1 AND peer_uid2 = $peer2) OR (peer_uid2 = $peer1 AND peer_uid1 = $peer2)) AND expire_date > NOW()");
		$result = $q_str->result_array();
		if(count($result)){
			return $result[0]['sessionId'];
		}else
			return $this->generateSessionId($peer1,$peer2);
	}
	
	function generateSessionId($peer1,$peer2){
		$apiObj = new OpenTokSDK();
		$session = $apiObj->create_session($_SERVER["REMOTE_ADDR"], array('p2p.preference' => 'enabled'));
		$sessionId = $session->getSessionId();
		$expr_date = date('Y-m-d H:i:s');
		$expr_date = strtotime($expr_date);
		$expr_date = strtotime("+7 day", $expr_date);
		$data = array('peer_uid1'=>$peer1,'peer_uid2'=>$peer2,'sessionId'=>$sessionId,'expire_date'=>date('Y-m-d H:i:s',$expr_date));
		$this->db->insert($this->table_name,$data);
		return $sessionId;
	}
	
	function getTokenId($sessionId){
		$apiObj = new OpenTokSDK();
		$this->load->model("User_model");
		$user = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$data = array(
					'uid'=>$user[0]['uid'],
					'firstname'=>$user[0]['firstname'],
					'lastname'=>$user[0]['lastname'],
					'profile_pic'=>$user[0]['profile_pic'],
					'sessionId'=>$sessionId
				);
		$token = $apiObj->generate_token($sessionId,RoleConstants::PUBLISHER, null, json_encode($data)); 
		return $token;
	}
}
?>