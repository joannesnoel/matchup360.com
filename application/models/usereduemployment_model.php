<?php
class Usereduemployment_model extends CI_Model {

    var $table_name   = 'user_education_employment';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database(); 
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){ 
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	function get_eduemployment($uid){
	$result = null;
		$eduemploy_q = $this->db->query("SELECT	level.name AS level,
												special.name AS specialty,
												employ.name AS employment,
												ue.job_title
										FROM user_education_employment ue
										LEFT JOIN list level ON ue.level = level.id
										LEFT JOIN list special ON ue.specialty = special.id
										LEFT JOIN list employ ON ue.employment = employ.id
										WHERE ue.uid = ".$uid)->result_array();
	if(!empty($eduemploy_q[0]))
		$result['eduemploy'] = $eduemploy_q[0];
		return $result;
	}
}
?>