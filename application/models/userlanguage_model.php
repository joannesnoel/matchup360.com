<?php
class Userlanguage_model extends CI_Model {

    var $table_name   = 'user_language';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	function delete($where){
		$this->db->where($where);
		$this->db->delete($this->table_name); 
	}
	
	function retrieve($where){
		$this->db->where($where);
		$query = $this->db->get($this->table_name);
		$result = $query->result_array();
		if(count($result)>0)
			return $result;
		return false;
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve_language_name($where){
		$this->db->select('ui.*, l.name AS language');
		$this->db->from($this->table_name.' ui');
		$this->db->where($where);
		$this->db->join('list l', 'ui.lid = l.id', 'left');
		return $this->db->get()->result_array();
	}
	
}
?>