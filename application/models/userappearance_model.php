<?php
class Userappearance_model extends CI_Model {

    var $table_name   = 'user_appearance';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	
	function get_appearance($uid){
		$result = null;
		$body_art_q = $this->db->query("SELECT bodyart.name
									FROM user_bodyart ub
									INNER JOIN list bodyart ON ub.lid = bodyart.id
									WHERE ub.uid = ".$uid)->result_array();
		if(!empty($body_art_q))
			$result['bodyart'] = $body_art_q;
		
		$appearance_q = $this->db->query("SELECT ethnicity.name AS ethnicity, 
											nationality.name AS nationality,
											eye_color.name AS eye_color,
											hair_color.name AS hair_color
									FROM user_appearance ua
									LEFT JOIN list ethnicity ON ua.ethnicity = ethnicity.id
									LEFT JOIN list nationality ON ua.nationality = nationality.id
									LEFT JOIN list eye_color ON ua.eye_color = eye_color.id
									LEFT JOIN list hair_color ON ua.hair_color = hair_color.id
									WHERE ua.uid = ".$uid)->result_array();
		if(!empty($appearance_q[0]))
			$result['appearance'] = $appearance_q[0];
		return $result;
	}
}
?>