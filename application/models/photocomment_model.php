<?php
class Photocomment_model extends CI_Model {

    var $table_name   = 'photo_comments';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	
	function retrieve($pid){
		/*$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();*/
		$comments = $this->db->query("SELECT u.uid, u.firstname, 
									u.lastname, u.profile_pic, e.id, e.comment, e.date, e.commentor_id, p.uid as owner_id
									FROM user as u
									JOIN photo_comments e ON u.uid = e.commentor_id
									JOIN user_photo p ON p.pid = e.reference
									WHERE e.reference = ".$pid)->result_array();
		return $comments;
	}
	function delete_photo_comments($pid){
		$this->db->query("DELETE FROM photo_comments where reference = ".$pid);
	}
	
	function delete($id){
		$this->db->query('DELETE FROM photo_comments WHERE id='.$id);
	}
}
?>