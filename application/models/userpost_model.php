<?php
class Userpost_model extends MY_Model{
    function __construct(){
        parent::__construct();
		$this->setTable('user_post');
    }
	function getPost($id){
		$this->load->model('Userphoto_model');
		$this->db->select('user.uid, user.firstname, user.lastname, user.xmpp_user, user.profile_pic, user_post.*');
		$this->db->from('user_post');
		$this->db->join('user','user.uid = user_post.uid');
		$this->db->where('user_post.post_id',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		if(is_array($result)){
			$result[0]['photos'] = $this->Userphoto_model->retrieve_largest(array('post_id'=>$result[0]['post_id']),true);
			if(!empty($result[0]['profile_pic']))
				$result[0]['profile_pic'] = config_item('s3_bucket_url').$result[0]['uid'].'/photos/'.$result[0]['profile_pic'];
			else{
				if($result[0]['sex'] == 'm')
					$result[0]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile.jpg';
				else
					$result[0]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile_female.jpg';
			}
			return $result[0];
		}else
			return false;
	}
	function getLast50($where = null){
		$this->load->model('Userphoto_model');
		$this->load->model('User_model');
		$me = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$me = $me[0];
		$this->db->select('user.uid, user.firstname, user.lastname, user.xmpp_user, user.profile_pic, user.geo_bounds, user_post.*, 
						( 6371 * acos( cos( radians('.$me['current_lattitude'].') ) * 
						cos( radians( user_post.lat ) ) * 
						cos( radians( user_post.lng ) - 
						radians('.$me['current_longitude'].') ) + 
						sin( radians('.$me['current_lattitude'].') ) * 
						sin( radians( user_post.lat ) ) ) ) 
						AS distance');
		$this->db->from('user_post');
		$this->db->join('user','user.uid = user_post.uid');
		$this->db->group_by("user_post.post_id"); 
		$this->db->having("distance <= ".$me['geo_bounds']);
		$this->db->order_by("user_post.datetimestamp", "desc");
		$this->db->where('user_post.parent_id IS NULL');
		if($where != NULL)
			$this->db->where($where);
		$this->db->limit(50);
		$query = $this->db->get();
		$result = $query->result_array();
		if(is_array($result)){
			for($i=0;$i<count($result);$i++){
				$result[$i]['photos'] = $this->Userphoto_model->retrieve_largest(array('post_id'=>$result[$i]['post_id']),true);
				if(!empty($result[$i]['profile_pic']))
					$result[$i]['profile_pic'] = config_item('s3_bucket_url').$result[$i]['uid'].'/photos/'.$result[$i]['profile_pic'];
				else{
					if($result[$i]['sex'] == 'm')
						$result[$i]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile.jpg';
					else
						$result[$i]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile_female.jpg';
				}
				$result[$i]['comments'] = $this->getComments($result[$i]['post_id']);
			}
			return $result;
		}else
			return false;
	}
	function getComments($post_id){
		$this->db->select('user.uid, user.firstname, user.lastname, user.profile_pic, user_post.*');
		$this->db->from('user_post');
		$this->db->join('user','user.uid = user_post.uid');
		$this->db->order_by("user_post.datetimestamp", "desc"); 
		$this->db->where('user_post.parent_id',$post_id);
		$this->db->limit(20);
		$query = $this->db->get();
		$result = $query->result_array();
		for($i=0;$i<count($result);$i++){
			if(!empty($result[$i]['profile_pic']))
				$result[$i]['profile_pic'] = config_item('s3_bucket_url').$result[$i]['uid'].'/photos/'.$result[$i]['profile_pic'];
			else{
				if($result[$i]['sex'] == 'm')
					$result[$i]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile.jpg';
				else
					$result[$i]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile_female.jpg';
			}
		}
		return $result;
	}
	function delete($post_id){
		$this->db->where('post_id',$post_id);
		$query = $this->db->get($this->table_name);
		$result = $query->result_array();
		if(is_array($result)){
			if($result[0]['uid'] == $this->session->userdata('logged_uid') OR $this->session->userdata('logged_uid') == 400){
				$this->db->delete($this->table_name, array('post_id' => $post_id));
				return true;
			}else
				return false;
		}
		return false;
	}
}
?>