<?php
class Userposts_model extends CI_Model {

    var $table_name   = 'user_posts';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	
	function insert($data){
		$data['post_time'] = date('Y-m-d H:i:s');
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function getPosts($uid){
		$q_post = "SELECT poster_uid, post, post_time, post_to_uid FROM user_posts";
		
		$posts = $this->db->query($q_post);
		$posts_result = $posts->result_array();
		$q_str = "SELECT * FROM user_buddies WHERE `status` = 'B' AND `from` = " . $uid;
		$q = $this->db->query($q_str);
		$result = $q->result_array();
		$post_array = null;
		
		foreach($posts_result as $post){	
		
			if($post['poster_uid'] == $uid){
				$post_array[] = $post;
				continue;
			}
				
			foreach($result as $value){
				if($post['poster_uid'] == $value['to']){
					$post_array[] = $post;
					break;
				}
			}
		}
		
		if(count($post_array) > 0) {
			$post_array = array_reverse($post_array);
		}
		
		return $post_array;
	}
	/**
	** GET POST FOR PROFILE
	**/
	public function getProfilePost($uid){
		$q_post = "SELECT ui.poster_uid, ui.post, ui.post_time, u.profile_pic, i.firstname, i.lastname, ui.post_to_uid FROM user_posts ui, user_info u, user i where ui.poster_uid = u.uid and i.uid = ui.poster_uid and ui.post_to_uid = ".$uid." order by ui.post_time desc";
		$result = $this->db->query($q_post);
		$result = $result->result_array();
		return $result;
	}
	/*public function getAllPosts(){
		$q_post = "SELECT * from user_posts";
		$result = $this->db->query($q_post);
		$result = $result->result_array();
		return $result;
	}*/
	public function deleteProfilePosts($uid){
		$deleted = $this->db->query("delete from user_posts where poster_uid = ".$uid);
		return $deleted;
	}
	
}
?>