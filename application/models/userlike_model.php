<?php
class Userlike_model extends CI_Model {

    var $table_name   = 'user_like';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	function delete($where){
		$this->db->where($where);
		$this->db->delete($this->table_name); 
	}
	
	function retrieve($where){
		$this->db->where($where);
		$query = $this->db->get($this->table_name);
		$result = $query->result_array();
		if(count($result)>0)
			return $result;
		return false;
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
}
?>