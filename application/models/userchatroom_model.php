<?php
class Userchatroom_model extends CI_Model {

    var $table_name   = 'user_chatroom';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	function getActiveRooms(){
		$q = $this->db->query("SELECT roomJid FROM ".$this->table_name." WHERE (date_opened > date_closed OR date_closed IS NULL) AND uid = ".$this->session->userdata('logged_uid'));
		return $q->result_array();
	}
}
?>