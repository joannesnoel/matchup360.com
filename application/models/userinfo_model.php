<?php
class Userinfo_model extends CI_Model {

    var $table_name   = 'user_info';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function retrieve($uid){
		$this->db->where('uid', $uid);
		$query = $this->db->get($this->table_name);
		$result = $query->result_array();
		if(count($result)>0){
			return $result[0];
		}else{
			return false;
		}
	}
	function delete_where_uid($uid){
		$this->db->query("delete user_info where uid = ".$uid);
	}
}
?>