<?php
class Invitation_model extends CI_Model {

    var $table_name   = 'invitation';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function retrieve($where){
		$this->db->where($where);
		$query = $this->db->get($this->table_name);
		$result = $query->result_array();
		return $result ;
	}
	function update($data,$where){
		$this->db->where($where);
		$query = $this->db->update($this->table_name,$data);
		$update_result = $query ? 1 : 0;
		return $update_result;
	}
}
?>