<?php
class User_model extends CI_Model {

    var $table_name   = 'user';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function retrieve($where = null){
		$this->db->select('u.*, city.name AS city, county.name AS county, state.name AS state, country.name AS country, YEAR(NOW()) - YEAR(u.birthdate) AS age');
		$this->db->from($this->table_name.' u');
		if($where != null)
			$this->db->where($where);
		$this->db->join('user_location city', 'city.locid = u.city','left');
		$this->db->join('user_location county', 'county.locid = u.county','left');
		$this->db->join('user_location state', 'state.locid = u.state','left');
		$this->db->join('user_location country', 'country.locid = u.country','left');
		$result = $this->db->get()->result_array();
		if(count($result)>0){
			for($i=0; $i<count($result); $i++){
				if(!empty($result[$i]['profile_pic']))
					$result[$i]['profile_pic'] = config_item('s3_bucket_url').$result[$i]['uid'].'/photos/'.$result[$i]['profile_pic'];
				else{
					if($result[$i]['sex'] == 'm')
						$result[$i]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile.jpg';
					else
						$result[$i]['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile_female.jpg';
				}
			}		
			return $result;
		}
		return false;
	}
	
	function retrieve_receipients(){
		$me = $this->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$me = $me[0];
		$this->db->select('u.xmpp_user, u.geo_bounds,
							( 6371 * acos( cos( radians('.$me['current_lattitude'].') ) * 
							cos( radians( u.current_lattitude ) ) * 
							cos( radians( u.current_longitude ) - 
							radians('.$me['current_longitude'].') ) + 
							sin( radians('.$me['current_lattitude'].') ) * 
							sin( radians( u.current_lattitude ) ) ) ) 
							AS distance');
		$this->db->from($this->table_name.' u');
		$this->db->group_by("u.uid"); 
		$this->db->having("distance <= geo_bounds");
		$result = $this->db->get()->result_array();		
		return $result;
	}
	
	function latest_members($uid, $offset = 0,$limit = 10){
		$q_str = "SELECT u.uid, u.firstname, u.lastname, i.profile_pic, u.xmpp_user, u.last_seen FROM user u, user_info i WHERE u.uid = i.uid AND u.uid <> ".$uid;
		
		$q_str .= " LIMIT $offset, $limit";
		$q = $this->db->query($q_str);
		$members = null;
		$result = $q->result_array();
		
		foreach($result as $member){
			$check_buddy = $this->db->query("SELECT status FROM user_buddies WHERE `from` = $uid AND `to` = ".$member['uid']);
			$check_buddy = $check_buddy->result_array();
			if(count($check_buddy)>0){
					$member['buddy'] = $check_buddy[0]['status'];
			}
			
			else {
					$member['buddy'] = "S";
			}
			
			$members[] = $member;
		}
		
		return $members;
	}
	function random(){
		$this->load->model('Userinterest_model');
		$uid = $this->session->userdata('logged_uid'); 
		$interest = $this->Userinterest_model->retrieve(array('uid'=>$uid));
		$interest = $interest[0];
		$q_str = "SElECT u.uid, u.firstname, u.lastname, u.sex, city.name AS city, county.name AS county, 
									state.name AS state, country.name AS country, YEAR(NOW()) - YEAR(u.birthdate) AS age 
							FROM user u
							LEFT JOIN user_location city ON city.locid = u.city
							LEFT JOIN user_location county ON county.locid = u.county
							LEFT JOIN user_location state ON state.locid = u.state
							LEFT JOIN user_location country ON country.locid = u.country
							WHERE u.uid NOT IN(
							SELECT encounter.to_uid FROM user_encounter encounter
							WHERE encounter.from_uid = $uid )
							AND u.uid IN(SELECT uid FROM user_photo)
							AND u.uid != $uid
							";
		if(!empty($interest['men']) && empty($interest['women']))
			$q_str .= " AND u.sex = 'm' ";
		if(!empty($interest['women']) && empty($interest['men']))
			$q_str .= " AND u.sex = 'f' ";
		$q_str .= " ORDER BY RAND()";
		$q = $this->db->query($q_str);
		$result = $q->result_array();
		if(count($result)>0){
			return $result[0];
		}else{
			return false;
		}
	}
	function random_members($count = 10){
		$uid = $this->session->userdata('logged_uid');
		$from_location['lat'] = $this->session->userdata('current_lattitude');
		$from_location['lng'] = $this->session->userdata('current_longitude');
		
		$q_str = "SELECT u.uid, u.firstname, u.lastname, i.profile_pic, u.xmpp_user, u.last_seen, 
						( 6371 * acos( cos( radians(".$from_location['lat'].") ) * 
						cos( radians( i.current_lattitude ) ) * 
						cos( radians( i.current_longitude ) - 
						radians(".$from_location['lng'].") ) + 
						sin( radians(".$from_location['lat'].") ) * 
						sin( radians( i.current_lattitude ) ) ) ) 
						AS distance FROM user u, user_info i WHERE u.uid = i.uid AND u.uid <> ".$uid;
		$q_str .= " LIMIT 0, 10";

		$q = $this->db->query($q_str);
		$result = $q->result_array();
		return $result;
	}
	
	function all_members($uid){
		$q_str = "SELECT u.uid, u.firstname, u.lastname, i.profile_pic, u.xmpp_user, u.last_seen FROM user u, user_info i WHERE u.uid = i.uid AND u.uid <> ".$uid;
		
		$q = $this->db->query($q_str);
		$members = null;
		$result = $q->result_array();
		
		foreach($result as $member){
			$check_buddy = $this->db->query("SELECT status FROM user_buddies WHERE `from` = $uid AND `to` = ".$member['uid']);
			$check_buddy = $check_buddy->result_array();
			if(count($check_buddy)>0){
					$member['buddy'] = $check_buddy[0]['status'];
			}
			$members[] = $member;
		}
		
		return $members;
	}
	
	function authenticate($email, $pass){
		//$this->db->where('email',$email);
		//$this->db->where('password',$this->encrypt->sha1($pass));
		//$user = $this->db->get($this->table_name);
		$user = $this->db->query("SELECT u.uid, u.current_longitude, u.current_lattitude 
									FROM user u
									WHERE email = '".$email."' AND password = '".$this->encrypt->sha1($pass)."'");
		$result = $user->result_array();
		if(count($result)>0){
			return $result[0];
		}else
			return false;
	}
	
	function generate_xmpp_user($firstname,$lastname){
		$firstname = str_replace(' ','',strtolower($firstname));
		$lastname = str_replace(' ','',strtolower($lastname));
		$q = $this->db->query("SELECT xmpp_user FROM ".$this->table_name." WHERE xmpp_user = '".$firstname.$lastname."'");
		$r = $q->result_array();
		if(count($r)>0){
			$i = 0;
			do{
				$new_xmpp_user = $firstname.$lastname.$i;
				$q = $this->db->query("SELECT xmpp_user FROM ".$this->table_name." WHERE xmpp_user = '".$new_xmpp_user."'");
				$r = $q->result_array();
				if(count($r)>0){
					$i += 1;
					$exist = true;
				}else{
					$exist = false;
					return $new_xmpp_user;
				}
			}while($exist);
		}else{
			return $firstname.$lastname;
		}
	}
	
	function update($data,$where){
		$this->db->where($where);
		$query = $this->db->update($this->table_name,$data);
		$update_result = $query ? 1 : 0;
		return $update_result;
	}
	function deduct_points($point_deduction){
		$this->db->query("UPDATE user SET points = points - ".$point_deduction." WHERE uid = ".$this->session->userdata('logged_uid'));
		return $this->get_user_points($this->session->userdata('logged_uid'));
	}
	function pending(){
		$q_str = 'SELECT * FROM user_buddies';
		$q = $this->db->query($q_str);
		$result = $q->result_array();
		
		return $result;
	}
	
	function check_email($email){
		$q_str = "SELECT uid FROM ".$this->table_name." WHERE email = '$email'";
		$q = $this->db->query($q_str);
		$result = $q->result_array();
		if(count($result)>0)
			return true;
		return false;
	}
	function getAll(){
		$all = $this->db->query("SELECT * FROM user");
		$all = $all->result_array();
		return $all;
	}
	function get_users($data){
		$users = $this->db->query("select * from user where ".$data['field']." = '".$data['key']."'");
		$users = $users->result_array();
		return $users;
		
	}
	function delete_user($uid){
		$user_q = $this->db->query("select xmpp_user FROM user where uid = ".$uid);
		$user = $user_q->result_array();
		//print_r($user_q); die();
		$this->db->query("delete from confirmation where uid = ".$uid);
		$this->db->query("delete from user where uid = ".$uid);
		$this->db->query("delete from user_appearance where uid = ".$uid);
		$this->db->query("delete from user_bodyart where uid = ".$uid);
		$this->db->query("delete from user_education_employment where uid = ".$uid);
		$this->db->query("delete from user_enjoyment where uid = ".$uid);
		$this->db->query("delete from user_follow where from = ".$uid);
		$this->db->query("delete from user_follow where to = ".$uid);
		$this->db->query("delete from user_interest where uid = ".$uid);
		$this->db->query("delete from user_language where uid = ".$uid);
		$this->db->query("delete from user_leisure where uid = ".$uid);
		$this->db->query("delete from user_looking where uid = ".$uid);
		$this->db->query("delete from user_messages where from_uid = ".$uid);
		$this->db->query("delete from user_messages where to_uid = ".$uid);
		$this->db->query("delete from user_personality where uid = ".$uid);
		$this->db->query("delete from user_pets where uid = ".$uid);
		$this->db->query("delete from user_photo where uid = ".$uid);
		$this->db->query("delete from user_posts where poster_uid = ".$uid);
		$this->db->query("delete from user_posts where post_to_uid = ".$uid);
		$this->db->query("delete from user_profile where uid = ".$uid);
		$this->db->query("delete from user_situation where uid = ".$uid);
		$this->db->query("delete from user_views where uid = ".$uid);
		return $user[0]['xmpp_user'];
	}
	function has_profile_photo($uid){
		$this->db->where('uid',$uid);
		$this->db->select('profile_pic');
		$q = $this->db->get($this->table_name);
		$r = $q->result_array();
		if(empty($r[0]['profile_pic']))
			return false;
		return true;
	}
	function get_user_details($uid){
	
		$from_location['lat'] = $this->session->userdata('current_lattitude');
		$from_location['lng'] = $this->session->userdata('current_longitude');
		
		$q_str = "SELECT u.uid, u.firstname, u.lastname, u.xmpp_user, u.sex, YEAR(NOW()) - YEAR(u.birthdate) AS age, u.last_seen, u.join_date,
		ui.relationship_status, ui.about_me, ui.interested_in_men, ui.interested_in_women, ui.favorite_quotation, ui.current_location, ui.hometown_location,
		ui.profile_pic,
						( 6371 * acos( cos( radians(".$from_location['lat'].") ) * 
						cos( radians( ui.current_lattitude ) ) * 
						cos( radians( ui.current_longitude ) - 
						radians(".$from_location['lng'].") ) + 
						sin( radians(".$from_location['lat'].") ) * 
						sin( radians( ui.current_lattitude ) ) ) ) 
						AS distance
		FROM user u, user_info ui
		WHERE u.uid = ui.uid
		AND u.uid = '".$uid."'";

		$user_details = $this->db->query($q_str);
		$user_details = $user_details->result_array();
		
		$location = $user_details[0]['hometown_location'];
		$full_location = "";
		$x = 1;		
		do {
			$q = "SELECT * FROM user_location WHERE locid = " . $location;
			$q = $this->db->query($q);
			$q = $q->result_array();
			$q = $q[0];
			if($x > 1)
				$full_location .= ", ";
			$full_location .= $q['name'];
			$x++;
			
			$location = $q['parent'];
		} while($location != 0);			
		$user_details[0]['hometown_location'] = $full_location;
		
		$location = $user_details[0]['current_location'];
		$full_location = "";
		$x = 1;		
		do {
			$q = "SELECT * FROM user_location WHERE locid = " . $location;
			$q = $this->db->query($q);
			$q = $q->result_array();
			$q = $q[0];
			if($x > 1)
				$full_location .= ", ";
			$full_location .= $q['name'];
			$x++;
			
			$location = $q['parent'];
		} while($location != 0);			
		$user_details[0]['current_location'] = $full_location;
		
		//print_r($user_details);die();
		return $user_details;
	}
	
	function get_user_photos($uid) {
		$q = "SELECT * FROM user_photo WHERE uid = ".$uid;
		$q = $this->db->query($q);
		$q = $q->result_array();
		
		return $q;
	}
	
	function add_points($points,$uid){
		$this->db->query("UPDATE whetwhew.user SET points = points+".$points." WHERE uid = ".$uid);
	}
	function get_user_points($uid){
		$points = $this->db->query("SELECT points from user where uid = ".$uid)->result_array();
		return $points[0]['points'];
	}
	function member_search($where = null,$limit = null){
		$this->load->model('Userinterest_model');
		$uid = $this->session->userdata('logged_uid');
		$user_data = $this->retrieve(array('uid'=>$uid));
		$user_data = $user_data[0];
		$from_location['lat'] = $user_data['current_lattitude'];
		$from_location['lng'] = $user_data['current_longitude'];
		$this->db->select("u.uid, u.firstname, u.lastname, u.profile_pic, u.sex, u.xmpp_user, city.name AS city, state.name AS state, country.name AS country,
						DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(u.birthdate, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(u.birthdate, '00-%m-%d')) AS age,
						( 6371 * acos( cos( radians(".$from_location['lat'].") ) * 
						cos( radians( u.current_lattitude ) ) * 
						cos( radians( u.current_longitude ) - 
						radians(".$from_location['lng'].") ) + 
						sin( radians(".$from_location['lat'].") ) * 
						sin( radians( u.current_lattitude ) ) ) ) 
						AS distance",false);
		$this->db->from("user u");
		$this->db->join('user_location city', 'city.locid = u.city','left');
		$this->db->join('user_location state', 'state.locid = u.state','left');
		$this->db->join('user_location country', 'country.locid = u.country','left');
		$this->db->where('u.uid !=', $uid);
		if($where != null)
			$this->db->where($where);
		if($limit != null)
			$this->db->limit($limit);
		$this->db->order_by("u.last_seen", "desc"); 
		$q = $this->db->get();
		$response = array();
		$members = $q->result_array();
		foreach($members as $member){
			$interested_in = $this->Userinterest_model->retrieve(array('uid'=>$member['uid']));
			if(count($interested_in))
				$member['interested_in'] = $interested_in[0];
			if(!empty($member['profile_pic']))
				$member['profile_pic'] = config_item('s3_bucket_url').$member['uid'].'/photos/'.$member['profile_pic'];
			else{
				if($member['sex'] == 'm')
					$member['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile.jpg';
				else
					$member['profile_pic'] = 'https://matchup360.com/assets/img/blank_profile_female.jpg';
			}
			$response[] = $member;
		}
		return $response;
	}
}
?>