<?php
class Userviews_model extends CI_Model {

    var $table_name   = 'user_views';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	function get_views($uid){
	$result = null;
		$views_q = $this->db->query("SELECT political.name AS political,
												religion.name AS religion
										FROM user_views uv
										LEFT JOIN list political ON uv.political = political.id
										LEFT JOIN list religion ON uv.religion = religion.id
										WHERE uv.uid = ".$uid)->result_array();
		if(!empty($views_q[0]))
		$result = $views_q[0];
		
		return $result;
	}
}
?>