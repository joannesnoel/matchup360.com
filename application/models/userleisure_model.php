<?php
class Userleisure_model extends CI_Model {

    var $table_name   = 'user_leisure';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	function get_leisure($uid){
		$leisure_q = $this->db->query("SELECT	leisure.name, leisure.`group`
										FROM user_leisure ul
										INNER JOIN list leisure ON ul.lid = leisure.id
										WHERE ul.uid = ".$uid)->result_array();
		$result = $leisure_q;
		return $result;
	}
}
?>