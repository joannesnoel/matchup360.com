<?php
class Userencounter_model extends CI_Model {

    var $table_name   = 'user_encounter';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
}
?>