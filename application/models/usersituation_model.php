<?php
class Usersituation_model extends CI_Model {

    var $table_name   = 'user_situation';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){ 
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	function get_situation($uid){
		$result = null;
		$pet_q = $this->db->query("SELECT pet.name
										FROM user_pets up
										LEFT JOIN list pet ON up.lid = pet.id
										WHERE up.uid = ".$uid)->result_array();
		if(count($pet_q) >= 1)	
			$result['pets'] = $pet_q;
		
		$status_q = $this->db->query("SELECT status.name AS marital_status,
												children.name AS has_children
										FROM user_situation us
										LEFT JOIN list status ON us.marital_status = status.id
										LEFT JOIN list children ON us.have_children = children.id
										WHERE us.uid = ".$uid)->result_array();
		if(!empty($status_q[0]))	
			$result['status'] = $status_q[0];
		return $result;
	}
}
?>