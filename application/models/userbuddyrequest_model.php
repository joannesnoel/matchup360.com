<?php
class Userbuddyrequest_model extends CI_Model {

    var $table_name   = 'user_buddies';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function addBuddy($data){
		$this->db->where($data);
		$check = $this->db->get($this->table_name);
		if($check->num_rows == 0){
			$data['status'] = 'P';
			$data['request_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->table_name,$data);
			return $this->db->insert_id();
		}
	}
	
	function buddyRequest($uid){
		$q_str = "SELECT * from `user_buddies` WHERE `to` = " . $uid . " AND `status` = 'P'";
		$q = $this->db->query($q_str);
		$result = $q->result_array();
		
		return $result;		
	}
	
	function acceptBuddy($data){
		$response_date = date('Y-m-d H:i:s');
		$from = $data['from'];
		$to = $data['to'];
		
		$q_str = "UPDATE " . $this->table_name . " SET `status` = 'B', `response_date` = '" . $response_date . "' WHERE `from` = " . $from . " AND `to` = " . $to;
		$this->db->query($q_str);
		
		$q_str = "SELECT * FROM " . $this->table_name . " WHERE `from` = " . $to . " AND `to` = " . $from;
		$check = $this->db->query($q_str);
		
		if($check->num_rows == 0){
			$temp = $data['to'];
			$data['to'] = $data['from'];
			$data['from'] = $temp;
			$data['status'] = 'B';
			$data['response_date'] = date('Y-m-d H:i:s');
			
			$this->db->insert($this->table_name,$data);
			return $this->db->insert_id();
		}
		
		else {
			$q_str = "UPDATE " . $this->table_name . " SET `status` = 'B', `response_date` = '" . $response_date . "' WHERE `from` = " . $to . " AND `to` = " . $from;
			$this->db->query($q_str);
			
			return -999;
		}
	
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	
}
?>