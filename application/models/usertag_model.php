<?php
class Usertag_model extends MY_Model{
    function __construct(){
        parent::__construct();
		$this->setTable('user_tag');
    }
	function getSubscriptions(){
		$q = $this->db->query("SELECT t.name FROM user_tag ut INNER JOIN tag t USING(tag_id) WHERE ut.uid = " . $this->session->userdata('logged_uid'));
		return $q->result_array();
	}
}
?>