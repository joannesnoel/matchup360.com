<?php
class Userpersonality_model extends CI_Model {

    var $table_name   = 'user_personality';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	function get_personality($uid){
		$result = null;
		$personality_q = $this->db->query("SELECT smoke.name AS smoke,
												drink.name AS drink,
												behave.name AS social_behaviour
										FROM user_personality up
										LEFT JOIN list smoke ON up.smoke = smoke.id
										LEFT JOIN list drink ON up.drink = drink.id
										LEFT JOIN list behave ON up.social_behaviour = behave.id
										WHERE up.uid = ".$uid)->result_array();
		if(!empty($personality_q[0]))
		$result['personality'] = $personality_q[0];
		
		$enjoy_q = $this->db->query("SELECT enjoy.name
											FROM user_enjoyment ue
											INNER JOIN list enjoy ON ue.lid = enjoy.id
											WHERE ue.uid = ".$uid)->result_array();
 		if(count($enjoy_q) >= 1)										
		$result['enjoy'] = $enjoy_q;
		return $result;
	}
}
?>