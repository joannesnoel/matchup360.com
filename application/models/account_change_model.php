<?php
class Account_change_model extends CI_Model {

    var $table_name   = 'account_change';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function retrieve($where = null){
		if($where!=null)
			$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	
}
?>