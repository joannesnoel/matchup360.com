<?php
class Confirmation_model extends CI_Model {

    var $table_name   = 'confirmation';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function confirm_code($uid,$code){
		$this->db->where('uid',$uid);
		$this->db->where('confirmation_code',$code);
		$query = $this->db->get($this->table_name);
		if(count($query->result())>0){
			$data = array(
							'confirm_date'=>date('Y-m-d H:i:s')
						);
			$this->db->where('uid',$uid);
			$this->db->where('confirmation_code',$code);
			$query = $this->db->update($this->table_name,$data);
			
			$data = array(
							'status'=>'A'
						);
			
			$this->db->where('uid',$uid);
			$query = $this->db->update('user',$data);
			
			return true;
		}else{
			return false;
		}
	}
}
?>