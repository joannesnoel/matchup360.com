<?php
class Photolikes_model extends CI_Model {

    var $table_name = 'photo_likes';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	function insert($data){
		$like_id = $this->db->insert($this->table_name, $data);
		return $like_id;
	}
	function retrieve($reference){
		$likes = $this->db->query('SELECT * FROM '.$this->table_name.' WHERE reference = '.$reference)->result_array();
		return $likes;
	}
	function get_likes($reference){
		$this->load->library('session');
		$uid = $this->session->userdata('logged_uid');
		$likes = $this->db->query("SELECT 'You' as name, l.uid FROM photo_likes l where l.uid = ".$uid." and reference = ".$reference."
									UNION
									SELECT CONCAT(u.firstname, ' ', u.lastname) as name, u.uid FROM photo_likes l
																		join user u on l.uid = u.uid WHERE l.uid != ".$uid."
									and reference = ".$reference)->result_array();
		$photo_likes_count = count($likes);
		$photo_likes = '';
		if($photo_likes_count > 1){
			$first_two = $likes[0]['name'].', '. $likes[1]['name'];
			$append = $photo_likes_count > 2 ? 
								' and <span class="others">'. ($photo_likes_count - 2) .' other '.($photo_likes_count - 2 == 1? 'person': 'persons').' like this photo. </span>': 
								' like this photo.';
			$photo_likes = $first_two . $append;
		}if($photo_likes_count == 1){
			$photo_likes = $likes[0]['name'] .' '. ($likes[0]['name'] == 'You'? 'like': 'likes') . ' this photo.';
		}
		return array('likes_string' => $photo_likes,
					 'likes' => $likes);
	} 
	function get_likes_string($reference){
		$this->load->library('session');
		$uid = $this->session->userdata('logged_uid');
		$likes = $this->db->query("SELECT 'You' as name FROM photo_likes l where l.uid = ".$uid." and reference = ".$reference."
									UNION
									SELECT CONCAT(u.firstname, ' ', u.lastname) as name FROM photo_likes l
																		join user u on l.uid = u.uid WHERE l.uid != ".$uid."
									and reference = ".$reference)->result_array();
		$photo_likes_count = count($likes);
		$photo_likes = '';
		if($photo_likes_count > 1){
			$first_two = $likes[0]['name'].', '. $likes[1]['name'];
			$append = $photo_likes_count > 2 ? 
								' and '. ($photo_likes_count - 2) .' other '.($photo_likes_count - 2 == 1? 'person': 'persons').' like this photo.': 
								' like this photo.';
			$photo_likes = $first_two . $append;
		}if($photo_likes_count == 1){
			$photo_likes = $likes[0]['name'] .' '. ($likes[0]['name'] == 'You'? 'like': 'likes') . ' this photo.';
		}
		return $photo_likes;
	}
	function delete($where){
		$this->db->where($where);
		$this->db->delete($this->table_name);
	}
	function delete_where($data){
		$this->db->query("DELETE FROM ".$this->table_name." WHERE reference = ".$data['reference']." AND uid = ".$data['uid']);
	}
	function is_liked($data){
		$result = $this->db->query("SELECT * FROM whetwhew.photo_likes where reference = ".$data['reference']." and uid = ".$data['uid'])->result_array();
		$is_liked = count($result) >= 1? true: false;
		return $is_liked;
	}
    
}
?>