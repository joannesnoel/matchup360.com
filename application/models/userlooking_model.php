<?php
class Userlooking_model extends CI_Model {

    var $table_name   = 'user_looking';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	function get_looking($uid){
	
		$looking_q = $this->db->query("SELECT looking.name
										FROM user_looking ul
										INNER JOIN list looking on ul.lid = looking.id
										WHERE ul.uid = ".$uid)->result_array();
		
		$result['looking']= $looking_q;
		
		return $result;
	}
}
?>