<?php
class Userlocation_model extends CI_Model {

    var $table_name   = 'user_location';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function retrieve($where = null){
		if(!is_null($where))
			$this->db->where($where);
		$query = $this->db->get($this->table_name);
		if(count($query->result())>0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function get_nearby_users($from_location, $km_radius){
		$query_str = "SELECT DISTINCT(loc.uid), 
						( 6371 * acos( cos( radians(".$from_location['lat'].") ) * 
						cos( radians( loc.lat ) ) * 
						cos( radians( loc.lng ) - 
						radians(".$from_location['lng'].") ) + 
						sin( radians(".$from_location['lat'].") ) * 
						sin( radians( loc.lat ) ) ) ) 
						AS distance FROM user_location loc, user u, user_info ui, user_photo up
						WHERE loc.uid = u.uid
						AND ui.uid = u.uid
						AND up.uid = u.uid
						HAVING distance < $km_radius ORDER BY distance ASC";
		
	}
}
?>