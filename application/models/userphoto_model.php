<?php
class Userphoto_model extends CI_Model {

    var $table_name   = 'user_photo';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	function insert_batch($data){
		$this->db->insert_batch($this->table_name,$data);
		//return $this->db->insert_id();
	}
	function retrieve($where = null,$dimension = 4){
		$this->db->from($this->table_name);
		if($where != null)
			$this->db->where($where);
		$this->db->where('dimension', $dimension);
		$this->db->order_by('pid', 'random');
		return $this->db->get()->result_array();
	}
	function retrieve_largest($where, $post_photo = false){
		$this->db->from($this->table_name);
		if($post_photo == false)
			$this->db->where('post_id IS NULL');
		if($where != null)
			$this->db->where($where);
		$this->db->where('dimension', 4);
		$this->db->order_by('pid', 'desc');
		$photos =  $this->db->get()->result_array();
		$photos_array = array();
		foreach($photos as $photo){
			$temp['largest'] = $this->getLargest($photo['reference']);
			$photo['url'] = config_item('s3_bucket_url').$photo['uid'].'/photos/'.$photo['filename'];
			$temp['thumbnail'] = $photo;
			$photos_array[] = $temp;
		}
		return $photos_array;
	}
	function random($rate = null){
		if($rate != null){
			$rate['uid'] = $this->session->userdata('logged_uid');
			$rate['datetimestamp'] = date('Y-m-d H:i:s');
			$this->db->insert('photo_hotornot',$rate);
		}
		$q_str = $this->db->query("SELECT * FROM user_photo WHERE reference IS NOT null AND reference NOT IN (SELECT reference FROM photo_hotornot WHERE uid =".$this->session->userdata('logged_uid').") ORDER BY rand() LIMIT 1");
		$result = $q_str->result_array();
		if(count($result)>0){
			$photo = $result[0];
			$photo = $this->getLargest($photo['reference']);
			return $photo;
		}else{
			return false;
		}
	}
	function retrieve_all_dimensions($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	function profile_photo($uid){
		$query = $this->db->query("SELECT profile_pic FROM user WHERE uid=".$uid);
		$result = $query->result_array();
		
		if(count($result)>0 && !empty($result[0]['profile_pic']))
			return $result[0]['profile_pic'];
		else
			return false;
	}
	private function getLargest($reference){
		$query = $this->db->query("SELECT uid, filename, reference, width, height FROM user_photo WHERE reference = ".$reference." ORDER BY dimension ASC LIMIT 1");
		$photo = $query->result_array();
		$photo[0]['url'] = config_item('s3_bucket_url').$photo[0]['uid'].'/photos/'.$photo[0]['filename'];
		$photo[0]['width'] =  $photo[0]['width'];
		$photo[0]['height'] = $photo[0]['height'];
		return $photo[0];
	}
	function set_profile_photo($uid,$photo_url){
		$this->db->query("UPDATE user SET profile_pic ='".$photo_url."' WHERE uid=".$uid);
	}
	function delete($id){
		$this->db->query("DELETE FROM user_photo where pid = ".$id);
	}
	function getThumbnail($pid){
		$this->db->select('user_photo.*, user.xmpp_user, user.firstname, user.lastname, user.profile_pic, user.sex');
		$this->db->from($this->table_name);
		$this->db->join('user', 'user.uid = user_photo.uid');
		$this->db->where('reference', $pid);
		$this->db->where('dimension', 4);
		$result =$this->db->get()->result_array();
		$result = $result[0];
		if(count($result)>0){ 
			if(!empty($result['profile_pic']))
				$result['profile_pic'] = config_item('s3_bucket_url').$result['uid'].'/photos/'.$result['profile_pic'];
			else{
				if($result['sex'] == 'm')
					$result['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile.jpg';
				else
					$result['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile_female.jpg';
			}	
			return $result;
		}
		return $result;
	}
	
}
?>