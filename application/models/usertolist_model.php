<?php
class Usertolist_model extends CI_Model {

    var $table_name   = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	
	function retrieve($where){
		$this->db->where($where);
		return $this->db->get($this->table_name)->result_array();
	}
	
	function delete($where){
		$this->db->where($where);
		$this->db->delete($this->table_name); 
	}
	
	function set_table($table){
		$this->table_name = $table;
	}
	
}
?>