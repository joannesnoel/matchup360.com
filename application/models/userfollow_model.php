<?php
class Userfollow_model extends CI_Model {

    var $table_name   = 'user_follow';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	function insert($data){
		$this->db->where($data);
		$result = $this->db->get($this->table_name);
		$result = $result->result_array();
		if(count($result) == 0){
			$this->db->insert($this->table_name,$data);
			return $this->db->insert_id();
		}
		return false;
	}
	function delete($where){
		$this->db->where($where);
		$this->db->delete($this->table_name);
	}
	function retrieve($where){
		$this->db->where($where);
		$result = $this->db->get($this->table_name)->result_array();
		if(count($result)>0)
			return $result;
		return false;
	}
	function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	function get_followed_members($uid){
		//$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userfollow_model');
		$from_location['lat'] = $this->session->userdata('current_lattitude');
		$from_location['lng'] = $this->session->userdata('current_longitude');
		
		$q_str = "SELECT u.uid, u.firstname, u.lastname, u.profile_pic, u.sex, u.xmpp_user, city.name AS city, state.name AS state, country.name AS country,
						DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(u.birthdate, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(u.birthdate, '00-%m-%d')) AS age,
						( 6371 * acos( cos( radians(".$from_location['lat'].") ) * 
						cos( radians( u.current_lattitude ) ) * 
						cos( radians( u.current_longitude ) - 
						radians(".$from_location['lng'].") ) + 
						sin( radians(".$from_location['lat'].") ) * 
						sin( radians( u.current_lattitude ) ) ) ) 
						AS distance FROM user u
						INNER JOIN user_follow f ON f.to = u.uid
						LEFT JOIN user_location city ON u.city = city.locid
						LEFT JOIN user_location state ON u.state = state.locid
						LEFT JOIN user_location country ON u.country = country.locid
						WHERE f.from = ".$uid;
		$q = $this->db->query($q_str);
		$response = array();
		if(is_array($q)){
			$members = $q->result_array();
			foreach($members as $member){
				$check_follow = $this->Userfollow_model->retrieve(array('from'=>$uid ,'to'=>$member['uid']));
				if(!empty($member['profile_pic']))
					$member['profile_pic'] = config_item('s3_bucket_url').$member['uid'].'/photos/'.$member['profile_pic'];
				else{
					if($member['sex'] == 'm')
						$member['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile.jpg';
					else
						$member['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile_female.jpg';
				}
				if($check_follow)
					$member['followed'] = true;
				else
					$member['followed'] = false;
				
				$response[] = $member;
			}
			return $response;
		}else
			return false;
	}
	function get_following_members($uid){
		//$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userfollow_model');
		$from_location['lat'] = $this->session->userdata('current_lattitude');
		$from_location['lng'] = $this->session->userdata('current_longitude');
		
		$q_str = "SELECT u.uid, u.firstname, u.lastname, u.profile_pic, u.sex, u.xmpp_user, city.name AS city, state.name AS state, country.name AS country,
						DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(u.birthdate, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(u.birthdate, '00-%m-%d')) AS age,
						( 6371 * acos( cos( radians(".$from_location['lat'].") ) * 
						cos( radians( u.current_lattitude ) ) * 
						cos( radians( u.current_longitude ) - 
						radians(".$from_location['lng'].") ) + 
						sin( radians(".$from_location['lat'].") ) * 
						sin( radians( u.current_lattitude ) ) ) ) 
						AS distance FROM user u
						INNER JOIN user_follow f ON f.from = u.uid
						LEFT JOIN user_location city ON u.city = city.locid
						LEFT JOIN user_location state ON u.state = state.locid
						LEFT JOIN user_location country ON u.country = country.locid
						WHERE f.to = ".$uid;
		$q = $this->db->query($q_str);
		$response = array();
		$members = $q->result_array();
		foreach($members as $member){
			$check_follow = $this->Userfollow_model->retrieve(array('from'=>$uid ,'to'=>$member['uid']));
			if(!empty($member['profile_pic']))
				$member['profile_pic'] = config_item('s3_bucket_url').$member['uid'].'/photos/'.$member['profile_pic'];
			else{
				if($member['sex'] == 'm')
					$member['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile.jpg';
				else
					$member['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile_female.jpg';
			}
			if($check_follow)
				$member['followed'] = true;
			else
				$member['followed'] = false;
			
			$response[] = $member;
		}
		return $response;
	}
	
}
?>