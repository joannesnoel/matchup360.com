<?php
class List_model extends CI_Model {

    var $table_name   = 'list';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
	function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	
	function retrieve($where = null){
		if($where!=null)
			$this->db->where($where);
		$q = $this->db->get($this->table_name);
		return $q->result_array();
	}
	
	function get_profile_completion(){
		$set = 0;
		$total = 0;
		
		$appearance = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_appearance')->result_array();
		$bodyart = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_bodyart')->result_array();
		$total++;
		if(count($appearance)>0 || count($bodyart)>0)
			$set++;
			
		$situation = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_situation')->result_array();
		$pets = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_pets')->result_array();
		$total++;
		if(count($situation)>0 || count($pets)>0)
			$set++;
			
		$educationemployment = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_education_employment')->result_array();
		$total++;
		if(count($educationemployment)>0)
			$set++;
			
		$leisure = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_leisure')->result_array();
		$total++;
		if(count($leisure)>0)
			$set++;
			
		$personality = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_personality')->result_array();
		$enjoyment = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_enjoyment')->result_array();
		$total++;
		if(count($personality)>0 || count($enjoyment)>0)
			$set++;
			
		$views = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_views')->result_array();
		$total++;
		if(count($views)>0)
			$set++;
			
		$looking = $this->db->where(array('uid'=>$this->session->userdata('logged_uid')))->get('user_looking')->result_array();
		$total++;
		if(count($looking)>0)
			$set++;
		return ($set/$total) * 100;
	}
	
}
?>