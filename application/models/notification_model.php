<?php
class Notification_model extends CI_Model {
	var $table_name = 'notification';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	function insert($data){
		$this->db->insert('notification', $data);
		return $this->db->insert_id();
	}
	function retrieve($where = null,$order_by = null){
		if($where!=null)
			$this->db->where($where);
		if($order_by!=null)
			$this->db->order_by($order_by['field'],$order_by['value']);
		return $this->db->get($this->table_name)->result_array();
	}
	function delete($where){
		$this->db->where($where);
		$this->db->delete('notification');
	}
	function set_notification_read($uid){
		$this->db->query("UPDATE notification SET status = 0 WHERE uid = ".$uid);
	}
	function get_notification_info($id){
		$this->db->select('u.uid, u.xmpp_user,u.profile_pic, u.firstname, u.lastname, u.points, n.id as notification_id');
		$this->db->from('notification n');
		$this->db->where(array('n.id' => $id));
		$this->db->join('user u', 'u.uid = n.from_uid','left');
		$notification = $this->db->get()->result_array();
		if(!empty($notification[0]['profile_pic']))
			$notification[0]['profile_pic'] = config_item('s3_bucket_url').$notification[0]['uid'].'/photos/'.$notification[0]['profile_pic'];
		else{
			if($notification[0]['sex'] == 'm')
				$notification[0]['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile.jpg';
			else
				$notification[0]['profile_pic'] = 'http://matchup360.com/assets/img/blank_profile_female.jpg';
		}
		return $notification[0];
	}
	function set_notification_revealed($id){
		$this->db->update('notification', array('revealed' => 1), array('id' => $id));
	}
	function exist($to_uid, $type, $data){
		if($type == 'photo_like')
			$exist = $this->db->query("SELECT * FROM notification where to_uid = ".$to_uid." AND type = '".$type."' AND pid = ".$data)->result_array();
		else if($type == 'follow')
			$exist = $this->db->query("SELECT * FROM notification where to_uid = ".$to_uid." AND type = '".$type."' AND from_uid = ".$data)->result_array();
		if(count($exist) >= 1)
			return true;
		else
			return false;
	}
} 
?>