<!DOCTYPE html>
<html>
    <head>
		<title> WhetWhew - Step 2 </title>
		
		<link rel="stylesheet" href="//wheewhew.com/assets/css/signup_step2.css" />
		<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    </head>
    <body>
		<div id="logout">
		<a href="http://wheewhew.com/welcome/logout"> Log Out </a>
		</div>
		<div id="form-box">
		<div id="errors">
		<?php echo validation_errors(); ?>
		</div>
		<?php echo form_open(); ?>
		<h1 id="info-banner"> Personal Information </h1>
        <label for="hometown">Hometown:</label><input type="text" id="hometown" />
        <label for="city">Current City:</label><input type="text" id="city" />
		<input type="hidden" name="hometown_components" id="hometown_components" />
		<input type="hidden" name="city_components" id="city_components" />
		<label for="relationship_status">Status:</label>
		<select name="relationship_status" id="status">
			<option value="S">Single</option>
			<option value="R">In a Relationship</option>
			<option value="E">Engaged</option>
			<option value="M">Married</option>
			<option value="C">It's Complicated</option>
			<option value="O">Open Relationship</option>
			<option value="D">Divorced</option>
			<option value="X">Separated</option>
			<option value="W">Widowed</option>
		</select>
		<label for="about_me">About Me:</label>
		<textarea name="about_me"></textarea>
		<label for="interested_in">Interested In:</label>
		<div class="radio-group">
		<table>
			<tr><td><input type="checkbox" name="interested_in[0]" value="1" /></td><td>Men</td>
			<td><input type="checkbox" name="interested_in[1]" value="1" /></td><td>Women</td></tr>
		</table>
		</div>
		<label for="age_group">Age Group:</label>
		<div id="age-from" class="radio-group">
		From: <select id="age_from" name="age_from" style="float:none; display:inline-block; width: 100px">
					<option>Select</option>
			<?php
				for($i=18;$i<=60;$i++){
					?>
			<option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php
				}
			?>
		</select>
		To: <select id="age_to" name="age_to"  style="float:none; display:inline-block; width: 100px" disabled>
		
		</select>
		</div>
		<label for="favorite_quotation">Favorite Quotation:</label>
		<textarea name="favorite_quotation"></textarea>
		<input type="submit" value="Update" />
		</form>
		</div>
        
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
        <script>
            var hometown = new google.maps.places.Autocomplete($("#hometown")[0], {});
            var city = new google.maps.places.Autocomplete($("#city")[0], {});

            google.maps.event.addListener(hometown, 'place_changed', function() {
                var place = hometown.getPlace();
                console.log(place.address_components);
				$('#hometown_components').val(JSON.stringify(place.address_components));
            });
			
			google.maps.event.addListener(city, 'place_changed', function() {
                var place = city.getPlace();
                console.log(place.address_components);
				$('#hometown_components').val(JSON.stringify(place.address_components));
            });
			$(document).ready(function(){
				$('#age_from').change(function(){
					var val = parseInt($(this).val());
					if(val > 0){
						$('#age_to')
							.find('option')
							.remove()
							.end()
							.append('<option value="'+(val+5)+'">'+(val+5)+'</option>')
							.val(val+5)
							.removeAttr('disabled')
						;
						for(var i=(val+6);i<=80;i++){
							$('#age_to').append('<option value="'+i+'">'+i+'</option>');
						}
					}else{
						$('#age_to')
							.find('option')
							.remove()
							.end()
							.attr('disabled', 'disabled').val('');
						;
					}
				});
			
			
			});
        </script>
    </body>