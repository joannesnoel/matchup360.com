<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>MatchUp360</title>

	<link rel="stylesheet" href="//wheewhew.com/assets/css/signup_page.css" />
	<link rel="stylesheet" href="//wheewhew.com/assets/css/jquery-impromptu.signup.css" />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="http://wheewhew.com/assets/js/jquery-impromptu.js"></script>
	<script src="//matchup360.com/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="//wheewhew.com/assets/js/jquery.filedrop.js"></script>
	<script src="//wheewhew.com/assets/js/signup_step3.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
	<script src="//wheewhew.com/assets/js/jquery.geocomplete.min.js"></script>
	<script src="//wheewhew.com/assets/js/jquery.Jcrop.js"></script>
	<link rel="stylesheet" href="//wheewhew.com/assets/css/jquery.Jcrop.css" type="text/css" />   
	<link rel="stylesheet" href="//matchup360.com/assets/css/redmond/jquery-ui-1.10.3.custom.min.css" />
	<script type="text/javascript">
		var geo_options = {
			  types: ['(cities)']
			 };
		var photos = [];
		var access_token = false;
	</script>
</head>
<body>
<div id="fb-root"></div>
<script>
  // Additional JS functions here
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '384283535015016', // App ID
      channelUrl : '//www.matchup360.com/welcome/channel', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional init code here
	// Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
	  $("#use_facebook").hide();
	  access_token =   FB.getAuthResponse()['accessToken'];
      FB.api('/me',function(response){
		var birthdate = response.birthday.split("/");
		$( "#birthdate" ).datepicker("setDate",birthdate[2]+'-'+birthdate[0]+'-'+birthdate[1]);
		geo_options.location = response.location.name;
		$("input#firstname").val(response.first_name);
		$("input#lastname").val(response.last_name);
		$("input#email").val(response.email);
		$("input#email-verify").val(response.email);
		$("input#access_token").val(access_token);
		if(response.gender == 'male')
			$('select#sex').val('m');
		else
			$('select#sex').val('f');
		if(response.languages){
			for(var i=0; i<response.languages.length; i++){
				$('[data-language="'+response.languages[i].name+'"]').attr('checked','checked');
			}
		}
	  });
	  album_id = null;
	  photos = new Array();
	  FB.api('/me/albums',function(response){
		for(var i=0; i<response.data.length; i++){
			if(response.data[i].name != 'Profile Pictures')
				continue;
			album_id = response.data[i].id;
		}
		if(album_id != null){
			FB.api('/'+album_id+'/photos',function(response){
				for(var i=0; i<response.data.length; i++){
					photos[i] = response.data[i].source;
				}
			});
		}
	  });
    } else if (response.status === 'not_authorized') {
      FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
    } else {
      FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>
<div id="container">
	<script type="text/javascript">	
				var photos_str = '';
				var signup_process = {
						state0: {
							title: 'Sign Up',
							html:'<button id="use_facebook">Quick Signup with Facebook</button>\
							<form id="form1" >\
							<input type="hidden" id="access_token" name="fb_access_token" value="" />\
						<div class="row"><label for="firstname">Firstname:</label>\
						<input class="textfield" type="text" id="firstname" name="firstname" value="" /><i class="field_error" id="firstname_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="lastname">Lastname:</label>\
						<input class="textfield" type="text" id="lastname" name="lastname" value="" /><i class="field_error" id="lastname_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="email">Email:</label>\
						<input class="textfield" type="text" id="email" name="email" value="" /><i class="field_error" id="email_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="email-verify">Confirm Email:</label>\
						<input class="textfield" type="text" id="email-verify" name="email-verify" value="" /><i class="field_error" id="email2_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="pwd">Password:</label>\
						<input class="textfield" type="password" id="pwd" name="pwd" value="" /><i class="field_error" id="password_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="pwd">Confirm Password:</label>\
						<input class="textfield" type="password" id="pwd-verify" name="pwd-verify" value="" /><i class="field_error" id="password_verify_error"></i>\
						<div style="clear: both"></div></div>\
						</form>',  
							buttons: { Next: 1 },
							focus: "input[name='firstname']",
							submit:function(e,v,m,f){ 
								e.preventDefault();
								$.ajax({
									url: 'ajax/create_user',
									type: 'post',
									data: $('#form1').serialize(),
									success: function(data){
										$('.field_error').css('display','none').attr('title','');
										var data = eval('('+data+')');
										if(data.success == false){
											$('.jqi').css({'width': '495px','margin-left':'-260px'});
											if(data.error.firstname != '')
												$('#firstname_error').css('display','inline-block').attr('title',data.error.firstname);
											if(data.error.lastname != '')
												$('#lastname_error').css('display','inline-block').attr('title',data.error.lastname);
											if(data.error.email != '')
												$('#email_error').css('display','inline-block').attr('title',data.error.email);
											if(data.error.email2 != '')
												$('#email2_error').css('display','inline-block').attr('title',data.error.email2);
											if(data.error.password != '')
												$('#password_error').css('display','inline-block').attr('title',data.error.password);
											
										}else{
											$('.jqiclose').hide();
											$('#jqistate_state1 .jqimessage .email').html(data.email);
											$.prompt.goToState('state1');
											var hometown_components = new Object();
											$("#hometown").geocomplete(geo_options)
											  .bind("geocode:result", function(event, result){
												hometown_components.address_components = result.address_components;
												hometown_components.lat = result.geometry.location.lat();
												hometown_components.lng = result.geometry.location.lng();
												$('#hometown_components').val(JSON.stringify(hometown_components));
											  })
											  .bind("geocode:error", function(event, status){
												console.log("ERROR: " + status);
											  });
											$.prompt.goToState('state1');
											$('.jqiclose').hide();
										}		
									}
								});	
							}
						},
						state1: {
							title: 'Profile Information',
							html:'<form id="per_info"> \
									<div class="row"> \
										<label for="hometown">Hometown/City:</label> \
										<input class="textfield" type="text" id="hometown" /> \
										<i class="field_error" id="hometown_error"></i> \
										<input type="hidden" name="hometown_components" id="hometown_components" /> \
									</div> \
									<div class="row"> \
										<label for="sex">Sex:</label> \
										<select id="sex" name="sex"> \
											<option value="">Select</option> \
											<option value="m">Male</option> \
											<option value="f">Female</option> \
										</select> \
										<i class="field_error" id="sex_error"></i> \
									</div> \
									<div class="row"> \
										<label for="birthdate">Birthdate:</label> \
										<input class="textfield" type="text" id="birthdate" name="birthdate" value="" readonly="true"/><i class="field_error" id="birthdate_error"></i> \
									</div> \
									<div class="row"> \
										<label for="language">I can speak:</label> \
										<div id="lang"> \
											<?php foreach($languages as $language){ ?>\
<div class="language"><input type="checkbox" data-language="<?php echo $language['name']; ?>" name="language[<?php echo $language['id']; ?>]" /> <?php echo $language['name']; ?></div> \
											<?php } ?>\
										</div> \
										<i class="field_error" id="language_error"></i> \
									</div> \
								</form>',
							buttons: { Next: 1 },
							focus: 1,
							submit:function(e,v,m,f){ 
							
								e.preventDefault();
								if(v==1){	
									$.ajax({
										url:'ajax/create_userInfo',
										type:'post',
										data: $('#per_info').serialize(),
										success: function(data){
											$('.field_error').css('display','none').attr('title','');
											var data = eval('('+data+')');
											if(data.success == false){
											$('.jqi').css({'width': '495px','margin-left':'-260px'});
												if(data.error.hometown_components != '')
													$('#hometown_error').css('display','inline-block').attr('title',data.error.hometown_components);
												if(data.error.sex != '')
													$('#sex_error').css('display','inline-block').attr('title',data.error.sex);
												if(data.error.birthdate != '')
													$('#birthdate_error').css('display','inline-block').attr('title',data.error.birthdate);
												if(data.error.language != '')
													$('#language_error').css('display','inline-block').attr('title',data.error.language);
											}else{
												if(photos.length<1)
													$.prompt.goToState('state3');
												else{
													$('#jqi_state1_buttonNext').replaceWith('<span class="loader">Importing Pictures<img src="./assets/img/loader.gif" /></span>');
													debugger;
													$.ajax({
														url: window.location.protocol + '//' + window.location.hostname + '/ajax/importFBPhotos',
														type: "post",
														data: "photos="+JSON.stringify(photos),
														success: function(response){
															response = JSON.parse(response);
															if(response.status == 'success'){
																getPhotos();
																$('.jqiclose').hide();
																$('#jqi_state4_buttonBack').hide();
																$.prompt.goToState('state4',function(){
																	$('#jqi_state4_buttonFinish').hide();
																	$(".photo").click(function(){
																		$('#jqi_state4_buttonFinish').hide();
																		var filename = $(this).attr('data');
																		var image_src_400 = $(this).find('img').attr('src');
																		var image_src_800 = image_src_400.replace("_thumb","_400");
																		$('#image-cropper').html('<img id="jcrop-target" src="'+image_src_800+'" />');
																		$('#orig_photo_url').val(image_src_800);
																		$('#jcrop-target').Jcrop({
																			onSelect: updateCoords,
																			onChange: updateCoords,
																			onRelease: updateCoords,
																			setSelect:   [ 0, 0, 400, 400 ],
																			aspectRatio: 1/1,
																			minSize: [400,400]
																		});
																		
																	});
																});
															}
														}
													});
												}
											}		
										}
									});
									$('.jqi').css({'width': 'auto'});
									//$.prompt.goToState('state3');
								}
							}
						},
						state3: {
							title: 'Upload Photos',
							html:'<div id="dropbox">\
							<span class="message">Drop images here to upload. <br /><i>(they will only be visible to you)</i></span>\
							</div>',
							buttons: { Next: 1, Skip: 2 },
							focus: 1,
							submit:function(e,v,m,f){ 
								if(v==1){
									getPhotos();
									$('.jqi').css({'width': 'auto'});
									$('.jqiclose').hide();
									$.prompt.goToState('state4', function(){
										$('#jqi_state4_buttonFinish').hide();
										$(".photo").click(function(){
											$('#jqi_state4_buttonFinish').hide();
											var filename = $(this).attr('data');
											var image_src_400 = $(this).find('img').attr('src');
											var image_src_800 = image_src_400.replace("_thumb","_400");
											$('#image-cropper').html('<img id="jcrop-target" src="'+image_src_800+'" />');
											$('#orig_photo_url').val(image_src_800);
											$('#jcrop-target').Jcrop({
												onSelect: updateCoords,
												onChange: updateCoords,
												onRelease: updateCoords,
												setSelect:   [ 5, 5, 390, 390 ],
												aspectRatio: 1/1,
												minSize: [200,200],
												maxSize: [390,390]
											});
											
										});
									});
									
								}else if(v==2){
									window.location = '/';
								}
								e.preventDefault();
							}
						},
						state4: {
							title: 'Select and Crop Profile Photo',
							html: '<div id="image-cropper">\
									</div>\
								<div id="photo-gallery"></div>\
							<?php echo form_open('/ajax/set_profile_coordinates', array('id' => 'set_coordinate')); ?>\
								<input type="hidden" name="orig_photo_url" id="orig_photo_url" />\
								<input type="hidden" name="x" id="x" />\
								<input type="hidden" name="y" id="y" />\
								<input type="hidden" name="x2" id="x2" />\
								<input type="hidden" name="x2" id="x2" />\
								<input type="hidden" name="w" id="w" />\
								<input type="hidden" name="h" id="h" />\
								<input type="hidden" name="action" value="crop" />\
							</form><div style="clear: both"></div>',
							buttons: { Back: -1, Finish: 1 },
							focus: 1,
							submit:function(e,v,m,f){
								if(v==1){
									if(access_token){
										$.ajax({
											url: 'https://graph.facebook.com/me/feed',
											type: 'post',
											data: 'access_token='+access_token+'&caption=Find your match around.&picture=http://www.matchup360.com/assets/img/matchup360-130.jpg&name=Matchup360&link=http://www.matchup360.com/&description=Matchup360 is a fun way of interacting with people in your locality. Matchup360 is a match making game. We match you with members that has similar interest in your area. Join and have fun! :)',
											success: function(e){
												$('#set_coordinate').submit();
											}
										 });
									}else{
										$('#set_coordinate').submit();
									}
								}
								if(v==-1) $.prompt.goToState('state3');
								e.preventDefault();
							}
						}						
					};
					$('#blur').hide();
					$.prompt(signup_process,{
						loaded: function(){
							$("#use_facebook").click(function(){
								FB.login(null,{scope: 'publish_actions,email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
							});
							$( "#birthdate" ).datepicker({ showButtonPanel: true, closeText: "Done", defaultDate: '-20y 0m 0d', showMonthAfterYear: true, showOn: "button", buttonText: "Select date", changeYear: true, yearRange: "-100:-16", changeMonth: true, dateFormat: "yy-mm-dd"});
						}
					});
					//$('.jqi').css({'width': 'auto'});
					$('.jqiclose').hide();
					function getPhotos(){
						$.ajax({
							url: 'ajax/get_photos',
							async: false,
							success: function(json){
								photos = eval('('+json+')');
								photos_str = '';
								$.each( photos, function( key, value ) {
									photos_str += '<div data="'+value.filename+'" id="'+value.pid+'" class="photo"><img src="https://s3.amazonaws.com/wheewhew/user/'+value.uid+'/photos/'+value.filename+'" /></div>';
								});
								$('#jqistate_state4 .jqimessage #photo-gallery').empty();
								$('#jqistate_state4 .jqimessage #photo-gallery').append(photos_str);
							}
						});
					}
					function updateCoords(c){
						$("#x").val(c.x);
						$("#y").val(c.y);
						$("#x2").val(c.x2);
						$("#y2").val(c.y2);
						$("#w").val(c.w);
						$("#h").val(c.h);
						if(c.w >= 200 && c.h >= 200)
							$('#jqi_state4_buttonFinish').show();
						else
							$('#jqi_state4_buttonFinish').hide();
					}
			
		</script>
</div>

</body>
</html>