<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to WhetWhew! Come in, find your soulmate.</title>

	<link rel="stylesheet" href="//wheewhew.com/assets/css/welcome_message.css" />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>
<body>
<div id="container">
	<div id="login-box">
		<div id="login-container">
		<?php echo validation_errors(); ?>
		<?php echo form_open('./welcome/login'); ?>
			<label for="email">Email:</label>
			<input type="text" id="email" name="email" value="<?php echo set_value('email'); ?>" />
			<label for="pwd">Password:</label>
			<input type="password" id="pwd" name="pwd" value="<?php echo set_value('pwd'); ?>" />
			<input type="submit" value="Login" />
		</form>
		</div>
	</div>
	<div id="signup-box">
		<a href="/signup">Sign up!</a>
	</div>
</div>
</body>
</html>