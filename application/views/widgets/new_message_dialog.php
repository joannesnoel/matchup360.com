<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">New Message</h3>
	</div>
	<div class="modal-body">
		<div class="message-recipient">To: <?php echo $firstname . ' ' . $lastname; ?></div>
		<div class="message-body">
			<textarea id="input-text" placeholder="Type your message..."></textarea>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button id="sendMessage" class="btn btn-primary">Send</button>
	</div>
	<script type="text/javascript">
		$('#myModal').modal('show');
		$('#myModal').on('hidden', function () {
			$(this).remove();
		});
		$('#myModal').on('shown', function () {
			$('#myModal #sendMessage').click(function() {
				sendMessage($('#input-text').val(), <?php echo $uid; ?>, '<?php echo $firstname . ' ' . $lastname; ?>', '<?php echo $profile_pic; ?>');
				$('#myModal').modal('hide');
			});
		});
	</script>
</div>