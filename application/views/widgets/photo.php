<div id="photo-wrapper">
	<div id="image-box">
		<img width="<?php echo $photo['width']; ?>" data-image-ref="<?php echo $photo['reference']?>" data-image-liked="<?php echo $is_liked?>" data-original-width="<?php echo $photo['width']; ?>" height="<?php echo $photo['height']; ?>" data-original-height="<?php echo $photo['height']; ?>" src="https://s3.amazonaws.com/wheewhew/user/<?php echo $photo['uid']; ?>/photos/<?php echo $photo['filename']; ?>" />
		<div id="image-hover-div">
			<span id="like_count"><?php echo $photo_likes['likes_string']?></span>
			<span id="like"></span>
			<div style="clear:both"></div>
		</div>
		
	</div>
	<div id="comment-box">
		<div id="comments">
			<ul>
			<?php
				$row_class = 'odd';
				foreach($comments as $row)
				{
					if($row['owner_id'] == $user[0]['uid'] || $row['commentor_id'] == $user[0]['uid'])
						$permission = 'data-delete-permission="true"';
					else
						$permission = '';
					echo '<li data-cid="'.$row['id'].'" class="'.$row_class.'" '.$permission.'><div class="commentor_pic"><img src="https://s3.amazonaws.com/wheewhew/user/'.$row['uid'].'/photos/'. $row['profile_pic'].'" width="35"></div><div class="commentside"><a id="commentor-name" href="./ajax/profileWidget/'.$row['uid'].'">' . $row['firstname'].' '.$row['lastname'].'</a> '.$row['comment'].'</div></li>';
					if($row_class == 'odd')
						$row_class = 'even';
					else
						$row_class = 'odd';
				}
			?>	
			</ul>
		</div>
		<div id="commentor">
			<div class="commentor_pic">
				<img id="pic" src="<?php echo $user[0]['profile_pic']; ?>" width="35" />
			</div>
			<form id="comment-form">
				<div id="textfield">
					<textarea name="comment" class="photo_comment"></textarea>
					<input type="hidden" name="reference" value="<?php echo $photo['reference']; ?>" />
					<input type="hidden" name="commentor_id" value="<?php echo $user[0]['uid']; ?>" />
				</div>
				<div id="post-button">
					<button id="post-comment-<?php echo $photo['reference']; ?>" class="post-comment" type="button">Post</button>	
				</div>
				<div style="clear:both"></div>
			</form>
		</div>
	</div>
	<div id="all-photo-likes" style="display: none">
		<button id="close-all-photo-likes">Close</button>
		<?php foreach($photo_likes['likes'] as $like){
				if($like['name'] != 'You'){?>
					<div class="liker_name" data-uid="<?php echo $like['uid']?>"><a><?php echo $like['name'] ?></a></div>
		<?php } 
		} ?>
	</div>
	<div style="clear:both"></div>
</div>

<script type="text/javascript">
	$('#post-comment-<?php echo $photo['reference']; ?>').unbind().click(postComment);
	$('#photo-wrapper').ready(function(){
		$('#close-all-photo-likes').click(function(){
			$('#all-photo-likes').slideUp('fast');
			$('#comment-box').slideDown('fast');
		});
		$('.liker_name').click(function(){
			var uid = $(this).attr('data-uid');
			loadPage('view_profile/'+uid);
		});
	});
	function postComment(){
		//$.colorbox.resize();
		var postData = $("#comment-form").serialize();
		$('textarea.photo_comment').val('');
		
		$.ajax({
			url: '../../ajax/addPhotoComment',
			type:'post',
			data: postData,
			success: function(data){
				if(data.success){
					$('#comment-box #comments ul').append('<li data-cid="'+data.id+'" data-delete-permission="true"><div class="commentor_pic"><img src="'+my_profile_pic+'" width="35"></div><div class="commentside"><a id="commentor-name" href="./ajax/profileWidget/'+uid+'"> '+my_fullname+'</a> '+data.comment+'</div></li>');
					
					$('#comment-box #comments').scrollTo($('#comment-box #comments ul li').last());
					
					attachHover();
					var stanza = $msg({'to': data.xmpp_user+'@matchup360.com/default', 'type': 'notification', 'notification_type': 'comment', 'from_fullname':my_fullname, 'from_photo':data.photo_url, 'notification_string': data.notification_string});
						connection.send(stanza);
				}else{
					alert('there was an error.');
				}
			}
		});
	}	
	attachHover();
	function attachHover(){
		$('#comment-box li[data-delete-permission=true]').unbind().hover(function(){
							$(this).find('.commentside').after('<i class="delete-comment-icon"></i>');
							$('.delete-comment-icon').click(deleteComment);
						},
						function(){
							$('.delete-comment-icon').remove();
						});
	}
	function deleteComment(){
		comment = $(this).parent();
		var cid = $(this).parent().attr('data-cid');
		$.ajax({
			url: '../../ajax/deleteComment/'+cid,
			success: function(){
				comment.remove();
			}
		});
	}
</script>