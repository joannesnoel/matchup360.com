<div class="entry popover-parent" data-post-id="<?php echo $post_id; ?>">
	<?php if($uid == $this->session->userdata('logged_uid') OR $this->session->userdata('logged_uid') == 400){ ?>
	<i class="remove">X</i>
	<?php } ?>
	<div class="thumbnail square_40 profile_link">
			<img src="<?php echo $profile_pic; ?>" alt="<?php echo $firstname .' '. $lastname; ?>">
	</div>
	<div class="message">
		<div class="head"><div class="profile_link"><?php echo $firstname .' '. $lastname; ?></div></div>
		<div class="body"><?php echo $message; ?></div>
		<div class="foot"><i class="icon-screenshot"></i> <?php echo $location; ?></div>
	</div>
	<div class="gallery">
<?php foreach($photos as $photo){ ?>
		<a href="<?php echo $photo['largest']['url']; ?>" class="thumbnail square_130">
			<img src="<?php echo $photo['thumbnail']['url']; ?>"/>
		</a>
<?php	} ?>
	</div>
	<div style="clear:both"></div>
	<div class="comments">
<?php for($i=count($comments)-1;$i>=0;$i--){ 
		 $comment = $comments[$i];
?>
		<div class="comment popover-parent" data-post-id="<?php echo $comment['post_id']; ?>">
			<?php if($comment['uid'] == $this->session->userdata('logged_uid') OR $this->session->userdata('logged_uid') == 400){ ?>
			<i class="remove">X</i>
			<?php } ?>
			<div class="square_30">
				<img src="<?php echo $comment['profile_pic']; ?>" alt="<?php echo $comment['firstname'] .' '. $comment['lastname']; ?>">
			</div>
			<div class="fullname"><?php echo $comment['firstname'] .' '. $comment['lastname']; ?></div>
			<div class="comment_text">
				<?php echo $comment['message']; ?>
			</div>
			<div style="clear:both"></div>
		</div>
<?php } ?>
	</div>
	<div class="write_comment">
		<textarea name="comment_text" class="input_comment" data-post-id="<?php echo $post_id; ?>" placeholder="Write a comment..."></textarea>
		<script type="text/javascript">
			$('.input_comment[data-post-id="<?php echo $post_id; ?>"]').keypress(function(event) {
				textarea = $(this);
				if ( event.which == 13 ) {
					event.preventDefault();
					if($('input[name="lat"]').val() == '' || $('input[name="lng"]').val() == '' || $('input[name="formatted_address"]').val() == ''){
						$('#post_location').focus();
					}else{
						message = $(this).val();
						if(message != ''){
							textarea.attr('readOnly',true);
							$.ajax({
								url: window.location.protocol + '//' + window.location.hostname + '/rest/post',
								type: 'post',
								data: 'message='+message+'&parent_id='+$(this).attr('data-post-id'),
								dataType: 'json',
								success: function(response){
									if(response.redirect)
										window.location = response.redirect;
									if(response.status == 'success'){
										textarea.removeAttr('readOnly');
										textarea.val('');
									}
								}
							});
						}
					}
				}
			});
			$.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/widgets/hover_profile/<?php echo $xmpp_user; ?>',
				async: false,
				success: function(response){
					$('.entry[data-post-id="<?php echo $post_id; ?>"] .profile_link')
						.popover({trigger:'hover',placement:'right',html:true,title:'<?php echo $firstname .' '. $lastname; ?>',content:response})
						.click(function(){
							loadBrowser('page/member/<?php echo $xmpp_user; ?>');
							if ( history.pushState ) 
								history.pushState( null, null, '/<?php echo $xmpp_user; ?>' );
							_gaq.push(['_trackPageview', '/<?php echo $xmpp_user; ?>']);
						});
				}
			});
			$('.entry[data-post-id="<?php echo $post_id; ?>"] .gallery .thumbnail').colorbox({
																						rel:'gallery-<?php echo $post_id; ?>',
																						scalePhotos: true,
																						maxHeight: $(window).height() * 0.75
																					});
			$('.entry[data-post-id="<?php echo $post_id; ?>"] .remove')
				.popover({html:true,placement:'left',title:'Delete this post?',content:'<button id="confirmed-delete" class="btn btn-danger btn-small"><i class="icon-white icon-remove"></i> Yes, delete</button> <button id="close-popover" class="btn btn-small">Cancel</button>'});
		</script>
	</div>
</div>