<?php 
	$interested_in_array = array();
	if(!empty($interested_in['men']))
		$interested_in_array[] = 'Men';
	if(!empty($interested_in['women']))
		$interested_in_array[] = 'Women';
	switch($sex){
		case 'f': $sex = 'Female'; break;
		case 'm': $sex = 'Male'; break;
	}
?>
<div class="mini_profile_wrapper">
	<img src="<?php echo $profile_pic; ?>" width="80" height="80"/>
	<div class="info"><strong>Sex:</strong> <?php echo $sex; ?></div>
	<div class="info"><strong>Age:</strong> <?php echo $age; ?></div>
	<div class="info"><strong>Interested in:</strong> <?php echo implode(',',$interested_in_array); ?></div>
	<div class="info"><strong>Location: </strong><?php echo $city; ?>, <?php echo $state; ?>, <?php echo $country; ?></div>
	<div style="clear:both"/>
</div>