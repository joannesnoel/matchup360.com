<div id="get_credits">
<h3>Get more coins</h3>
<p>You will receive <b>10</b> credits for each person that you invite and another <b>40</b> when they signup.</p>
<p align="center"><b>Use</b></p>
<div id="networks">
<button id="fb_inviter">Facebook</button>
<button id="gmail_inviter">Gmail</button>
</div>
</div>
<script type="text/javascript">
// $('#fb_inviter').click(function(){
	 // FB.login(function(response) {
	   // if (response.authResponse) {
		  // access_token =   FB.getAuthResponse()['accessToken'];
		  // $.get(window.location.protocol + '//' + window.location.hostname + '/ajax/updateExtendedAccessToken'); 
		 // FB.api('/me', function(response) {
		   // $.colorbox({href:"/widgets/invite_facebook", width:600, height:400});
		 // });
	   // }
	// });
// });
$('#fb_inviter').click(function(){
	FB.ui({
	  method: 'apprequests',
	  message: 'Hey! I am inviting you to Matchup360',
	  data: '{"fbinvite":'+uid+'}'
	},function(response){
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax/fb_invite_multiple',
			type: 'post',
			data: 'fb_userids='+JSON.stringify(response.to),
			success: function(invite_response){
				$.prompt('<p>'+invite_response.invited+' people invited. '+invite_response.duplicated+' invites ignored as you have invited them in the past.</p>');
				$('#matchup_coins i').html(parseInt($('#matchup_coins i').html())+invite_response.invited*10);
				return true;
			}
		});
	});
});
$('#gmail_inviter').click(function(){
	$.colorbox({
		href:'/widgets/invite_gmail', 
		width: 560, 
		height: 485
	})
});
</script>