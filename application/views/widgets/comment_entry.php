<div class="comment popover-parent" data-post-id="<?php echo $post_id; ?>">
	<?php if($uid == $this->session->userdata('logged_uid')){ ?>
	<i class="remove">X</i>
	<?php } ?>
	<div class="square_30">
		<img src="<?php echo $profile_pic; ?>" alt="<?php echo $firstname .' '. $lastname; ?>">
	</div>
	<div class="fullname"><?php echo $firstname .' '. $lastname; ?></div>
	<div class="comment_text"><?php echo $message; ?></div>
	<div style="clear:both"></div>
</div>