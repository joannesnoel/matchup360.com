<div id="inviterBox">
<h3>Invite Facebook friends</h3>
<div id="friends-list">
<div id="invite-friends">
	<ul>
	</ul>
</div>
</div>
</div>
<script type="text/javascript">
	FB.api('/me/friends', {
	  fields: 'id,first_name,last_name,name,birthday,gender,location,link,installed'
	}, function(response){
	  $.ajax({
		url: window.location.protocol + '//' + window.location.hostname + '/ajax/get_invited_fb_ids',
		type:'post',
		data: 'from_uid='+uid+'&type=facebook',
		success: function(fb_ids){
			for(var i=0;i<response.data.length; i++){
				friend = response.data[i];
				if(!friend.installed && $.inArray(friend.id,fb_ids) < 0)
					$('#friends-list ul').append('<li><span>'+friend.first_name+' '+friend.last_name+'</span> <button class="invite" data-fbuid="'+friend.id+'">Invite</button></li>');
			}
			$('button.invite').click(function(){
			fb_uid = $(this).attr('data-fbuid');
			to_array = $("#invite-friends input:checkbox:checked").map(function(){
																		  return $(this).val();
																		}).get();
			var obj = {
			  method: 'send',
			  to: fb_uid,
			  link: 'http://www.matchup360.com/?fbinvite='+uid,
			  picture: 'https://www.matchup360.com/assets/img/matchup360-130.jpg',
			  name: 'Matchup360',
			  caption: 'Find your match around',
			  description: 'Matchup360 is a fun way of interacting with people in a specified location. You simply set your location, adjust your radar and mingle with people! Aside from that, we match you with people that may share the same interest with you.'
			};
			function callback(response) {
				if(response){
					$.ajax({
						url: window.location.protocol + '//' + window.location.hostname + '/ajax/fb_invite',
						type: 'post',
						data: 'fb_uid='+fb_uid,
						success: function(response){
							$.colorbox.close();
							if(!response.success)
								$.prompt(response.message);
							else
								$('#matchup_coins i').html(parseInt($('#matchup_coins i').html())+10);
						}
					});
				}
			}
			FB.ui(obj, callback);
		});
		}
	  });
	});
</script>