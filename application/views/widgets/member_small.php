<?php 
	$interested_in_array = array();
	if(!empty($interested_in['men']))
		$interested_in_array[] = 'Men';
	if(!empty($interested_in['women']))
		$interested_in_array[] = 'Women';
	switch($sex){
		case 'f': $sex = 'Female'; break;
		case 'm': $sex = 'Male'; break;
	}
?>
<div class="member mini_profile_wrapper" data-uid="<?php echo $uid; ?>">
	<div class="thumbnail square_90 profile_link">
		<img src="<?php echo $profile_pic; ?>"/>
	</div>
	<div class="info profile_link"><strong><?php echo $firstname . ' ' . $lastname;?></strong> - Age <?php echo $age; ?></div>
	<div class="info"><strong>Sex:</strong> <?php echo $sex; ?></div>
	<div class="info"><strong>Interested in:</strong> <?php echo implode(',',$interested_in_array); ?></div>
	<div class="info"><strong>Location: </strong><?php echo $city; ?>, <?php echo $state; ?>, <?php echo $country; ?></div>
	<div style="clear:both"></div>
</div>
<script type="text/javascript">
	$('.member[data-uid="<?php echo $uid; ?>"] .profile_link').click(function(){
		loadBrowser('page/member/<?php echo $xmpp_user; ?>');
		if ( history.pushState ) 
			history.pushState( null, null, '/<?php echo $xmpp_user; ?>' );
		_gaq.push(['_trackPageview', '/<?php echo $xmpp_user; ?>']);
	});
</script>