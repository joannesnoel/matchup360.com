<div id="member-wrapper">
<div id="random-member" data-uid="">
	<div id="meet">
		<p></p>
		<div>
			<button class="meet-btn" name="encounter" value="1"><span>Yes</span></button>
			<button class="meet-btn" id="maybe-btn"><span>Maybe</span></button>
			<button class="meet-btn" name="encounter" value="0"><span>No</span></button>
		</div>
	</div>
	<div id="user-info-container" style="position: relative">
		<div id="image-wrapper">
			
			</div>
		</div>
	</div>
	<div id="gallery-wrapper">
		<div id="gallery">
		</div>
	</div>
</div>
<script type="text/javascript">
	function loadMember(){
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/roulette',
			success: function(response){
				if(response.user){
					$('#random-member').attr('data-uid',response.user.uid);
					if(response.user.sex == 'm')
						$('#meet p').html('Does <span id="member-gender">he interest you?</span>');
					else
						$('#meet p').html('Does <span id="member-gender">she interest you?</span>');
					$('#gallery').empty();
					for(var i=0; i< response.user_photos.length; i++){
						$('#gallery').append('<div class="photo-wrapper" data-largest="'+response.user_photos[i].largest.filename+'" data-largest-width="'+response.user_photos[i].largest.width+'" data-largest-height="'+response.user_photos[i].largest.height+'">\
												<img src="<?php echo config_item('s3_bucket_url'); ?>'+response.user.uid+'/photos/'+response.user_photos[i].thumbnail.filename+'" />\
											</div>');
					}
					$('#gallery .photo-wrapper').unbind().click(function(){
						var image_largest = $(this).attr('data-largest');
						var image_largest_width = parseInt($(this).attr('data-largest-width'));
						var image_largest_height = parseInt( $(this).attr('data-largest-height'));
						if(image_largest_height > ($(window).height() - 300)){
							ratio = (($(window).height() - 300)/image_largest_height);
							image_largest_height = image_largest_height*ratio;
							image_largest_width  = image_largest_width*ratio;
						}
						if(image_largest_width < 300)
							cBoxWidth = 300;
						else
							cBoxWidth = image_largest_width;
						$.colorbox.resize({innerWidth:cBoxWidth, innerHeight:image_largest_height+170});
						$('#gallery-wrapper').css('width',image_largest_width);
						$('#gallery-wrapper #gallery').width(80*$('#gallery .photo-wrapper').length);
						$('#image-wrapper').empty();
						$('#image-wrapper').html('<div id="image">\
													<img width="'+image_largest_width+'" height="'+image_largest_height+'" src="<?php echo config_item('s3_bucket_url'); ?>'+response.user.uid+'/photos/'+image_largest+'"/>\
												</div>');
						$('#image-wrapper img').unbind().bind('load', function(){
							$(this).hover(
								function(){
									$('#image').css('width', $(this).width());
									$(this).parent().append('<div id="profile-info" class="info-box">\
																<div style="margin-bottom: 10px"><h1 style="color: white; display: inline-block; font-size: 20px">'+response.user.firstname+' '+response.user.lastname+'</h1>\
																<span>-- Age '+response.user.age+'</span></div>\
																<div style="word-wrap: break-word;">'+response.user.city+', '+response.user.state+', '+response.user.country+'</div>\
															</div>');},
								function(){
									$('#profile-info').remove();
								});
						});
					});
					$('#gallery .photo-wrapper')[0].click();
				}else{
					$.colorbox({scrolling:false,href: window.location.protocol + '//' + window.location.hostname + '/widgets/roulette_done', overlayClose: false, closeButton: false});
				}
			}
		});
	}
	$('.meet-btn').unbind().click(function(){
		 var btn_id = $(this);
		 var random_user_uid = $('#random-member').attr('data-uid');
		 if(btn_id.attr('id') == 'maybe-btn'){
			loadMember();
		}else{
			 $.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/ajax/addUserEncounter',
				data: 'random_user_uid='+random_user_uid+'&encounter='+btn_id.val(),
				type: 'post',
				success: function(data){
					if(btn_id.val() == 1){
						var stanza = $msg({'to': data.xmpp_user+'@matchup360.com', 'type': 'notification', 'notification_type': 'interested', 'notification_string': data.notification_string});
						connection.send(stanza);
					}
					loadMember();
				}
			 });
		 }
	});
	loadMember();
</script>
</div>