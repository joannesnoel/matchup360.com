<div id="my_photos_widget">
<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.Jcrop.min.js"></script>
<link href="//matchup360.com/assets/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
<h1>Choose a Primary Picture</h1>
<div style="max-width:800px; height: 126px;overflow-x: auto;">
<div id="thumbnails">
<?php
	foreach($photos as $photo){
?>
		<div class="thumbnail_photo" data-photo="<?php echo config_item('s3_bucket_url').$photo['largest']['uid'].'/photos/'.$photo['largest']['filename']?>" data-photo-width="<?php echo $photo['largest']['width'] ?>" data-photo-height="<?php echo $photo['largest']['height'] ?>"><img src="<?php echo config_item('s3_bucket_url').$photo['thumbnail']['uid'].'/photos/'.$photo['thumbnail']['filename']?>" width="100" height="100" /></div>
<?php
	}
?>
</div>
</div>
<div class="photo_preview">
	<img id="photo_cropper" />
	<button id="crop_button" style="display:none">Set as Primary</button>
</div>
<form action='/ajax/set_profile_coordinates' method="post" id='set_coordinate'>
	<input type="hidden" name="orig_photo_url" id="orig_photo_url" />
	<input type="hidden" name="x" id="x" />
	<input type="hidden" name="y" id="y" />
	<input type="hidden" name="x2" id="x2" />
	<input type="hidden" name="x2" id="x2" />
	<input type="hidden" name="w" id="w" />
	<input type="hidden" name="h" id="h" />
	<input type="hidden" name="requester" value="canvas"/>
	<input type="hidden" name="action" value="crop" />
</form>
</div>
<script type="text/javascript">
$('#thumbnails').css('width',106*$('.thumbnail_photo').length);
$('.thumbnail_photo').click(function(){
	photo = $(this).attr('data-photo');
	width = $(this).attr('data-photo-width');
	height = $(this).attr('data-photo-height');
	$('#photo_cropper').attr('src',photo);
	$('#my_photos_widget').css('width',width);
	$('#my_photos_widget').css('height',height);
	$('.photo_preview').attr('width',width);
	$('.photo_preview').attr('height',height);
	$.colorbox.resize({'innerWidth':$('#my_photos_widget').width(),'innerHeight':$('#my_photos_widget').height()+$('#thumbnails').height()+50});
	$('#crop_button').show();
	$('#crop_button').unbind().click(function(){
		$('#set_coordinate').submit();
	});
	$('#orig_photo_url').val(photo);
	$('#photo_cropper').Jcrop({
		onSelect: updateCoords,
		onRelease: updateCoords,
		setSelect:   [ 0, 0, 400, 400 ],
		aspectRatio: 1/1,
		minSize: [400,400]
	});
	function updateCoords(c){
		$("#x").val(c.x);
		$("#y").val(c.y);
		$("#x2").val(c.x2);
		$("#y2").val(c.y2);
		$("#w").val(c.w);
		$("#h").val(c.h);
	}
});
</script>