<div class="widget_wrapper">
<h1>Choose a password</h1>
<p>We need to secure your account with a password of your choice.</p>
<form id="quick_register_form">
<p id="email_line" style="display:none"><span class="form_label">Email:</span><input type="text" name="email" /></p>
<p id="confirm_email_line" style="display:none"><span class="form_label">Confirm Email:</span><input type="text" name="confirm_email" /></p>
<p><span class="form_label">Password:</span><input type="password" name="password" /></p>
<p><span class="form_label">Confirm Password:</span><input type="password" name="confirm_password" /></p>
</form>
<button id="quick_register">Register</button>
</div>
<script type="text/javascript">
	if(!me.email){
		$('#email_line').show();
		$('#confirm_email_line').show();
	}
	$('#quick_register').click(function(){
		if(!me.email){
			email = $('#email_line input').val();
			confirm_email = $('#confirm_email_line input').val();
		}
		if($('input[name="password"]').val().length < 8){
			$.prompt('<p>Password needs to be at least 8 characters.</p>',{submit: function(){$('input[name="password"]').focus();}});
		}else if($('input[name="password"]').val() != $('input[name="confirm_password"]').val()){
			$.prompt('<p>Both password fields must match.</p>',{submit: function(){$('input[name="password"]').focus();}});
		}else if(!me.email && !validateEmail(email)){
			$.prompt('<p>Please provide valid email.</p>',{submit: function(){$('input[name="email"]').focus();}});
		}else if(!me.email && email!=confirm_email){
			$.prompt('<p>Both email fields must match.</p>',{submit: function(){$('input[name="confirm_email"]').focus();}});
		}else{
			if(!me.email)
				me.email = $('#email_line input').val();
			me.password = $('input[name="confirm_password"]').val();
			$.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/quick_register',
				type: 'post',
				data: 'me='+encodeURIComponent(JSON.stringify(me)),
				beforeSend: function(){
					$('#quick_register').attr('disabled','disabled');
					$('#quick_register').html('Please wait...');
				},
				success: function(response){
					if(response.result){
						check_profile_photo(response.uid);
					}
				}
			});
		}
	});
	function validateEmail(email) { 
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA	-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	} 
</script>