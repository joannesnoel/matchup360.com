<div id="my_appearance_box">
	<form id="myappearance-form" name="myappearance-form" method="post">
			
		<label class="leftside" for="ethnicity"> My ethnicity is </label>
			<div class="select-style">
				<select name="ethnicity">
					<option value="later"> I'll tell you later </option>
					<?php
						foreach($ethnicities as $ethnicity){
					?>
						<option value="<?php echo $ethnicity['id'] ?>"><?php echo $ethnicity['name'] ?></option>
					<?php
						}
					?>
				</select>
			</div>
		<label class="leftside" for="nationality"> My nationality is </label>
			<div class="select-style">
				<select name="nationality">
					<option value="">I'll tell you later</option>
					<?php
						foreach($nationalities as $nationality){
					?>
						<option value="<?php echo $nationality['id'] ?>"><?php echo $nationality['name'] ?></option>
					<?php
						}
					?>
				</select>
			</div>
		<label class="leftside" for="height"> My height is </label>
			<div class="select-style">
				<select name="height">
					<option value="">I'll tell you later</option>
					<option value="150">4' 11" (150 cm) or shorter</option>
					<option value="152">5' 0" (152 cm)</option>
					<option value="155">5' 1" (155 cm)</option>
					<option value="157">5' 2" (157 cm)</option>
					<option value="160">5' 3" (160 cm)</option>
					<option value="163">5' 4" (163 cm)</option>
					<option value="165">5' 5" (165 cm)</option>
					<option value="168">5' 6" (168 cm)</option>
					<option value="170">5' 7" (170 cm)</option>
					<option value="173">5' 8" (173 cm)</option>
					<option value="175">5' 9" (175 cm)</option>
					<option value="178">5' 10" (178 cm)</option>
					<option value="180">5' 11" (180 cm)</option>
					<option value="183">6' 0" (183 cm)</option>
					<option value="185">6' 1" (185 cm)</option>
					<option value="188">6' 2" (188 cm)</option>
					<option value="191">6' 3" (191 cm)</option>
					<option value="193">6' 4" (193 cm)</option>
					<option value="196">6' 5" (196 cm)</option>
					<option value="198">6' 6" (198 cm)</option>
					<option value="200">6' 7" (200 cm)</option>
					<option value="203">6' 8" (203 cm)</option>
					<option value="206">6' 9" (206 cm)</option>
					<option value="208">6' 10" (208 cm)</option>
					<option value="211">6' 11" (211 cm)</option>
					<option value="213">7' 0" (213 cm) or taller</option>
				</select>
			</div>
		<label class="leftside" for="eye-color"> My eye color is </label>
			<div class="select-style">
				<select name="eye_color">
					<option value="">I'll tell you later</option>
					<?php
						foreach($eye_colors as $color){
					?>
						<option value="<?php echo $color['id'] ?>"><?php echo $color['name'] ?></option>
					<?php
						}
					?>
				</select>
			</div>
		<label class="leftside" for="hair-color"> My hair color is </label>
			<div class="select-style">
				<select name="hair_color">
					<option value="">I'll tell you later</option>
					<?php
						foreach($hair_colors as $color){
					?>
						<option value="<?php echo $color['id'] ?>"><?php echo $color['name'] ?></option>
					<?php
						}
					?>
				</select>
			</div>
			<div class="box-buttons">
				<button type="button" value="update" id="update-appearance-box"> Update </button>
			</div>
	</form>
</div>
<script type="text/javascript">
	$('#update-appearance-box').click(updateAppearanceData);
	
	function updateAppearanceData(){
				var postData = $("#myappearance-form").serialize();
				$.ajax({
					url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/update_my_appearance',
					type:'post',
					data:postData,
					success: function(response){
						if(response.success)
							runApp();
					}
				});
			} 
</script>