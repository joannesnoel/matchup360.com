<div id="gmail_wrapper">
	<h3>Invite Gmail friends</h3>
	<button id="loginGmail" onClick="checkAuth();">Import Contacts</button>
	<form id="email_invite" name="email_invite">
	<div id="gmail_contacts">
	</div>
	<div id="invite-msg" style="display:none">
		<p style="font-size: 14px">Optional Message: </p>
		<textarea name="custom-message"></textarea>
		<div style="text-align:right">
			<button id="email_invite_button" type="button">Invite</button>
		</div>
	</div>
	</form>
</div>
<script>
	clientId = '821178776978.apps.googleusercontent.com';
	apiKey = 'AIzaSyBnaUsYRIH2mUSSGyqVNcGkRShS_dwkkq4';
	scopes = 'https://www.google.com/m8/feeds';
	contacts = [];
	function handleClientLoad() {
	  gapi.client.setApiKey(apiKey);
	  window.setTimeout(checkAuth,1);
	}
	function checkAuth() {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
	  var authorizeButton = document.getElementById('loginGmail');
	  if (authResult && !authResult.error) {
		$(authorizeButton).hide();
		$('#invite-msg').show();
		makeApiCall();
	  } else {
		$(authorizeButton).show();
		authorizeButton.onclick = handleAuthClick;
	  }
	}

	function handleAuthClick(event) {
	  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
	  return false;
	}
	function makeApiCall(){
		var authParams = gapi.auth.getToken() // from Google oAuth
		authParams.alt = 'json';
		$.ajax({
		  url: 'https://www.google.com/m8/feeds/contacts/default/full',
		  dataType: 'jsonp',
		  data: authParams,
		  success: function(data) {
			entries = data.feed.entry;
			for(var i=0; i<entries.length; i++){
				entry = entries[i];
				title = entry.title.$t;
				if(title.indexOf("support") !== -1)
					continue;
				for(var x=0; x<entry.gd$email.length; x++){
					email = entry.gd$email[x].address;
					if(email.indexOf("support") !== -1)
						continue;
					if(title == '')
						title = email;
					$('#gmail_contacts').append('<div class="contact"><input type="checkbox" name="contacts[]" value="'+email+'" /><div class="title"> '+title+'</div></div>');
				}
			}
		  }
		});
	}
	var handleContactsFeed = function (result){
		var entries = result.feed.entry;
		for (var i = 0; i < entries.length; i++)
		{
			var contactEntry = entries[i];
			var emailAddresses = contactEntry.getEmailAddresses();

			for (var j = 0; j < emailAddresses.length; j++)
			{
				var emailAddress = emailAddresses[j].getAddress();
				alert(emailAddress);
			}
		}
	}

	function handleError(e){
		alert("There was an error!" + (e.cause ? e.cause.statusText : e.message));
	}
	$('#email_invite_button').click(function(){
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax/email_invite',
			type: 'post',
			data: $('#email_invite').serialize(),
			success: function(response){
				$('#matchup_coins i').html(parseInt($('#matchup_coins i').html())+10*parseInt(response.sent));
			}
		});
	});
</script>
<script src="https://apis.google.com/js/client.js?onload=checkAuth"></script>
