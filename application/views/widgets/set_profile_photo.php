<div id="image-cropper" style="overflow: hidden">
	<img id="jcrop-target" src="<?php echo 'https://s3.amazonaws.com/wheewhew/user/'.$user.'/photos/'.$photo[0]['filename']?>" height="<?php echo $photo[0]['height']; ?>" width="<?php echo $photo[0]['width']; ?>"/>
</div>
<?php echo form_open('/ajax/set_profile_coordinates', array('id' => 'set_coordinate')); ?>
	<input type="hidden" name="orig_photo_url" id="orig_photo_url" />
	<input type="hidden" name="x" id="x" />
	<input type="hidden" name="y" id="y" />
	<input type="hidden" name="x2" id="x2" />
	<input type="hidden" name="x2" id="x2" />
	<input type="hidden" name="w" id="w" />
	<input type="hidden" name="h" id="h" />
	<input type="hidden" name="requester" value="my_photos"/>
	<input type="hidden" name="action" value="crop" />
</form>
<button id="set-profile-button"><span>Set as Profile</span></button>
<div style="clear: both"></div>
<script type="text/javascript">
	$('#set-profile-button').click(function(){
		$('#set_coordinate').submit();
	});
</script>