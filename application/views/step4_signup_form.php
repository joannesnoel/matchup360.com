<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Set Profile Photo</title>

        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="//wheewhew.com/assets/css/signup_step3.css" />

        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		
		<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
		<style>
		.photo {
			float: left;
			margin: 6px 3px;
			width: 300px;
			text-align: center;
			background: black;
		}
		</style>
		<script src="//wheewhew.com/assets/js/jquery.Jcrop.js"></script>
		<link rel="stylesheet" href="//wheewhew.com/assets/css/jquery.Jcrop.css" type="text/css" />
    </head>

    <body>
		<div id="image-cropper">
		</div>
		<div id="photo-gallery">
<?php
				foreach($photos as $photo){
?>
			<div data="<?php echo $photo['unique_code']; ?>" id="<?php echo $photo['pid'] ?>" class="photo"><img src="https://s3.amazonaws.com/wheewhew/user/<?php echo $this->session->userdata('logged_uid'); ?>/photos/<?php echo $photo['unique_code'] ?>_300_cropped.jpg" /></div>
<?php
				}
?>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".photo").click(function(){
					var unique_code = $(this).attr('data');
					$('#image-cropper').html('<img id="jcrop-target" src="https://s3.amazonaws.com/wheewhew/user/<?php echo $this->session->userdata('logged_uid'); ?>/photos/'+unique_code+'_600.jpg" />');
					$('#orig_photo_url').val('https://s3.amazonaws.com/wheewhew/user/<?php echo $this->session->userdata('logged_uid'); ?>/photos/'+unique_code+'_600.jpg');
					$('#jcrop-target').Jcrop({
						onSelect: updateCoords,
						onChange: updateCoords,
						aspectRatio: 1/1,
						minSize: [150,150]
					});
				})
			});
			function updateCoords(c){
				$("#x").val(c.x);
				$("#y").val(c.y);
				$("#x2").val(c.x2);
				$("#y2").val(c.y2);
				$("#w").val(c.w);
				$("#h").val(c.h);

				if(c.w >= 150 && c.h >= 150)
					$('#update-btn').show();
				else
					$('#update-btn').hide();
			}
		</script>
		<?php echo form_open(); ?>
			<input type="hidden" name="orig_photo_url" id="orig_photo_url" />
			<input type="hidden" name="x" id="x" />
			<input type="hidden" name="y" id="y" />
			<input type="hidden" name="x2" id="x2" />
			<input type="hidden" name="x2" id="x2" />
			<input type="hidden" name="w" id="w" />
			<input type="hidden" name="h" id="h" />
			<input type="hidden" name="action" value="crop" />
			<button id="update-btn" style="display:none">Update</button>
		</form>
    </body>
</html>