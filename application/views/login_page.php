<!DOCTYPE html>
<html>  
            
<title>Matchup360</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="title" content="Matchup360" />
<meta itemprop="name" content="Matchup360" />
<meta name="description" content="Matchup360 is a fun way of interacting with people in a specified location. You simply set your location, adjust your radar and mingle with people! Aside from that, we match you with people that may share the same interest with you." />
<meta itemprop="description" content="Matchup360 is a fun way of interacting with people in a specified location. You simply set your location, adjust your radar and mingle with people! Aside from that, we match you with people that may share the same interest with you." /> 
<meta name="keywords" content="local girls, local men, local ladies, asian, filipina, online date, meet up, online match making" />

	<link rel="stylesheet" href="//www.matchup360.com/assets/css/jquery-impromptu.signup.css" />
	<link href='//fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
	<link href="//matchup360.com/assets/css/uploadify.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="//matchup360.com/assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery-impromptu.js"></script>
	<script src="//www.matchup360.com/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.uploadify.min.js"></script>
	<script src="//www.matchup360.com/assets/js/jquery.filedrop.js"></script>
	<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
	<script src="//www.matchup360.com/assets/js/jquery.geocomplete.min.js"></script>
	<script src="//www.matchup360.com/assets/js/jquery.Jcrop.js"></script>
	<link rel="stylesheet" href="//www.matchup360.com/assets/css/jquery.Jcrop.css" type="text/css" />
	<link rel="stylesheet" href="//www.matchup360.com/assets/css/redmond/jquery-ui-1.10.3.custom.min.css" />
	<link rel="stylesheet" href="//www.matchup360.com/assets/css/welcome.css" />
	<link rel="shortcut icon" href="//www.matchup360.com/assets/img/favicon.ico" type="image/x-icon" />
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-42434331-1', 'matchup360.com');
	  ga('send', 'pageview');

	</script>
	<script type="text/javascript">
		var geo_options = {
			  types: ['(cities)']
			 };
		var photos = [];
		var access_token = false;
	</script>
	<script type="text/javascript">
		var pic_per_row;
		var color_palette = ['#58ba77','#bed267','#91d152','#eaae62','#eb7055','#dd7499','#b166a0'];
		$(document).ready(function(){
			$('#container2').css('background-color',color_palette[Math.floor(Math.random()*color_palette.length)]);
			width = $(window).width();
			pic_per_row = Math.floor(width / 220);
			$('#page_container').width(220*pic_per_row);
			$('#windowSize').css('height',$(window).height());
			excess = $('.photocollage img').length % pic_per_row;
			for(var i=0; i<excess; i++)
				$('.photocollage').last().hide();
			$('#email').keydown(function(event){
				if(event.keyCode == 9){
					event.preventDefault();
					$('#pwd').focus();
				}

			});
			/*$('#login-box .login input').focusout(function(){
				value = $(this).val();
				if(value != '')
					$(this).parent().find('label').hide();
				else
					$(this).parent().find('label').show();
			});*/
		});
	</script>
	<script type="text/javascript">
$(document).ready(function(){
				var photos_str = '';
				var signup_process = {
						state0: {
							title: 'Sign Up <button id="use_facebook"><i></i>Signup with Facebook</button><div style="clear: both"></div>',
							html:'<form id="form1"  style="width: 435px">\
							<input type="hidden" id="access_token" name="fb_access_token" value="" />\
						<div class="row"><label for="firstname">Firstname:</label>\
						<input class="textfield" type="text" id="firstname" name="firstname" value="" /><i class="field_error" id="firstname_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="lastname">Lastname:</label>\
						<input class="textfield" type="text" id="lastname" name="lastname" value="" /><i class="field_error" id="lastname_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="email">Email:</label>\
						<input class="textfield" type="text" id="email" name="email" value="" /><i class="field_error" id="email_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="email-verify">Confirm Email:</label>\
						<input class="textfield" type="text" id="email-verify" name="email-verify" value="" /><i class="field_error" id="email2_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="pwd">Password:</label>\
						<input class="textfield" type="password" id="pwd" name="pwd" value="" /><i class="field_error" id="password_error"></i>\
						<div style="clear: both"></div></div>\
						<div class="row"><label for="pwd">Confirm Password:</label>\
						<input class="textfield" type="password" id="pwd-verify" name="pwd-verify" value="" /><i class="field_error" id="password_verify_error"></i>\
						<div style="clear: both"></div></div>\
						</form>',
							buttons: { Next: 1 },
							focus: "input[name='firstname']",
							submit:function(e,v,m,f){
								e.preventDefault();
								$.ajax({
									url: window.location.protocol + '//' + window.location.hostname + '/ajax/create_user',
									type: 'post',
									data: $('#form1').serialize(),
									success: function(data){
										$('.field_error').css('display','none').attr('title','');
										if(data.success == false){
											$('.jqi').css({'width': '495px','margin-left':'-260px'});
											if(data.error.firstname != '')
												$('#firstname_error').css('display','inline-block').attr('title',data.error.firstname);
											if(data.error.lastname != '')
												$('#lastname_error').css('display','inline-block').attr('title',data.error.lastname);
											if(data.error.email != '')
												$('#email_error').css('display','inline-block').attr('title',data.error.email);
											if(data.error.email2 != '')
												$('#email2_error').css('display','inline-block').attr('title',data.error.email2);
											if(data.error.password != '')
												$('#password_error').css('display','inline-block').attr('title',data.error.password);

										}else{
											$('.jqiclose').hide();
											$.prompt.goToState('state1');
										}
									}
								});
							}
						},
						state1: {
							title:'Profile Information',
							html:'<form id="per_info" style="width: 535px">\
									<div class="row">\
									<label for="hometown">Hometown/City:</label>\
										<input class="textfield" type="text" id="hometown" />\
										<i class="field_error" id="hometown_error"></i> \
										<input type="hidden" name="hometown_components" id="hometown_components" />\
									</div>\
									<div class="row">\
										<label for="sex">Sex:</label>\
										<select id="sex" name="sex">\
											<option value="" disabled selected>Select</option>\
											<option value="m">Male</option>\
											<option value="f">Female</option>\
										</select>\
										<i class="field_error" id="sex_error"></i>\
									</div>\
									<div class="row">\
										<label for="birthdate">Birthdate:</label>\
											<select id="monthdropdown" name="month" style="width: 60px;margin-right: 0;"></select>\
											<select id="daydropdown" name="day"  style="width: 50px;margin-right: 0;"></select>\
											<select id="yeardropdown" name="year"  style="width: 60px;margin-right: 0;"></select>\
											<i class="field_error" id="birthdate_error"></i>\
									</div>\
									<div>\
										<label for="language">I can speak:</label>\
										<div id="lang">\
											<?php foreach($languages as $language){ ?>\
		<div class="language checkbox"><input type="checkbox" data-language="<?php echo $language['name']; ?>" name="language[<?php echo $language['id']; ?>]" /><label> <?php echo $language['name'];?> </label></div>\
											<?php } ?>\
										</div>\
										<i class="field_error" id="language_error"></i>\
									</div>\
									</form>',
							buttons: { Next: 1 },
							focus: 1,
							submit:function(e,v,m,f){

								e.preventDefault();
								if(v==1){
									$.ajax({
										url: window.location.protocol + '//' + window.location.hostname + '/ajax/create_userInfo',
										type:'post',
										data: $('#per_info').serialize(),
										success: function(data){
											$('.field_error').css('display','none').attr('title','');
											if(data.success == false){
											$('.jqi').css({'width': '495px','margin-left':'-260px'});
												if(data.error.hometown_components != '')
													$('#hometown_error').css('display','inline-block').attr('title',data.error.hometown_components);
												if(data.error.sex != '')
													$('#sex_error').css('display','inline-block').attr('title',data.error.sex);
												if(data.error.birthdate != '')
													$('#birthdate_error').css('display','inline-block').attr('title',data.error.birthdate);
												if(data.error.language != '')
													$('#language_error').css('display','inline-block').attr('title',data.error.language);
											}else{
												if(photos.length<1){
													$.prompt.goToState('state3');
												}else{
													$.get(window.location.protocol + '//' + window.location.hostname + '/ajax/updateExtendedAccessToken');  
													$('#jqi_state1_buttonNext').replaceWith('<span class="loader">Please wait while we set your account up <img src="//www.matchup360.com/assets/img/loader.gif" /> </span> ');
													$.ajax({
														url: window.location.protocol + '//' + window.location.hostname + '/ajax/importFBPhotos',
														type: "post",
														data: "photos="+JSON.stringify(photos),
														success: function(response){
															if(response.status == 'success'){
																getPhotos();
																$('.jqiclose').hide();
																$('#jqi_state4_buttonBack').hide();
																$.prompt.goToState('state4');
															}
														}
													});
												}
											}
										}
									});
									$('.jqi').css({'width': 'auto'});
									//$.prompt.goToState('state3');
								}
							}
						},
						state3: {
							title: 'Upload Photos',
							html:'<div id="dropbox">\
							<span class="message">Drop images here to upload.</span>\
								<div id="uploadify_wrap" style="display:none">\
									<input type="file" name="file_upload" id="file_upload" />\
								</div>\
							</div>',
							buttons: { Next: 1, Skip: 2 },
							focus: 1,
							submit:function(e,v,m,f){
								if(v==1){
									getPhotos();
									$('.jqi').css({'width': 'auto'});
									$('.jqiclose').hide();
									$.prompt.goToState('state4');

								}else if(v==2){
									window.location = '/';
								}
								e.preventDefault();
							}
						},
						state4: {
							title: 'Select and Crop Profile Photo',
							html: '<div id="image-cropper">\
									</div>\
								<div id="photo-gallery"></div>\
							<?php echo form_open('//ajax/set_profile_coordinates', array('id' => 'set_coordinate')); ?>\
								<input type="hidden" name="orig_photo_url" id="orig_photo_url" />\
								<input type="hidden" name="x" id="x" />\
								<input type="hidden" name="y" id="y" />\
								<input type="hidden" name="x2" id="x2" />\
								<input type="hidden" name="y2" id="y2" />\
								<input type="hidden" name="w" id="w" />\
								<input type="hidden" name="h" id="h" />\
								<input type="hidden" name="action" value="crop" />\
							</form><div style="clear: both"></div>',
							buttons: { Back: -1, Finish: 1 },
							focus: 1,
							submit:function(e,v,m,f){
								if(v==1){
									if(access_token){
										$.ajax({
											url: 'https://graph.facebook.com/me/feed',
											type: 'post',
											data: 'access_token='+access_token+'&caption=Find your match around.&picture=https://www.matchup360.com/assets/img/matchup360-130.jpg&name=Matchup360&link=https://www.matchup360.com/&description=Matchup360 is a fun way of interacting with people in a specified location. You simply set your location, adjust your radar and mingle with people! Aside from that, we match you with people that may share the same interest with you.',
											success: function(e){
												$('#set_coordinate').submit();
											}
										 });
									}else{
										$('#set_coordinate').submit();
									}
								}
								if(v==-1) $.prompt.goToState('state3');
								e.preventDefault();
							}
						}
					};
					$('#signup_button').click(function(){
						$('#blur').hide();
						$.prompt(signup_process,{
							opacity: 0.4,
							overlayspeed: 0,
							promptspeed: 0,
							loaded: function(){
								$(".jqiclose").remove();
								$("#use_facebook").click(function(){
									FB.login(function(response) {
												// Here we specify what we do with the response anytime this event occurs. 
												if (response.authResponse){
												  $("#use_facebook").hide();
												  access_token =   FB.getAuthResponse()['accessToken'];
												  FB.api('/me',function(response){
													if(response.birthday){
														var birthdate = response.birthday.split("/");
														$( "#birthdate" ).datepicker("setDate",birthdate[2]+'-'+birthdate[0]+'-'+birthdate[1]);
													}
													if(response.location){
														geo_options.location = response.location.name;
													}
													if(response.first_name){
														$("input#firstname").val(response.first_name);
													}
													if(response.last_name){
														$("input#lastname").val(response.last_name);
													}
													if(response.email){
														$("input#email").val(response.email);
														$("input#email-verify").val(response.email);
													}
													$("input#access_token").val(access_token);
													if(response.gender){
														if(response.gender == 'male')
															$('select#sex').val('m');
														else
															$('select#sex').val('f');
													}
													if(response.languages){
														for(var i=0; i<response.languages.length; i++){
															$('[data-language="'+response.languages[i].name+'"]').attr('checked','checked');
														}
													}
												  });
												  album_id = null;
												  photos = new Array();
												  FB.api('/me/albums',function(response){
													for(var i=0; i<response.data.length; i++){
														if(response.data[i].name != 'Profile Pictures')
															continue;
														album_id = response.data[i].id;
													}
													if(album_id != null){
														FB.api('/'+album_id+'/photos',function(response){
															for(var i=0; i<response.data.length; i++){
																photos[i] = response.data[i].source;
															}
														});
													}
												  });
												} /*else if (response.status === 'not_authorized') {
												  FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
												} else {
												  FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
												}*/
											},{scope: 'publish_actions,email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
								});
								$( "#birthdate" ).datepicker({ showButtonPanel: true, closeText: "Done", defaultDate: '-20y 0m 0d', showMonthAfterYear: true, showOn: "button", buttonText: "Select date", changeYear: true, yearRange: "-100:-16", changeMonth: true, dateFormat: "yy-mm-dd"});
							},
							statechanged: function(event,statename){
								switch(statename){
									case 'state1':
										$('.language label').unbind().click(function(){
											checkbox = $(this).parent().find('input[type="checkbox"]');
											if(typeof(checkbox.attr('checked'))=='undefined' || checkbox.attr('checked') == false)
												checkbox.attr('checked',true);
											else
												checkbox.attr('checked',false);
										});
										$('#monthdropdown, #yeardropdown').unbind().change(function(){
											dayfield=document.getElementById("daydropdown");
											val = $("#daydropdown").val();
											$("#daydropdown option").remove();
											for (var i=0; i<28; i++){
													dayfield.options[i]=new Option(i+1, pad(i+1,2));
											}
											if($('#monthdropdown').val() == '02'){
												if(isLeapYear($('#yeardropdown').val())){
													dayfield.options[i]=new Option(29, pad(29,2));
												}
											}else{
												dayfield.options[i]=new Option(30, pad(30,2));
												var month = $('#monthdropdown').val();
												if(month == '01' || month == '03' || month == '05' ||
													month == '07' || month == '08' || month == '10' ||	month == '12' ){
													dayfield.options[i]=new Option(31, pad(31,2));
												}
											}
											val = $("#daydropdown").val(val);
										});
										populatedropdown("daydropdown", "monthdropdown", "yeardropdown");
										var hometown_components = new Object();
										$("#hometown").geocomplete(geo_options)
										  .bind("geocode:result", function(event, result){
											hometown_components.address_components = result.address_components;
											hometown_components.lat = result.geometry.location.lat();
											hometown_components.lng = result.geometry.location.lng();
											$('#hometown_components').val(JSON.stringify(hometown_components));
										  })
										  .bind("geocode:error", function(event, status){
											console.log("ERROR: " + status);
										  });
										$('.jqiclose').hide();
									break;
									case 'state3': 
										var dropbox = $('#dropbox'),
											message = $('.message', dropbox);
										$('#file_upload').uploadify({
											'swf'      : 'assets/etc/uploadify.swf',
											'uploader' : 'ajax/upload_photo',
											'width' : 125,
											'height': 35,
											'scriptData'     : {'session' : '<?php echo $this->session->userdata('session_id') ?>'},
										});
										dropbox.filedrop({
											paramname: 'Filedata',
											maxfiles: 8,
											maxfilesize: 4,
											url: window.location.protocol + '//' + window.location.hostname + '/ajax/upload_photo',
											uploadFinished: function(i, file, response) {
												if(response.status == 'Upload success!'){
													$.data(file)
														.addClass('done');
													$('#jqi_state3_buttonSkip').hide();
												}else{
													$.data(file).find('.uploaded').removeClass('uploaded').addClass('upload_response_text').html(response.status);
												}
											},
											error: function(err, file) {
												switch (err) {
												case 'BrowserNotSupported':
													message.hide();
													$('#dropbox').prepend('<p align="center">Your browser does not support File Drag \'n Drop.<br />Use the file browser instead.<\p>');
													$('#dropbox p').delay(3000).fadeOut(400,function(){
														$('#uploadify_wrap').show();
													});
													break;
												case 'TooManyFiles':
													alert('Too many files! Please select 5 at most! (configurable)');
													break;
												case 'FileTooLarge':
													alert(file.name + ' is too large! Please upload files up to 2mb (configurable).');
													break;
												default:
													break;
												}
											},
											beforeEach: function(file) {
												if (!file.type.match(/^image\//)) {
													alert('Only images are allowed!');
													return false;
												}
											},
											uploadStarted: function(i, file, len) {
												createImage(file);
												$('#jqi_state3_buttonNext').hide();
												$('#jqi_state3_buttonSkip').hide();
											},
											progressUpdated: function(i, file, progress) {
												$.data(file)
													.find('.progress')
													.width(progress);
											},
											afterAll: function() {
												if($("#dropbox .done").length > 0){
													$('#jqi_state3_buttonNext').show();
												}
											}
										});
										var template = '<div class="preview">' + '<span class="imageHolder">' + '<img />' + '<span class="uploaded"></span>' + '</span>' + '<div class="progressHolder">' + '<div class="progress"></div>' + '</div>' + '</div>';
										function createImage(file) {
											var preview = $(template),
												image = $('img', preview);
											var reader = new FileReader();
											image.width = 100;
											image.height = 100;
											reader.onload = function(e) {
												// e.target.result holds the DataURL which
												// can be used as a source of the image:
												image.attr('src', e.target.result);
											};
											// Reading the file as a DataURL. When finished,
											// this will trigger the onload function above:
											reader.readAsDataURL(file);
											message.hide();
											preview.appendTo(dropbox);
											// Associating a preview container
											// with the file, using jQuery's $.data():
											$.data(file, preview);
										}
										function showMessage(msg) {
											message.html(msg);
										}
									break;
									case 'state4': 
																	$('#jqi_state4_buttonFinish').hide();
																	$(".photo").click(function(){
																		$('#jqi_state4_buttonFinish').hide();
																		var filename = $(this).attr('data');
																		var image_src_400 = $(this).find('img').attr('src');
																		var image_src_800 = image_src_400.replace("_thumb","_400");
																		$('#image-cropper').html('<img id="jcrop-target" src="'+image_src_800+'" style="width:400;"/>');
																		$(window).resize();
																		$('#orig_photo_url').val(image_src_800);
																		$('#jcrop-target').Jcrop({
																			onSelect: updateCoords,
																			onChange: updateCoords,
																			onRelease: updateCoords,
																			setSelect:   [ 5, 5, 390, 390 ],
																			aspectRatio: 1/1,
																			minSize: [200,200],
																			maxSize: [390,390]
																		});

																	});
									break;
								}
							}
						});
					});
					$('.jqi').css({'width': 'auto'});
					$('.jqiclose').hide();
});
function getPhotos(){
						$.ajax({
							url: window.location.protocol + '//' + window.location.hostname + '/ajax/get_photos',
							async: false,
							success: function(json){
								photos_str = '';
								$.each( json, function( key, value ) {
									photos_str += '<div data="'+value.filename+'" id="'+value.pid+'" class="photo"><img src="<?php echo config_item('s3_bucket_url');?>'+value.uid+'/photos/'+value.filename+'" /></div>';
								});
								$('#jqistate_state4 .jqimessage #photo-gallery').empty();
								$('#jqistate_state4 .jqimessage #photo-gallery').append(photos_str);
							}
						});
					}
					function updateCoords(c){
						$("#x").val(c.x);
						$("#y").val(c.y);
						$("#x2").val(c.x2);
						$("#y2").val(c.y2);
						$("#w").val(c.w);
						$("#h").val(c.h);
						if(c.w >= 200 && c.h >= 200)
							$('#jqi_state4_buttonFinish').show();
						else
							$('#jqi_state4_buttonFinish').hide();
			
					}
					function populatedropdown(dayfield, monthfield, yearfield){
						var monthtext=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
						var today=new Date();
						var dayfield=document.getElementById(dayfield);
						var monthfield=document.getElementById(monthfield);
						var yearfield=document.getElementById(yearfield);
						for (var i=0; i<31; i++){
							dayfield.options[i]=new Option(i+1, pad(i+1,2));
						}
						
						for (var m=0; m<12; m++){
							monthfield.options[m]=new Option(monthtext[m], pad(m+1,2));
						}
					
						var thisyear=today.getFullYear()-18;
						for (var y=0; y<30; y++){
							yearfield.options[y]=new Option(thisyear, thisyear);
							thisyear-=1;
						}
					}
					function pad(number, length) {
			   
						var str = '' + number;
						while (str.length < length) {
							str = '0' + str;
						}
					   
						return str;

					}
					function isLeapYear(year){
						if(year % 400 == 0)
						   return true;
						else if(year % 100 == 0)
						   return false;
						else if(year % 4 == 0)
						   return true;
						else
						   return false;
					}
		</script>
</head>
<body>
<div id="fb-root"></div>
<script>
  // Additional JS functions here
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '384283535015016', // App ID
      channelUrl : '//welcome/channel', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
  /*FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
	  $("#use_facebook").hide();
	  access_token =   FB.getAuthResponse()['accessToken'];
      FB.api('/me',function(response){
		var birthdate = response.birthday.split("/");
		$( "#birthdate" ).datepicker("setDate",birthdate[2]+'-'+birthdate[0]+'-'+birthdate[1]);
		geo_options.location = response.location.name;
		$("input#firstname").val(response.first_name);
		$("input#lastname").val(response.last_name);
		$("input#email").val(response.email);
		$("input#email-verify").val(response.email);
		$("input#access_token").val(access_token);
		if(response.gender == 'male')
			$('select#sex').val('m');
		else
			$('select#sex').val('f');
		if(response.languages){
			for(var i=0; i<response.languages.length; i++){
				$('[data-language="'+response.languages[i].name+'"]').attr('checked','checked');
			}
		}
	  });
	  album_id = null;
	  photos = new Array();
	  FB.api('/me/albums',function(response){
		for(var i=0; i<response.data.length; i++){
			if(response.data[i].name != 'Profile Pictures')
				continue;
			album_id = response.data[i].id;
		}
		if(album_id != null){
			FB.api('/'+album_id+'/photos',function(response){
				for(var i=0; i<response.data.length; i++){
					photos[i] = response.data[i].source;
				}
			});
		}
	  });
    } else if (response.status === 'not_authorized') {
      FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
    } else {
      FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
    }
  }); */
  };

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>
<div id="blur">
<div id="welcome-box">
	<div id="intro">
		<div id="logo"><img src="//www.matchup360.com/assets/img/matchup360-130.jpg" /></div>
		<h1>Matchup 360</h1>
		<p>Matchup360 is a fun way of interacting with people in a specified location. You simply set your location, adjust your radar and mingle with people! Aside from that, we match you with people that may share the same interest with you.</p>
		<p>Create an account (if you don't have any yet), tell us where you are located or any other area that you might be interested in, and mingle! Have fun!</p>
	</div>
	<div id="get-started">
		<div id="login-box">
		<h4>Login</h4>
		<?php echo validation_errors(); ?>
		<?php 
			echo form_open(); 
		?>
		<table align="center">
			<tr>
				<td width="100">Email:</td>
				<td><input type="text" id="email" name="email" value="<?php echo set_value('email'); ?>"/></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" id="pwd" name="pwd" value="<?php echo set_value('pwd'); ?>"/></td>
			</tr>
			<tr>
				<td></td>
				<td align="right"><input class="button" type="submit" value="Login"/></td>
			</tr>
		</table>
		</form>
		</div>
		<hr />
		<div id="signup-box">
			<button class="button" id="signup_button">Sign me up!</button>
		</div>
	</div>
</div>
</div>
<div id="windowSize">
<div id="page_container">
<?php
$ctr = 0;
foreach($photos as $photo){ ?>
<div class="photocollage"><img src="<?php echo config_item('s3_bucket_url').$photo['uid'].'/photos/'.$photo['filename']; ?>" /></div>
<?php
	$ctr++;
	if($ctr == 30)
		break;
} ?>
<div style="clear:both"></div>
</div>
</div>
</body>
</html>