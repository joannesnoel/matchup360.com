<div id="photos">
</div>
<script>
	$(document).ready(function(){ 
		getPhotos(0,50);
	});
	function getPhotos(offset,limit){
		$.ajax({
			url: '../ajax_moderator/getPhotos',
			type: 'post',
			data: 'offset='+offset+'&limit='+limit,
			success: function(response){
				response = JSON.parse(response);
				for(var i=0; i<response.length; i++){
					$('#photos').append('<div class="photo" id="'+response[i].reference+'"><img src="<?php echo config_item('s3_bucket_url'); ?>'+response[i].uid+'/photos/'+response[i].filename+'" /><button class="delete">Delete</button></div>');
				}
				$("#photos .photo button.delete").click(function(){
					photoDiv = $(this).parents('.photo');
					$.ajax({
						url: '../ajax_moderator/deletePhoto/'+photoDiv.attr('id'),
						success: function(){
							photoDiv.remove();
						}
					});
				});
			}
		});
	}
</script>