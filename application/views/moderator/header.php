<html>
	<head>
		<link rel="stylesheet" href="//www.matchup360.com/assets/css/moderator/style.css" />
		<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.min.js"></script>
	</head>
	<body>
		<div id="header-wrapper">
			<div id="header-inner">	
				<a href="../" target="_blank">
					<div id="logo-div">Matchup360.com</div>
				</a>
				<?php if($logged_in){?>
				<div id="menu">
					<ul>
						<li ><a href="/moderator">Home</a></li>
						<li ><a href="/moderator/users">Users</a></li>
						<li ><a href="/moderator/logout">Logout</a></li>
					</ul>
				</div>
				<?php }?>
			</div>
		</div>
		<div style="clear: both"></div>
		<div id="container">