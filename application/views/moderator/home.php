<div class="filter-box">
	<label>Search: </label>
	<form id="search">
		<select name="field" style="float: left">
			<option value="firstname">First Name</option>
			<option value="lastname">Last Name</option>
			<option value="email">Email</option>
			<option value="sex">Sex </option>
		</select>
		<div id="key-box" style="float: left">
			<input type="text" name="key">
		</div>
		<input id="search-button" type="button" name="search" value="Search">
	</form>
</div>
<div id="users_table" class="table">
	
</div>
<script>
	$(document).ready(function(){ 
		$('#search-button').click(function(){
			$.ajax({
				url: '../ajax_moderator/getUsers',
				data: $('#search').serialize(),
				type: 'post',
				success: function(json){
					var users = eval('('+json+')');
					var row = '';
					var table_html = '<table cellspacing="0">\
						<tr>\
							<td>ID</td>\
							<td>First Name</td>\
							<td>Last Name</td>\
							<td>Email</td>\
							<td></td>\
						</tr>';
					$.each(users, function(index, value){
							if(index % 2)
								row = 'odd';
							else
								row = 'even';
							table_html += '<tr class="'+row+'">\
								<td>'+value.uid+'</td>\
								<td>'+value.firstname+'</td>\
								<td>'+value.lastname+'</td>\
								<td>'+value.email+'</td>\
								<td><button class="remove-button" name="remove" value="'+value.uid+'">Remove</button></td>\
							</tr>';
					});
					table_html += '</table>';
					$('#users_table').empty();
					$('#users_table').append(table_html);
					attachRemoveButton();
				}
			});
		});
		$('#search-button').click();
		$('select[name=field]').change(function(){
			var key = '';
			if($(this).val() == 'sex'){
				key = '<select name="key">\
					<option value="f">Female</option>\
					<option value="m">Male</option>\
				</select>';
			}else{
				key = '<input type="text" name="key">';
			}
			$('#key-box').empty().append(key);
		});
		
		function attachRemoveButton(){
			$('.remove-button').click(function(){
			var id = $(this).val();
			var thisButton  = $(this);
			$.ajax({
				url: '../ajax_moderator/removeUser/'+id,
				success: function(){
					thisButton.parent().parent().remove();
				}
			});
		});
		}
	});
</script>