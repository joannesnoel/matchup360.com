<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>WheeWhew Dashboard</title>

        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="//wheewhew.com/assets/css/dashboard.css" />

        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		
		<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
		<script src="//wheewhew.com/assets/js/strophe.min.js"></script>
		<script src="//wheewhew.com/assets/js/jabber.js"></script>
		<script type="text/javascript">
			var BOSH_SERVICE = 'http://wheewhew.com:5280/http-bind';
			var connection = null;
			var xmpp_user = "<?php echo $xmpp_user; ?>@wheewhew.com/default";
			var xmpp_pass = "<?php echo $xmpp_password; ?>";
			var uid		  = "<?php echo $uid; ?>";
			
			$(document).ready(function () {				
				$('#btn-logout').click(logout);
				$('.add').click(addBuddy);
				$('.accept-btn').click(acceptBuddy);
				connectXMPP();
				//updateLastSeen();
				
			});
			
			
			
		</script>
    </head>

    <body>
		<div id="overlay"></div>
		<div id="msgdiv-overlay">
			<div id="compose-msg-div">
			<button type="button" id="cancel-msg-btn"> Cancel </button>
			</div>
		</div>
		<div id="mainWrapper">
			<div id="top-control">
				<div id="personal-menu">
					<div id="home-anchor">
						<a href="http://wheewhew.com/dashboard"> Wheewhew </a>
					</div>
				</div>
				<div id="searchimg">
					<input type="text" name="search-box" id="search-box" placeholder="Search"> 
				</div>
				<div id="account-menu">	
					<div id="logout-bar">
						<ul>
							<li><p id="btn-request" class="buttons"><a href="#">Click this button <div id="request-div">
							<?php 
								$rows = count($buddy_requests,0); 
								if($rows == 0) {
									echo '<div id="no-request-container">You have no Buddy Requests at the moment. </div>';
								}
								
								else {
									foreach($buddy_requests as $request) {
										foreach($all_members as $member) {
											if($request['from'] == $member['uid']){
												echo '<div class="req_container">';
												echo '<img src="https://s3.amazonaws.com/wheewhew/user/' . $member['uid'] . "/photos/" . $member['profile_pic'] . '" class="req_img" />';
												echo '<div class="member-name-request-container">' . $member['firstname']." ".$member['lastname'] . '</div>';
												echo '<div class="accept-decline-btn-container" sender-uid="' . $member['uid'] . '" id="request-' . $member['uid'] . '">';
												echo '<button type="button" class="accept-btn" id="accept-btn">' . 'Confirm' . '</button>';
												echo '<button type="button" class="decline-btn" id="decline-btn">' . 'Not Now' . '</button>';
												echo '</div>';
												echo '</div>';
				
											}
										}
									}
								}
							?>
							</div></a></p></li>

							<li><p id="btn-photo" class="buttons"><a href="#">Click this button</a></p></li>
							<li><p id="btn-msg" class="buttons"><a href="#">Click this button</a></p></li>
							<li><p id="btn-notif" class="buttons"><a href="#">Click this button</a></p></li>
							<li><p id="btn-logout" class="buttons"><a href="#">Click this button</a></p></li>
						</ul>
					</div>
				</div>
				<br class="clear" />
			</div>
			
			<div id="content">
				<div id="user-content">
					<div id="photo">
					<img src="https://s3.amazonaws.com/wheewhew/user/<?php echo $user_data['uid'];?>/photos/<?php echo $user_photo;?>">
					<h1><?php echo $user_data['firstname']." ".$user_data['lastname']; ?></h1>
					<button type="button" id="msg-btn"> Message </button>
					<div style="clear:both"></div>
					</div>
					<div>
						<table>
							
							<tr><td>Email : </td><td><?php echo $user_data['email']; ?></td></tr>
							<tr><td>Sex : </td><td><?php echo $user_data['sex']; ?></td></tr>
							<tr><td>Birthdate : </td><td><?php echo $user_data['birthdate']; ?></td></tr>
							<tr><td>Status : </td><td><?php echo $user_data['status']; ?></td></tr>
						</table>
					</div>
				</div>
				
				<script>
						$('#btn-request').click(function () {
							$('#request-div').fadeToggle(0);
						});
				

						$('#msg-btn').click(function () {
							$('#overlay').show();
							$('#msgdiv-overlay').show();
						});
						
						$('#cancel-msg-btn').click(function () {
							$('#overlay').hide();
							$('#msgdiv-overlay').hide();
						});
						

				</script>
				
				<div id="box1">
					<div class="sidebars">
					</div>
					<div id="status-container">
						<?php echo form_open(''); ?>
							<textarea name="update-status-box" id="update-status-box" placeholder="What's on your mind?"></textarea><br/>
							<input type="hidden" name="poster_uid" />
							<input type="submit" value="Post" id="post-btn" />
						</form>
					
							
					<div class="main-content-rule">
					
					</div>
							
						<div id="posts_container">
							<?php 
								if(count($buddy_posts) > 0) {
									foreach($buddy_posts as $posts) {
										//foreach($all_members as $member) {
											//if($posts['poster_uid'] == $user_data['uid']){
												echo '<div class="single-post">';
												echo '<a href="http://www.wheewhew.com/member/profile/'.$posts['poster_uid'].'"><img src="https://s3.amazonaws.com/wheewhew/user/'.$posts['poster_uid'].'/photos/'.$posts['profile_pic'].'" class="post-img" /></a>';
												echo '<div class="member-name-post"><a href="http://www.wheewhew.com/member/profile/'.$posts['poster_uid'].'">' . $posts['firstname']." ".$posts['lastname'] . '</a> >> <a href="http://www.wheewhew.com/member/profile/'.$posts['post_to_uid'].'">' . $user_data['firstname']." ".$user_data['lastname'] . '</a></div>';
												echo '<p class="post-string">' . $posts['post'] . '</p>';
												echo '<p class="post-time">' . $posts['post_time'] . '</p>';
												echo '</div>';
												echo '<div class="main-content-rule"></div>';
												//break;			
											//}
										
									}
								}
								else {
									echo '<div id="no-post"> There are no post to show at the moment </div>';
									}	
							?>
							
						</div>
					</div>
							
				</div>
			
			</div> <!-- END OF CONTENT -->
			
			<div id="new_members">

			</div>
			<div id="buddy-list">
			
			</div>
			<div id="push"><!-- Leave this element empty --></div>
		</div>
		<div id="footer">
			
		</div>
		
    </body>
</html>

