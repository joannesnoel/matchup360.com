<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>WheeWhew Dashboard</title>

        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="//wheewhew.com/assets/css/dashboard.css" />

        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		
		<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
		<script src="//wheewhew.com/assets/js/strophe.min.js"></script>
		<script src="//wheewhew.com/assets/js/jabber.js"></script>
		<script type="text/javascript">
			var BOSH_SERVICE = 'http://wheewhew.com:5280/http-bind';
			var connection = null;
			var xmpp_user = "<?php echo $xmpp_user; ?>@wheewhew.com/default";
			var xmpp_pass = "<?php echo $xmpp_password; ?>";
			var uid		  = "<?php echo $uid; ?>";
			
			$(document).ready(function () {				
				$('#btn-logout').click(logout);
				connectXMPP();
				updateLastSeen();
			});
		</script>
    </head>

    <body>
		<div id="top-control">
			<div id="profile-photo"><img src="<?php echo $profile_photo; ?>" /></div>
			<div id="account-menu">
				<ul>
					<li><a href="//wheewhew.com/welcome/logout" id="btn-logout">Logout</a></li>
				</ul>
			</div>
			<br class="clear" />
			<div id="buddy-list">
			
			</div>
		</div>
    </body>
</html>