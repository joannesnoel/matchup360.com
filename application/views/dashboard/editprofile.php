<!Doctype HTML>
<html>
	<head>
		<link rel="stylesheet" href="//matchup360.com/assets/css/dashboard.css" />
		
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
		<script src="//matchup360.com/assets/js/strophe.min.js"></script>
		<script src="//matchup360.com/assets/js/jabber.js"></script>
		<script type="text/javascript" src="//matchup360.com/assets/js/jquery-impromptu.js"></script>
		
		<script>
			$(document).ready(function(){
					$('.toggle-header').click(function(){
						//if($('#personal-description-box').css('display') == 'none')
						//	$('#personal-description-box').slideDown('fast');
						//else
						//	$('#personal-description-box').slideUp('fast');
						
						var box = $(this).attr('box-name');
						
						$.ajax({
						type: 'post',
						success: function(res){
							if($('#'+box+'-box').css('display') == 'none')
								$('#'+box+'-box').slideDown('fast');
								
							else
								$('#'+box+'-box').slideUp('fast');
						}
					});
				});
					
			});
		</script>
	</head>
	<body>
		<div id="edit-profile-container">
			<div id="personal-description-header" box-name="personal-description" class="toggle-header"> My Personal Description </div>
			<div id="personal-description-box" class="toggle-box"> 
				<label for="select-gender"> I am: </label> <br />
				<select name="select-gender">
					<option value="m"> Male </option>
					<option value="f"> Female </option>
				</select> <br />
				<label for="looking-for"> I am looking for: </label> <br />
				<select name="looking-for">
					<option value="m"> Male </option>
					<option value="f"> Female </option>
				</select> <br />
				<label for="birthday-picker"> My date of birth is: </label> <br />
				<input type="date" name="birthday-picker"> <br />
				<label for="country-picker"> My country is: </label> <br />
				<select name="country-picker"> 
					<option value="" selected="selected">Select Country</option> 
					<option value="United States">United States</option> 
					<option value="United Kingdom">United Kingdom</option> 
					<option value="Afghanistan">Afghanistan</option> 
					<option value="Albania">Albania</option> 
					<option value="Algeria">Algeria</option> 
					<option value="American Samoa">American Samoa</option> 
					<option value="Andorra">Andorra</option> 
					<option value="Angola">Angola</option> 
					<option value="Anguilla">Anguilla</option> 
					<option value="Antarctica">Antarctica</option> 
					<option value="Antigua and Barbuda">Antigua and Barbuda</option> 
					<option value="Argentina">Argentina</option> 
					<option value="Armenia">Armenia</option> 
					<option value="Aruba">Aruba</option> 
					<option value="Australia">Australia</option> 
					<option value="Austria">Austria</option> 
					<option value="Azerbaijan">Azerbaijan</option> 
					<option value="Bahamas">Bahamas</option> 
					<option value="Bahrain">Bahrain</option> 
					<option value="Bangladesh">Bangladesh</option> 
					<option value="Barbados">Barbados</option> 
					<option value="Belarus">Belarus</option> 
					<option value="Belgium">Belgium</option> 
					<option value="Belize">Belize</option> 
					<option value="Benin">Benin</option> 
					<option value="Bermuda">Bermuda</option> 
					<option value="Bhutan">Bhutan</option> 
					<option value="Bolivia">Bolivia</option> 
					<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
					<option value="Botswana">Botswana</option> 
					<option value="Bouvet Island">Bouvet Island</option> 
					<option value="Brazil">Brazil</option> 
					<option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
					<option value="Brunei Darussalam">Brunei Darussalam</option> 
					<option value="Bulgaria">Bulgaria</option> 
					<option value="Burkina Faso">Burkina Faso</option> 
					<option value="Burundi">Burundi</option> 
					<option value="Cambodia">Cambodia</option> 
					<option value="Cameroon">Cameroon</option> 
					<option value="Canada">Canada</option> 
					<option value="Cape Verde">Cape Verde</option> 
					<option value="Cayman Islands">Cayman Islands</option> 
					<option value="Central African Republic">Central African Republic</option> 
					<option value="Chad">Chad</option> 
					<option value="Chile">Chile</option> 
					<option value="China">China</option> 
					<option value="Christmas Island">Christmas Island</option> 
					<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
					<option value="Colombia">Colombia</option> 
					<option value="Comoros">Comoros</option> 
					<option value="Congo">Congo</option> 
					<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
					<option value="Cook Islands">Cook Islands</option> 
					<option value="Costa Rica">Costa Rica</option> 
					<option value="Cote D'ivoire">Cote D'ivoire</option> 
					<option value="Croatia">Croatia</option> 
					<option value="Cuba">Cuba</option> 
					<option value="Cyprus">Cyprus</option> 
					<option value="Czech Republic">Czech Republic</option> 
					<option value="Denmark">Denmark</option> 
					<option value="Djibouti">Djibouti</option> 
					<option value="Dominica">Dominica</option> 
					<option value="Dominican Republic">Dominican Republic</option> 
					<option value="Ecuador">Ecuador</option> 
					<option value="Egypt">Egypt</option> 
					<option value="El Salvador">El Salvador</option> 
					<option value="Equatorial Guinea">Equatorial Guinea</option> 
					<option value="Eritrea">Eritrea</option> 
					<option value="Estonia">Estonia</option> 
					<option value="Ethiopia">Ethiopia</option> 
					<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
					<option value="Faroe Islands">Faroe Islands</option> 
					<option value="Fiji">Fiji</option> 
					<option value="Finland">Finland</option> 
					<option value="France">France</option> 
					<option value="French Guiana">French Guiana</option> 
					<option value="French Polynesia">French Polynesia</option> 
					<option value="French Southern Territories">French Southern Territories</option> 
					<option value="Gabon">Gabon</option> 
					<option value="Gambia">Gambia</option> 
					<option value="Georgia">Georgia</option> 
					<option value="Germany">Germany</option> 
					<option value="Ghana">Ghana</option> 
					<option value="Gibraltar">Gibraltar</option> 
					<option value="Greece">Greece</option> 
					<option value="Greenland">Greenland</option> 
					<option value="Grenada">Grenada</option> 
					<option value="Guadeloupe">Guadeloupe</option> 
					<option value="Guam">Guam</option> 
					<option value="Guatemala">Guatemala</option> 
					<option value="Guinea">Guinea</option> 
					<option value="Guinea-bissau">Guinea-bissau</option> 
					<option value="Guyana">Guyana</option> 
					<option value="Haiti">Haiti</option> 
					<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
					<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
					<option value="Honduras">Honduras</option> 
					<option value="Hong Kong">Hong Kong</option> 
					<option value="Hungary">Hungary</option> 
					<option value="Iceland">Iceland</option> 
					<option value="India">India</option> 
					<option value="Indonesia">Indonesia</option> 
					<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
					<option value="Iraq">Iraq</option> 
					<option value="Ireland">Ireland</option> 
					<option value="Israel">Israel</option> 
					<option value="Italy">Italy</option> 
					<option value="Jamaica">Jamaica</option> 
					<option value="Japan">Japan</option> 
					<option value="Jordan">Jordan</option> 
					<option value="Kazakhstan">Kazakhstan</option> 
					<option value="Kenya">Kenya</option> 
					<option value="Kiribati">Kiribati</option> 
					<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
					<option value="Korea, Republic of">Korea, Republic of</option> 
					<option value="Kuwait">Kuwait</option> 
					<option value="Kyrgyzstan">Kyrgyzstan</option> 
					<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
					<option value="Latvia">Latvia</option> 
					<option value="Lebanon">Lebanon</option> 
					<option value="Lesotho">Lesotho</option> 
					<option value="Liberia">Liberia</option> 
					<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
					<option value="Liechtenstein">Liechtenstein</option> 
					<option value="Lithuania">Lithuania</option> 
					<option value="Luxembourg">Luxembourg</option> 
					<option value="Macao">Macao</option> 
					<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
					<option value="Madagascar">Madagascar</option> 
					<option value="Malawi">Malawi</option> 
					<option value="Malaysia">Malaysia</option> 
					<option value="Maldives">Maldives</option> 
					<option value="Mali">Mali</option> 
					<option value="Malta">Malta</option> 
					<option value="Marshall Islands">Marshall Islands</option> 
					<option value="Martinique">Martinique</option> 
					<option value="Mauritania">Mauritania</option> 
					<option value="Mauritius">Mauritius</option> 
					<option value="Mayotte">Mayotte</option> 
					<option value="Mexico">Mexico</option> 
					<option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
					<option value="Moldova, Republic of">Moldova, Republic of</option> 
					<option value="Monaco">Monaco</option> 
					<option value="Mongolia">Mongolia</option> 
					<option value="Montserrat">Montserrat</option> 
					<option value="Morocco">Morocco</option> 
					<option value="Mozambique">Mozambique</option> 
					<option value="Myanmar">Myanmar</option> 
					<option value="Namibia">Namibia</option> 
					<option value="Nauru">Nauru</option> 
					<option value="Nepal">Nepal</option> 
					<option value="Netherlands">Netherlands</option> 
					<option value="Netherlands Antilles">Netherlands Antilles</option> 
					<option value="New Caledonia">New Caledonia</option> 
					<option value="New Zealand">New Zealand</option> 
					<option value="Nicaragua">Nicaragua</option> 
					<option value="Niger">Niger</option> 
					<option value="Nigeria">Nigeria</option> 
					<option value="Niue">Niue</option> 
					<option value="Norfolk Island">Norfolk Island</option> 
					<option value="Northern Mariana Islands">Northern Mariana Islands</option> 
					<option value="Norway">Norway</option> 
					<option value="Oman">Oman</option> 
					<option value="Pakistan">Pakistan</option> 
					<option value="Palau">Palau</option> 
					<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
					<option value="Panama">Panama</option> 
					<option value="Papua New Guinea">Papua New Guinea</option> 
					<option value="Paraguay">Paraguay</option> 
					<option value="Peru">Peru</option> 
					<option value="Philippines">Philippines</option> 
					<option value="Pitcairn">Pitcairn</option> 
					<option value="Poland">Poland</option> 
					<option value="Portugal">Portugal</option> 
					<option value="Puerto Rico">Puerto Rico</option> 
					<option value="Qatar">Qatar</option> 
					<option value="Reunion">Reunion</option> 
					<option value="Romania">Romania</option> 
					<option value="Russian Federation">Russian Federation</option> 
					<option value="Rwanda">Rwanda</option> 
					<option value="Saint Helena">Saint Helena</option> 
					<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
					<option value="Saint Lucia">Saint Lucia</option> 
					<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
					<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
					<option value="Samoa">Samoa</option> 
					<option value="San Marino">San Marino</option> 
					<option value="Sao Tome and Principe">Sao Tome and Principe</option> 
					<option value="Saudi Arabia">Saudi Arabia</option> 
					<option value="Senegal">Senegal</option> 
					<option value="Serbia and Montenegro">Serbia and Montenegro</option> 
					<option value="Seychelles">Seychelles</option> 
					<option value="Sierra Leone">Sierra Leone</option> 
					<option value="Singapore">Singapore</option> 
					<option value="Slovakia">Slovakia</option> 
					<option value="Slovenia">Slovenia</option> 
					<option value="Solomon Islands">Solomon Islands</option> 
					<option value="Somalia">Somalia</option> 
					<option value="South Africa">South Africa</option> 
					<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
					<option value="Spain">Spain</option> 
					<option value="Sri Lanka">Sri Lanka</option> 
					<option value="Sudan">Sudan</option> 
					<option value="Suriname">Suriname</option> 
					<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
					<option value="Swaziland">Swaziland</option> 
					<option value="Sweden">Sweden</option> 
					<option value="Switzerland">Switzerland</option> 
					<option value="Syrian Arab Republic">Syrian Arab Republic</option> 
					<option value="Taiwan, Province of China">Taiwan, Province of China</option> 
					<option value="Tajikistan">Tajikistan</option> 
					<option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
					<option value="Thailand">Thailand</option> 
					<option value="Timor-leste">Timor-leste</option> 
					<option value="Togo">Togo</option> 
					<option value="Tokelau">Tokelau</option> 
					<option value="Tonga">Tonga</option> 
					<option value="Trinidad and Tobago">Trinidad and Tobago</option> 
					<option value="Tunisia">Tunisia</option> 
					<option value="Turkey">Turkey</option> 
					<option value="Turkmenistan">Turkmenistan</option> 
					<option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
					<option value="Tuvalu">Tuvalu</option> 
					<option value="Uganda">Uganda</option> 
					<option value="Ukraine">Ukraine</option> 
					<option value="United Arab Emirates">United Arab Emirates</option> 
					<option value="United Kingdom">United Kingdom</option> 
					<option value="United States">United States</option> 
					<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
					<option value="Uruguay">Uruguay</option> 
					<option value="Uzbekistan">Uzbekistan</option> 
					<option value="Vanuatu">Vanuatu</option> 
					<option value="Venezuela">Venezuela</option> 
					<option value="Viet Nam">Viet Nam</option> 
					<option value="Virgin Islands, British">Virgin Islands, British</option> 
					<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
					<option value="Wallis and Futuna">Wallis and Futuna</option> 
					<option value="Western Sahara">Western Sahara</option> 
					<option value="Yemen">Yemen</option> 
					<option value="Zambia">Zambia</option> 
					<option value="Zimbabwe">Zimbabwe</option>
				</select> <br />

				<label for="postal-code"> My postal code is: </label> <br />
				<input type="text" name="postal-code"> <br />
				
				<label for="city-picker"> My city is: </label> <br />
				<input type="text" name="city-picker"> <br />
				
				<label for="profile-heading"> My profile heading is: </label> <br />
				<input type="text" name="profile-heading"> <br />
				
				<label for="self-description"> I describe myself as: </label> <br />
				<textarea name="self-description" > </textarea><br />
				
				<label> I can speak: </label> <br />
				<table>
					<tr> <td><input type="checkbox" name="language-picker" value="english"> English</td>
					<td><input type="checkbox" name="language-picker" value="french"> French </td></tr>
					<tr><td> <input type="checkbox" name="language-picker" value="spanish"> Spanish</td>
					<td><input type="checkbox" name="language-picker" value="italian"> Italian </td></tr>
					<tr><td> <input type="checkbox" name="language-picker" value="german"> German</td>
					<td><input type="checkbox" name="language-picker" value="portugese"> Portugese</td></tr>
					<tr><td> <input type="checkbox" name="language-picker" value="dutch"> Dutch</td>
					<td><input type="checkbox" name="language-picker" value="chinese"> Chinese</td>
					<tr> <td><input type="checkbox" name="language-picker" value="japanese"> Japanese</td>
					<td><input type="checkbox" name="language-picker" value="arabic"> Arabic</td></tr>
					<tr><td> <input type="checkbox" name="language-picker" value="hebrew"> Hebrew</td>
					<td><input type="checkbox" name="language-picker" value="russian"> Russian</td></tr>
					<tr> <td><input type="checkbox" name="language-picker" value="hindi"> Hindi</td>
					<td><input type="checkbox" name="language-picker" value="tagalog"> Tagalog</td></tr>
					<tr><td> <input type="checkbox" name="language-picker" value="urdu"> Urdu</td>
					<td><input type="checkbox" name="language-picker" value="binary"> Binary</td></tr>
					<tr> <td><input type="checkbox" name="language-picker" value="polish"> Polish</td>
					<td><input type="checkbox" name="language-picker" value="turkish"> Turkish</td></tr>
					<tr> <td><input type="checkbox" name="language-picker" value="other"> Other</td><td> </td></tr>
				</table> <br />
				<button type="button" value="update"> Update </button>
			</div>
		
			<div id="interested-in-header" box-name="interested-in" class="toggle-header"> I am interested in... </div>
			<div id="interested-in-box" class="toggle-box">
				<table>
					<tr><td><input type="checkbox" name="interest-picker" value="hang"> Hang-out</td>
					<td><input type="checkbox" name="interest-picker" value="talk"> Talk/ E-mail</td></tr>
					<tr><td><input type="checkbox" name="interest-picker" value="ltr"> Long term relationship</td>
					<td><input type="checkbox" name="interest-picker" value="date"> Dating</td></tr>
					<tr><td><input type="checkbox" name="interest-picker" value="friends"> Friends</td>
					<td><input type="checkbox" name="interest-picker" value="intimate"> Intimate encounter</td></tr>
					<tr><td><input type="checkbox" name="interest-picker" value="activity-partner"> Activity partner</td>
					<td><input type="checkbox" name="interest-picker" value="other"> Other relationship</td></tr>
				</table>
			</div>
			
			<div id="appearance-header" box-name="appearance" class="toggle-header"> My appearance </div>
			<div id="appearance-box" class="toggle-box">
				<label for="ethnicity"> My ethnicity is: </label> <br />
				<select name="ethnicity">
					<option value=""> I'll tell you later </option>
					<option value=""> Caucasian White </option>
					<option value=""> Black </option>
					<option value=""> Asian </option>
					<option value=""> Latino/ Hispanic </option>
					<option value=""> Mixed Race </option>
					<option value=""> Middle Eastern </option>
					<option value=""> Indian </option>
					<option value=""> Native American </option>
					<option value=""> Other </option>
				</select> <br />
				<label for="nationality"> My ethnicity is: </label> <br />
				<select name="nationality">
					<option value=””>I'll tell you later</option>
					<option value=”AF”>Afghan</option>
					<option value=”AX”>Aland Islander</option>
					<option value=”AL”>Albanianr</option>
					<option value=”DZ”>Algerian</option>
					<option value=”AS”>American Samoan</option>
					<option value=”AD”>Andorran</option>
					<option value=”AO”>Angolan</option>
					<option value=”AI”>Anguillan</option>
					<option value=”AG”>of Antigua and Barbuda</option>
					<option value=”AR”>Argentinian</option>
					<option value=”AM”>Armenian</option>
					<option value=”AW”>Aruban</option>
					<option value=”AU”>Australian</option>
					<option value=”AT”>Austrian</option>
					<option value=”AZ”>Azerbaijani</option>
					<option value=”BS”>Bahamian</option>
					<option value=”BH”>Bahraini</option>
					<option value=”BD”>Bangladeshi</option>
					<option value=”BB”>Barbadian</option>
					<option value=”BY”>Belarusian</option>
					<option value=”BE”>Belgian</option>
					<option value=”BZ”>Belizean</option>
					<option value=”BJ”>Beninese</option>
					<option value=”BM”>Bermudian</option>
					<option value=”BT”>Bhutanese</option>
					<option value=”BO”>Bolivian</option>
					<option value=”BA”>of Bosnia and Herzegovina</option>
					<option value=”BW”>Botswanan</option>
					<option value=”BR”>Brazilian</option>
					<option value=”VG”>British Virgin Islander</option>
					<option value=”BN”>Bruneian</option>
					<option value=”BG”>Bulgarian</option>
					<option value=”BF”>Burkinabe</option>
					<option value=”MM”>of Burma/Myanmar</option>
					<option value=”BI”>Burundian</option>
					<option value=”KH”>Cambodian</option>
					<option value=”CM”>Cameroonian</option>
					<option value=”CA”>Canadian</option>
					<option value=”CV”>Cape Verdean</option>
					<option value=”KY”>Caymanian</option>
					<option value=”CF”>Central African</option>
					<option value=”TD”>Chadian</option>
					<option value=”CL”>Chilean</option>
					<option value=”CN”>Chinese</option>
					<option value=”CX”>Christmas Islander</option>
					<option value=”CC”>Cocos Islander</option>
					<option value=”CO”>Colombian</option>
					<option value=”KM”>Comorian</option>
					<option value=”CG”>Congolese</option>
					<option value=”CK”>Cook Islander</option>
					<option value=”CR”>Costa Rican</option>
					<option value=”CI”>Ivorian</option>
					<option value=”HR”>Croat</option>
					<option value=”CU”>Cuban</option>
					<option value=”CW”>Curaçaoan</option>
					<option value=”CY”>Cypriot</option>
					<option value=”CZ”>Czech</option>
					<option value=”CD”>of the Democratic Republic of the Congo</option>
					<option value=”DK”>Dane</option>
					<option value=”DJ”>Djiboutian</option>
					<option value=”DM”>Dominican</option>
					<option value=”DO”>Dominican</option>
					<option value=”TL”>East Timorese</option>
					<option value=”EC”>Ecuadorian</option>
					<option value=”EG”>Egyptian</option>
					<option value=”SV”>Salvadorian</option>
					<option value=”GQ”>Equatorial Guinean</option>
					<option value=”ER”>Eritrean</option>
					<option value=”EE”>Estonian</option>
					<option value=”ET”>Ethiopian</option>
					<option value=”FO”>Faeroese</option>
					<option value=”FK”>Falkland Islander</option>
					<option value=”FJ”>Fijian</option>
					<option value=”FI”>Finn</option>
					<option value=”FR”>Frenchman; Frenchwoman</option>
					<option value=”GF”>Guianese</option>
					<option value=”PF”>Polynesian</option>
					<option value=”GA”>Gabonese</option>
					<option value=”GM”>Gambian</option>
					<option value=”GE”>Georgian</option>
					<option value=”DE”>German</option>
					<option value=”GH”>Ghanaian</option>
					<option value=”GI”>Gibraltarian</option>
					<option value=”EL”>Greek</option>
					<option value=”GL”>Greenlander</option>
					<option value=”GD”>Grenadian</option>
					<option value=”GP”>Guadeloupean</option>
					<option value=”GU”>Guamanian</option>
					<option value=”GT”>Guatemalan</option>
					<option value=”GG”>of Guernsey</option>
					<option value=”GN”>Guinean</option>
					<option value=”GW”>Guinea-Bissau national</option>
					<option value=”GY”>Guyanese</option>
					<option value=”HT”>Haitian</option>
					<option value=”VA”>of the Holy See/of the Vatican</option>
					<option value=”HN”>Honduran</option>
					<option value=”HK”>Hong Kong Chinese</option>
					<option value=”HU”>Hungarian</option>
					<option value=”IS”>Icelander</option>
					<option value=”IN”>Indian</option>
					<option value=”ID”>Indonesian</option>
					<option value=”IR”>Iranian</option>
					<option value=”IQ”>Iraqi</option>
					<option value=”IE”>Irishman; Irishwoman</option>
					<option value=”IM”>Manxman; Manxwoman</option>
					<option value=”IL”>Israeli</option>
					<option value=”IT”>Italian</option>
					<option value=”JM”>Jamaican</option>
					<option value=”JP”>Japanese</option>
					<option value=”JE”>of Jersey</option>
					<option value=”JO”>Jordanian</option>
					<option value=”KZ”>Kazakh</option>
					<option value=”KE”>Kenyan</option>
					<option value=”KI”>Kiribatian</option>
					<option value=”KW”>Kuwaiti</option>
					<option value=”KG”>Kyrgyz</option>
					<option value=”LA”>Lao</option>
					<option value=”LV”>Latvian</option>
					<option value=”LB”>Lebanese</option>
					<option value=”LS”>Basotho</option>
					<option value=”LR”>Liberian</option>
					<option value=”LY”>Libyan</option>
					<option value=”LI”>Liechtensteiner</option>
					<option value=”LT”>Lithuanian</option>
					<option value=”LU”>Luxembourger</option>
					<option value=”MO”>Macanese</option>
					<option value=”MG”>Malagasy</option>
					<option value=”MW”>Malawian</option>
					<option value=”MY”>Malaysian</option>
					<option value=”MV”>Maldivian</option>
					<option value=”ML”>Malian</option>
					<option value=”MT”>Maltese</option>
					<option value=”MH”>Marshallese</option>
					<option value=”MQ”>Martinican</option>
					<option value=”MR”>Mauritanian</option>
					<option value=”MU”>Mauritian</option>
					<option value=”YT”>Mahorais</option>
					<option value=”MX”>Mexican</option>
					<option value=”FM”>Micronesian</option>
					<option value=”MD”>Moldovan</option>
					<option value=”MC”>Monegasque</option>
					<option value=”MN”>Mongolian</option>
					<option value=”ME”>Montenegrin</option>
					<option value=”MS”>Montserratian</option>
					<option value=”MA”>Moroccan</option>
					<option value=”MZ”>Mozambican</option>
					<option value=”NA”>Namibian</option>
					<option value=”NR”>Nauruan</option>
					<option value=”NP”>Nepalese</option>
					<option value=”NL”>Dutchman; Dutchwoman; Netherlander</option>
					<option value=”NC”>New Caledonian</option>
					<option value=”NZ”>New Zealander</option>
					<option value=”NI”>Nicaraguan</option>
					<option value=”NE”>Nigerien</option>
					<option value=”NG”>Nigerian</option>
					<option value=”NU”>Niuean</option>
					<option value=”NF”>Norfolk Islander</option>
					<option value=”KP”>North Korean</option>
					<option value=”MP”>Northern Mariana Islander</option>
					<option value=”NO”>Norwegian</option>
					<option value=”OM”>Omani</option>
					<option value=”PK”>Pakistani</option>
					<option value=”PW”>Palauan</option>
					<option value=”PA”>Panamanian</option>
					<option value=”PG”>Papua New Guinean</option>
					<option value=”PY”>Paraguayan</option>
					<option value=”PE”>Peruvian</option>
					<option value=”PH”>Filipino</option>
					<option value=”PN”>Pitcairner</option>
					<option value=”PL”>Pole</option>
					<option value=”PT”>Portuguese</option>
					<option value=”PR”>Puerto Rican</option>
					<option value=”QA”>Qatari</option>
					<option value=”RE”>Reunionese</option>
					<option value=”RO”>Romanian</option>
					<option value=”RU”>Russian</option>
					<option value=”RW”>Rwandan; Rwandese</option>
					<option value=”BL”>of Saint Barthélemy</option>
					<option value=”SH”>Saint Helenian</option>
					<option value=”KN”>Kittsian; Nevisian</option>
					<option value=”LC”>Saint Lucian</option>
					<option value=”MF”>of Saint Martin</option>
					<option value=”PM”>St-Pierrais; Miquelonnais</option>
					<option value=”VC”>Vincentian</option>
					<option value=”WS”>Samoan</option>
					<option value=”SM”>San Marinese</option>
					<option value=”ST”>São Toméan</option>
					<option value=”SA”>Saudi Arabian</option>
					<option value=”SN”>Senegalese</option>
					<option value=”RS”>Serb</option>
					<option value=”SC”>Seychellois</option>
					<option value=”SL”>Sierra Leonean</option>
					<option value=”SG”>Singaporean</option>
					<option value=”SX”>Sint Maartener</option>
					<option value=”SK”>Slovak</option>
					<option value=”SI”>Slovene</option>
					<option value=”SB”>Solomon Islander</option>
					<option value=”SO”>Somali</option>
					<option value=”ZA”>South African</option>
					<option value=”KR”>South Korean</option>
					<option value=”SS”>South Sudanese</option>
					<option value=”ES”>Spaniard</option>
					<option value=”LK”>Sri Lankan</option>
					<option value=”SD”>Sudanese</option>
					<option value=”SR”>Surinamer</option>
					<option value=”SJ”>of Svalbard, of Jan Mayen</option>
					<option value=”SZ”>Swazi</option>
					<option value=”SE”>Swede</option>
					<option value=”CH”>Swiss</option>
					<option value=”SY”>Syrian</option>
					<option value=”TW”>Taiwanese</option>
					<option value=”TJ”>Tajik</option>
					<option value=”TZ”>Tanzanian</option>
					<option value=”TH”>Thai</option>
					<option value=”TG”>Togolese</option>
					<option value=”TK”>Tokelauan</option>
					<option value=”TO”>Tongan</option>
					<option value=”TT”>Trinidadian; Tobagonian</option>
					<option value=”TN”>Tunisian</option>
					<option value=”TR”>Turk</option>
					<option value=”TM”>Turkmen</option>
					<option value=”TC”>Turks and Caicos Islander</option>
					<option value=”TV”>Tuvaluan</option>
					<option value=”UG”>Ugandan</option>
					<option value=”UA”>Ukrainian</option>
					<option value=”AE”>Emirian</option>
					<option value=”UK”>Briton</option>
					<option value=”US”>American;US citizen</option>
					<option value=”UY”>Uruguayan</option>
					<option value=”VI”>US Virgin Islander</option>
					<option value=”UZ”>Uzbek</option>
					<option value=”VU”>Vanuatuan</option>
					<option value=”VE”>Venezuelan</option>
					<option value=”VN”>Vietnamese</option>
					<option value=”WF”>Wallisian</option>
					<option value=”EH”>Sahrawi</option>
					<option value=”YE”>Yemenite</option>
					<option value=”ZM”>Zambian</option>
					<option value=”ZW”>Zimbabwean</option>
			</select>
				<label for="build"> My build is: </label> <br />
					<select name="build">
						<option value="">I'll tell you later</option>
						<option value="extrapounds">A few extra pounds</option>
						<option value="athletic">Athletic / toned</option>
						<option value="average">Average</option>
						<option value="bbw">BBW/BHM</option>
						<option value="curvy">Curvy</option>
						<option value="fit">Fit</option>
						<option value="heavy">Heavyset</option>
						<option value="muscular">Muscular</option>
						<option value="slender">Slender</option>
						<option value="slim">Slim</option>
						<option value="volutptuous">Voluptuous</option>
						<option value="stocky">Stocky</option>
						<option value="thick">Thick</option>
						<option value="other">Other</option>
						<option value="bodybuild">Bodybuilder</option>
					</select>
			
			</div>
		</div>
	</body>
</html>