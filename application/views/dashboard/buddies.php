<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>WheeWhew Dashboard</title>

        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="//wheewhew.com/assets/css/dashboard.css" />

        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		
		<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>
		<script src="//wheewhew.com/assets/js/strophe.min.js"></script>
		<script src="//wheewhew.com/assets/js/jabber.js"></script>
		<script type="text/javascript">
			var BOSH_SERVICE = 'http://wheewhew.com:5280/http-bind';
			var connection = null;
			var xmpp_user = "<?php echo $xmpp_user; ?>@wheewhew.com/default";
			var xmpp_pass = "<?php echo $xmpp_password; ?>";
			var uid		  = "<?php echo $uid; ?>";
			
			$(document).ready(function () {				
				$('#btn-logout').click(logout);
				$('.add').click(addBuddy);
				$('.accept-btn').click(acceptBuddy);
				connectXMPP();
				//updateLastSeen();
				
			});
			
			
			
		</script>
    </head>

    <body>
		<div id="mainWrapper">
			<div id="top-control">
				<div id="personal-menu">
					<div id="home-anchor">
						<a href="http://wheewhew.com/dashboard"> Wheewhew </a>
					</div>
				</div>
				<div id="searchimg">
					<input type="text" name="search-box" id="search-box" placeholder="Search"> 
				</div>
				<div id="account-menu">	
					<div id="logout-bar">
						<ul>
							<li><p id="btn-request" class="buttons"><a href="#">Click this button <div id="request-div">
							<?php 
								$rows = count($buddy_requests,0); 
								if($rows == 0) {
									echo '<div id="no-request-container">You have no Buddy Requests at the moment. </div>';
								}
								
								else {
									foreach($buddy_requests as $request) {
										foreach($latest_members as $member) {
											if($request['from'] == $member['uid']){
												echo '<div class="req_container">';
												echo '<img src="https://s3.amazonaws.com/wheewhew/user/' . $member['uid'] . "/photos/" . $member['profile_pic'] . '" class="req_img" />';
												echo '<div class="member-name-request-container">' . $member['firstname']." ".$member['lastname'] . '</div>';
												echo '<div class="accept-decline-btn-container" sender-uid="' . $member['uid'] . '" id="request-' . $member['uid'] . '">';
												echo '<button type="button" class="accept-btn" id="accept-btn">' . 'Confirm' . '</button>';
												echo '<button type="button" class="decline-btn" id="decline-btn">' . 'Not Now' . '</button>';
												echo '</div>';
												echo '</div>';
				
											}
										}
									}
								}
							?>
							</div></a></p></li>
							<script>
								window.onload = function() {
									$('#btn-request').click(function () {
										$('#request-div').fadeToggle(0);
									});
								};
							</script>
							<li><p id="btn-photo" class="buttons"><a href="#">Click this button</a></p></li>
							<li><p id="btn-msg" class="buttons"><a href="#">Click this button</a></p></li>
							<li><p id="btn-notif" class="buttons"><a href="#">Click this button</a></p></li>
							<li><p id="btn-logout" class="buttons"><a href="#">Click this button</a></p></li>
						</ul>
					</div>
				</div>
				<br class="clear" />
			</div>
			
			<div id="content">
				<div id="buddies-container">
				
				<div id="buddies-header">
				 <p> Buddies </p> 
				</div>
				
				<div id="buddies-container-container">
					<?php foreach($user_buddies as $buddies) { ?>
						<?php foreach($all_members as $member) { ?>
						
							<?php if($buddies['from'] == $member['uid']){ ?>
						
							<div class="buddies" data-user="<?php echo $member['xmpp_user']; ?>" data-uid="<?php echo $member['uid']; ?>">
								<a href="http://www.wheewhew.com/member/profile/<?php echo $member['uid']; ?>"><img src="https://s3.amazonaws.com/wheewhew/user/<?php echo $member['uid']; ?>/photos/<?php echo $member['profile_pic']; ?>" class="buddy-pic" /></a>
								<div class="buddy-name"><a href="http://www.wheewhew.com/member/profile/<?php echo $member['uid']; ?>"><?php echo $member['firstname']." ".$member['lastname']; ?></a></div>
								<div class="frule"></div>
							</div>
						
							<?php } ?>
						<?php } ?>
					<?php } ?>
					</div>

				
				</div>
				</div>
							
			</div> <!-- END OF CONTENT -->
			
			<div id="new_members">

			</div>
			<div id="buddy-list">
			
			</div>
			<div id="push"><!-- Leave this element empty --></div>
		</div>
		<div id="footer">
			
		</div>
		
    </body>
</html>

