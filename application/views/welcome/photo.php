<html>
    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object#">
        <title><?php echo $title; ?></title>
        <meta property="fb:app_id" content="384283535015016" />
        <meta property="og:title" content="<?php echo $title; ?>" />
        <meta property="og:image" content="<?php echo $img_src; ?>" />
        <meta property="og:url" content="<?php echo $public_url; ?>" />
        <meta property="og:type" content="matchupthreesixty:picture" />
		
		<link rel="shortcut icon" href="//matchup360.com/assets/img/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="//matchup360.com/assets/css/canvas.css" />
		 
    </head>

    <body>
		<div id="mainWrapper">
			<!--<div id="rightBar">
				<div style="height: 50px"></div>
			</div>-->
			<!--<div id="topControl">
				<div style="float: left;width: 688px; height: 50px; border-left: 1px #287cc6 solid;">
					<div id="home-anchor">
							<a href="//matchup360.com/" id="logo"></a>
					</div>
				</div>-->
				<div style="float: right; margin-right: 158px;padding: 6px 0px;">
					<!--<a href="http://matchup360.com" style="color: white;font-weight: bold;">Login</a>-->
					<a target="_blank" href="http://matchup360.com"><button>Visit Site</button></a>
				</div>
			<!-- </div>END OF topControl -->
			<div id="content">
				<div id="page-loader">
					<img src="http://matchup360.com/assets/img/loader-big.gif">
				</div>
				<!--<div id="mainContent">-->
					<div id="photo-wrapper">
						<div id="image-box">
							<img src="<?php echo $img_src; ?>" />
						</div>
						<div id="comments" style="max-height:<?php echo $photo['height'] - 38; ?>px">
							<ul>
							<?php	
								$row_class = 'odd';
								foreach($comments as $row)
								{
									if($row['owner_id'] == $user[0]['uid'] || $row['commentor_id'] == $user[0]['uid'])
										$permission = 'data-delete-permission="true"';
									else
										$permission = '';
									echo '<li data-cid="'.$row['id'].'" class="'.$row_class.'" '.$permission.'><div class="commentor_pic"><img src="https://s3.amazonaws.com/wheewhew/user/'.$row['uid'].'/photos/'. $row['profile_pic'].'" width="35"></div><div class="commentside"><a id="commentor-name" href="./ajax/profileWidget/'.$row['uid'].'">' . $row['firstname'].' '.$row['lastname'].'</a> '.$row['comment'].'</div><div style="clear:both"></div></li>';
									if($row_class == 'odd')
										$row_class = 'even';
									else
										$row_class = 'odd';
								}
							?>	
							</ul>
						</div>
						<!--<div id="meet-buttons">
							<button <?php echo($user[0]['sex'] == 'm'? 'style="display:none"': '')?>>Meet Her</button >
							<button <?php echo($user[0]['sex'] == 'f'? 'style="display:none"': '')?>>Meet Him</button>
						</div>-->
					</div>
				<!--</div>-->
			</div> <!-- END OF content -->
		</div> <!-- END OF mainWrapper -->
	</body>
</html>


