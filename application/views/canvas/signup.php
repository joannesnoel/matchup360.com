<html>
<head>
<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.geocomplete.min.js"></script>
<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery-impromptu.js"></script>
<link rel="stylesheet" type="text/css" href="//www.matchup360.com/assets/css/canvas_signup.css">
<link rel="stylesheet" type="text/css" href="//www.matchup360.com/assets/css/jquery-impromptu.css">
</head>
<body>
<div id="fb-root"></div>
<script>
  $(document).ready(function(){
	$('#get-started-button').click(function(e){
		$('#get-started-loader').show();
		buttonWrapper = $(this).parents('p');
		buttonWrapper.hide();
		e.preventDefault();
		FB.login(function(response) {
		   if (response.authResponse) {
			 console.log('Gathering information.... ');
		   } else {
			 buttonWrapper.show();
			 $('#get-started-loader').hide();
		   }
		 },{scope: 'email,user_likes,user_photos,user_about_me,user_birthday,user_hometown,user_relationship_details,user_location,user_relationships,user_work_history,friends_online_presence'});
	});
	$('#signup-button').click(function(){
		$('#signup-button').attr('disabled','disabled').css({width: '130px'}).html('Please wait <img src="//www.matchup360.com/assets/img/loading.gif" />');
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/signup',
			type: 'post',
			data: $('#signup').serialize(),
			success: function(response){
				if(response.success){
					window.location = response.redirect;
				}else{
					$.prompt(response.errors);
					$('#signup-button').css({width: 'auto'}).removeAttr('disabled').html('Join Now!');
				}
			}
		});
	});
  });
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '384283535015016', // App ID
    channelUrl : '//www.matchup360.com/welcome/channel', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });
  var me = null
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    if (response.status === 'connected') {
		FB.api('/me', function(data) {
			me = data;
			$.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/fb_user_exist',
				type: 'post',
				data: 'fb_userid='+me.id,
				success: function(response){
					if(response.result)
						window.location = window.location.protocol + '//' + window.location.hostname + '/canvas/chat';
					else{
						if(!me.email){
							$('input[name="email"]').removeAttr('readonly');
						}
						geocoder = new google.maps.Geocoder();
						geocoder.geocode( {'address': me.location.name },
						  function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								me.address_components = results[0].address_components;
								me.current_longitude  = results[0].geometry.location.jb;
								me.current_latittude  = results[0].geometry.location.kb;
							}
							FB.api('me/picture?width=300&height=300', function(picture){
								me.picture = picture.data.url;
								FB.api('me/likes', function(likes){
									me.likes = likes.data;
									  album_id = null;
									  photos = new Array();
									  FB.api('/me/albums',function(response){
										for(var i=0; i<response.data.length; i++){
											if(response.data[i].name != 'Profile Pictures')
												continue;
											album_id = response.data[i].id;
										}
										if(album_id != null){
											FB.api('/'+album_id+'/photos',function(response){
												for(var i=0; i<response.data.length; i++){
													photos[i] = response.data[i].source;
												}
												me.photos = photos;
												$('#signup [name="me"]').val(JSON.stringify(me));
												$('#signup [name="firstname"]').val(me.first_name);
												$('#signup [name="lastname"]').val(me.last_name);
												$('#signup [name="email"]').val(me.email);
												$('#get-started-loader').hide();
												$('#welcome-box').animate({width: '900px'},200,'swing',function(){$('#get-started').show();});
												$('#signup').show();
											});
										}
									  });
								});
							});
						  }
						);
					}
				}
			});
		});
    } else if (response.status === 'not_authorized') {
	  $('#get-started-button').parents('p').show();
      //FB.login(null,{scope: 'email,user_likes,user_photos,user_about_me,user_birthday,user_hometown,user_relationship_details,user_location,user_relationships,user_work_history,friends_online_presence'});
    } else {
	  $('#get-started-button').parents('p').show();
      //FB.login(null,{scope: 'email,user_likes,user_photos,user_about_me,user_birthday,user_hometown,user_relationship_details,user_location,user_relationships,user_work_history,friends_online_presence'});
    }
  });
   FB.getLoginStatus(function(response) {
	  if (response.status === 'connected') {
		
	  } else if (response.status === 'not_authorized') {
		$('#get-started-button').parents('p').show();
	  } else {
		$('#get-started-button').parents('p').show();
	  }
   });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));
</script>
<div id="blur">
<div id="welcome-box">
	<div id="intro">
		<div id="logo"><img width="85" height="85" src="//www.matchup360.com/assets/img/matchup360-130.jpg" /></div>
		<h1>Matchup360</h1>
		<p>Do you want to meet new people, participate to discussion group on topics that interest you, or just be a listener and see what other people have to say.</p>
		<p>Matchup360's Chat application in Facebook makes it easier for anyone to interact with others that are not in your friends network.</p>
		<p>We at Matchup360 think that room-based chatting as an extension to the existing social network sites would be fun! So, hop in and enjoy the Matchup360 App here in Facebook.</p>
		<p align="right" style="display:none; line-height: 32px;vertical-align: top;"><a href="#" id="get-started-button"><img src="//www.matchup360.com/assets/img/get-started.jpg"/></a></p>
		<p align="right" id="get-started-loader" style="display:none; line-height: 32px;vertical-align: top;">Please wait <img src="//www.matchup360.com/assets/img/loading.gif" /></p>
	</div>
	<div id="get-started">
		<h2>New User Signup</h2>
		<form name="signup" id="signup" method="post">
			<div><label for="firstname">Firstname </label><input type="text" name="firstname" id="firstname" value="" readonly/></div>
			<div><label for="lastname">Lastname </label><input type="text" name="lastname" id="lastname" value="" readonly/></div>
			<div><label for="email">Email </label><input type="text" name="email" id="email" value="" readonly/></div>
			<div><label for="password">Password </label><input type="password" name="password" id="password" value="" /></div>
			<div><label for="cpassword">Confirm Password </label><input type="password" name="cpassword" id="cpassword" value="" /></div>
			<input type="hidden" name="me" value="" />
			<button id="signup-button" type="button">Join Now!</button>
			<div style="clear:both"></div>
		</form>
	</div>
</div>
</div>
<div id="windowSize">
<div id="page_container">
<?php
$ctr = 0;
foreach($photos as $photo){ ?>
<div class="photocollage"><img src="<?php echo config_item('s3_bucket_url').$photo['uid'].'/photos/'.$photo['filename']; ?>" /></div>
<?php
	$ctr++;
	if($ctr == 30)
		break;
} ?>
<div style="clear:both"></div>
</div>
</div>
</body>
</html>