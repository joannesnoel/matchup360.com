<html>
	<head>
		<link rel="stylesheet" href="//www.matchup360.com/assets/css/canvas.css" /> 
		<link rel="stylesheet" href="//matchup360.com/assets/css/colorbox.css" />
		<script type="text/javascript" src="//matchup360.com/assets/js/jquery.min.js"></script>
		<script src="//matchup360.com/assets/js/jquery.colorbox-min.js"></script>
		<link rel="stylesheet" href="//matchup360.com/assets/css/jquery-impromptu.css" />
		<script type="text/javascript" src="//matchup360.com/assets/js/jquery-impromptu.js"></script>
		<script src="//matchup360.com/assets/js/strophe.min.js"></script>
	</head>
	<body style="overflow: hidden">
	<div id="fb-root"></div>
	<script>
	var BOSH_SERVICE;
	var connection = null;
	var xmpp_user = null;
	var xmpp_pass = null;
	var xmpp_host = '@matchup360.com';
	function connectXMPP(){
		if(window.location.protocol == 'https:')
			BOSH_SERVICE = 'https://matchup360.com:5281/http-bind';
		else
			BOSH_SERVICE = 'http://matchup360.com:5280/http-bind';
		connection = new Strophe.Connection(BOSH_SERVICE);
		
		// Strophe debugging
		connection.rawInput = function(a) { console.log("in:", a);};
		connection.rawOutput = function(a) {console.log("out:", a);};
		
		connection.connect(xmpp_user+xmpp_host,
								xmpp_pass,
								onConnect);
	}
	function onConnect(status)
	{
		if (status === Strophe.Status.CONNECTED) { 
			connection.addHandler(onSubscriptionRequest, null, "presence", "subscribe");
			connection.addHandler(presenceHandler, null, 'presence');
			connection.addHandler(notificationHandler, null, 'message' , 'notification');
			connection.send($pres().c('show').t('chat').up().c('priority').t('1').tree());
			connection.sendIQ($iq({type: "get"}).c("query", {xmlns: 
								Strophe.NS.ROSTER}), manageRoster);
			updateRoster();
		}
	}
	function presenceHandler(stanza){
		from = stanza.getAttribute("from");
		debugger;
	}
	function notificationHandler(){
	
	}
	function updateRoster(){
		users = $('.online-status');
		for(var i=0; i<users.length; i++){
			connection.send($pres({ to: $(users[i]).attr('data-peerjid')+xmpp_host, type: "subscribe" }));
		}
	}
	function onSubscriptionRequest(stanza){
		if(stanza.getAttribute("type") == "subscribe" && stanza.getAttribute("from")){
			connection.send($pres({ to: stanza.getAttribute("from"), type: "subscribed" }));
		}
		return true;
	}
	function manageRoster(stanza){
		items = $(stanza).find('item');
		for(var i=0; i<items.length; i++){
			xmpp_user = $(items[0]).attr('jid').split('@')[0];
			if($('.online-status[data-peerjid="'+xmpp_user+'"]').length<1)
				connection.send($pres({ to: xmpp_user+xmpp_host, type: "unsubscribe" }));
		}
	}

	var me = {};
	var geo_options = {
			  types: ['(cities)']
			 };
	var photos = [];
  // Additional JS functions here
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '384283535015016', // App ID
      channelUrl : '//welcome/channel', // Channel File
	  frictionlessRequests : true,
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
  FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationship_details,user_photos,publish_actions'});
  FB.Event.subscribe('auth.login',function(){
  
  });
  FB.Event.subscribe('auth.statusChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
	  access_token =   FB.getAuthResponse()['accessToken'];
      FB.api('/me',function(response){
		me.fb_userid = response.id;
		me.firstname = response.first_name;
		me.lastname  = response.last_name;
		me.email	 = response.email;
		me.gender	 = response.gender;
		me.birthdate = response.birthday;
		me.bio		 = response.bio;
		me.quotes	 = response.quotes;
		me.work		 = response.work;
		if(response.location){
			$.ajax({
				url:'https://maps.googleapis.com/maps/api/geocode/json?address='+response.location.name+'&sensor=false',
				success: function(geo){
					if(geo.results){
						me.address_components = geo.results[0].address_components;
						me.current_longitude = geo.results[0].geometry.location.lng;
						me.current_lattitude = geo.results[0].geometry.location.lat;
					}
				}
			});
		}
		me.interested_in = response.interested_in;
		me.languages = response.languages;
		FB.api('/me/picture?width=400&height=400',function(response){
			if(response.data.is_silhouette == false)
				me.picture = response.data.url;
		});
		FB.api('/me/likes',function(response){
			me.likes = response.data;
			album_id = null;
			  FB.api('/me/albums',function(response){
				for(var i=0; i<response.data.length; i++){
					if(response.data[i].name != 'Profile Pictures')
						continue;
					album_id = response.data[i].id;
				}
				if(album_id != null){
					FB.api('/'+album_id+'/photos',function(response){
						for(var i=0; i<response.data.length; i++){
							photos[i] = response.data[i].source;
						}
						me.photos = photos;
						initMatchup360();
					});
				}
			  });
	    });
	  });
    } else if (response.status === 'not_authorized') {
      FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationship_details,user_photos,publish_actions'});
    } else {
      FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationship_details,user_photos,publish_actions'});
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
	function runApp(){
		connectXMPP();
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/recent_invitation',
			type: 'post',
			success: function(response){
				var friends = [];
				if(response.invites.length < 50){
					FB.api('/me/friends',function(fb_response){
						fb_response.data.sort(function() {return 0.5 - Math.random()});
						if(fb_response.data.length > 35)
							friends_count = 35;
						else
							friends_count = fb_response.data.length;
						for(var i=0; i<friends_count; i++){
							friends.push(fb_response.data[i].id);
						}
						to_friends=friends.join(",");
						FB.ui({
						  method: 'apprequests',
						  message: 'Matchup360 is a fun way of interacting with people in a specified location. You simply set your location, adjust your radar and mingle with people! Aside from that, we match you with people that may share the same interest with you.',
						  to: to_friends,
						  data: '{"fbinvite":'+response.uid+'}'
						},function(response_apprequest){
							$.ajax({
								url: window.location.protocol + '//' + window.location.hostname + '/ajax/fb_invite_multiple',
								type: 'post',
								data: 'fb_userids='+JSON.stringify(response_apprequest.to),
								success: function(invite_response){
									$.prompt('<p>'+invite_response.invited+' people invited. '+invite_response.duplicated+' invites ignored as you have invited them in the past.</p>');
									$('#matchup_coins i').html(parseInt($('#matchup_coins i').html())+invite_response.invited*10);
									return true;
								}
							});
						});
					});
				}
			}
		});
		$.colorbox({scrolling:false,href: window.location.protocol + '//' + window.location.hostname + '/widgets/roulette', overlayClose: false, closeButton: false});
	}
	function check_user_exist(){
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/fb_user_exist',
			type: 'post',
			async: true,
			data: 'fb_userid='+me.fb_userid,
			success: function(response){
				if(response.result == false){
					$.colorbox({scrolling:false,href: window.location.protocol + '//' + window.location.hostname + '/widgets/quick_register', overlayClose: false, closeButton: false});
				}else{
					check_profile_photo(response.uid);
					xmpp_user = response.xmpp_user;
					xmpp_pass = response.xmpp_pass;
				}
			}
		});
	}
	function check_profile_details(uid){
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/has_profile_details',
			type: 'post',
			async: true,
			data: 'uid='+uid,
			success: function(response){
				if(response.result == false){
					$.colorbox({overlayClose:false,closeButton:false,scrolling:false,href: window.location.protocol + '//' + window.location.hostname + '/widgets/my_appearance' });
				}else{
					runApp();
				}
			}
		});
	}
    function check_profile_photo(uid){
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax_canvas/has_profile_photo',
			type: 'post',
			async: true,
			data: 'uid='+uid,
			success: function(response){
				if(response.result == false){
					$.colorbox({overlayClose:false,closeButton:false,scrolling:false,href: window.location.protocol + '//' + window.location.hostname + '/widgets/my_photos' });
				}else{
					check_profile_details(uid);
					xmpp_user = response.xmpp_user;
					xmpp_pass = response.xmpp_pass;
				}
			}
		});
	}
	function initMatchup360(){
		check_user_exist();
	}
</script>
		<div id="messagesBar">
			<i id="message_notification" class="notification">0</i>
			<div class="ticker">Messages</div>
			<div style="position:relative">
				<div id="threadsList">
			<?php	if(count($message_threads) > 0){ 
						foreach($message_threads as $thread){ ?>
						<div class="thread online-status" data-peerjid="<?php echo $thread['xmpp_user']; ?>" data-peerid="<?php echo $thread['uid']; ?>">
							<img src="<?php echo $thread['profile_pic'] ?>" width="50" />
							<div class="name"><?php echo $thread['firstname'] .' '. $thread['lastname']; ?></div>
							<div class="message"><?php echo $thread['message']; ?></div>
						</div>
				<?php   }
					}else{ ?>
					<p align="center">Your conversations will appear here.</p>
				<?php } ?>
				</div>
				<div id="messagesBox">
					<div id="messengerBox">
						<textarea style="display: none"></textarea>
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
		</div>
		<div id="blur">
		</div>
	</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$('#page-container').css({'width':$(window).width() + 200});
		$('#messagesBar .ticker').click(function(e){
			messagesBar = $(this).parent();
			if(messagesBar.css('left') == '0px'){
				messagesBar.animate({'left':'-670px'},400);
				$('#messageBoxOverlay').animate({'opacity':'0.'},400,null,function(){$(this).hide();});
			}else{
				messagesBar.animate({'left':'0px'},400);
				$('#message_notification').html('0').hide();
				$('#messageBoxOverlay').css('opacity',0).show().animate({'opacity':'0.5'},400);
			}
			e.stopPropagation();
		});
	});
</script>