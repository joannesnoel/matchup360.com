<html>
	<head>
		<link rel="stylesheet" href="//matchup360.com/assets/css/dashboard.css" />
	</head>
	<body>
		<div id="public-profile">
			<div id="left-box">
				<div style="margin-right: 500px;">
					<img src="<?php echo $user['profile_pic']?>" width="404"> 
					<?php foreach($photos as $photo){ ?>
						<img src="<?php echo config_item('s3_bucket_url').$photo['uid'].'/photos/'.$photo['filename']?>" width="200" height="200">
					<?php }?>
					<div style="clear: both"></div>
				</div>
			</div>
			<div id="right-box">
				<div>
					<div id="profile_name">
						<span style="font-weight: bold"><?php echo $user['firstname'].' '.$user['lastname']?></span>
						<span style="font-size: 16px"> -- Age <?php echo $user['age']?></span>
					</div>
					<div id="profile_address"><span><?php echo $user['city'].', '.$user['state'].', '.$user['country']?></span></div>
					<div id="profile_linkoff"><a href="http://<?php echo config_item('site_domain').'/view_profile/'.$user['uid']?>" target="_blank"><button><span>Meet <?php echo($user['sex'] == 'f'? 'Her' : 'Him') ?></span></button></a></div>
				</div>
				<div>
					<?php if(!empty($user_appearance)&&
								(isset($user_appearance['appearance']['ethnicity'])||
								isset($user_appearance['appearance']['nationality'])||
								isset($user_appearance['appearance']['eye_color'])||
								isset($user_appearance['appearance']['hair_color'])||
								isset($user_appearance['bodyart']['name']))){ ?>
						<div class="content_header"><h4>Appearance</h4></div>
						<div class="info_content">
							<table id="appearance-table">
							<?php
							if(isset($user_appearance['appearance']['ethnicity'])){
								echo '<tr><td class="content-label">Ethnicity : </td>
										<td>'.$user_appearance['appearance']['ethnicity'].'</td></tr>'; 
							}
							if(isset($user_appearance['appearance']['nationality'])){
								echo '<tr><td class="content-label">Nationality :</td>
										<td>'.$user_appearance['appearance']['nationality'].'</td></tr>'; 
							}
							if(isset($user_appearance['appearance']['eye_color'])){
								echo '<tr><td class="content-label">Eye color : </td>
										<td>'.$user_appearance['appearance']['eye_color'].'</td></tr>'; 
							}
							if(isset($user_appearance['appearance']['hair_color'])){
								echo '<tr><td class="content-label">Hair color : </td>
										<td>'.$user_appearance['appearance']['hair_color'].'</td></tr>'; 
							}
							if(isset($user_appearance['bodyart']) && count($user_appearance['bodyart']) > 0){
								echo '<tr><td class="content-label">Body Art :</td><td>'.
								implode(', ',$user_appearance['bodyart']).'</td></tr>';
							}
							?>
							</table>
						</div>
					<?php } ?> <!-- end of appearance -->
					<?php if(!empty($user_situation) && (
								isset($user_situation['status']['marital_status'])||
								isset($user_situation['status']['has_children'])||
								 isset($user_situation['pets']) && count($user_situation['pets']))){ ?>
						<div class="content_header"><h4>Situation</h4></div>
						<div  class="info_content">
						<table id="situation-table">	
							<?php
								if(isset($user_situation['status']['marital_status'])){
								echo '<tr><td class="content-label">Marital Status : </td>
										<td>'.$user_situation['status']['marital_status'].'</td></tr>'; 
								}
								if(isset($user_situation['status']['has_children'])){
									echo '<tr><td class="content-label">Have Children : </td>
											<td>'.$user_situation['status']['has_children'].'</td></tr>'; 
								}
								if(isset($user_situation['pets']) && count($user_situation['pets']) > 0){
									echo '<tr><td class="content-label">Pets :</td><td>'.
									implode(', ',$user_situation['pets']).'</td></tr>';
								}
							?>
						</table>
						</div>
					<?php } ?> <!-- end of situation -->
					<?php if(!empty($user_eduemploy) && 
						(isset($user_eduemploy['eduemploy']['level']) || 
						isset($user_eduemploy['eduemploy']['specialty']) ||
						isset($user_eduemploy['eduemploy']['employment']) ||
						(isset($user_eduemploy['eduemploy']['job_title']) && $user_eduemploy['eduemploy']['job_title'] != '' ))){ ?>
						<div class="content_header"><h4>Education and Employment</h4></div>
						<div  class="info_content">
						<table id="eduemploy-table">	
							<?php
								if(isset($user_eduemploy['eduemploy']['level'])){
								echo '<tr><td class="content-label">Education Level : </td>
										<td>'.$user_eduemploy['eduemploy']['level'].'</td></tr>'; 
								}
								if(isset($user_eduemploy['eduemploy']['specialty'])){
									echo '<tr><td class="content-label">Specialty : </td>
											<td>'.$user_eduemploy['eduemploy']['specialty'].'</td></tr>'; 
								}
								if(isset($user_eduemploy['eduemploy']['employment'])){
									echo '<tr><td class="content-label">Employment Status : </td>
											<td>'.$user_eduemploy['eduemploy']['employment'].'</td></tr>'; 
								}
								if(isset($user_eduemploy['eduemploy']['job_title']) && $user_eduemploy['eduemploy']['job_title'] != ''){
									echo '<tr><td class="content-label">Job Title : </td>
											<td>'.$user_eduemploy['eduemploy']['job_title'].'</td></tr>'; 
								}
							?>
						</table>					
						</div>
					<?php } ?> <!-- end of education and employment -->
					<?php if(!empty($user_leisure)){ ?>
						<div class="content_header"><h4>Leisure and Fun</h4></div>
						<div class="info_content">
						<table id="leisure-table">	
							<?php
								if(isset($user_leisure['tv_genre']) && count($user_leisure['tv_genre']) > 0){
									echo '<tr><td class="content-label">TV shows: </td><td>'.implode(', ',$user_leisure['tv_genre']).'</td></tr>';
								}
								if(isset($user_leisure['movie_genre']) && count($user_leisure['movie_genre']) > 0){
									echo '<tr><td class="content-label">Movie types: </td><td>'.implode(', ',$user_leisure['movie_genre']).'</td></tr>';
								}
								if(isset($user_leisure['music_genre']) && count($user_leisure['music_genre']) > 0){
									echo '<tr><td class="content-label">Music types: </td><td>'.implode(', ',$user_leisure['music_genre']).'</td></tr>';
								}
								if(isset($user_leisure['book_genre'])&& count($user_leisure['book_genre']) > 0){
									echo '<tr><td class="content-label">Book types: </td><td>'.implode(', ',$user_leisure['book_genre']).'</td></tr>';
								}
								if(isset($user_leisure['hobbies']) && count($user_leisure['hobbies']) > 0){
									echo '<tr><td class="content-label">My hobbies: </td><td>'.implode(', ',$user_leisure['hobbies']).'</td></tr>';
								}
								
							?>
						</table>
						</div>
					<?php } ?> <!-- end of leisure and fun -->
					<?php if(!empty($user_personality) && 
								(isset($user_personality['personality']['smoke'])||
								isset($user_personality['personality']['drink'])||
								isset($user_personality['personality']['socia_behaviour'])||
								isset($user_personality['enjoy']['name']))){ ?>
						<div class="content_header"><h4>Personality</h4></div>
						<div class="info_content">
						<table id="personality-table">
							<?php
								if(isset($user_personality['personality']['smoke'])){
								echo '<tr><td class="content-label">Smoking habit: </td>
										<td>'.$user_personality['personality']['smoke'].'</td></tr>'; 
								}
								if(isset($user_personality['personality']['drink'])){
									echo '<tr><td class="content-label">Drinking habit: </td>
											<td>'.$user_personality['personality']['drink'].'</td></tr>'; 
								}
								if(isset($user_personality['personality']['social_behaviour'])){
									echo '<tr><td class="content-label">They say that I am: </td>
											<td>'.$user_personality['personality']['social_behaviour'].'</td></tr>'; 
								}
								if(isset($user_personality['enjoy']) && count($user_personality['enjoy']) > 0){
									echo '<tr><td class="content-label">Idea of great time: </td><td>'.implode(', ',$user_personality['enjoy']).'</td></tr>';
								}
							?>
						</table>
						</div>
					<?php } ?> <!-- end of personality -->
					<?php if(!empty($user_views)&&
								(isset($user_views['political'])||
								isset($user_views['religion']))){ ?>
						<div class="content_header"><h4>Views</h4></div>
						<div class="info_content">
						<table id="views-table">
							<?php
								if(isset($user_views['political'])){
								echo '<tr><td class="content-label">Political View: </td>
										<td>'.$user_views['political'].'</td></tr>'; 
								}
								if(isset($user_views['religion'])){
									echo '<tr><td class="content-label">Religion: </td>
											<td>'.$user_views['religion'].'</td></tr>'; 
								}
							?>
						</table>
						</div>
					<?php } ?> <!-- end of views -->
						<?php if(!empty($user_looking['looking'])){ ?>
						<div class="content_header"><h4>Looking for</h4></div>
						<div class="info_content">
						<table id="looking-table">
							<?php
								if(isset($user_looking['looking']) && count($user_looking['looking']) > 0){
									echo '<tr><td class="content-label">A person with :</td><td>'.
									implode(', ',$user_looking['looking']).'</td></tr>';
								}
								
							?>
						</table>
						</div>
					<?php } ?><!-- end of looking for -->
				</div>
			</div>
			<div style="clear: both"></div>
		</div>
	</body>
</html>