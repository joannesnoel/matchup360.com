<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="//www.matchup360.com/assets/chat/res/default.css" />
	<link rel="stylesheet" type="text/css" href="//www.matchup360.com/assets/css/canvas_chat.css" />
	<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery-impromptu.js"></script>
	<link rel="stylesheet" type="text/css" href="//www.matchup360.com/assets/css/jquery-impromptu.css">
	<script type="text/javascript" src="//www.matchup360.com/assets/chat/libs/libs.min.js"></script>
	<script type="text/javascript" src="//www.matchup360.com/assets/chat/candy.bundle.js"></script>
	<script type="text/javascript">
		var picture = '<?php echo $profile_pic; ?>';
		var firstname = '<?php echo $firstname; ?>';
		var lastname = '<?php echo $lastname; ?>';
		var user_id = '<?php echo $fb_userid; ?>';
		if(window.location.protocol == 'https:')
			BOSH_SERVICE = 'https://www.matchup360.com:5281/http-bind';
		else
			BOSH_SERVICE = 'http://www.matchup360.com:5280/http-bind';
		$(document).ready(function() {
			Candy.init(BOSH_SERVICE, {
				core: { debug: true, autojoin: ['world@conference.matchup360.com',<?php echo $rooms ?>] },
				view: { resources: '../../assets/chat/res/', language: 'en' }
			});
			Candy.Core.connect('<?php echo $xmpp_user; ?>@matchup360.com', '<?php echo $xmpp_pass; ?>');
		});
		function roomInvite(roomName){
			$('#'+roomName+'-invite').attr('disabled','disabled');
			FB.api('/me/permissions',function(response){
				if(response.data.friends_online_presence){
					loadInvite(roomName);
				}else{
					 FB.login(function(response) {
						if(response.authResponse){
							loadInvite(roomName);
						}
					 }, {scope: 'friends_online_presence,publish_actions'});
				}
			});
		}
		function loadInvite(roomName){
			$('#'+roomName+'-invite').removeAttr('disabled');
			FB.api({
								method:'fql.query',
								query:'SELECT uid, name, pic_square, online_presence FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me()) ORDER BY online_presence'
							}, function(response){
							liArr = '';
							for(var i=0;i<response.length; i++){
								friend = response[i];
								if(!friend.installed){
									if(friend.online_presence == 'active')
										online_presence = '<i class="active">&#8226;</i>';
									else if(friend.online_presence == 'idle')
										online_presence = '<i class="idle">&#8226;</i>';
									else
										online_presence = '';
									liArr += '<li><img width="30" height="30" src="'+friend.pic_square+'"/>'+online_presence+'<span class="facebook_name">'+friend.name+'</span><button class="room_invite" data-id="'+friend.uid+'">Invite</button></li>';
								}
							}
							friendsListContext = '<div id="fiends-list"><ul>'+liArr+'</ul></div>';
							$.prompt(friendsListContext,{
								buttons: {Done:true},
								loaded: function(){
									$('.room_invite').unbind().click(function(){
										the_button = $(this);
										to_id = the_button.attr('data-id');//$.map(z, function (value, key) { return key.split('[')[1].replace('[','').replace(']',''); }).join(',');
										 // calling the API ...
										var obj = {
										  method: 'feed',
										  to: to_id,
										  link: 'https://apps.facebook.com/matchupthreesixty/?room='+roomName,
										  picture: 'https://www.matchup360.com/assets/img/matchup360-130.jpg',
										  name: 'Join the #'+roomName+' chatroom!',
										  description: 'You are invited you to join the #'+roomName+' chatroom!'
										};
										FB.ui(obj, function(response){
											if(response){
												the_button.attr('disabled','disabled').css('background-color','#999');
											}
										});
									});
								},
								submit: function(w,x,y,z){
									
								}
							});
							/*
								fb_uid = $(this).attr('data-fbuid');
								to_array = $("#invite-friends input:checkbox:checked").map(function(){
																							  return $(this).val();
																							}).get();
								var obj = {
								  method: 'send',
								  to: fb_uid,
								  link: 'http://www.matchup360.com/?fbinvite='+uid,
								  picture: 'https://www.matchup360.com/assets/img/matchup360-130.jpg',
								  name: 'Matchup360',
								  caption: 'Find your match around',
								  description: 'Matchup360 is a fun way of interacting with people in a specified location. You simply set your location, adjust your radar and mingle with people! Aside from that, we match you with people that may share the same interest with you.'
								};
								function callback(response) {
									if(response){
										$.ajax({
											url: window.location.protocol + '//' + window.location.hostname + '/ajax/fb_invite',
											type: 'post',
											data: 'fb_uid='+fb_uid,
											success: function(response){
												$.colorbox.close();
												if(!response.success)
													$.prompt(response.message);
												else
													$('#matchup_coins i').html(parseInt($('#matchup_coins i').html())+10);
											}
										});
									}
								}
								FB.ui(obj, callback);
							*/
					});
			
			
					// room_jid = $(this).parents('.room-pane').attr('data-roomjid');
					// FB.ui({method: 'apprequests',
					  // message: 'I am inviting you to Chat360',
					// }, function(response){
						// if(response){
							// $('.invite-bubble').fadeOut();
						// }
					// });
		}
	</script>
</head>
<body>
	<div id="fb-root"></div>
<script>
	function likeBox(){
		FB.api('/me/likes/1390639437820162',function(result) {
			if (result.data.length){  
			} else { 
				$('#likeBox').show();
				$('#likeBox i').unbind().click(function(){
					$(this).parent().hide();
				});
				setTimeout(likeBox,1000*60*10);
			}
		});
	}
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '384283535015016', // App ID
    channelUrl : '//www.matchup360.com/welcome/channel', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });
	FB.Event.subscribe('auth.authResponseChange', function(response) {
		// Here we specify what we do with the response anytime this event occurs. 
		if (response.status === 'connected') {
		  likeBox();
		}
	  });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));
</script>
	<div id="top-bar">
		<img src="//www.matchup360.com/assets/img/matchup-chat360.png"/>
	</div>
	<div id="candy"></div>
	<div id="likeBox">
		<i>x</i>
		<div class="fb-like-box" data-href="https://www.facebook.com/Matchup360" data-width="292" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
	</div>
</body>
</html>