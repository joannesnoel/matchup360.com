<html>
	<head>
		<link rel="stylesheet" href="//www.matchup360.com/assets/css/canvas.css" /> 
		<script type="text/javascript" src="//matchup360.com/assets/js/jquery.min.js"></script>
	</head>
	<body style="overflow: hidden">
		<div id="page-container">
			<?php
			$ctr = 0;
			foreach($photos as $photo){ ?>
			<div class="photocollage"><img src="<?php echo config_item('s3_bucket_url').$photo['uid'].'/photos/'.$photo['filename']; ?>" /></div>
			<?php
				$ctr++;
				if($ctr == 30)
					break;
			} ?>
			<div style="clear:both"></div>
		</div>
		<div id="blur">
			<div id="notification-box">
				<?php echo $snippet; ?>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		$('#page-container').css({'width':$(window).width() + 200});
	});
</script>