<html>
	<head>
		<link rel="stylesheet" href="//www.matchup360.com/assets/css/canvas.css" /> 
		<script type="text/javascript" src="//matchup360.com/assets/js/jquery.min.js"></script>
	</head>
	<body style="overflow: hidden">
	<div id="fb-root"></div>
	<script>
  // Additional JS functions here
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '384283535015016', // App ID
      channelUrl : '//welcome/channel', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
  /*FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
	  $("#use_facebook").hide();
	  access_token =   FB.getAuthResponse()['accessToken'];
      FB.api('/me',function(response){
		var birthdate = response.birthday.split("/");
		$( "#birthdate" ).datepicker("setDate",birthdate[2]+'-'+birthdate[0]+'-'+birthdate[1]);
		geo_options.location = response.location.name;
		$("input#firstname").val(response.first_name);
		$("input#lastname").val(response.last_name);
		$("input#email").val(response.email);
		$("input#email-verify").val(response.email);
		$("input#access_token").val(access_token);
		if(response.gender == 'male')
			$('select#sex').val('m');
		else
			$('select#sex').val('f');
		if(response.languages){
			for(var i=0; i<response.languages.length; i++){
				$('[data-language="'+response.languages[i].name+'"]').attr('checked','checked');
			}
		}
	  });
	  album_id = null;
	  photos = new Array();
	  FB.api('/me/albums',function(response){
		for(var i=0; i<response.data.length; i++){
			if(response.data[i].name != 'Profile Pictures')
				continue;
			album_id = response.data[i].id;
		}
		if(album_id != null){
			FB.api('/'+album_id+'/photos',function(response){
				for(var i=0; i<response.data.length; i++){
					photos[i] = response.data[i].source;
				}
			});
		}
	  });
    } else if (response.status === 'not_authorized') {
      FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
    } else {
      FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
    }
  }); */
  };

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>
		<div id="page-container">
			<?php
			$ctr = 0;
			foreach($photos as $photo){ ?>
			<div class="photocollage"><img src="<?php echo config_item('s3_bucket_url').$photo['uid'].'/photos/'.$photo['filename']; ?>" /></div>
			<?php
				$ctr++;
				if($ctr == 30)
					break;
			} ?>
			<div style="clear:both"></div>
		</div>
		<div id="blur">
			<div id="welcome-box">
				<div id="intro">
					<div id="logo"></div>
					<h1>Matchup 360</h1>
					<p>Matchup360 is a fun way of interacting with people in a specified location. You simply set your location, adjust your radar and mingle with people! Aside from that, we match you with people that may share the same interest with you.</p>
					<p>Create an account (if you don't have any yet), tell us where you are located or any other area that you might be interested in, and mingle! Have fun!</p>
				</div>
				<div id="get-started" class="vertical-align-center-parent">
					<div id="coming-soon" class="vertical-align-center-child">
						<h1>Soon on facebook apps</h1>
						<hr />
						<p><a href="http://<?php echo config_item('site_domain')?>" target="_blank"> 
							<button class="button" id="visit_website">Visit Website</button></a>
						</p>
					</div> 
				</div>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		debugger;
		$('#page-container').css({'width':$(window).width() + 200});
	});
	function authorize(){
		FB.login(null,{scope: 'email,user_likes,user_birthday,user_location,user_about_me,user_relationships,user_photos'});
	}
</script>