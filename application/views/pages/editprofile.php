		<div id="edit-profile-container">
			<div id="personal-description-header" style="background-color: #dd7499" box-name="personal-description" class="toggle-header"><label class="toggle">My Personal Description </label></div>
			<div id="personal-description-box" style="background-color: #dd7499" class="toggle-box"> 
			<form class="edit-form" id="personal-description-form" name="personal-description-form" method="post">
			
				<label class="leftside" for="sex"> Gender </label>
					<div class="radio">
						<input id="male" type="radio" name="sex" value="m"> 
							<label for="male">Male</label> 
					</div>
					<div class="radio">
						<input id="female" type="radio" name="sex" value="f"> 
							<label for="female">Female</label>
					</div>
				
				<label class="leftside" for="birthday-picker"> Birthdate </label>
					<div class="birthdate-select-style">
						<select id="monthdropdown" name="month"></select> 
					</div>
					<div class="birthdate-select-style">
						<select id="daydropdown" name="day"></select> 
					</div>
					<div class="birthdate-select-style">
						<select id="yeardropdown" name="year"></select>
					</div>
				
				<label class="leftside" for="hometown">Hometown/City:</label>
										<input class="rightside" type="text" id="hometown" />
										<i class="field_error" id="hometown_error"></i>
										<input type="hidden" name="hometown_components" id="hometown_components" />
				<div style="clear:both"></div>
				<label class="leftside" for="profile-heading"> My profile heading is </label>
				<input class="rightside" type="text" id="profile-heading" name="profile-heading">
				<div style="clear:both"></div>
				<label class="leftside" for="profile-introduction"> My profile introduction </label>
				<textarea class="rightside" id="profile-introduction" name="profile-introduction" ></textarea>
				<div style="clear:both"></div>
			<div style="clear:both"></div>	
			<fieldset>
				<label> I can speak </label>
					<?php 
					$ctr = 0;
					foreach($languages as $language){ ?>
						<div class="language checkbox"><input id="language[<?php echo $language['id']; ?>]" type="checkbox" name="language[<?php echo $language['id']; ?>]" /> <label for="language[<?php echo $language['id']; ?>]"><?php echo $language['name']; ?></label></div>
					<?php 
						$ctr++;
					} ?>
			</fieldset>
				<div style="clear:both"></div>	
				<div class="box-buttons">
					<button type="button" id="update-personal-description"> Update </button>
				</div>
			</form>
			</div>
		<!--
			<div id="interested-in-header" style="background-color: #91d152" box-name="interested-in" class="toggle-header"><label class="toggle">I am interested in... </label></div>
			<div id="interested-in-box" style="background-color: #91d152" class="toggle-box">
			<form class="edit-form" id="interested-in-form" name="interested-in-form" method="post">
				<?php 
					$ctr = 0;
					foreach($interested_in as $interest){ ?>
						<div class="interest checkbox"><input id="interested_in[<?php echo $interest['id']; ?>]" type="checkbox" name="interested_in[<?php echo $interest['id']; ?>]" /> <label for="interested_in[<?php echo $interest['id']; ?>]"><?php echo $interest['name']; ?></label></div>
					<?php 
						$ctr++;
						//if($ctr % 3 == 0){
							//echo '';
							//$ctr = 0;
						//}
					} ?>
					<div style="clear:both"></div>
				
				<div class="box-buttons">
					<button type="button" value="update" id="update-interested-in"> Update </button>
				</div>
			
			</form>
			</div>
		-->	
			<div id="appearance-header" style="background-color: #3894e7" box-name="appearance" class="toggle-header"><label class="toggle">My appearance </label></div>
			<div id="appearance-box" style="background-color: #3894e7" class="toggle-box">
			<form class="edit-form" id="appearance-form" name="appearance-form" method="post">
			
				<label class="leftside" for="ethnicity"> My ethnicity is </label>
				<div class="select-style">
					<select name="ethnicity">
						<option value="later"> I'll tell you later </option>
					<?php
						foreach($ethnicities as $ethnicity){
					?>
						<option value="<?php echo $ethnicity['id'] ?>"><?php echo $ethnicity['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
				<label class="leftside" for="nationality"> My nationality is </label>
				<div class="select-style">
					<select name="nationality">
						<option value="">I'll tell you later</option>
					<?php
						foreach($nationalities as $nationality){
					?>
						<option value="<?php echo $nationality['id'] ?>"><?php echo $nationality['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
				<label class="leftside" for="height"> My height is </label>
				<div class="select-style">
					<select name="height">
						<option value="">I'll tell you later</option>
						<option value="150">4' 11" (150 cm) or shorter</option>
						<option value="152">5' 0" (152 cm)</option>
						<option value="155">5' 1" (155 cm)</option>
						<option value="157">5' 2" (157 cm)</option>
						<option value="160">5' 3" (160 cm)</option>
						<option value="163">5' 4" (163 cm)</option>
						<option value="165">5' 5" (165 cm)</option>
						<option value="168">5' 6" (168 cm)</option>
						<option value="170">5' 7" (170 cm)</option>
						<option value="173">5' 8" (173 cm)</option>
						<option value="175">5' 9" (175 cm)</option>
						<option value="178">5' 10" (178 cm)</option>
						<option value="180">5' 11" (180 cm)</option>
						<option value="183">6' 0" (183 cm)</option>
						<option value="185">6' 1" (185 cm)</option>
						<option value="188">6' 2" (188 cm)</option>
						<option value="191">6' 3" (191 cm)</option>
						<option value="193">6' 4" (193 cm)</option>
						<option value="196">6' 5" (196 cm)</option>
						<option value="198">6' 6" (198 cm)</option>
						<option value="200">6' 7" (200 cm)</option>
						<option value="203">6' 8" (203 cm)</option>
						<option value="206">6' 9" (206 cm)</option>
						<option value="208">6' 10" (208 cm)</option>
						<option value="211">6' 11" (211 cm)</option>
						<option value="213">7' 0" (213 cm) or taller</option>
					</select>
				</div>
				
				
				<label class="leftside" for="eye-color"> My eye color is </label>
				<div class="select-style">
					<select name="eye_color">
						<option value="">I'll tell you later</option>
					<?php
						foreach($eye_colors as $color){
					?>
						<option value="<?php echo $color['id'] ?>"><?php echo $color['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
				<label class="leftside" for="hair-color"> My hair color is </label>
				<div class="select-style">
					<select name="hair_color">
						<option value="">I'll tell you later</option>
					<?php
						foreach($hair_colors as $color){
					?>
						<option value="<?php echo $color['id'] ?>"><?php echo $color['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
				
				<div style="clear:both"></div>
				<fieldset>
				<label> My body art is </label>	
					<?php 
					$ctr = 0;
					foreach($body_arts as $body_art){ ?>
						<div class="interest checkbox"><input id="body_art[<?php echo $body_art['id']; ?>]" type="checkbox" name="body_art[<?php echo $body_art['id']; ?>]" /> <label for="body_art[<?php echo $body_art['id']; ?>]"><?php echo $body_art['name']; ?></label></div>
					<?php 
						$ctr++;
						//if($ctr % 3 == 0){
							//echo '<div style="clear:both"></div>';
							//$ctr = 0;
						//}
					} ?>
				</fieldset>
				<div style="clear:both"></div>
				<div class="box-buttons">
					<button type="button" value="update" id="update-appearance-header"> Update </button>
				</div>
			</form>
			</div>
			<div id="situation-header" style="background-color: #b166a0" box-name="situation" class="toggle-header"><label class="toggle">My Situation</label> </div>
			<div id="situation-box" style="background-color: #b166a0" class="toggle-box">
			<form class="edit-form" id="situation-form" name="situation-form" method="post">
			
				<label class="leftside" for="marital-status"> My marital status is </label>
				<div class="select-style">
					<select name="marital_status">
						<option value="">I'll tell you later</option>
					<?php
						foreach($marital_status as $status){
					?>
						<option value="<?php echo $status['id'] ?>"><?php echo $status['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
			
			
				<label class="leftside" for="have-children"> Do you have children? </label>
				<div class="select-style">
					<select name="have_children">
						<option value="">I'll tell you later</option>
					<?php
						foreach($has_children as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
			
			<div style="clear:both"></div>
			<fieldset>
				<label> Do you have pets? </label>	
					<?php 
					$ctr = 0;
					foreach($pets as $row){ ?>
						<div class="interest checkbox"><input id="pets[<?php echo $row['id']; ?>]" type="checkbox" name="pets[<?php echo $row['id']; ?>]" /> <label for="pets[<?php echo $row['id']; ?>]"><?php echo $row['name']; ?></label></div>
					<?php 
						$ctr++;
						//if($ctr % 3 == 0){
							//echo '<div style="clear:both"></div>';
							//$ctr = 0;
						//}
					} ?>
			</fieldset>
					<div style="clear:both"></div>
				<div class="box-buttons">
					<button type="button" value="update" id="update-situation"> Update </button>
				</div>
			</form>		
			</div>
		
			<div id="education-employment-header" style="background-color: #bed267" box-name="education-employment" class="toggle-header"><label class="toggle">My Education & Employment </label></div>
			<div id="education-employment-box" style="background-color: #bed267" class="toggle-box">
			<form class="edit-form" id="education-employment-form" name="education-employment-form" method="post">
			
				<label class="leftside" for="level"> My Education Level is </label>
				<div class="select-style">
					<select name="level">
						<option value="">I'll tell you later</option>
					<?php
						foreach($education as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
				<label class="leftside" for="specialty"> My specialty is </label>
				<div class="select-style">
					<select	name="specialty">
						<option value="">I'll tell you later</option>
					<?php
						foreach($specialty as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
			
			
				<label class="leftside" for="employment"> My employment status is </label>
				<div class="select-style">
					<select name="employment">
						<option value="">I'll tell you later</option>
					<?php
						foreach($employment as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
				<label class="leftside" for="job-title"> My job title is </label>
					<input type="text" id="job_title" name="job_title"> 
			
			<div style="clear:both"></div>
				<div class="box-buttons">
					<button type="button" value="update" id="update-education-employment"> Update </button>
				</div>
			</form>	
			</div>
		
			<div id="leisure-header" style="background-color: #eaae62" box-name="leisure" class="toggle-header"><label class="toggle">My Leisure & Fun </label></div>
			<div id="leisure-box" style="background-color: #eaae62" class="toggle-box">
			<form class="edit-form" id="leisure-form" name="leisure-form" method="post">
			<fieldset>
				<label> What do you watch on TV? </label>
					<?php 
					$ctr = 0;
					foreach($tv_genre as $row){ ?>
						<div class="interest checkbox"><input id="tv_genre[<?php echo $row['id']; ?>]" type="checkbox" name="tv_genre[<?php echo $row['id']; ?>]" /> <label for="tv_genre[<?php echo $row['id']; ?>]"><?php echo $row['name']; ?></label></div>
					<?php 
						$ctr++;
						//if($ctr % 3 == 0){
							//echo '<div style="clear:both"></div>';
							//$ctr = 0;
						//}
					} ?>
			</fieldset>
			<div style="clear:both"></div>
			<fieldset>
				<label> Movie types I like </label>
					<?php 
					$ctr = 0;
					foreach($movie_genre as $row){ ?>
						<div class="interest checkbox"><input id="movie_genre[<?php echo $row['id']; ?>]" type="checkbox" name="movie_genre[<?php echo $row['id']; ?>]" /> <label for="movie_genre[<?php echo $row['id']; ?>]"><?php echo $row['name']; ?></label></div>
					<?php 
						$ctr++;
						//if($ctr % 3 == 0){
							//echo '<div style="clear:both"></div>';
							//$ctr = 0;
						//}
					} ?>
				</fieldset>
					<div style="clear:both"></div>
				<fieldset>
				<label> My favorite music types are </label>
				<div style="clear:both"></div>
					<?php 
					$ctr = 0;
					foreach($music_genre as $row){ ?>
						<div class="interest checkbox"><input id="music_genre[<?php echo $row['id']; ?>]" type="checkbox" name="music_genre[<?php echo $row['id']; ?>]" /> <label for="music_genre[<?php echo $row['id']; ?>]"><?php echo $row['name']; ?></label></div>
					<?php 
						$ctr++;
						//if($ctr % 3 == 0){
							//echo '<div style="clear:both"></div>';
							//$ctr = 0;
						//}
					} ?>
				</fieldset>
					<div style="clear:both"></div>
				<fieldset>
				<label> My favorite book types are </label>
				<div style="clear:both"></div>
				<?php 
				$ctr = 0;
				foreach($book_genre as $row){ ?>
					<div class="interest checkbox"><input id="book_genre[<?php echo $row['id']; ?>]" type="checkbox" name="book_genre[<?php echo $row['id']; ?>]" /> <label for="book_genre[<?php echo $row['id']; ?>]"><?php echo $row['name']; ?></label></div>
				<?php 
					$ctr++;
					//if($ctr % 3 == 0){
						//echo '<div style="clear:both"></div>';
						//$ctr = 0;
					//}
				} ?>
				</fieldset>
					<div style="clear:both"></div>
				<fieldset>
				<label> My hobbies are </label>
				<?php 
				$ctr = 0;
				foreach($hobbies as $row){ ?>
					<div class="interest checkbox"><input id="hobbies[<?php echo $row['id']; ?>]" type="checkbox" name="hobbies[<?php echo $row['id']; ?>]" /> <label for="hobbies[<?php echo $row['id']; ?>]"><?php echo $row['name']; ?></label></div>
				<?php 
					$ctr++;
					//if($ctr % 3 == 0){
						//echo '<div style="clear:both"></div>';
						//$ctr = 0;
					//}
				} ?>
			    </fieldset>
					<div style="clear:both"></div>
					<div class="box-buttons">
						<button type="button" value="update" id="update-leisure"> Update </button>
					</div>
			</form>	
			</div>
			<!--
			<div id="personality-header" style="background-color: #eb7055" box-name="personality" class="toggle-header"><label class="toggle">My Personality</label> </div>
			<div id="personality-box" style="background-color: #eb7055" class="toggle-box">
			<form class="edit-form" id="personality-form" name="personality-form" method="post">
			
				<label class="leftside" for="smoke"> Do you smoke? </label>
				<div class="select-style">
					<select name="smoke">
						<option value="">I'll tell you later</option>
					<?php
						foreach($smoking_habit as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
				<label class="leftside" for="drink"> My drinking habit is </label>
				<div class="select-style">
					<select name="drink">
						<option value="">I'll tell you later</option>
					<?php
						foreach($drinking_habit as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
			
			
				<label class="leftside" for="social-behaviour"> Most people say that I am... </label>
				<div class="select-style">
					<select name="social_behaviour">
						<option value="">I'll tell you later</option>
					<?php
						foreach($social_behaviou as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
			
				<div style="clear:both"></div>
			<fieldset>
				<label> My idea of great time is </label>
					<?php 
					$ctr = 0;
					foreach($i_enjoy as $row){ ?>
						<div class="interest checkbox"><input id="i_enjoy[<?php echo $row['id']; ?>]" type="checkbox" name="i_enjoy[<?php echo $row['id']; ?>]" /> <label for="i_enjoy[<?php echo $row['id']; ?>]"><?php echo $row['name']; ?></label></div>
					<?php 
						$ctr++;
						//if($ctr % 3 == 0){
							//echo '<div style="clear:both"></div>';
							//$ctr = 0;
						//}
					} ?>
			</fieldset>
				<div style="clear:both"></div>
				<div class="box-buttons">
					<button type="button" value="update" id="update-personality"> Update </button>
				</div>
			</form>
			</div>
				
			<div id="views-header" style="background-color: #58ba77" box-name="views" class="toggle-header"><label class="toggle">My Views</label></div>
			<div id="views-box" style="background-color: #58ba77" class="toggle-box">
			<form class="edit-form" id="views-form" name="views-form" method="post">
			
				<label class="leftside" for="political"> My political view is.. </label>
				<div class="select-style">
					<select name="political">
						<option value="">I'll tell you later</option>
					<?php
						foreach($political_views as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div>
			
			
				<label class="leftside" for="religion"> My religion is </label>
				<div class="select-style">
					<select name="religion">
						<option value="">I'll tell you later</option>
					<?php
						foreach($religion as $row){
					?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
					<?php
						}
					?>
					</select>
				</div> 
			
			<div style="clear:both"></div>
				<div class="box-buttons">	
					<button type="button" value="update" id="update-views"> Update </button>
				</div>
			</form>
			</div>
		
			<div id="looking-header" style="background-color: #bed267" box-name="looking" class="toggle-header"><label class="toggle">I Am Looking For </label></div>
			<div id="looking-box" style="background-color: #bed267" class="toggle-box">
			<form class="edit-form" name="looking-for-form" id="looking-for-form" method="post">
			<fieldset>
				<label> What do you find attractive? </label>
				<?php 
				$ctr = 0;
				foreach($looking_for as $row){ ?>
					<div class="interest checkbox"><input id="find-attractive[<?php echo $row['id']; ?>]" type="checkbox" name="find-attractive[<?php echo $row['id']; ?>]" /> <label for="find-attractive[<?php echo $row['id']; ?>]"><?php echo $row['name']; ?></label></div>
				<?php 
					$ctr++;
					//if($ctr % 3 == 0){
						//echo '<div style="clear:both"></div>';
						//$ctr = 0;
					//}
				} ?>
			</fieldset>
				<div style="clear:both"></div>
				<div class="box-buttons">
					<button type="button" value="update" id="update-lookingfor"> Update </button>
				</div>
			</form>	
			</div>
			-->
		</div>
		
		<script type="text/javascript">
			$('.toggle-header').click(function(){ 
				$('.toggle-header').removeClass('active');
				$(this).addClass('active');
			});
			function loadPersonalDescription(){
				header = $(this);	
				$.ajax({
					url:'./ajax/getPersonalDescription',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
				
					success: function(data){
					//debugger;
						changeBox('#personal-description-box');
						$($(header).find('.loader')[0]).remove();
					
						$('[name="sex"][value="'+data.sex+'"]').attr("checked",true);
						for(var i=0; i<data.languages.length;i++){
							$('[name="language['+data.languages[i].lid+']"]').attr('checked', true);
						}
						$('[name="day"] [value="'+data.day+'"]').attr("selected", "selected");
						$('[name="month"] [value="'+data.month+'"]').attr("selected", "selected");
						$('[name="year"] [value="'+data.year+'"]').attr("selected", "selected");
						$('#hometown').val(data.city+', '+data.county+', '+data.state+', '+data.country);
						$('#profile-heading').val(data.profile.heading);
						$('#profile-introduction').val(data.profile.intro);
						$('#personal-description-box').slideDown('fast');
					}
				});
			}
			function updatePersonalDescription(){
				var postData = $("#personal-description-form").serialize();
				//debugger;
				$.ajax({
					url: './ajax/updatePersonalDescription',
					type:'post',
					data: postData,
					beforeSend: function(){
						$('#personal-description-header').append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						$('#personal-description-header .loader').remove();
						if(data.success){
							$('#personal-description-box').slideUp('fast');
							$('#appearance-header').click();
						}
					}
				});
			}
			function loadInterestedInData(){
				header = $(this);	
				$.ajax({
					url:'./ajax/getInterestedIn',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						changeBox('#interested-in-box');
						$($(header).find('.loader')[0]).remove();
						for(var i=0; i<data.interests.length;i++){
							$('[name="interested_in['+data.interests[i].lid+']"]').attr('checked', true);
						}
					}
				});
			}
			function updateInterestedInData(){	
				var postData = $("#interested-in-form").serialize();
				$.ajax({
					url:'./ajax/updateInterestedIn',
					type:'post',
					data:postData,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						$('#interested-in-header .loader').remove();
							$('#interested-in-box').slideUp('fast');
							$('#appearance-header').click();
						
					}
				});
			}
			function loadAppearanceData(){
				
				header = $(this);	
				$.ajax({
					url:'./ajax/getAppearance',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						changeBox('#appearance-box');
						$($(header).find('.loader')[0]).remove();
						
						$('[name="ethnicity"] [value='+data[0].ethnicity+']').attr("selected", "selected");
						$('[name="nationality"] [value='+data[0].nationality+']').attr("selected", "selected");
						$('[name="height"] [value='+data[0].height+']').attr("selected", "selected");
						$('[name="eye_color"] [value='+data[0].eye_color+']').attr("selected", "selected");
						$('[name="hair_color"] [value='+data[0].hair_color+']').attr("selected", "selected");
						
						body_arts = data.body_arts;
						for(var i=0; i<body_arts.length; i++){
							$('[name="body_art['+body_arts[i].lid+']"]').attr('checked', 'checked');
						}
					}
				});
			}
			function updateAppearanceData(){	
				var postData = $("#appearance-form").serialize();
				$.ajax({
					url:'./ajax/updateAppearance',
					type:'post',
					data:postData,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
					
						$('#appearance-header .loader').remove();
							$('#appearance-box').slideUp('fast');
							$('#situation-header').click();
						
					}
				});
			}
			function loadSituationData(){
				
				header = $(this);	
				$.ajax({
					url:'./ajax/getSituation',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						changeBox('#situation-box');
						$($(header).find('.loader')[0]).remove();

						$('[name="marital_status"] [value='+data[0].marital_status+']').attr("selected", "selected");
						
						$('[name="have_children"] [value='+data[0].have_children+']').attr("selected", "selected");
						
						pets = data.pets;
						//debugger;
						for(var i=0; i<pets.length; i++){
							$('[name="pets['+pets[i].lid+']"]').attr('checked', 'checked');
						}
					}
				});
			}
			
			function updateSituationData(){	
				var postData = $("#situation-form").serialize();
				$.ajax({
					url:'./ajax/updateSituation',
					type:'post',
					data:postData,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
					
						$('#situation-header .loader').remove();
							$('#situation-box').slideUp('fast');
							$('#education-employment-header').click();
						
					}
				});
			}
					
			function loadEducationEmploymentData(){
				
				header = $(this);	
				$.ajax({
					url:'./ajax/getEducationEmployment',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						changeBox('#education-employment-box');
						$($(header).find('.loader')[0]).remove();
					
						$('[name="level"] [value='+data[0].level+']').attr("selected", "selected");
						$('[name="specialty"] [value='+data[0].specialty+']').attr("selected", "selected");
						$('[name="employment"] [value='+data[0].employment+']').attr("selected", "selected");
						$('[name="income"] [value='+data[0].income+']').attr("selected", "selected");
						$('#job_title').val(data[0].job_title);
						
					}
				});
			}
			
			function updateEducationEmploymentData(){	
				var postData = $("#education-employment-form").serialize();
				$.ajax({
					url:'./ajax/updateEducationEmployment',
					type:'post',
					data:postData,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
					
						$('#education-employment-header .loader').remove();
							$('#education-employment-box').slideUp('fast');
							$('#leisure-header').click();
						
					}
				});
			}
			
			function loadLeisureData(){
				
				header = $(this);	
				$.ajax({
					url:'./ajax/getLeisure',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						changeBox('#leisure-box');
						//debugger;
						$($(header).find('.loader')[0]).remove();
						//debugger;
						for(var i=0; i<data.length; i++){
							$('[name="tv_genre['+data[i].lid+']"]').attr('checked', 'checked');
							$('[name="movie_genre['+data[i].lid+']"]').attr('checked', 'checked');
							$('[name="music_genre['+data[i].lid+']"]').attr('checked', 'checked');
							$('[name="book_genre['+data[i].lid+']"]').attr('checked', 'checked');
							$('[name="hobbies['+data[i].lid+']"]').attr('checked', 'checked');
						}
					}
				});
			}
			function updateLeisureData(){	
				var postData = $("#leisure-form").serialize();
				$.ajax({
					url:'./ajax/updateLeisure',
					type:'post',
					data:postData,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
					
						$('#leisure-header .loader').remove();
							$('#leisure-box').slideUp('fast');
							$('#personality-header').click();
						
					}
				});
			}
			
			function loadPersonalityData(){
				header = $(this);	
				$.ajax({
					url:'./ajax/getPersonality',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						changeBox('#personality-box');
						$($(header).find('.loader')[0]).remove();

						$('[name="smoke"] [value='+data[0].smoke+']').attr("selected", "selected");
						$('[name="drink"] [value='+data[0].drink+']').attr("selected", "selected");
						$('[name="social_behaviour"] [value='+data[0].social_behaviour+']').attr("selected", "selected");
						i_enjoy = data.i_enjoy;
						for(var i=0; i<i_enjoy.length; i++){
							$('[name="i_enjoy['+i_enjoy[i].lid+']"]').attr('checked', 'checked');
						}
					}
				});
			}
			function updatePersonalityData(){	
				var postData = $("#personality-form").serialize();
				$.ajax({
					url:'./ajax/updatePersonality',
					type:'post',
					data:postData,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
					
						$('#personality-header .loader').remove();
						$('#personality-box').slideUp('fast');
						$('#views-header').click();
						
					}
				});
			}
			function loadViewsData(){
				header = $(this);	
				$.ajax({
					url:'./ajax/getViews',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						changeBox('#views-box');
						$($(header).find('.loader')[0]).remove();

						$('[name="political"] [value='+data[0].political+']').attr("selected", "selected");
						$('[name="religion"] [value='+data[0].religion+']').attr("selected", "selected");
					}
				});
			}
			function updateViewsData(){	
				var postData = $("#views-form").serialize();
				$.ajax({
					url:'./ajax/updateViews',
					type:'post',
					data:postData,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						$('#views-header .loader').remove();
						$('#views-box').slideUp('fast');
						$('#looking-header').click();
					}
				});
			}
			function loadLookingForData(){
				header = $(this);	
				$.ajax({
					url:'./ajax/getLookingFor',
					type:'post',
					data:'uid='+uid,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						changeBox('#looking-box');
						$($(header).find('.loader')[0]).remove();

						for(var i=0; i<data.length; i++){
							$('[name="find-attractive['+data[i].lid+']"]').attr('checked', 'checked');
						}
					}
				});
			}
			function updateLookingForData(){	
				var postData = $("#looking-for-form").serialize();
				$.ajax({
					url:'./ajax/updateLookingFor',
					type:'post',
					data:postData,
					beforeSend: function(){
						$(header).append('<span class="loader"><img src="./assets/img/loader.gif" /></span>');
					},
					success: function(data){
						$('#looking-header .loader').remove();
						$('#looking-box').slideUp('fast');
					}
				});
			}
			$("#mainContent").ready(function(){
				/***********************************************
				* Drop Down Date select script- by JavaScriptKit.com
				* This notice MUST stay intact for use
				* Visit JavaScript Kit at http://www.javascriptkit.com/ for this script and more
				***********************************************/

				//populatedropdown(id_of_day_select, id_of_month_select, id_of_year_select)
				//window.onload=function(){
				$('#monthdropdown, #yeardropdown').unbind().change(function(){
					dayfield=document.getElementById("daydropdown");
					val = $("#daydropdown").val();
					$("#daydropdown option").remove();
					for (var i=0; i<28; i++){
							dayfield.options[i]=new Option(i+1, pad(i+1,2));
					}
					if($('#monthdropdown').val() == '02'){
						if(isLeapYear($('#yeardropdown').val())){
							dayfield.options[i]=new Option(29, pad(29,2));
						}
					}else{
						dayfield.options[i]=new Option(30, pad(30,2));
						var month = $('#monthdropdown').val();
						if(month == '01' || month == '03' || month == '05' ||
							month == '07' || month == '08' || month == '10' ||	month == '12' ){
							dayfield.options[i]=new Option(31, pad(31,2));
						}
					}
					val = $("#daydropdown").val(val);
				});
				populatedropdown("daydropdown", "monthdropdown", "yeardropdown");
				//}
				var hometown_components = new Object();
				//hometown_components.address_components = place.address_components;
				//hometown_components.lat = place.geometry.location.lat();
				//hometown_components.lng = place.geometry.location.lng();
				//$('#hometown_components').val(JSON.stringify(hometown_components));
				var options = {
				  types: ['(cities)']
				 };
				$("#hometown").geocomplete(options)
				  .bind("geocode:result", function(event, result){
					hometown_components.address_components = result.address_components;
					hometown_components.lat = result.geometry.location.lat();
					hometown_components.lng = result.geometry.location.lng();
					$('#hometown_components').val(JSON.stringify(hometown_components));
				  })
				  .bind("geocode:error", function(event, status){
					console.log("ERROR: " + status);
				  });
				
				$('#personal-description-header').click(loadPersonalDescription);
				$('#update-personal-description').click(updatePersonalDescription);
				$('#interested-in-header').click(loadInterestedInData);
				$('#update-interested-in').click(updateInterestedInData);
				$('#appearance-header').click(loadAppearanceData);
				$('#update-appearance-header').click(updateAppearanceData);
				$('#situation-header').click(loadSituationData);
				$('#update-situation').click(updateSituationData);
				$('#education-employment-header').click(loadEducationEmploymentData);
				$('#update-education-employment').click(updateEducationEmploymentData);
				$('#leisure-header').click(loadLeisureData);
				$('#update-leisure').click(updateLeisureData);
				$('#personality-header').click(loadPersonalityData);
				$('#update-personality').click(updatePersonalityData);
				$('#views-header').click(loadViewsData);
				$('#update-views').click(updateViewsData);
				$('#looking-header').click(loadLookingForData);
				$('#update-lookingfor').click(updateLookingForData);
			});
			function changeBox(selector){
				$(".toggle-box:not("+selector+")").slideUp('fast',function(){
					$(selector).slideDown('fast');
				});
			}
			function populatedropdown(dayfield, monthfield, yearfield){
				var monthtext=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
				var today=new Date();
				var dayfield=document.getElementById(dayfield);
				var monthfield=document.getElementById(monthfield);
				var yearfield=document.getElementById(yearfield);
				for (var i=0; i<31; i++){
					dayfield.options[i]=new Option(i+1, pad(i+1,2));
				}
				
				for (var m=0; m<12; m++){
					monthfield.options[m]=new Option(monthtext[m], pad(m+1,2));
				}
			
				var thisyear=today.getFullYear()-18;
				for (var y=0; y<30; y++){
					yearfield.options[y]=new Option(thisyear, thisyear);
					thisyear-=1;
				}
			}
			function pad(number, length) {
			   
				var str = '' + number;
				while (str.length < length) {
					str = '0' + str;
				}
			   
				return str;

			}
			function isLeapYear(year){
				if(year % 400 == 0)
				   return true;
				else if(year % 100 == 0)
				   return false;
				else if(year % 4 == 0)
				   return true;
				else
				   return false;
			}
			
		</script>