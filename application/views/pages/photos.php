<div id="photo-wrapper" style="margin:0 auto">
	<div id="image-box" class="random">
	<button id="next">Next</button>
		<img data-image-ref="" data-image-liked="" />
		<div id="image-hover-div">
			<span id="like_count"></span>
			<span id="like"></span>
		</div>
		
	</div>
	<div id="comment-box">
		<div id="comments">
			<ul>
			
			</ul>
		</div>
		<div id="commentor">
			<div class="commentor_pic">
				<img id="pic" width="35" />
			</div>
			<form id="comment-form">
			<div id="textfield">
				<textarea name="comment" class="photo_comment"></textarea>
				<input type="hidden" name="reference" value="" />
				<input type="hidden" name="commentor_id" value="" />
			</div>
			<div id="post-button">
				<button id="post-comment" type="button">Post</button>	
			</div>
			<div style="clear:both"></div>
			</form>
		</div>	
	</div>
	<div style="clear:both"></div>
</div>
<div style="clear:both"></div>
<script type="text/javascript">
	$('#MainMenu li').removeClass('active');
	$('#photos').addClass('active');
	$('#post-comment').unbind().click(postComment);
	function postComment(){
	
		var postData = $("#comment-form").serialize();
		$('textarea.photo_comment').val('');
		
		$.ajax({
			url: '../../ajax/addPhotoComment',
			type:'post',
			data: postData,
			success: function(data){
				if(data.success){
					$('#comment-box #comments ul').append('<li data-cid="'+data.id+'" data-delete-permission="true"><div class="commentor_pic"><img src="'+my_profile_pic+'" width="35"></div><div class="commentside"><a id="commentor-name" href="./ajax/profileWidget/'+uid+'"> '+my_fullname+'</a> '+data.comment+'</div><div style="clear:both"></div></li>');
					
					$('#comment-box #comments').scrollTo($('#comment-box #comments ul li').last());
					
					attachHover();
					var stanza = $msg({'to': data.xmpp_user+'@matchup360.com/default', 'type': 'notification', 'notification_type': 'comment', 'from_fullname':my_fullname, 'from_photo':data.photo_url, 'notification_string': data.notification_string});
						connection.send(stanza);
				}else{
					alert('there was an error.');
				}
			}
		});
	}	
	attachHover();
	function attachHover(){
		$('#comment-box li[data-delete-permission=true]').unbind().hover(function(){
							$(this).find('.commentside').after('<i class="delete-comment-icon"></i>');
							$('.delete-comment-icon').click(deleteComment);
						},
						function(){
							$('.delete-comment-icon').remove();
						});
	}
	function deleteComment(){
		comment = $(this).parent();
		var cid = $(this).parent().attr('data-cid');
		$.ajax({
			url: '../../ajax/deleteComment/'+cid,
			success: function(){
				comment.remove();
			}
		});
	}
	function randomPhoto(){
		$.ajax({
			url: '../../ajax/randomPhoto',
			type: 'post',
			success: function(response){
				if(response.success){
					$('#photo-wrapper #image-box img').css('width',response.width);
					$('#photo-wrapper #image-box img').attr('src',response.url);
					$('#photo-wrapper #image-box img').attr('data-image-ref',response.reference);
					$('input[name="reference"]').val(response.reference);
					$('input[name="commentor_id"]').val(uid);
					$('#photo-wrapper #image-box img').attr('data-image-liked',response.is_liked);
					$('#photo-wrapper #pic').attr('src',my_profile_pic);
					$('#photo-wrapper #like_count').html(response.photo_likes.likes_string);				
					$('#photo-wrapper #comments').css('max-height',response.height);
					if(response.comments.length>0)
						debugger;
					for(var i=0; i<response.comments.length; i++){
						if(response.comments[i].owner_id == uid || response.comments[i].commentor_id == uid)
							permission = 'data-delete-permission="true"';
						else
							permission = '';
						comment = '<li data-cid="'+response.comments[i].id+'" '+permission+'><div class="commentor_pic"><img src="https://s3.amazonaws.com/wheewhew/user/'+response.comments[i].uid+'/photos/'+response.comments[i].profile_pic+'" width="35"></div><div class="commentside"><a id="commentor-name" href="./ajax/profileWidget/'+response.comments[i].uid+'">' +response.comments[i].firstname+' '+response.comments[i].lastname+'</a> '+response.comments[i].comment+'</div><div style="clear:both"></div></li>';
						$('#photo-wrapper #comments ul').append(comment);
					}
					attachHover();
					$('#photo-wrapper').css('width',$('#photo-wrapper #image-box').width()+$('#photo-wrapper #comment-box').width()+5);
				}else{ 
					alert(response.message);
				}
			}
		});
	}
</script>
<script type="text/javascript">
$('#mainContent').ready(function(){
	randomPhoto();
	$('#next').click(function(){
		randomPhoto();
	});
	$('#image-box').hover(function(){ 
		var is_liked = $(this).parent().find('img').attr('data-image-liked');
		if(is_liked || is_liked == 'false')
			$(this).find('#image-hover-div #like').html('Like');
		else 
			$(this).find('#image-hover-div #like').html('Unlike');
		$('#like').unbind().click(like_photo);
		$('#image-hover-div').fadeIn();
	},
	function(){
		$('#image-hover-div').fadeOut();
	});
});
</script> 