<?php 
	$interested_in_array = array();
	if(!empty($user_data['interested_in']['men']))
		$interested_in_array[] = 'Men';
	if(!empty($user_data['interested_in']['women']))
		$interested_in_array[] = 'Women';
	switch($user_data['sex']){
		case 'f': $user_data['sex'] = 'Female'; break;
		case 'm': $user_data['sex'] = 'Male'; break;
	}
?>
			<div class="user_column">
				<div id="profile_name"><h1><?php echo $user_data['firstname']." ".$user_data['lastname']; ?></h1></div>
				<div id="profile_pic" class="thumbnail"><img src="<?php echo $user_photo;?>" width="150" height="150" /></div>
				<div id="profile_address"><span><?php echo $user_data['city'].', '.$user_data['state'].', '.$user_data['country'] ?></span></div>
				<button id="shouts" class="btn btn-info"><i class="icon-white icon-align-justify"></i> Shoutouts</button> <br />
				<button id="my-photos" class="btn btn-info"><i class="icon-white icon-user"></i> My Photos</button> <br />
				<?php if($user_data['uid'] != $logged_user_uid) {?>
				<button id="send-message" class="btn btn-info"><i class="icon-white icon-envelope"></i> Send Message</button> <br />
				<?php } ?>
			</div>
			<div class="info_column">
				<div class="info"><strong>Sex:</strong> <?php echo $user_data['sex']; ?></div>
				<div class="info"><strong>Age:</strong> <?php echo $user_data['age']; ?></div>
				<div class="info"><strong>Interested in:</strong> <?php echo implode(',',$interested_in_array); ?></div>
				<div class="info"><strong>Location: </strong><?php echo $user_data['city']; ?>, <?php echo $user_data['state']; ?>, <?php echo $user_data['country']; ?></div>
				<div id="info_container" class="stream">
				</div>
			</div>
			<div style="clear:both"></div>
			<script type="text/javascript">
				$('#main-menu li').removeClass('active');
				$('#main-menu li#members').addClass('active');
				function load_info_column(function_name, div, callback){
					$('.user_column button').removeClass('active');
					div.addClass('active');
					$('#info_container').load(window.location.protocol + '//' + window.location.hostname + '/page/'+function_name+'/<?php echo $user_data['uid']?>',function(){
						if(callback)
							callback();
					});
				}
				$('#shouts').click(function(){
					load_info_column('user_shouts',$(this),function(){
						if($('#info_container').children().length == 0)
							$('#my-photos').click();
					});
				}).click();
				$('#my-photos').click(function(){
					load_info_column('user_photos',$(this));
				});
				$('#send-message').unbind().click(function(e){
					sendMessageDialog(<?php echo $user_data['uid'] ?>);
				});
			</script>