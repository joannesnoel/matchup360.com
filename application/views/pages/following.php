<h1>Following</h1>
<?php
	$color_palette = array('#3894e7','#1880d6','#58ba77','#bed267','#91d152','#eaae62','#eb7055','#dd7499','#b166a0');
	foreach($follow as $user){
		$bgcolor = $color_palette[floor(rand(0,count($color_palette)-1))];
	?>
	<div class="mCard" data-id="<?php echo $user['uid']?>" data-sex="<?php echo $user['sex']?>" data-member-uid="<?php echo $user['uid']?>" data-xmpp-user="<?php echo $user['xmpp_user']?>" style="background-color: <?php echo $bgcolor ?>">							
		<div data-uid="<?php echo $user['uid']?>" data-url="./ajax/profileWidget/<?php echo $user['uid']?>" data-title="<?php echo $user['firstname'] .' '. $user['lastname']?>" class="profile-pic"> 								
			<img src="<?php echo $user['profile_pic']?>"> 							
		</div> 							
		<div data-uid="<?php echo $user['uid']?>" data-url="./ajax/profileWidget/<?php echo $user['uid']?>" data-title="<?php echo $user['firstname'] .' '. $user['lastname']?>" class="profile-name">
			<span class="full-name"><?php echo $user['firstname'] .' '. $user['lastname']?></span><span class="age"> - Age <?php echo $user['age']?></span>
		</div> 							
		<div class="info"><?php echo $user['city'] .' '. $user['state'] . ' ' . $user['country']?><br>							
			<i><?php echo (round((int)$user['distance']) == 0? 'less than 5 kilometers' : 'around '.round((int)$user['distance']) . ' kilometers')?></i>
		</div>							
		<div style="clear:both"></div> 							
		<div class="cardOptions" style="display: none;">
			<button type="button">Message</button>
			<button type="button" class="followBtn" style="display: inline-block;">Follow</button>
			<button class="unfollowBtn" type="button" style="display: none;">Unfollow</button>
		</div> 				  
	</div>
<?php }?>
<script type="text/javascript">
	$('.mCard').click(function(){
		var uid = $(this).attr('data-id');
		loadPage('view_profile/'+uid);
	});
</script>