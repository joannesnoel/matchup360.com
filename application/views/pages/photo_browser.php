<script src="js/lightbox.js"></script>

<link href="css/lightbox.css" rel="stylesheet" />

<div id="lightbox" style="display: block; top: 458.4px; left: 0px;">
	<div class="lb-outerContainer" style="width: 520px; height: 520px;">
		<div class="lb-container">
			<img class="lb-image" src="https://s3.amazonaws.com/wheewhew/user/'.$photos['uid'].'/photos/'.$photos['unique_code'].'_300.jpg" style="display: inline;">
			
			<div class="lb-nav" style="display: block;"><a class="lb-prev" style="display: none;"></a><a class="lb-next" style="display: block;"></a></div>
			<div class="lb-loader" style="display: none;"><a class="lb-cancel"><img src="img/loadingg.gif"></a></div>
		</div>
	</div>
	<div class="lb-dataContainer" style="display: block; width: 520px;">
		<div class="lb-data">
			<div class="lb-details"><span class="lb-caption" style="display: inline;">Click on the right side of the image to move forward.</span><span class="lb-number" style="display: block;">Image 1 of  4</span></div>
			<div class="lb-closeContainer"><a class="lb-close"><img src="images/close.png"></a></div>
		</div>
	</div>
</div>