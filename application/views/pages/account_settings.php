<div id="account_settings">
	<div id="dialog" style="display: none"></div>
	<h1>General Account Settings</h1>
	<ul>
		<li id="fullname">
			<span class="title">Name</span>
			<div class="initial">
				<span id="fullname_span" class="value"><?php echo $firstname .' '. $lastname ?></span>
				<span class="action"><a class="edit">edit</a></span>
			</div>
			<div class="edit-container">
				<form id="fullname_form">
					<div class="row">
						<span class="title">First Name</span>
						<input type="text" name="firstname" value="<?php echo $firstname ?>" class="field">
					</div>
					<div class="row">
						<span class="title">Last Name</span>
						<input type="text" name="lastname" value="<?php echo $lastname ?>" class="field">
					</div>
					<div class="row" style="text-align: right">
						<button type="button" class="save">Save</button>
						<button type="button" class="cancel">Cancel</button>
					</div>
				</form>
			</div>
		</li>
		<li id="email">
			<span class="title">Email Address</span>
			<div class="initial">
				<span id="email_span" class="value"><?php echo $email ?></span>
				<span class="action"><a class="edit">edit</a></span>
			</div>
			<div class="edit-container">
				<form id="email_form">
					<div class="row">
						<span class="title">Email</span>
						<input type="text" name="email" value="<?php echo $email ?>" class="field">
					</div>
					<div class="row" style="text-align: right">
						<button type="button" class="save">Save</button>
						<button type="button" class="cancel">Cancel</button>
					</div>
				</form>
			</div>
		</li>
		<li id="password">
			<span class="title">Password</span>
			<div class="initial">
				<span id="password_span" class="value">**********</span>
				<span class="action"><a class="edit">edit</a></span>
			</div>
			<div class="edit-container">
				<form id="password_form">
					<div class="row">
						<span class="title">Password</span>
						<input type="password" name="password" class="field">
					</div>
					<div class="row">
						<span class="title">Confirm Password</span>
						<input type="password" name="password_confirm" class="field">
					</div>
					<div class="row" style="text-align: right">
						<button type="button" class="save">Save</button>
						<button type="button" class="cancel">Cancel</button>
					</div>
				</form>
			</div>
		</li>
	
	</ul>
</div> 
<script type="text/javascript">
	$('#MainMenu li').removeClass('active');
	$('#account_settings').ready(function(){
		$('.save').click(save);
		$('.edit').click(function(){
			var parent = $(this).parent().parent().parent();
			$('#account_settings li ').each(function(){
				hide($(this));
			});
			toggle(parent);
		});
		$('.cancel').click(function(){
			var parent =  $(this).parent().parent().parent().parent();
			toggle(parent);
		});
		function showDialog(){
			$("#dialog").unbind().dialog({
				height: 100,
				modal: true,
				open: function(event, ui){
				 setTimeout("$('#dialog').dialog('close')",2000);
				}
			});
		}
		function toggle(div){ 
			if(div.find('.edit-container').css('display') == 'none' &&
			   (div.find('.initial').css('display') == 'inline-block' ||
			   div.find('.initial').css('display') == 'block')){
					show(div);
			}else if((div.find('.edit-container').css('display') == 'inline-block' ||
				div.find('.edit-container').css('display') == 'block') &&
			   div.find('.initial').css('display') == 'none'){
					hide(div);
			}
		}
		function hide(div){
			div.removeClass('active');
			div.find('.edit-container').slideUp();//css({'display' : 'none'});
			div.find('.initial').slideDown();
		}
		function show(div){
			div.addClass('active');
			div.find('.initial').css({'display' : 'none'});//slideUp();
			div.find('.edit-container').slideDown();
		}
		function save(){
			var parent = $(this).parent().parent().parent().parent();
			var form_id = $(this).parent().parent().attr('id');
			var data = $('#'+form_id).serialize();
			var function_name = '';
			if(form_id == 'fullname_form')
				function_name = 'updateName';
			else if(form_id == 'email_form')
				function_name = 'updateEmail';
			else if(form_id == 'password_form')
				function_name = 'updatePassword';
				
			$.ajax({
				url: '/ajax/'+function_name,
				data: data,
				type: 'post',
				success: function(data){
					if(data.status == 1){
						debugger;
						/*$.each(data.data, function(key, value){
							$('#'+key+'_span').html(value);
						});*/
						hide(parent);
						$( "#dialog" ).html(data.message);
						showDialog();
						$('#user-fullname').html(data.data.fullname);
					}else{
						$( "#dialog" ).html(data.message);
						showDialog();
					}
				}
			});
		}
		
	
	});
</script>