		<div id="my-photos-wrapper">
			<div id="photos-action">
				<a id="add-photos"><span class="ui-button">Add Photos</span></a>
			</div>
			<div style="clear: both"></div>
			<div id="photos-container">
				<?php if(count($my_photos) >= 1){
						$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
						foreach($my_photos as $photo){	?>
						<div data-photoref="<?php echo $photo['reference']?>" class="photo-wrapper">
							<a href="<?php echo $protocol.$_SERVER['HTTP_HOST']; ?>/widgets/photo/<?php echo $photo['reference']?>" rel="user_photos" class="user_photo">
								<img src="https://s3.amazonaws.com/wheewhew/user/<?php echo $photo['uid']?>/photos/<?php echo $photo['filename']?>" width="200">
							</a>
						</div>
				<?php }
				}else{ ?>
					<div style="text-align: center">
						<span>You have no photo(s) uploaded.</span>
						<img src="">
					</div>
				<?php } ?>
			</div>
		</div>

<script type="text/javascript">
$('#MainMenu li').removeClass('active');
	$('#mainContent').ready(function(){
		$('#add-photos').click(function(){
			$.colorbox({html:'<div id="dropbox">\
								<span class="message">Drop images here to upload. <br /></span>\
								<div id="uploadify_wrap" style="display:none">\
									<input type="file" name="file_upload" id="file_upload" />\
								</div>\
							</div>',
						overlayClose: false,
						escKey: false,
						onComplete: function(){
							$('#file_upload').uploadify({
								'swf'      : 'assets/etc/uploadify.swf',
								'uploader' : 'ajax/upload_photo',
								'width' : 125,
								'height': 35,
								'scriptData'     : {'session' : '<?php echo $this->session->userdata('session_id') ?>'},
							});
						},
						onClosed:  function(){
							loadPage('my_photos');
						}
			});
			$(document).bind('cbox_complete', function(){ 
				var dropbox = $('#dropbox'),
					message = $('.message', dropbox);
				dropbox.unbind().filedrop({
					// The name of the $_FILES entry:
					paramname: 'Filedata',

					maxfiles: 8,
					maxfilesize: 4,
					url: './ajax/upload_photo',

					uploadFinished: function(i, file, response) {
						if(response.status == 'Upload success!'){
							$.data(file)
								.addClass('done');
							$('#jqi_state3_buttonSkip').hide();
						}else{
							$.data(file).find('.uploaded').removeClass('uploaded').addClass('upload_response_text').html(response.status);
						}
					},

					error: function(err, file) {
						switch (err) {
						case 'BrowserNotSupported':
							message.hide();
							$('#dropbox').prepend('<p align="center">Your browser does not support File Drag \'n Drop.<br />Use the file browser instead.<\p>');
							$('#dropbox p').delay(3000).fadeOut(400,function(){
								$('#uploadify_wrap').show();
							});
							break;
						case 'TooManyFiles':
							alert('Too many files! Please select 5 at most! (configurable)');
							break;
						case 'FileTooLarge':
							alert(file.name + ' is too large! Please upload files up to 2mb (configurable).');
							break;
						default:
							break;
						}
					},

					// Called before each upload is started
					beforeEach: function(file) {
						if (!file.type.match(/^image\//)) {
							alert('Only images are allowed!');

							// Returning false will cause the
							// file to be rejected
							return false;
						}
					},

					uploadStarted: function(i, file, len) {
						createImage(file);
						$('#jqi_state3_buttonNext').hide();
					},

					progressUpdated: function(i, file, progress) {
						$.data(file)
							.find('.progress')
							.width(progress);
					},
					afterAll: function() {
						// runs after all files have been uploaded or otherwise dealt with
						if($("#dropbox .done").length > 0){
							$('#jqi_state3_buttonNext').show();
						}
					}

				});

				var template = '<div class="preview">' + '<span class="imageHolder">' + '<img />' + '<span class="uploaded"></span>' + '</span>' + '<div class="progressHolder">' + '<div class="progress"></div>' + '</div>' + '</div>';


				function createImage(file) {

					var preview = $(template),
						image = $('img', preview);

					var reader = new FileReader();

					image.width = 100;
					image.height = 100;

					reader.onload = function(e) {

						// e.target.result holds the DataURL which
						// can be used as a source of the image:

						image.attr('src', e.target.result);
					};

					// Reading the file as a DataURL. When finished,
					// this will trigger the onload function above:
					reader.readAsDataURL(file);

					message.hide();
					preview.appendTo(dropbox);

					// Associating a preview container
					// with the file, using jQuery's $.data():

					$.data(file, preview);
				}

				function showMessage(msg) {
					message.html(msg);
				}
			});
		});
	});
	$('.photo-wrapper').hover(function(){
										var photo_id = $(this).attr('data-photoref');
										var orig_photo = $(this).find('img').attr('src').replace('_thumb', '');
										$(this).append('<div class="photo-action-wrapper">\
															<a id="delete-photo" title="Delete" style="float: left"><button style="background:rgba(238, 66, 66, 0.6)">Delete</button></a>\
															<a id="set_as_profile_pic" href="'+ window.location.protocol + '//' + window.location.hostname +'/widgets/set_profile_photo/'+photo_id+'" class="user_photo" rel="set_profile_photo" style="float:right"><button>Set as Profile Pic</button></a>\
														</div>');
										$('#delete-photo').unbind().click(function(){
											photo = $(this).parent().parent();
											$.ajax({
												url: 'ajax/deletePhoto/'+photo_id,
												success: function(){
													$(photo).remove(); 
												}
											});
										});
										$('#set_as_profile_pic').unbind().click(function(){
											$('.user_photo').colorbox({rel:"set_profile_photo"});
											$(document).bind('cbox_complete', function(){ 
												$('#orig_photo_url').val(orig_photo);
												$('#jcrop-target').Jcrop({
													onSelect: updateCoords,
													onRelease: updateCoords,
													setSelect:   [ 0, 0, 400, 400 ],
													aspectRatio: 1/1,
													minSize: [400,400]
												});
												function updateCoords(c){
													$("#x").val(c.x);
													$("#y").val(c.y);
													$("#x2").val(c.x2);
													$("#y2").val(c.y2);
													$("#w").val(c.w);
													$("#h").val(c.h);
												}
											});
										});
										
									},
									function(){
										$(this).find('.photo-action-wrapper').remove();
									});
	$('.user_photo').colorbox({
		rel:"user_photos",
		transition: "none",
		onComplete: function(){
			photo_colorbox_oncomplete();
		}
	});
	$(window).unbind().resize(function(){
		var ratio = ($(window).height() - 100) / $('#photo-wrapper #image-box img').attr('data-original-height');
		if(ratio < 1){
			$('#photo-wrapper #image-box img').css('width',$('#photo-wrapper #image-box img').attr('data-original-width')*ratio);
			$('#photo-wrapper #image-box img').css('height',$('#photo-wrapper #image-box img').attr('data-original-height')*ratio);
		}
		$('#photo-wrapper').css('width',$('#image-box').width() + $('#comment-box').width()+2);
		$('#comments').css('max-height',$('#image-box').height()-$('#commentor').height()-3);
		$.colorbox.resize({innerHeight:$('#photo-wrapper').height()+20,innerWidth:$('#photo-wrapper').width()+20});
	});	
</script> 