<h1>Latest Members</h1>
<div id="latest-members" style="position: relative">
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			url: window.location.protocol + '//' + window.location.hostname + '/ajax/get_members',
			type: 'post',
			success: function(members){
				var mCard = '';
				$("#latest-members").empty();
				for (var i = 0; i < members.length; i++) {
				  member = members[i];
				  if(member.distance < 5)
					member.distance = 'less than '+5+' kilometers';
				  else
					member.distance = 'around '+parseInt(member.distance)+' kilometers';

					profile_pic_url = member.profile_pic;

				  bgcolor = color_palette[Math.floor(Math.random()*color_palette.length)];
				  mCard = '<div class="mCard" style="background-color: '+bgcolor+'" data-id="'+member.uid+'" data-sex="'+member.sex+'" data-member-uid="'+member.uid+'" data-xmpp-user="'+member.xmpp_user+'">\
							<div data-uid="'+member.uid+'" data-url="./ajax/profileWidget/'+member.uid+'" data-title="'+member.firstname+' '+member.lastname+'" class="profile-pic"> \
								<img src="'+profile_pic_url+'" /> \
							</div> \
							<div data-uid="'+member.uid+'" data-url="./ajax/profileWidget/'+member.uid+'" data-title="'+member.firstname+' '+member.lastname+'" class="profile-name"><span class="full-name">'+member.firstname+' '+member.lastname+'</span><span class="age"> - Age '+member.age+'</span></div> \
							<div class="info"><span class="address">'+member.city+', '+member.state+', '+member.country+'</span><br />\
							<i>'+member.distance+'</i>\
							</div> \
							<div style="clear:both"></div> \
							<div class="presenceIndicator"></div>\
							<div class="cardOptions"><div class="button follow"></div><div class="button unfollow" type="button"></div><div class="button chat"></div><div class="button last-seen"></div></div> \
				  </div>';
				  $("#latest-members").append(mCard);
				  if(member.followed){
					$('.mCard[data-id="'+member.uid+'"] .cardOptions .unfollow').show();
					$('.mCard[data-id="'+member.uid+'"] .cardOptions .follow').hide();
				  }else{
					$('.mCard[data-id="'+member.uid+'"] .cardOptions .follow').show();
					$('.mCard[data-id="'+member.uid+'"] .cardOptions .unfollow').hide();
				  }
				}
				
				/*$(".mCard").hover(function(){
					$(this).find('.cardOptions').delay(200).stop().fadeIn(200);
									},function(){
					$(this).find('.cardOptions').fadeOut(200);
				});*/
				
				$(".profile-pic, .profile-name").click(function(){
					var uid = $(this).attr('data-uid');
					var title = $(this).attr('data-title');
					loadPage('view_profile/'+uid,title);
				});
				
				$('.chat').click(function(e){
					var peer_id = $(this).parents('.mCard').attr('data-id');
					var fullname = $($(this).parents('.mCard').find('.profile-name .full-name')[0]).html();
					var profile_pic = $($(this).parents('.mCard').find('img')[0]).attr('src');
					var address = $(this).parents('.mCard').find('.info .address').html();
					startChat(peer_id,fullname,profile_pic,address);;				
					e.stopPropagation();
				});
				$('.follow').click(function(){
					var mCard = $(this).parents('.mCard');
					var peer_id = mCard.attr('data-id');
					var xmpp_user = mCard.attr('data-xmpp-user');
					$.ajax({
						url: 'ajax/followMember',
						type: 'post',
						data: 'to_uid='+peer_id,
						success: function(data){
							if(data.status == 'success'){
								if(data.user.profile_pic)
									profile_pic = data.user.profile_pic;
								var node = '<li data-jid="'+data.user.xmpp_user+'@matchup360.com" data-uid="'+data.user.uid+'"> \
												<img src="'+profile_pic+'" /> \
												<span>'+data.user.firstname+' '+data.user.lastname+'</span> \
												<i class="presence"></i> \
											</li>';
								$('ul.contact-list').append(node);
								$(mCard.find('.follow')[0]).hide();
								$(mCard.find('.unfollow')[0]).show();
								sortContactList();
							}
							$('.contact-list li').unbind().click(openChatFromContactList);
							if(data.notification_string != ''){
								var stanza = $msg({'to': data.xmpp_user+'@matchup360.com/default', 'type': 'notification', 'notification_type': 'follow', 'from_fullname':my_fullname, 'from_photo':data.photo_url, 'notification_string': data.notification_string});
								 connection.send(stanza);
							}
						}
					});
				});
				$('.unfollow').click(function(){
					var mCard = $(this).parents('.mCard');
					var peer_id = mCard.attr('data-id');
					var xmpp_user = mCard.attr('data-xmpp-user');
					$.ajax({
						url: 'ajax/unfollowMember',
						type: 'post',
						data: 'to_uid='+peer_id,
						success: function(data){
							data = eval('('+data+')');
							if(data.status == 'success'){
								$('ul.contact-list [data-uid="'+data.user.uid+'"]').remove();
								$(mCard.find('.follow')[0]).show();
								$(mCard.find('.unfollow')[0]).hide();
								sortContactList();
							}
							$('.contact-list li').unbind().click(openChatFromContactList);
						}
					});
				});
				refreshPresenceMCard();
			}
		});
	});
	function remove_block(remove_id){
		var replacement = $('.mCard[data-id='+remove_id+']').next().next().next();
		var replacement_id = replacement.attr('data-id');
		var duration = 2000;

		$('.mCard[data-id='+remove_id+']').css({'opacity': 0});

		$($('.mCard[data-id='+replacement_id+']').clone().css({'position': 'relative', 'top': replacement.top, 'opacity': 0, 'margin-left': 4})
			.addClass('clone').removeAttr('data-id'))
			.insertAfter($('.mCard[data-id='+replacement_id+']')
			.css({'position': 'absolute', top: replacement.offset().top - 119,'left':replacement.offset().left - 167, 'z-index':1}));

		if( $('.mCard[data-id='+replacement_id+']').position().left == 652.5 )
			duration = 0;
		$('.mCard[data-id='+replacement_id+']').animate({'top': replacement.offset().top - 220, 'left': replacement.offset().left - 167}, function(){
			$($('.mCard[data-id='+replacement_id+']').css({'position': 'relative', 'top': 0, 'left': 0}))
									.insertAfter($('.mCard[data-id='+remove_id+']').hide());
			$('.mCard[data-id='+remove_id+']').remove();
			$('.clone').animate({'margin-left' : -322}, duration, function(){
				$(this).remove();
			});
		});
		
	}
</script>