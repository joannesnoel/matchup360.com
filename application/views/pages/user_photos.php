<?php
	if($this->session->userdata('logged_uid') == $uid){
?>
		<a id="upload_photos" href="#" class="thumbnail square_130">
			<img src="http://placehold.it/130x130&text=Upload%20Photos"/>
		</a>
<?php	
	}
	foreach($user_photos as $photo){
?>
		<a href="<?php echo $photo['largest']['url']; ?>" class="user_photo thumbnail square_130 popover-parent" data-photo-reference="<?php echo $photo['thumbnail']['reference']; ?>">
<?php
		if($this->session->userdata('logged_uid') == $uid){
?>
			<div class="options">
				<button class="btn btn-danger btn-mini remove" data-placement="bottom" title="Remove Photo"><i class="icon-remove"></i></button>
				<button class="btn btn-primary btn-mini primary" data-placement="bottom" title="Set as Primary Picture"><i class="icon-user"></i></button>
			</div>
<?php		
		}
?>
			<img src="<?php echo $photo['thumbnail']['url']; ?>"/>
		</a>
<?php
	}
?>
<script type="text/javascript">
$('button[title]').tooltip();
$('.user_photo').colorbox({
							rel:'user_photo_gallery',
							scalePhotos: true,
							maxHeight: $(window).height() * 0.75
						});
$('.user_photo .remove')
				.popover({html:true,placement:'left',title:'Delete this photo?',content:'<button id="confirmed-delete" class="btn btn-danger btn-small"><i class="icon-white icon-remove"></i> Yes, delete</button> <button id="close-popover" class="btn btn-small">Cancel</button>'});
$('.user_photo .remove').click(function(e){
	e.preventDefault();
	e.stopPropagation();
});
$('#upload_photos').click(function(e){
	$('body').prepend('<div id="uploadPhotoModal" class="modal hide fade">\
									  <div class="modal-header">\
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
										<h3>Upload Photos</h3>\
									  </div>\
									  <div class="modal-body">\
										<div id="upload_user_photos">\
											<span class="message">Drop images here to upload.</span>\
										</div>\
									  </div>\
									  <div class="modal-footer">\
										<button data-dismiss="modal" data-complete-text="Done" data-loading-text="Uploading..." class="btn footer-close">Close</button>\
									  </div>\
									</div>');
	$('#uploadPhotoModal').modal({ show: true });
	$(function(){

		var dropbox = $('#upload_user_photos'),
			message = $('.message', dropbox);

		dropbox.filedrop({
			// The name of the $_FILES entry:
			paramname:'userfile',

			maxfiles: 5,
			maxfilesize: 2, // in mb
			url: window.location.protocol + '//' + window.location.hostname + '/upload/do_upload',

			uploadFinished:function(i,file,response){
				$.data(file).attr('uploaded_photo',response[0].url).addClass('done');
				removeIcon = $('<i class="icon-remove"/>').click(function(){
					$.ajax({
						url: response[0].delete_url,
						dataType: 'json',
						success: function(response){
							if(response[0].success){
								$.data(file).fadeOut().remove();
								if($('#upload_user_photos [uploaded_photo]').length == 0){
									$('#upload_user_photos .message').show();
									$('#uploadPhotoModal button.footer-close').button('reset');
								}
							}
						}
					});
				});
				$.data(file).find('.imageHolder').prepend(removeIcon);
				// response is the JSON object that post_file.php returns
			},
			
			afterAll: function(){
				$('#uploadPhotoModal button.close').show();
				$('#uploadPhotoModal button.footer-close').button('complete');
			},

			error: function(err, file) {
				switch(err) {
					case 'BrowserNotSupported':
						showMessage('Your browser does not support HTML5 file uploads!');
						break;
					case 'TooManyFiles':
						alert('Too many files! Please select 5 at most!');
						break;
					case 'FileTooLarge':
						alert(file.name+' is too large! Please upload files up to 2mb.');
						break;
					default:
						break;
				}
			},

			// Called before each upload is started
			beforeEach: function(file){
				if(!file.type.match(/^image\//)){
					alert('Only images are allowed!');

					// Returning false will cause the
					// file to be rejected
					return false;
				}
			},

			uploadStarted:function(i, file, len){
				createImage(file);
				$('#uploadPhotoModal button.close').hide();
				$('#uploadPhotoModal button.footer-close').button('loading');
			},

			progressUpdated: function(i, file, progress) {
				$.data(file).find('.progress').width(progress);
			}

		});

		var template = '<div class="preview">'+
							'<span class="imageHolder">'+
								'<img />'+
								'<span class="uploaded"></span>'+
							'</span>'+
							'<div class="progressHolder">'+
								'<div class="progress"></div>'+
							'</div>'+
						'</div>'; 

		function createImage(file){

			var preview = $(template),
				image = $('img', preview);

			var reader = new FileReader();

			image.width = 100;
			image.height = 100;

			reader.onload = function(e){

				// e.target.result holds the DataURL which
				// can be used as a source of the image:

				image.attr('src',e.target.result);
			};

			// Reading the file as a DataURL. When finished,
			// this will trigger the onload function above:
			reader.readAsDataURL(file);

			message.hide();
			preview.appendTo(dropbox);

			// Associating a preview container
			// with the file, using jQuery's $.data():

			$.data(file,preview);
		}

		function showMessage(msg){
			message.html(msg);
		}

	});
	$('#uploadPhotoModal').on('hide',function(e){
		if($('#upload_user_photos [uploaded_photo]').length > 0){
			uploaded_photos = [];
			$('#upload_user_photos [uploaded_photo]').each(function(i){
				uploaded_photos.push($(this).attr('uploaded_photo'));
			});
			$.ajax({
				url: window.location.protocol + '//' + window.location.hostname + '/rest/save_user_photos',
				type: 'post',
				data: 'photos='+JSON.stringify(uploaded_photos),
				dataType: 'json',
				success: function(response){
					if(response.status == 'success')
						$('#my-photos').click();
				}
			});
		}
	});
	$('#uploadPhotoModal').on('hidden',function(){
		$('#uploadPhotoModal').remove();
	});
	e.preventDefault();
});
</script>