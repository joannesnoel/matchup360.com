<div style="width: 600px; margin: 0 auto">
<?php if($profile_completion < 70){ ?> 
	<div class="home-account-settings">
		<div class="header">Update Profile</div>
		<div class="content">
			<div class="left-box" style="width: 300px;">
				<div class="vertical-align-center-parent" style="width: 220px; height: 150px">
					<span class="vertical-align-center-child" style="height: 60px">
						To improve your profile and for better matchup, update your profile.
						<a id="update-profile-link">Update my profile</a>
					</span>
				</div>
			</div>
			<div class="right-box">
				<div class="image">
					<img src="http://matchup360.com/assets/img/update-profile-snippet.PNG">
				</div>
			</div>
		</div>
	</div>
	<hr />
<?php } ?>
<?php if($profile_pic == ''){ ?>
	<div class="home-account-settings">
		<div class="header">Upload a Profile Picture</div>
		<div class="content">
			<div class="left-box">
				<div class="image">
					<img src="http://matchup360.com/assets/img/blank_profile_female.jpg">
				</div>
			</div>
			<div class="right-box" style="width: 300px;">
				<div class="vertical-align-center-parent" style="height: 150px; width: 100%;">
					<div id="upload-from-pc" class="upload vertical-align-center-child" style="height: 45px; width: 120px;">
						<a id="upload-photos">Upload photos</a>
						<hr style="margin-bottom: 0px;"/>
						<span>from your computer</span>
					</div>
				</div>
				<div style="text-align: center; color: red; font-size: 12px">
					Your profile will not be visible to other users without a profile picture.
				</div>
			</div>
		</div>
	</div>
	<hr />
<?php } ?>
</div> 
<script type="text/javascript">
	$('#update-profile-link').click(function(){
		loadPage('edit_profile', 'Edit Profile');
	});
	$('#upload-photos').click(function(){
		loadPage('my_photos', 'My Photos');
	}); 
	
</script>  