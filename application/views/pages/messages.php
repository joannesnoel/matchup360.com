<?php if(count($message_threads) >= 1){ ?>
	<div id="message-threads-container" style="width: 250px; float: left; cursor:pointer">
		<div id="message-threads">
			<?php foreach($message_threads as $thread){?>
				<div class="thread" data-uid="<?php echo $thread['uid']?>">
					<img class="profile_pic" src="<?php echo $thread['profile_pic']?>">
					<span class="name">
						<?php echo $thread['firstname'];?> <?php echo $thread['lastname']?>
					</span>
				</div>
			<?php 	}?>
		</div>
	 </div>
	 <div id="message-thread-container">
		
	 </div>
	 <div style="clear:both"></div>
<?php }else{ ?>
	<div style="text-align: center;"><p>No Messages</p></div>
<?php } ?>
 <script type="text/javascript">
	clickThread = function(){
			message_uid = $(this).attr('data-uid');
			$('.thread').removeClass('active');
			thread = $(this);
			$.ajax({
				url: 'ajax/getMessageThread/'+message_uid,
				success: function(data){
					data = eval('('+data+')');
					html = '';
					thread.addClass('active'); 
					$('#message-thread-container').empty();
					$('#message-thread-container').append('<ul id="messages-container" data-peer="'+message_uid+'"></ul>');
					row_class = 'even';
					$.each(data, function(i, value){
						
						if(i ==0){
							$('#messages-container').append('<li class="message-wrapper odd" data-from="'+value.from_uid+'">\
										<img class="profile_pic" src="'+value.profile_pic+'">\
										<div class="message"><p>'+value.message+'</p></div>\
										<div class="sent">'+value.sent_date+'</div>\
										<div style="clear: both"></div>\
									</li>');
						}else{
							if($('.message-wrapper').last().attr('data-from') == value.from_uid){
										$('.message-wrapper').last().find('.message').append('<p>'+value.message+'</p>');
							}else{
								$('#messages-container').append('<li class="message-wrapper '+row_class+'" data-from="'+value.from_uid+'">\
										<img class="profile_pic" src="'+value.profile_pic+'">\
										<div class="message"><p>'+value.message+'</p></div>\
										<div class="sent">'+value.sent_date+'</div>\
										<div style="clear: both"></div>\
									</li>');
									if(row_class == 'odd')
										row_class = 'even';
									else
										row_class = 'odd';
							}
						}
						
						
					});
					$('#message-thread-container').append('<div id="textarea">\
															<textarea class="message-box" style="width: 95%;"></textarea>\
															<button class="send-button" \
															data-to='+message_uid+'><span class="ui-button">Send</span></button>\
														</div>');
					if($('.message-wrapper').length != 0)
						$('#messages-container').scrollTo($('.message-wrapper').last());
					$('.send-button').click(function(){
						to_uid = $(this).attr('data-to');
						message = $(this).parent().find('textarea').val();
						$.ajax({
							url: 'ajax/sendMessage',
							data: 'to_uid='+to_uid+'&message='+message,
							type: 'post',
							success: function(data){
								
								data = eval('('+data+')');
								message_info = eval('('+data.message+')');
								row_class = 'odd';
								$('.message-box').val('');
								if($('.message-wrapper').last().attr('data-from') == message_info.from_uid){
										$('.message-wrapper').last().find('.message').append('<p>'+message+'</p>');
								}else{
								$('#messages-container').append('<li class="message-wrapper '+row_class+'" data-from="'+message_info.from_uid+'">\
										<img class="profile_pic" src="'+message_info.from_profile_pic+'">\
										<div class="message">'+message+'</div>\
										<div class="sent"></div>\
										<div style="clear: both"></div>\
									</li>');
									
								}
								if(row_class == 'odd')
										row_class = 'even';
									else
										row_class = 'odd';
								var stanza = $msg({'to': data.to_jid, 'type': 'chat'}).c('body').t(data.message);
								connection.send(stanza);
								if($('.message-wrapper').length != 0)
									$('#messages-container').scrollTo($('.message-wrapper').last());
								
							}
						});
					});		
					$(".message-box").keypress(function(event) {
					  if ( event.which == 13 ) {
						 event.preventDefault();
						 $('.send-button').click();
					   }
					});
				}
			
			});
		}
	$('#MainMenu li').removeClass('active');
	$('#messages').addClass('active');
	$('#message-threads-container').ready(function(){
		if($('.thread').length>0){
			$('.thread').click(clickThread);
			$('.thread')[0].click(); 
		}
	});
 </script>