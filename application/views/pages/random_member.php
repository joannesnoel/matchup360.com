<?php if($user){ ?><div id="random-member" data-uid="<?php echo $user['uid']?>">
	<div id="meet">
		<p>Does <span id="member-gender"><?php echo($user['sex'] == 'f'? 'she': 'he')?> interest you?</span></p>
		<div>
			<button class="meet-btn" name="encounter" value="1"><span>Yes</span></button>
			<button class="meet-btn" id="maybe-btn"><span>Maybe</span></button>
			<button class="meet-btn" name="encounter" value="0"><span>No</span></button>
		</div>
	</div>
	<div id="user-info-container" style="position: relative">
		<div id="image-wrapper" style="min-height: 375px;">		
			<?php $first_photo = $user_photos[0];?>	
			<div id="image">
				<!--<img src="<?php 
						echo config_item('s3_bucket_url').$user['uid'].'/photos/'.$first_photo['largest']['filename']?>"/>-->
			</div>
		</div>
	</div>
	<div id="gallery-wrapper">
		<div id="gallery">
			<?php foreach($user_photos as $photo){?>
				<div class="photo-wrapper" data-largest="<?php echo $photo['largest']['filename']?>" data-largest-width="<?php echo $photo['largest']['width']?>">
					<img src="<?php echo config_item('s3_bucket_url').$user['uid'].'/photos/'.$photo['thumbnail']['filename']?>">
				</div>
			<?php }?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#random-member').ready(function(){
		$('#image-wrapper img').css('max-height', $(window).height()-300);
		$('#gallery-wrapper #gallery').width(($('#gallery .photo-wrapper').width()+5)*$('#gallery .photo-wrapper').length);
		$('#gallery .photo-wrapper').click(function(){
			var image_largest = $(this).attr('data-largest');
			var image_largest_width = $(this).attr('data-largest-width');
			$('#image-wrapper').html('<div id="image">\
										<img src="<?php echo config_item('s3_bucket_url').$user['uid'].'/photos/'?>'+image_largest+'"/>\
									</div>');
			
			$('#image-wrapper img').css('max-height', $(window).height()-300);
			$('#image-wrapper img').bind('load', function(){
				$(this).hover(
					function(){
						$('#image').css('width', $(this).width());
						$(this).parent().append('<div id="profile-info" class="info-box">\
													<div style="margin-bottom: 10px"><h1 style="color: white; display: inline-block; font-size: 20px"><?php echo $user['firstname'].' '.$user['lastname']?></h1>\
													<span>-- Age <?php echo $user['age']?></span></div>\
													<div style="word-wrap: break-word;"><?php echo $user['city'].', '.$user['state'].', '.$user['country']?></div>\
												</div>');},
					function(){
						$('#profile-info').remove();
					});
			});
		});
		$('#gallery .photo-wrapper')[0].click();
		$('.meet-btn').click(function(){
			 var btn_id = $(this);
			 var random_user_uid = $('#random-member').attr('data-uid');
			 if(btn_id.attr('id') == 'maybe-btn'){
				loadPage('random_member');
			}else{
				 $.ajax({
					url: './../ajax/addUserEncounter',
					data: 'random_user_uid='+random_user_uid+'&encounter='+btn_id.val(),
					type: 'post',
					success: function(data){
						//data = eval('('+data+')');
						debugger; 
						if(btn_id.val() == 1){
							var stanza = $msg({'to': data.xmpp_user+'@matchup360.com', 'type': 'notification', 'notification_type': 'interested', 'notification_string': data.notification_string});
							connection.send(stanza);
						}
						loadPage('random_member');
					}
				 });
			 }
		});
	});
</script>
<?php }else { ?>
	<div style="text-align: center"><p>Please come back in few hours to use this feature again.</p></div>
<?php }?>