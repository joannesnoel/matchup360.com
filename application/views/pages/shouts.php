	<div class="well">
		<div class="thumbnail square_40">
			<img src="<?php echo $me['profile_pic']; ?>" alt="<?php echo $me['firstname'] .' '. $me['lastname']; ?>">
		</div>
		<textarea id="text_shout" placeholder="What do you want to say?"></textarea>
		<div class="post_options btn-group">
			<button id="post_location" class="btn"><i class="icon-screenshot"></i> <?php echo (!empty($me['current_location_text']))? $me['current_location_text'] : 'Set your location'; ?></button>
			<button id="attach_photos" class="btn"><i class="icon-picture"></i> Attach Photo/s</button>
			<button id="post_submit" class="btn"><i class="icon-edit"></i> Post</button>
		</div>
		<div style="clear:both"></div>
		<div id="dropbox">
            <span class="message">Drop images here to upload.</span>
        </div>
	</div>
	<script type="text/javascript">
	$('#ad160x600').appendTo("div#feeds");
	$("#ad160x600").show();
	$('#post_location').tooltip({ html:true, placement:'bottom', title:'Change current location', trigger:'hover' });
	$('#post_location').click(function(){
		$('#locationModal').modal('show');
	});
	$('#attach_photos').click(function(){
		$(this).button('toggle');
		$('#dropbox').toggle();
		if(!$(this).hasClass('active')){
			$('#dropbox .preview').remove();
		}
	});
	if($('input[name="lat"]').val() == '' || $('input[name="lng"]').val() == '' || $('input[name="formatted_address"]').val() == '')
		$('#locationModal').modal('show');
	$('#post_submit').click(function(){
		if($('input[name="lat"]').val() == '' || $('input[name="lng"]').val() == '' || $('input[name="formatted_address"]').val() == ''){
			$('#post_location').focus();
		}else{
			message = $('#text_shout').val();
			attach_photos = [];
			$('[attached_photo]').each(function(i){
				attach_photos.push($(this).attr('attached_photo'));
			});
			if(message != ''){
				theButton = $(this);
				theButton.button("loading");
				$.ajax({
					url: window.location.protocol + '//' + window.location.hostname + '/rest/post',
					type: 'post',
					data: 'message='+message+'&photos='+JSON.stringify(attach_photos),
					dataType: 'json',
					success: function(response){
						if(response.redirect)
							window.location = response.redirect;
						if(response.status == 'success'){
							theButton.button("reset");
							$('div#feeds .stream .alert').fadeOut();
							if($('#dropbox').is(":visible"))
								$('#attach_photos').click();
							$('#text_shout').val('');
						}
					}
				});
			}
		}
	});
	$(function(){

		var dropbox = $('#dropbox'),
			message = $('.message', dropbox);

		dropbox.filedrop({
			// The name of the $_FILES entry:
			paramname:'userfile',

			maxfiles: 5,
			maxfilesize: 2, // in mb
			url: window.location.protocol + '//' + window.location.hostname + '/upload/do_upload',

			uploadFinished:function(i,file,response){
				$.data(file).attr('attached_photo',response[0].url).addClass('done');
				removeIcon = $('<i class="icon-remove"/>').click(function(){
					$.ajax({
						url: response[0].delete_url,
						dataType: 'json',
						success: function(response){
							if(response[0].success)
								$.data(file).fadeOut().remove();
						}
					});
				});
				$.data(file).find('.imageHolder').prepend(removeIcon);
				// response is the JSON object that post_file.php returns
			},

			error: function(err, file) {
				switch(err) {
					case 'BrowserNotSupported':
						showMessage('Your browser does not support HTML5 file uploads!');
						break;
					case 'TooManyFiles':
						alert('Too many files! Please select 5 at most!');
						break;
					case 'FileTooLarge':
						alert(file.name+' is too large! Please upload files up to 2mb.');
						break;
					default:
						break;
				}
			},

			// Called before each upload is started
			beforeEach: function(file){
				if(!file.type.match(/^image\//)){
					alert('Only images are allowed!');

					// Returning false will cause the
					// file to be rejected
					return false;
				}
			},

			uploadStarted:function(i, file, len){
				createImage(file);
			},

			progressUpdated: function(i, file, progress) {
				$.data(file).find('.progress').width(progress);
			}

		});

		var template = '<div class="preview">'+
							'<span class="imageHolder">'+
								'<img />'+
								'<span class="uploaded"></span>'+
							'</span>'+
							'<div class="progressHolder">'+
								'<div class="progress"></div>'+
							'</div>'+
						'</div>'; 

		function createImage(file){

			var preview = $(template),
				image = $('img', preview);

			var reader = new FileReader();

			image.width = 100;
			image.height = 100;

			reader.onload = function(e){

				// e.target.result holds the DataURL which
				// can be used as a source of the image:

				image.attr('src',e.target.result);
			};

			// Reading the file as a DataURL. When finished,
			// this will trigger the onload function above:
			reader.readAsDataURL(file);

			message.hide();
			preview.appendTo(dropbox);

			// Associating a preview container
			// with the file, using jQuery's $.data():

			$.data(file,preview);
		}

		function showMessage(msg){
			message.html(msg);
		}

	});
	</script>
	<div class="stream">
<?php 
	if(count($posts)>0){
		foreach($posts as $post){ 
			$this->load->view('widgets/stream_entry',$post);
		} 
	}else{ ?>
		<div class="alert" style="text-align:center">No one posted something in this place yet. </div>
<?php	} ?>