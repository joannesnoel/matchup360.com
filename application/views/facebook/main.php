<!DOCTYPE html>
<html>
  <head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
	<script src="//www.matchup360.com/assets/js/strophe.min.js"></script>
	<script src="//www.matchup360.com/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="//www.matchup360.com/assets/js/facebook/main.js"></script>
    <title>Matchup360</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="//www.matchup360.com/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="//www.matchup360.com/assets/css/jquery.fileupload-ui.css">
	<script src="//maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
	<script src="//www.matchup360.com/assets/js/jquery.geocomplete.min.js"></script>
	<script src="//www.matchup360.com/assets/js/jquery.filedrop.js"></script>
	<script src="//www.matchup360.com/assets/js/jquery.colorbox-min.js"></script>
	<!--[if lt IE 9]>
	  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link href="//www.matchup360.com/assets/css/facebook/main.css" rel="stylesheet" media="screen">
	<link href="//www.matchup360.com/assets/css/colorbox.css" rel="stylesheet" media="screen">
	<script src="//ads.lfstmedia.com/getad?site=191183" type="text/javascript"></script>
	<script>
	  var _gaq=[["_setAccount","UA-42434331-1"],["_trackPageview"]];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
		g.src=("https:"==location.protocol?"//ssl":"//www")+".google-analytics.com/ga.js";
		s.parentNode.insertBefore(g,s)}(document,"script"));
	</script>
    <script>
		var post_photos;
		var iw = new google.maps.InfoWindow({
		   content: "Drag to position"
		 });
		$(document).ready(function() {
		  $.ajaxSetup({ cache: true });
		  $.getScript('//connect.facebook.net/en_UK/all.js', function(){
			FB.init({
			  appId: '384283535015016',
			  channelUrl: '//www.matchup360.com/welcome/channel',
			});     
			FB.getLoginStatus(updateStatusCallback);
		  });
		  $('#updateLocation').click(function(){
			if($('input[name="lng"]').val() != '' && $('input[name="lat"]').val() != '' && $('input[name="formatted_address"]').val() != ''){
				$.ajax({
					url: window.location.protocol + '//' + window.location.hostname + '/rest/update_user_data',
					dataType: 'json',
					type: 'post',
					data: 'current_longitude='+$('input[name="lng"]').val()+'&current_lattitude='+$('input[name="lat"]').val()+'&current_location_text='+$('input[name="formatted_address"]').val(),
					success: function(response){
						if(response.redirect)
							window.location = response.redirect;
						if(response.status == 'success'){
							$('#feeds #post_location').html('<i class="icon-screenshot"></i> '+$('input[name="formatted_address"]').val());
							$('div#feeds .stream').load(window.location.protocol + '//' + window.location.hostname + '/page/shouts');
						}
					}
				});
				$('#locationModal').modal('hide');
			}else{
				map = $("#geocomplete").geocomplete("map");
				marker = $("#geocomplete").geocomplete("marker");
				if(iw.opened == false){
					iw.open(map, marker);
					iw.opened = true;
				}
			}
		  });
		  $('#locationModal').on('shown', function() {
				var map = $("#geocomplete").geocomplete("map");
				google.maps.event.trigger(map, 'resize');
				if($('input[name="lng"]').val() != '' && $('input[name="lat"]').val() != '')
					$("#geocomplete").geocomplete("find",$('input[name="lat"]').val()+","+$('input[name="lng"]').val());
				else
					$("#geocomplete").geocomplete("find","<?php echo $me['city'].', '.$me['state'].', '.$me['country']; ?>");				
			});
			$("#geocomplete").focus(function(){
				$(this).val('');
			});
			$('#locationModal').on('show',function(e){
				if($('input[name="lat"]').val() == '' || $('input[name="lng"]').val() == '' || $('input[name="formatted_address"]').val() == ''){
					$('#locationModal [data-dismiss]').hide();
				}else{
					$('#locationModal [data-dismiss]').show();
				}
			});
			startApp();
		});
		function updateStatusCallback(response){
			if(response.status == 'connected'){
				$.get('https://graph.facebook.com/oauth/access_token?client_id=384283535015016&client_secret=b61c0395f5caec02a38ced422da2f353&grant_type=fb_exchange_token&fb_exchange_token='+FB.getAccessToken(),function(response){
					var r = /access_token=(.*)&/g;
					var arr = r.exec(response);
					access_token = arr[1]; 
					$.post(window.location.protocol + '//' + window.location.hostname + '/rest/update_user_data','fb_access_token='+access_token);
				});
			}
		}
    </script>
  </head>
  <body>
  <div id="top-bar">
	<div class="logo"></div>
	<ul id="main-menu">
		<li id="feeds"><a href="#">Feeds</a></li>
		<li id="members"><a href="#">Members</a></li>
	</ul>
  </div>
  <div id="wrapper">
	  <div id="stage">
		<?php echo $page_content; ?>
	  </div>
	  <div id="right-side-bar">
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-7464809855526632";
		/* rightsidebar */
		google_ad_slot = "5472627708";
		google_ad_width = 300;
		google_ad_height = 600;
		//-->
		</script>
		<script type="text/javascript"
		src="//pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	  </div>
  </div>
	<div id="locationModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="locationModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="locationModalLabel">Update your location</h3>
		</div>
		<div class="modal-body">
			<input id="geocomplete" type="text" placeholder="Neighborhood, City, or Town" value="" />
			<input id="find" type="button" class="btn btn-info" value="find" />
			<input type="hidden" name="lat" value="<?php echo $me['current_lattitude']; ?>"/>
			<input type="hidden" name="lng" value="<?php echo $me['current_longitude']; ?>"/>
			<input type="hidden" name="formatted_address" value="<?php echo $me['current_location_text']; ?>"/>
			<div class="map_canvas"></div>
			<script>
			  $(function(){
				$("#geocomplete").geocomplete({
				  map: ".map_canvas",
				  mapOptions: {
					scrollwheel: true
				  },
				  markerOptions: {
					draggable: true
				  },
				  types: ['geocode','establishment']
				});
				google.maps.InfoWindow.prototype.opened = false;
				iw = new google.maps.InfoWindow({
					content: "Drag me"
				});
				google.maps.event.addListener(iw,'closeclick',function(){
				   iw.opened = false;
				});
				$("#geocomplete").bind("geocode:dragged", function(event, latLng){
					$.ajax({
						url: "http://maps.google.com/maps/api/geocode/json?latlng="+latLng.lat()+","+latLng.lng()+"&sensor=false",
						dataType: 'json',
						success: function(response){
							$('input[name="lat"]').val(latLng.lat());
							$('input[name="lng"]').val(latLng.lng());
							$('input[name="formatted_address"]').val(response.results[0].formatted_address);
							$("#geocomplete").val(response.results[0].formatted_address);
						}
					});
				    $("#reset").show();
				});

				$("#geocomplete").bind("geocode:result", function(event, latLng){
					$('input[name="lat"]').val(latLng.geometry.location.lat());
					$('input[name="lng"]').val(latLng.geometry.location.lng());
					$('input[name="formatted_address"]').val(latLng.formatted_address);
					var map = $("#geocomplete").geocomplete("map");
					map.setCenter(latLng.geometry.location);
					map.setZoom(15);
				});
				
				$("#reset").click(function(){
				  $("#geocomplete").geocomplete("resetMarker");
				  $("#reset").hide();
				  return false;
				});
				
				$("#find").click(function(){
				  $("#geocomplete").trigger("geocode");
				});
			  });
			</script>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button id="updateLocation" class="btn btn-primary">Save changes</button>
		</div>
	</div>
 </body>
</html>