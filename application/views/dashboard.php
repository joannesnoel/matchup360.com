<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Matchup360</title>

        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="//matchup360.com/assets/css/dashboard.css" />
		<link rel="stylesheet" href="//matchup360.com/assets/css/jquery-impromptu.css" />
		<link rel="stylesheet" href="//matchup360.com/assets/css/colorbox.css" />
		<link rel="stylesheet" href="//matchup360.com/assets/css/perfect-scrollbar-0.4.1.min.css" />
		<link href="//matchup360.com/assets/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
		<link href="//matchup360.com/assets/css/uploadify.css" rel="stylesheet" type="text/css" />
		<!--<link href="//matchup360.com//assets/css/signup_page.css" rel="stylesheet" type="text/css" />-->
		<link rel="shortcut icon" href="//matchup360.com/assets/img/favicon.ico" type="image/x-icon" />
		
		<style>
			.jqi{
				width: auto !important;
			}
		</style>


        <!--[if lt IE 9]>
          <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
		<script src="//matchup360.com/assets/js/jquery.min.js"></script>
		<script src="//matchup360.com/assets/js/perfect-scrollbar-0.4.1.with-mousewheel.min.js"></script>
		<script src="//matchup360.com/assets/js/jquery.scrollTo-min.js"></script>
		<script src="//matchup360.com/assets/js/jquery.ba-hashchange.min"></script>
		<script src="//matchup360.com/assets/js/jquery.colorbox-min.js"></script>
		<!--<script src="https://swww.tokbox.com/webrtc/v2.0/js/TB.min.js"></script>-->
		<!--<script src="//static.opentok.com/v1.1/js/TB.min.js" ></script>-->
		<script src="//matchup360.com/assets/js/strophe.min.js"></script>
		<script src="//matchup360.com/assets/js/jabber.js"></script>
		<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
		<script src="//matchup360.com/assets/js/jquery.geocomplete.min.js"></script>
		<script type="text/javascript" src="//matchup360.com/assets/js/jquery-impromptu.js"></script>
		<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.uploadify.min.js"></script>
		<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.filedrop.js"></script>
		<script type="text/javascript" src="//www.matchup360.com/assets/js/jquery.Jcrop.min.js"></script>
		<script type="text/javascript">
					
			var connection = null;
			var xmpp_user = "<?php echo $xmpp_user; ?>@matchup360.com/"+makeid();
			var xmpp_pass = "<?php echo $xmpp_password; ?>";
			var uid		  = "<?php echo $uid; ?>";
			var my_fullname = "<?php echo $firstname . " " . $lastname ?>";
			var my_profile_pic = "<?php echo $profile_photo ?>";
			var access_token = '';
			//var color_palette = ['#A200FF','#FF0097','#00ABA9','#8CBF26','#A05000','#E671B8','#F09609','#1BA1E2','#E51400','#339933'];
			//var color_palette = ['#00A0B1','#2E8DEF','#A700AE','#643EBF','#BF1E4B','#DC572E','#00A600','#0A5BC4','#9fc255'];
			//var color_palette = ['#3894e7','#1880d6','#58ba77','#bed267','#91d152','#eaae62','#eb7055','#dd7499','#b166a0'];
			var color_palette = ['#eaae62','#eb7055','#dd7499','#b166a0'];
			$(document).ready(function () {				
				$('#btn-logout').click(logout);
				$('#update-profile').click(function(){
												loadPage('edit_profile');
												$('#account-menu-list').slideUp('fast');
				});
				$('#my-photos').click(function(){
												loadPage('my_photos');
												$('#account-menu-list').slideUp('fast');
				}); 
				$('#account-info').click(function(){
												loadPage('account_settings');
												$('#account-menu-list').slideUp('fast');
				}); 
				$('#message_notification').click(function(){
												loadPage('messages');
				}); 
				$('#profile-photo, #user-fullname').click(function(){
												loadPage('view_profile/'+uid);
				});
				/*$('#user-fullname').click(function(){
												loadPage('view_profile/'+uid);
				});*/
				connectXMPP();
				if(window.location.pathname.substr(1) != ''){
					loadPage(window.location.pathname.substr(1));
				}else{
					loadPage('home');
				}
				window.onpopstate = function(event) {
					if(event.state)
						loadPage(event.state.page_path);
				};
				
				$('.contact-list li').unbind().click(openChatFromContactList);
				$(window).hashchange( function(){
								loadPage(location.hash.replace('#',''));
							});
				setTimeout(refreshPresenceMCard,60000);
				
				$('#threadsList .thread').unbind().click(loadMessageThread);
				$('#threadsList .thread').first().click();
				$('#messageBoxOverlay').click(function(){
					$('#messagesBar').animate({'left':'-670px'},400);
					$('#messageBoxOverlay').animate({'opacity':'0.'},400,null,function(){$(this).hide();});
				});
				$('#messagesBar .ticker').click(function(e){
					messagesBar = $(this).parent();
					if(messagesBar.css('left') == '0px'){
						messagesBar.animate({'left':'-670px'},400);
						$('#messageBoxOverlay').animate({'opacity':'0.'},400,null,function(){$(this).hide();});
					}else{
						messagesBar.animate({'left':'0px'},400);
						$('#message_notification').html('0').hide();
						$('#messageBoxOverlay').css('opacity',0).show().animate({'opacity':'0.5'},400);
					}
					e.stopPropagation();
				});
				$('#messagesBar').click(function(e){
					e.stopPropagation();
				});
				$('#messagesBar #messengerBox textarea').bind('keypress', function(e) {
																	var code = (e.keyCode ? e.keyCode : e.which);
																	if(code == 13) { //Enter keycode
																	  sendMessage(this);
																	  e.preventDefault();
																	}
																});
			});
			
			function loadMessageThread(caller){
				$('#threadsList').css('width','265px');
				$('#messagesBox textarea').show();
				peerid = $(this).attr('data-peerid');
				$('#threadsList .thread').removeClass('active');
				$('#threadsList .thread[data-peerid="'+peerid+'"]').addClass('active');
				$('#messagesBar #messagesBox').attr('data-peerid',peerid);
				$.ajax({
					url: window.location.protocol + '//' + window.location.hostname + '/ajax/getMessageThread/' + peerid,
					beforeSend: function(){
						$('#messagesBar #messagesBox .messages').fadeOut(400);
					},
					success: function(messages){
						$('#threadsList .thread[data-peerid="'+peerid+'"]').removeClass('unread');
						$('#messagesBar #messagesBox .messages').remove();
						$('#messagesBar #messagesBox').prepend('<div class="messages"></div>');
						for(var i=0;i<messages.length; i++){
							message = messages[i];
							if($('#messagesBar #messagesBox .messages .line').length == 0 || $('#messagesBar #messagesBox .messages .line').last().attr('data-from-uid') != message.from_uid){
								if(message.from_uid == uid)
									$('#messagesBar #messagesBox .messages').append('<div class="line" data-from-uid="'+message.from_uid+'"></div>');
								else
									$('#messagesBar #messagesBox .messages').append('<div class="line recieved" data-from-uid="'+message.from_uid+'"></div>');
							}
							$('#messagesBar #messagesBox .messages .line').last().append('<p>'+message.message+'</p>');
						}
						if($('#messagesBar #messagesBox .messages p').length > 0)
							$('#messagesBar #messagesBox .messages').scrollTo($('#messagesBar #messagesBox .messages p').last());
						$('#messagesBox .messages').perfectScrollbar();
					}
				});
			}
			function startChat(peer_id,fullname,profile_pic,address){
				threadsList = $('#threadsList');
				threadsList.css('width','265px');
				$('#messagesBox textarea').show();
				if(threadsList.find('.thread').first().attr('data-peerid') != peer_id){
					$(threadsList.find('[data-peerid="'+peer_id+'"]')[0]).remove();
					thread = '<div class="thread unread" data-peerid="'+peer_id+'">\
										<img src="'+profile_pic+'" width="50">\
										<div class="name">'+fullname+'</div>\
										<div class="message"></div>\
									</div>';
					thread = $(thread).click(loadMessageThread);
					threadsList.prepend(thread);
				}
				if($('#threadsList .thread[data-peerid="'+peer_id+'"]').length > 0)
					$('#threadsList .thread[data-peerid="'+peer_id+'"]').click();
				else{
					thread = '<div class="thread unread" data-peerid="'+peer_id+'">\
										<img src="'+profile_pic+'" width="50">\
										<div class="name">'+fullname+'</div>\
									</div>';
					thread = $(thread).click(loadMessageThread);
					threadsList.prepend(thread);
					thread.click();
				}
				if($('#messagesBar').css('left') != '0px')
					$('#messagesBar .ticker').click();
				$('#messagesBar textarea').focus();
			}
			function makeid(){
				var text = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

				for( var i=0; i < 5; i++ )
					text += possible.charAt(Math.floor(Math.random() * possible.length));

				return text;
			}
			function loadPage(path){
				$('#page-loader').show();
				pagePath = window.location.protocol + '//' + window.location.hostname + '/page/' + path;
				$.ajax({
					url: pagePath,
					cache: false,
					success: function(response){
						response = JSON.parse(response);
						if(response.redirect){
							window.location = window.location.protocol + '//' + window.location.hostname + response.redirect;
						}
						$('#mainContent').html(response.html);
						document.title = response.title;
						$('#page-loader').hide();
						urlPath = window.location.protocol + '//' + window.location.hostname + '/' + path;
						window.history.pushState({"html":response.html,"title":response.title,"page_path":path},"", urlPath);
						_gaq.push(['_trackPageview', '/'+path]);
					}
				});
			}
			function openChatFromContactList(){
				var uid = $(this).attr('data-uid');
				var fullname = $($(this).find('span')[0]).html();
				var profile_pic_url = $($(this).find('img')[0]).attr('src');
				var address = $(this).attr('data-address');
				openChatWindow(uid,fullname,profile_pic_url, address);
			}
			function refreshPresenceMCard(){
				var users = $('.mCard');
				var usersArr = new Array();
				for(var i=0; i<users.length; i++){
					user = new Object();
					user.id = $(users[i]).attr('data-id');
					user.xmpp_user = $(users[i]).attr('data-xmpp-user');
					usersArr.push(user);
				}
				data = JSON.stringify(usersArr);
				$.ajax({
					url: window.location.protocol + '//' + window.location.hostname + '/ajax/check_online_users',
					type: 'post',
					data: 'users='+data,
					success: function(response){
						for(var i=0; i<response.length; i++){
							if(response[i].presence)
								$('.mCard[data-member-uid="'+response[i].id+'"]').addClass('online');
							else
								$('.mCard[data-member-uid="'+response[i].id+'"]').removeClass('online');
						}
					}
				});
			}
			function photo_colorbox_oncomplete(){
				if($('#comment-box #comments ul li').length != 0)
					$('#comment-box #comments').scrollTo($('#comment-box #comments ul li').last());
				if($('#photo-wrapper #image-box img').height() > $(window).height() - 100){
					var ratio = ($(window).height() - 100) / $('#photo-wrapper #image-box img').attr('data-original-height');
					$('#photo-wrapper #image-box img').css('width',$('#photo-wrapper #image-box img').attr('data-original-width')*ratio);
					$('#photo-wrapper #image-box img').css('height',$('#photo-wrapper #image-box img').attr('data-original-height')*ratio);
				}
				$('#photo-wrapper').css('width',$('#image-box').width() + $('#comment-box').width()+2);
				$('#comments').css('max-height',$('#image-box').height()-$('#commentor').height()-3);
				$.colorbox.resize({innerHeight:$('#photo-wrapper').height()+20,innerWidth:$('#photo-wrapper').width()+20});
				$('#image-box').hover(function(){ 
					var is_liked = $(this).parent().find('img').attr('data-image-liked');
					if(is_liked)
						$(this).find('#image-hover-div #like').html('Unlike');
					else 
						$(this).find('#image-hover-div #like').html('Like');
					$('#like').unbind().click(like_photo);
					$('.others').unbind().click(function(){
						$('#comment-box').slideUp('fast');
						$('#all-photo-likes').slideDown('fast');
					});
					$('#image-hover-div').fadeIn();
				},
				function(){
					$('#image-hover-div').fadeOut();
				});
			}
			function read_notification(){
				$.ajax({
					url: '../ajax/setNotificationsUnread/',
					type: 'post',
					data: 'uid='+uid
				});
			}
		</script>
		<script language="javascript" type="text/javascript">
				$(document).ready(function(){
					$('#mainWrapper').click(function(e){
						$('.list').slideUp('fast');
						$('#messagesBar').animate({left: '-670px'});
						$('#arrow').removeClass('arrowup').addClass('arrowdown');
					});
					$('#webcam').click(function(){
						getUserMediaWithConstraints();
						$(this).hide();
						var uplvl = $(this).parent();
						uplvl.prepend('<p class="notice">Please allow browser to access webcam.</p>');
					});
					$('#account-menu-button').click(function(e){	
						if($('#account-menu-list').css('display') == 'none'){
							$(this).find('#arrow').removeClass('arrowdown').addClass('arrowup');
							$('#account-menu-list').slideDown('fast');
						}else{
							$(this).find('#arrow').removeClass('arrowup').addClass('arrowdown');
							$('#account-menu-list').slideUp('fast');
						}
						e.stopPropagation();
					});
					$(".changePage").click(function(){
						$(this).parent().parent().find('li').removeClass('active');
						$(this).parent().addClass('active');
						loadPage($(this).attr('data-page'));
					});
					$("#leftBar").hover(function(){
						$(this).stop().animate({'right':0});
					},function(){
						$(this).stop().animate({'right':-146});
					});
					$('#threadsList').perfectScrollbar();
					$('#matchup_coins').click(function(){
						$.colorbox({href:'/widgets/get_credits'});
					});
				});
				function sortContactList(){
					var items = $('.contact-list li').get();
					items.sort(function(a,b){
					  var keyA = $(a).find('span').text();
					  var keyB = $(b).find('span').text();
					  if($(a).hasClass('online'))
						keyA = '0'+keyA;
					  if($(b).hasClass('online'))
						keyB = '0'+keyB;
					  if (keyA < keyB) return -1;
					  if (keyA > keyB) return 1;
					  return 0;
					});
					var ul = $('.contact-list');
					$.each(items, function(i, li){
					  ul.append(li);
					});
				}
				function playSound(soundfile,loop) {
					if(loop)
						loopA = 'loop';
					else
						loopA = '';
					 $("#soundContainer").html('<audio autoplay="true" '+loopA+'> \
												  <source src="'+soundfile+'" type="audio/mpeg"> \
												  Your browser does not support the audio tag. \
												</audio>');
				}
				function like_photo(){
					var image = $(this).parent().parent().find('img');
					var photo_id = image.attr('data-image-ref');
					var is_liked = $(this).parent().parent().find('img').attr('data-image-liked');
					if(is_liked) 
						function_name = 'unlike_photo';
					else
						function_name = 'like_photo';
					$.ajax({
						url: '../ajax/'+function_name+'/'+photo_id,
						success: function(data){
							$('#like').html(data.value);
							if(data.value == 'Like'){
								image.attr('data-image-liked', '');
							}else if(data.value == 'Unlike'){
								image.attr('data-image-liked', '1');
								if(data.notification_string != ''){
									var stanza = $msg({'to': data.xmpp_user+'@matchup360.com', 'type': 'notification', 'notification_type': 'photo_like', 'from_fullname':my_fullname, 'from_photo':data.photo_url, 'notification_string': data.notification_string});
									connection.send(stanza);
								}
								$.ajax({
									url:'https://graph.facebook.com/me/og.likes',
									type: 'post',
									data: 'access_token='+access_token+'&method=POST&object=http://matchup360.com/p/photo/'+data.photo_reference
								});
							}
							$('#like_count').html(data.likes_string);
						}
					});
				}
			</script>
			<script>
			  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');			  
			  // ga('create', 'UA-42434331-1', config_item('site_domain'));
			  // ga('send', 'pageview');
			  var _gaq=[["_setAccount","UA-42434331-1"],["_trackPageview"]];
				(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
				g.src=("https:"==location.protocol?"//ssl":"//www")+".google-analytics.com/ga.js";
				s.parentNode.insertBefore(g,s)}(document,"script"));
			</script>
    </head>

    <body>
	<div id="fb-root"></div>
	<script>
	  // Additional JS functions here
	  window.fbAsyncInit = function() {
		FB.init({
		  appId      : '384283535015016', // App ID
		  channelUrl : '//www.matchup360.com/welcome/channel', // Channel File
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  xfbml      : true  // parse XFBML
		});	  
	  };

	  // Load the SDK asynchronously
	  (function(d){
		 var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement('script'); js.id = id; js.async = true;
		 js.src = "//connect.facebook.net/en_US/all.js";
		 ref.parentNode.insertBefore(js, ref);
	   }(document));
	</script>
		<div id="mainWrapper">
			<div id="leftBar" style="display:none">
				<div style="height: 50px"></div>
				<div class="bar-heading">My Contacts</div>
				<ul class="contact-list">
				<?php
					foreach($followed_members as $member){ 
						$profile_pic = $member['profile_pic'];
					?>
						<li data-jid="<?php echo $member['xmpp_user']; ?>@matchup360.com" data-uid="<?php echo $member['uid']; ?>" data-address="<?php echo $member['city'].', '.$member['state'].', '.$member['country'] ?>"> 
							<img src="<?php echo $profile_pic; ?>" />
							<span><?php echo $member['firstname'] . ' ' . $member['lastname']; ?></span>
							<i class="presence"></i>
						</li>
				<?php }
					
				?>
				</ul>
			</div>
			
			<div id="messagesBar">
				<i id="message_notification" class="notification">0</i>
				<div class="ticker">Messages</div>
				<div style="position:relative">
					<div id="threadsList">
				<?php	if(count($message_threads) > 0){ 
							foreach($message_threads as $thread){ ?>
							<div class="thread" data-peerid="<?php echo $thread['uid']; ?>">
								<img src="<?php echo $thread['profile_pic'] ?>" width="50" />
								<div class="name"><?php echo $thread['firstname'] .' '. $thread['lastname']; ?></div>
								<div class="message"><?php echo $thread['message']; ?></div>
							</div>
					<?php   }
						}else{ ?>
						<p align="center">Your conversations will appear here.</p>
					<?php } ?>
					</div>
					<div id="messagesBox">
						<div id="messengerBox">
							<textarea style="display: none"></textarea>
						</div>
					</div>
				</div>
				<div style="clear:both"></div>
			</div>
			
			<div id="topControl">
				<div style="float: left;width: 688px; height: 50px; border-left: 1px #287cc6 solid;">
				<div id="home-anchor">
						<a href="//matchup360.com/" id="logo"></a>
				</div>
				
					<ul id="MainMenu">
						<li id="home" class="active">
							<a class="changePage" data-page="home">Home</a>
						</li>
						<li id="photos">
							<a class="changePage" data-page="random_member">Random Member</a>
						</li>
						<li id="activities">
							<a class="changePage" data-page="activities">Activities</a>
							<i id="activity_notification" class="notification">0</i>
						</li>
					</ul>
				</div>
				<div id="matchup_coins">
					<i><?php echo $coins; ?></i>
				</div>
				<div id="user-info">
					<img id="profile-photo" src="<?php echo $profile_photo; ?>" />
					<p id="user-fullname"> <?php echo $firstname . " " . $lastname ?> </p>
					<div style="
						float: left;
					">
						<div id="account-menu-button">
							<div id="arrow" class="arrowdown"></div>
						</div>
						<div id="account-menu-list" class="list" style="display: none" >
							<ul>
								<li><a id="account-info">Account Info </a></li>
								<li><a id="update-profile"> Update Profile </a></li>
								<li><a id="my-photos">My Photos</a></li>
								<li><a id="btn-logout">Logout </a></li>
							</ul>
						</div>
					</div>
				</div> 
				<!--
				<div id="account-menu">
					<button type="button" id="btn-logout"> Logout </button>
				</div>-->
				
			</div> <!-- END OF topControl -->
			<div id="content">
				<div id="page-loader">
					<!--<img src="//matchup360.com/assets/img/loader-big.gif">-->
				</div>
				<div id="mainContent">
				</div>
			</div> <!-- END OF content -->
			<div id="push"></div>
		</div> <!-- END OF mainWrapper -->
		<div id="bottom-bar"></div>
		<div id="soundContainer"></div>
		<div id="webcam_wrapper">
		</div>
	<div id="messageBoxOverlay" style="display: none;"></div>
	</body>
</html>
