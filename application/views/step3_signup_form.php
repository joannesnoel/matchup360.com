<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Upload Photos</title>

        <!-- Our CSS stylesheet file -->
        <link rel="stylesheet" href="//wheewhew.com/assets/css/signup_step3.css" />

        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body>
	
		<div id="logout">
		<a href="http://wheewhew.com/welcome/logout"> Log Out </a>
		</div>

		<header>
			<h1>Upload Photos</h1>
		</header>

		<div id="dropbox">
			<span class="message">Drop images here to upload. <br /><i>(they will only be visible to you)</i></span>
		</div>

        <!-- Including The jQuery Library -->
		<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>

		<!-- Including the HTML5 Uploader plugin -->
		<script src="//wheewhew.com/assets/js/jquery.filedrop.js"></script>

		<!-- The main script file -->
        <script src="//wheewhew.com/assets/js/signup_step3.js"></script>
		
		<a href="http://wheewhew.com/dashboard" id="next-btn" style="display:none">Next - Profile Picture!</a>

    </body>
</html>