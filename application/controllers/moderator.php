<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Moderator extends CI_Controller {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->model('moderator_model');
		$this->load->model('user_model');
		$this->load->library('session');
		$this->load->helper('url');
    }
	public function index(){
		if($this->session->userdata('moderator_id')){	
			$data = array('logged_in' => true);
			$this->load->view('moderator/header', $data);
			$this->load->view('moderator/footer');
		}else{
			redirect('/moderator/login');
		}
	}
	public function login(){
		if(isset($_POST['submit'])){ 
			
			$authenticated = $this->moderator_model->authenticate($_POST);
			if($authenticated['exist']){
				$this->session->set_userdata('moderator_id', $authenticated['id']);
				redirect('/moderator');
			}else{
				echo 'not loggedin';
			}		
		}else{
			$data = array('logged_in' => false);
			$this->load->view('moderator/header', $data);
			$this->load->view('moderator/login');
			$this->load->view('moderator/footer');
		}
	}
	public function photos(){
		if($this->session->userdata('moderator_id')){	
			$data = array('logged_in' => true);
			$this->load->view('moderator/header', $data);
			$this->load->view('moderator/photos');
			$this->load->view('moderator/footer');
		}else{
			redirect('/moderator/login');
		}
	}
	public function logout(){
		$this->session->unset_userdata('moderator_id');
		redirect('/moderator/login');
	}
	/** PAGES  **/
	public function users(){
		$session = array('logged_in' => true);
		$this->load->view('moderator/header', $session);
		$data['users'] = $this->user_model->getAll();
		$this->load->view('moderator/home', $data);
		$this->load->view('moderator/footer');
	}
}