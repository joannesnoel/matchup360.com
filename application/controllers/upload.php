<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$this->load->view('admin/upload', array('error' => ''));
	}

	public function do_upload()
	{
		$info = new STDClass();;
		$upload_path_url = base_url().'uploads/';
	
		$config['upload_path'] = '/var/www/uploads/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$config['max_size'] = '30000';
		
	  	$this->load->library('upload', $config);
	  	if (!$this->upload->do_upload()) {
	  		$error = array('error' => $this->upload->display_errors());
			echo json_encode($error);
		} else {
			$data = $this->upload->data();
			//set the data for the json array	
			$info->name = $data['file_name'];
	        $info->size = $data['file_size'];
			$info->type = $data['file_type'];
		    $info->url = $upload_path_url .$data['file_name'];
			// I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
			$info->thumbnail_url = $upload_path_url .$data['file_name'];
		    $info->delete_url = base_url().'upload/deleteImage/'.$data['file_name'];
		    $info->delete_type = 'DELETE';
			
			echo json_encode(array($info));
		}
	}
	
	public function deleteImage($file)//gets the job done but you might want to add error checking and security
	{
		$info = new STDClass();;
		$success =unlink('/var/www/uploads/' .$file);
		//info to see if it is doing what it is supposed to	
		$info->success =$success;
		$info->path =base_url().'uploads/' .$file;
		$info->file =is_file('/var/www/uploads/' .$file);
		echo json_encode(array($info));
	}
}