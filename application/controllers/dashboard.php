<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	var $user_data = '';
	var $additional_info = '';
	var $user_photos = '';
	var $profile_pic = '';
	var $page = '';
	var $page_data = '';
	var $page_rendered = 0;

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('form', 'url', 'string'));
		$this->load->model('User_model');
		$this->load->model('Userfollow_model');
		// if($this->session->userdata('logged_uid') == '' && $_SERVER['REQUEST_URI'] != '/' && empty($_GET['url'])){
			// redirect('https://www.matchup360.com/?url='.$_SERVER['REQUEST_URI']);
		// }
    }
	
	private function renderPage(){
		if($this->page_rendered == 0){
	
			$this->load->view($this->page, $this->page_data);
			$this->page_rendered = 1;
		}else
			$this->page_rendered = 1;
	}
	public function testView(){
		$data['unsub_link'] = '';
		$data['firstname'] = 'Joannes';
		$data['lastname'] = 'Noel';
		$data['link'] = '';
		$this->load->view('emails/invitation_email',$data);
	}
	public function index(){	
	
		if($this->session->userdata('logged_uid') == ''){
			if(!empty($_GET['fbinvite']))
				$this->session->set_userdata('referrer_uid',$_GET['invite']);
			preg_match('/(\/[^?]*)/i', $_SERVER['REQUEST_URI'], $matches);
			$redirect = $matches[0];
			if($_SERVER['REQUEST_URI'] != '/')
				$this->session->set_userdata('redirect',$redirect);
			$this->load->helper(array('form', 'url', 'string'));
			$this->load->library(array('form_validation','encrypt'));
			
			$this->form_validation->set_rules('email', 'Email', 'valid_email|required');
			$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[8]');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->model('Userphoto_model');
				$data['photos'] = $this->Userphoto_model->retrieve();
				$this->load->model('List_model');
				$data['languages'] = $this->List_model->retrieve(array('group'=>'languages'));
				$this->load->view('login_page',$data);
			}
			else
			{
				$email = $this->input->post('email', TRUE);
				$password = $this->input->post('pwd', TRUE);
				
				$this->load->model('User_model');
				$auth = $this->User_model->authenticate($email, $password);
				
				$this->load->library('session');
				$this->session->set_userdata('logged_uid',$auth['uid']);
				$this->session->set_userdata('current_lattitude',$auth['current_lattitude']);
				$this->session->set_userdata('current_longitude',$auth['current_longitude']);
				//echo $this->session->userdata('logged_uid');
				$redirect = $this->session->userdata('redirect');
				if(!empty($redirect)){
					$this->session->unset_userdata('redirect');
					redirect($redirect);
				}else
					redirect('/');
			}
		}else{	
			$uid = $this->session->userdata('logged_uid');
			$this->load->model('Usermessages_model');
			$message_threads = $this->Usermessages_model->getMessageThreads($uid);
			$user_notifications = $this->getNotifications();
			$user = $this->User_model->retrieve(array('uid'=>$uid));
			$this->user = $user[0];
			
			$this->session->set_userdata('current_longitude', $this->user['current_longitude']);
			$this->session->set_userdata('current_lattitude', $this->user['current_lattitude']);
			
			$this->page_data['profile_photo'] = $this->user['profile_pic'];
			$this->page_data['firstname'] = $this->user['firstname'];
			$this->page_data['lastname'] = $this->user['lastname'];
			$this->page_data['xmpp_user'] = $this->user['xmpp_user'];
			$this->page_data['xmpp_password'] = $this->user['xmpp_password'];
			$this->page_data['uid'] = $this->session->userdata('logged_uid');
			$this->page_data['gender'] = $this->user['sex'];
			$this->page_data['notifications'] = $user_notifications;
			$followed_members = $this->Userfollow_model->get_followed_members($uid);
			$this->page_data['followed_members'] =  $followed_members;
			$this->page_data['message_threads'] =  $message_threads;
			$this->page_data['coins'] =  $this->user['points'];
			$this->page = 'dashboard';
			$this->renderPage();
		}		
	}
	
	public function step4_signup(){
	
		if($this->input->post('action') == 'crop'){
		
			$random_str = random_string('alnum', 12);
			
			copy($this->input->post('orig_photo_url'), '/var/www/uploads/'.$random_str.'.jpg');
		
			$this->load->library('image_lib');
			$this->load->library('s3');
			
			$config['image_library'] = 'gd2';
			$config['source_image']	= '/var/www/uploads/'.$random_str.'.jpg';
			$config['maintain_ratio'] = FALSE;
			$config['x_axis'] = $this->input->post('x');
			$config['y_axis'] = $this->input->post('y');
			$config['width'] = $this->input->post('w');
			$config['height'] = $this->input->post('h');

			$this->image_lib->initialize($config); 

			if ( ! $this->image_lib->crop())
			{
				echo $this->image_lib->display_errors();
			}
			
			$this->image_lib->clear();
			
			$config['image_library'] = 'gd2';
			$config['source_image']	= '/var/www/uploads/'.$random_str.'.jpg';
			$config['maintain_ratio'] = TRUE;
			$config['width']	 = 150;
			$config['height']	= 150;
			
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->load->model('Userphoto_model');
			$profile_photo = $this->Userphoto_model->profile_photo($this->session->userdata('logged_uid'));
			
			if($profile_photo){
				S3::deleteObject("wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$profile_photo);
			}
			
			S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_profile.jpg", S3::ACL_PUBLIC_READ);
			
			$this->Userphoto_model->set_profile_photo($this->session->userdata('logged_uid'),$random_str.'_profile.jpg');
			
			unlink('/var/www/uploads/'.$random_str.'.jpg');
			
			redirect('./dashboard');
		}else{
			$this->load->model('Userphoto_model');
			$user_photos = $this->Userphoto_model->retrieve($this->session->userdata('logged_uid'));
			
			$this->page_data['photos'] = $user_photos;
			$this->page = 'step4_signup_form';
			$this->renderPage();
		}
	}
	private function getNotifications(){
		$this->load->library('session');	
		$this->load->model('Notification_model');
		$this->load->model('User_model');
		$uid = $this->session->userdata('logged_uid');
		$notifications = $this->Notification_model->retrieve(array('to_uid' =>$uid), array('field' => 'date','value' => 'desc'));
		$new_notification = array();
		foreach($notifications as $notification){
			$user = $this->User_model->retrieve(array('uid'=>$notification['from_uid']));
			if($notification['type'] == 'follow'){
				$notification['notification_string'] = '<a href="../../view_profile/'.$user[0]['uid'].'">
																<img src="'.$user[0]['profile_pic'].'" width="35" class="notification-from-image">
																<span>'.$user[0]['firstname'].' '.$user[0]['lastname'].' followed you.</span>
															</a>';
			}else if($notification['type'] == 'photo_like'){
				$notification['notification_string'] = '<a href="../../view_profile/'.$user[0]['uid'].'">
																<img src="'.$user[0]['profile_pic'].'" width="35" class="notification-from-image">'.
																$user[0]['firstname'].' '.$user[0]['lastname'].'
														</a> 
														liked your <a href="../../widgets/photo/'.$notification['pid'].'" class="photo_notification">photo.</a>';
			}
			$new_notification[] = $notification;
		}
		return $new_notification;
	}
}