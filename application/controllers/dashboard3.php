<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	var $user_data = '';
	var $additional_info = '';
	var $user_photos = '';
	var $profile_pic = '';
	var $page = '';
	var $page_data = '';
	var $page_rendered = 0;

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('form', 'url', 'string'));
		
		if($this->session->userdata('logged_uid') == '')
			redirect('./welcome/login');
		
		$this->load->model('User_model');
		$this->user_data = $this->User_model->retrieve($this->session->userdata('logged_uid'));
		$this->session->set_userdata('basic_userinfo',$this->user_data);
		
		$this->load->model('Userinfo_model');
		$this->additional_info = $this->Userinfo_model->retrieve($this->session->userdata('logged_uid'));
		if($this->additional_info == false){		
			//redirect('./dashboard/step2_signup');
			$this->step2_signup();
		}
		
		$this->load->model('Userphoto_model');
		$this->user_photos = $this->Userphoto_model->retrieve($this->session->userdata('logged_uid'));
		if(count($this->user_photos)<1){
			$this->step3_signup();
		}
		
		$this->profile_pic = $this->Userphoto_model->profile_photo($this->session->userdata('logged_uid'));
		if($this->profile_pic == false){
			$this->step4_signup();
		}
		$this->user = $this->session->userdata('basic_userinfo');
		$this->load->model('Userbuddyrequest_model');
		$this->load->model('Userbuddies_model');
		$this->load->model('Userposts_model');
		
    }
	
	private function renderPage(){
		if($this->page_rendered == 0){
	
			$this->load->view($this->page, $this->page_data);
			$this->page_rendered = 1;
		}else
			$this->page_rendered = 1;
	}
	public function checkSession(){
		echo $this->session->userdata('logged_uid');
	}
	public function index()
	{
		$this->page_data['profile_photo'] = 'https://s3.amazonaws.com/wheewhew/user/'.$this->session->userdata('logged_uid').'/photos/'.$this->profile_pic;
		$this->page_data['firstname'] = $this->user['firstname'];
		$this->page_data['lastname'] = $this->user['lastname'];
		$this->page_data['xmpp_user'] = $this->user['xmpp_user'];
		$this->page_data['xmpp_password'] = $this->user['xmpp_password'];
		$this->page_data['uid'] = $this->session->userdata('logged_uid');
		$this->page_data['latest_members'] = $this->User_model->latest_members($this->session->userdata('logged_uid'));
		$this->page_data['has_request'] = $this->User_model->pending();
		$this->page_data['all_members'] = $this->User_model->all_members($this->session->userdata('logged_uid'));
		$this->page_data['buddy_requests'] = $this->Userbuddyrequest_model->buddyRequest($this->session->userdata('logged_uid'));
		$this->page_data['buddy_posts'] = $this->Userposts_model->getPosts($this->session->userdata('logged_uid'));
		
		$this->form_validation->set_rules('update-status-box', 'Status Box', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->page = 'dashboard';
			$this->renderPage();
		}
		
		else {
		
			$user_post = array(
								'post' => $this->input->post('update-status-box'),
								'poster_uid' => $this->session->userdata('logged_uid')
							);
							
			$this->Userposts_model->insert($user_post);
		
		}
		
		$this->page = 'dashboard';
		$this->renderPage();		
	}
	
	public function members(){
		$this->page_data['profile_photo'] = 'https://s3.amazonaws.com/wheewhew/user/'.$this->session->userdata('logged_uid').'/photos/'.$this->profile_pic;
		$this->page_data['firstname'] = $this->user['firstname'];
		$this->page_data['lastname'] = $this->user['lastname'];
		$this->page_data['xmpp_user'] = $this->user['xmpp_user'];
		$this->page_data['xmpp_password'] = $this->user['xmpp_password'];
		$this->page_data['uid'] = $this->session->userdata('logged_uid');
		
		$this->page ='dashboard/members';
	}
	
	public function buddies(){
		$this->page_data['profile_photo'] = 'https://s3.amazonaws.com/wheewhew/user/'.$this->session->userdata('logged_uid').'/photos/'.$this->profile_pic;
		$this->page_data['firstname'] = $this->user['firstname'];
		$this->page_data['lastname'] = $this->user['lastname'];
		$this->page_data['xmpp_user'] = $this->user['xmpp_user'];
		$this->page_data['xmpp_password'] = $this->user['xmpp_password'];
		$this->page_data['uid'] = $this->session->userdata('logged_uid');
		$this->page_data['latest_members'] = $this->User_model->latest_members($this->session->userdata('logged_uid'));
		$this->page_data['has_request'] = $this->User_model->pending();
		$this->page_data['all_members'] = $this->User_model->all_members($this->session->userdata('logged_uid'));
		$this->page_data['buddy_requests'] = $this->Userbuddyrequest_model->buddyRequest($this->session->userdata('logged_uid'));
		$this->page_data['user_buddies'] = $this->Userbuddies_model->get_buddies($this->session->userdata('logged_uid'));
		
		$this->page ='dashboard/buddies';
		$this->renderPage();
	}
	
	private function step2_signup(){
		$this->form_validation->set_rules('hometown_components', 'Hometown', 'required');
		$this->form_validation->set_rules('relationship_status', 'Relationship Status', 'required');			
		$this->form_validation->set_rules('interested_in', 'Interested In', 'required');	
		$this->form_validation->set_rules('about_me', 'About Me', 'required|min_length[30]');	
		$this->form_validation->set_rules('age_from', 'Age From', 'required');	
		$this->form_validation->set_rules('age_to', 'Age To', 'required');	
		
		if($this->form_validation->run() == FALSE){
			$this->page = 'step2_signup_form';
			$this->renderPage();
		}else{
			$post = $this->input->post();
			$post['hometown_components'] = json_decode($post['hometown_components']);
			
			$this->load->model('Userlocation_model');
			foreach($post['hometown_components'] as $key => $location){
				$data = array(
							'location' => 'hometown',
							'type' => $location->types[0],
							'name' => $location->long_name,
							'uid' => $this->session->userdata('logged_uid'),
							'arrange' => $key
						);
				$this->Userlocation_model->insert($data);
			}
			
			if(!empty($post['city_components'])){
				$post['city_components'] = json_decode($post['city_components']);
				foreach($post['city_components'] as $key => $location){
					$data = array(
								'location' => 'current',
								'type' => $location->types[0],
								'name' => $location->long_name,
								'uid' => $this->session->userdata('logged_uid'),
								'arrange' => $key
							);
					$this->Userlocation_model->insert($data);
				}
			}
			
			$data = array(
						'uid' => $this->session->userdata('logged_uid'),
						'relationship_status' => $post['relationship_status'],
						'about_me' => $post['about_me'],
						'age_from' => $post['age_from'],
						'age_to' => $post['age_to']
					);
			if(!empty($post['favorite_quote']))
				$data['favorite_quote'] = $post['favorite_quote'];
			if(!empty($post['interested_in'][0]))
				$data['interested_in_men'] = 'Y';
			if(!empty($post['interested_in'][1]))
				$data['interested_in_women'] = 'Y';
				
			$this->load->model('Userinfo_model');
			$this->Userinfo_model->insert($data);
			redirect('./dashboard');
		}
	}
	
	private function step3_signup(){
		$this->page = 'step3_signup_form';
		$this->renderPage();
	}
	
	public function step4_signup(){
	
		if($this->input->post('action') == 'crop'){
		
			$random_str = random_string('alnum', 12);
			
			copy($this->input->post('orig_photo_url'), '/var/www/uploads/'.$random_str.'.jpg');
		
			$this->load->library('image_lib');
			$this->load->library('s3');
			
			$config['image_library'] = 'gd2';
			$config['source_image']	= '/var/www/uploads/'.$random_str.'.jpg';
			$config['maintain_ratio'] = FALSE;
			$config['x_axis'] = $this->input->post('x');
			$config['y_axis'] = $this->input->post('y');
			$config['width'] = $this->input->post('w');
			$config['height'] = $this->input->post('h');

			$this->image_lib->initialize($config); 

			if ( ! $this->image_lib->crop())
			{
				echo $this->image_lib->display_errors();
			}
			
			$this->image_lib->clear();
			
			$config['image_library'] = 'gd2';
			$config['source_image']	= '/var/www/uploads/'.$random_str.'.jpg';
			$config['maintain_ratio'] = TRUE;
			$config['width']	 = 150;
			$config['height']	= 150;
			
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->load->model('Userphoto_model');
			$profile_photo = $this->Userphoto_model->profile_photo($this->session->userdata('logged_uid'));
			
			if($profile_photo){
				S3::deleteObject("wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$profile_photo);
			}
			
			S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_profile.jpg", S3::ACL_PUBLIC_READ);
			
			$this->Userphoto_model->set_profile_photo($this->session->userdata('logged_uid'),$random_str.'_profile.jpg');
			
			unlink('/var/www/uploads/'.$random_str.'.jpg');
			
			redirect('./dashboard');
		}else{
			$this->load->model('Userphoto_model');
			$user_photos = $this->Userphoto_model->retrieve($this->session->userdata('logged_uid'));
			
			$this->page_data['photos'] = $user_photos;
			$this->page = 'step4_signup_form';
			$this->renderPage();
		}
	}
	
	public function confirm($uid,$code){
		$this->load->model('Confirmation_model');
		$result = $this->Confirmation_model->confirm_code($uid,$code);
		if($result){
			$this->session->set_userdata('logged_uid',$uid);
			redirect('./dashboard/');
		}else
			echo "Confirmation failure!";
	}
}