<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SignUp extends CI_Controller {


	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->helper(array('form', 'url', 'string'));
		$this->load->library(array('form_validation','encrypt','session'));
    }
	
	public function index(){
		$this->load->model('List_model');
		$pageData['languages'] = $this->List_model->retrieve(array('group'=>'languages'));
		$this->load->view('signup_page',$pageData);
	}
}