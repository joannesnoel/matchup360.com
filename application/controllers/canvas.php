<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Canvas extends CI_Controller {
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Userprofile_model');
		$this->load->model('Userappearance_model');
		$this->load->model('Usersituation_model');
		$this->load->model('Usereduemployment_model');
		$this->load->model('Userleisure_model');
		$this->load->model('Userpersonality_model');
		$this->load->model('Userviews_model');
		$this->load->model('Userlooking_model');
		$this->load->model('List_model');
		$this->load->model('Usermessages_model');
		$this->load->model('Userfollow_model');
		$this->load->model('Notification_model');
		//$this->load->model('Notification_model');
		$this->load->library('session');
		$this->load->helper('url');
    }
	function index2(){		
		$this->load->model('Userphoto_model');
		$data['photos'] = $this->Userphoto_model->retrieve();
		$this->load->view('canvas/visit_website', $data);
	}
	function index(){
		if(isset($_REQUEST['room']))
			$this->session->set_userdata('room',$_REQUEST['room']);
		// if(isset($_REQUEST['request_ids']))
			// $this->session->set_userdata('request_ids',$_REQUEST['request_ids']);
		// $this->load->model('Userphoto_model');
		// $data['photos'] = $this->Userphoto_model->retrieve();
		// $uid = $this->session->userdata('logged_uid');
		// if(empty($uid))
			// redirect('/canvas/signup');
		// $this->load->model('Usermessages_model');
		// $data['message_threads'] = $this->Usermessages_model->getMessageThreads($uid);
		// $this->load->view('canvas/index', $data);
		redirect('/canvas/signup');
	}
	function signup(){
		$this->load->model('Userphoto_model');
		$data['photos'] = $this->Userphoto_model->retrieve();
		$this->load->view('canvas/signup',$data);
	}
	function chat(){
		$this->load->model('Userchatroom_model');
		$me = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$data['name'] = $me[0]['firstname'].' '.$me[0]['lastname'];
		$data['firstname'] = $me[0]['firstname'];
		$data['lastname'] = $me[0]['lastname'];
		$data['profile_pic'] = $me[0]['profile_pic'];
		$data['xmpp_user'] = $me[0]['xmpp_user'];
		$data['xmpp_pass'] = $me[0]['xmpp_password'];
		$data['fb_userid'] = $me[0]['fb_userid'];
		
		if(!empty($me[0]['city']))
			$rooms_array[] = "'".str_replace(' ','',strtolower($me[0]['city']))."@conference.matchup360.com'";
		if($this->session->userdata('room')){
			$rooms_array[] = "'".$this->session->userdata('room')."@conference.matchup360.com'";
			$this->session->unset_userdata('room');
		}
		$activeRooms = $this->Userchatroom_model->getActiveRooms();
		if(is_array($activeRooms)){
			foreach($activeRooms as $room){
				if(!in_array("'".$room['roomJid']."'",$rooms_array))
					$rooms_array[] = "'".$room['roomJid']."'";
			}
		}
		$rooms = implode(',',$rooms_array);
		$data['rooms'] = $rooms;
		$this->load->view('canvas/chat',$data);
	}
	function notification($nid){
		$notification = $this->Notification_model->retrieve(array('id'=>$nid));
		$notification = $notification[0];
		$from_user = $this->User_model->retrieve(array('uid'=>$notification['from_uid']));
		$from_user = $from_user[0];
		$to_user = $this->User_model->retrieve(array('uid'=>$notification['to_uid']));
		$to_user = $to_user[0];
		switch($notification['type']){
			case 'interested': 
				$str = ($from_user['sex'] == 'm')? 'man says he': 'woman says she';
				$snippet['notification_str'] = 'A '. $str .' is interested in you.';
				$snippet['picture'] = ($from_user['sex'] == 'm')? 'https://matchup360.com/assets/img/blank_profile.jpg' : 'https://matchup360.com/assets/img/blank_profile_female.jpg';
				$page['snippet'] = $this->load->view('canvas/notification/secret_admirer',$snippet,true);
				$page['photos'] = $this->Userphoto_model->retrieve();
				$this->load->view('canvas/notification/common',$page);
			break;
		}
	}
	function profile($uid){
		$user_appearance = $this->Userappearance_model->get_appearance($uid);
		$user_situation = $this->Usersituation_model->get_situation($uid);
		$user_eduemploy = $this->Usereduemployment_model->get_eduemployment($uid);
		$user_leisure = $this->Userleisure_model->get_leisure($uid);
		$user_personality = $this->Userpersonality_model->get_personality($uid);
		$user_views = $this->Userviews_model->get_views($uid);
		$user_looking = $this->Userlooking_model->get_looking($uid);
		
		if($user_appearance != false){
			$bodyart = array();
			if(!empty($user_appearance['bodyart']))
				foreach($user_appearance['bodyart'] as $row){
					$bodyart[] = $row['name'];
				}
			$user_appearance['bodyart'] = $bodyart;
			$page_data['user_appearance'] = $user_appearance;
		}
		if($user_situation != false){
			$pets = array();
			if(!empty($user_situation['pets']))	
				foreach($user_situation['pets'] as $rows){
					$pets[] = $rows['name'];
				}
			$user_situation['pets'] = $pets;
			$page_data['user_situation'] = $user_situation;
		}
		if($user_eduemploy != false){
			$page_data['user_eduemploy'] = $user_eduemploy;
			}
		if($user_leisure != false){
			$leisure_grouped = array();
			foreach($user_leisure as $leisure){
				$leisure_grouped[$leisure['group']][] = $leisure['name'];
			} 
			$page_data['user_leisure'] = $leisure_grouped;
		}
		if($user_personality != false){
			$enjoy = array();
			if(!empty($user_personality['enjoy']))
				foreach($user_personality['enjoy'] as $rows){
					$enjoy[] = $rows['name'];
				}
			$user_personality['enjoy'] = $enjoy;
			$page_data['user_personality'] = $user_personality;
		}
		if($user_views != false){
			$page_data['user_views'] = $user_views;
		}
		if($user_looking != false){
			$look = array();
			if(!empty($user_looking['looking']))
				foreach($user_looking['looking'] as $rows){
					$look[] = $rows['name'];
				}
			$user_looking['looking'] = $look;
			$page_data['user_looking'] = $user_looking;
		}
	
		$user = $this->User_model->retrieve(array('uid' => $uid));
		$today = new DateTime('00:00:00');
		$page_data['user'] = $user[0];
		$page_data['user']['age'] = $today->diff(new DateTime($page_data['user']['birthdate']))->y; 
		$page_data['photos'] = $this->Userphoto_model->retrieve(array('uid'=>$uid));
		//print_r($page_data['user']);
		$this->load->view('canvas/profile', $page_data);
	}
}