<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		$this->load->helper('url');
		redirect('welcome/login');
		$this->load->helper(array('form', 'url', 'string'));
		$this->load->library(array('form_validation','encrypt'));
		
		$this->form_validation->set_rules('firstname', 'Firstname', 'required|min_length[5]');
		$this->form_validation->set_rules('lastname', 'Lastname', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|matches[email-verify]');
		$this->form_validation->set_rules('email-verify', 'Email Confirmation', 'valid_email|required');
		$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[8]');
		$this->form_validation->set_rules('sex', 'Sex', 'required');
		$this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('welcome_message');
		}
		else
		{
			$user_data = array(
								'firstname'=> $this->input->post('firstname', TRUE),
								'lastname'=> $this->input->post('lastname', TRUE),
								'email'=> $this->input->post('email', TRUE),
								'password'=>  $this->encrypt->sha1($this->input->post('pwd', TRUE)),
								'sex'=>  $this->input->post('sex', TRUE),
								'birthdate'=>  $this->input->post('birthdate', TRUE)
							);
			$this->load->model('User_model');
			$uid = $this->User_model->insert($user_data);
			
			$confirmation_code = random_string('alnum', 35);
			
			$this->load->model('Confirmation_model');
			$confirmation_data = array(
									'uid' => $uid,
									'confirmation_code' => $confirmation_code
								);
			$this->Confirmation_model->insert($confirmation_data);
			
			$page_data = array(
								'email'=>$this->input->post('email', TRUE)
							);
			$this->load->view('signup_confirmation',$page_data);
			
			$this->load->library('email');

			$this->email->from('webmaster@wheewhew.com', 'WheeWhew');
			$this->email->to($this->input->post('email', TRUE)); 
			
			$this->email->subject('WheeWhew Sign-Up Confirmation Link');
			$this->email->message('http://www.wheewhew.com/dashboard/confirm/'.$uid.'/'.$confirmation_code);	

			$this->email->send();

			echo $this->email->print_debugger();
			
			$username = $this->User_model->generate_xmpp_user($this->input->post('firstname', TRUE), $this->input->post('lastname', TRUE));
			$password  = random_string('alnum', 15);
			$this->User_model->update(array('xmpp_user'=>$username, 'xmpp_password'=>$password),array('uid'=>$uid));
			
			$node = 'wheewhew.com';
			exec('sudo /usr/sbin/ejabberdctl register '.$username.' '.$node.' '.$password.' 2>&1',$output,$status);
			if($output == 0)
			{
				// Success!
			}
			else
			{
				// Failure, $output has the details
				echo '<pre>';
				foreach($output as $o)
				{
					echo $o."\n";
				}
				echo '</pre>';
			}
		}
	}
	public function login(){
		$this->load->helper(array('form', 'url', 'string'));
		$this->load->library(array('form_validation','encrypt'));
		
		$this->form_validation->set_rules('email', 'Email', 'valid_email|required');
		$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[8]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->model('Userphoto_model');
			$data['photos'] = $this->Userphoto_model->retrieve();
			$this->load->model('List_model');
			$data['languages'] = $this->List_model->retrieve(array('group'=>'languages'));
			$this->load->view('login_page',$data);
		}
		else
		{
			$email = $this->input->post('email', TRUE);
			$password = $this->input->post('pwd', TRUE);
			
			$this->load->model('User_model');
			$auth = $this->User_model->authenticate($email, $password);
			
			$this->load->library('session');
			$this->session->set_userdata('logged_uid',$auth['uid']);
			$this->session->set_userdata('current_lattitude',$auth['current_lattitude']);
			$this->session->set_userdata('current_longitude',$auth['current_longitude']);
			//echo $this->session->userdata('logged_uid');
			if(!isset($_GET['url']))
				redirect('./');
			else
				redirect($_GET['url']);
		}
	}
	
	public function logout(){
		$this->load->library('session');
		$this->load->helper('url');
		$this->session->sess_destroy();
		redirect('./');
	}
	public function confirm($uid,$code){
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Confirmation_model');
		$result = $this->Confirmation_model->confirm_code($uid,$code);
		if($result){
			$this->session->set_userdata('logged_uid',$uid);
			redirect('/');
		}else
			echo "Confirmation failure!";
	}
	public function confirm_account_change($uid, $type, $code){
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Account_change_model');
		$this->load->model('User_model');
		$result = $this->Account_change_model->retrieve(array('uid' => $uid,
																	'type' => $type,
																    'confirmation_code' => $code));
		if($result){
			$where = array('uid' => $result[0]['uid']);
			$data = array($result[0]['type'] => $result[0]['change']);
			$update_result = $this->User_model->update($data, $where);
			$this->session->set_userdata('logged_uid',$uid);
			redirect('/');
		}else
			echo "Confirmation failure!";
	}
	public function channel(){
		echo '<script src="//connect.facebook.net/en_US/all.js"></script>';
	}
	public function photo($pid){
		$this->load->model('User_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Photocomment_model');
		$this->load->model('Photolikes_model');
		$sizes = 1;
		$photo = $this->Userphoto_model->retrieve(array('reference' => $pid));
		if($photo == false)
			die('Your link is invalid.');
		$photo = $this->Userphoto_model->retrieve(array('reference' => $pid),3);
		$data['photo'] = $photo[0];
		$where = array('uid'=>$data['photo']['uid']);
		$data['user'] = $this->User_model->retrieve($where);
		$data['comments'] = $this->Photocomment_model->retrieve($pid);
		
		$data['photo_likes'] = count($this->Photolikes_model->retrieve($pid)); 
		
		$data['title'] = 'A Photo of '.$data['user'][0]['firstname'].' '.$data['user'][0]['lastname'];
		$data['img_src'] = 'https://s3.amazonaws.com/wheewhew/user/'.$data['user'][0]['uid'].'/photos/'.$data['photo']['filename'];
		$data['public_url'] = 'http://www.matchup360.com/p/photo/'.$pid;
		$this->load->view('welcome/photo',$data);
	}
	public function profile($uid){
		//$data['title'] = 'A Photo of '.$data['user'][0]['firstname'].' '.$data['user'][0]['lastname'];
		//$data['img_src'] = 'https://s3.amazonaws.com/wheewhew/user/'.$data['user'][0]['uid'].'/photos/'.$data['photo']['filename'];
		//$data['public_url'] = 'http://www.matchup360.com/p/profile/'.$pid;
		//$this->load->view('welcome/profile',$data);
	}
	public function test(){
		//ejabberdctl register admin1 example.org FgT5bk3
		$this->load->database();
		$q = $this->db->query("SELECT xmpp_user, xmpp_password FROM user");
		$r = $q->result_array();
		foreach($r as $u){
			exec("sudo ejabberdctl register ".$u['xmpp_user']." matchup360.com ".$u['xmpp_password'],$output);
			echo "ejabberdctl register ".$u['xmpp_user']." matchup360.com ".$u['xmpp_password']."<br />";
			foreach($output as $text)
				echo $text."<br />";
		}
	}
}