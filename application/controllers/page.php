<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Userprofile_model');
		$this->load->model('Userappearance_model');
		$this->load->model('Usersituation_model');
		$this->load->model('Usereduemployment_model');
		$this->load->model('Userleisure_model');
		$this->load->model('Userpersonality_model');
		$this->load->model('Userviews_model');
		$this->load->model('Userlooking_model');
		$this->load->model('List_model');
		$this->load->model('Usermessages_model');
		$this->load->model('Userfollow_model');
		$this->load->model('Notification_model');
		$this->load->library('session');
		$this->load->helper('url');
		$uid = $this->session->userdata('logged_uid');
		if($uid<1){
			die(json_encode(array('redirect'=>'/')));
		}
    }
	function home(){ 
		$this->load->model('List_model');
		$uid = $this->session->userdata('logged_uid');
		$profile_pic = $this->Userphoto_model->profile_photo($uid);
		$data['profile_pic'] = $profile_pic;
		$data['profile_completion'] = $this->List_model->get_profile_completion();
		//if($profile_pic == '' || $data['profile_completion'] < 20)
		//	echo json_encode(array('title'=>'Home','html'=>$this->load->view('pages/home',$data,true)));
		//else
			echo json_encode(array('title'=>'Home','html'=>$this->load->view('pages/latest_members',null,true)));
	}
	
	function edit_profile(){
		$languages = $this->List_model->retrieve(array('group'=>'languages'));
		$interested_in = $this->List_model->retrieve(array('group'=>'interested_in'));
		$ethnicities = $this->List_model->retrieve(array('group'=>'ethnicity'));
		$nationalities = $this->List_model->retrieve(array('group'=>'nationality'));
		$eye_colors = $this->List_model->retrieve(array('group'=>'eye_color'));
		$hair_colors = $this->List_model->retrieve(array('group'=>'hair_color'));
		$body_arts = $this->List_model->retrieve(array('group'=>'body_art'));
		$marital_status = $this->List_model->retrieve(array('group'=>'marital_status'));
		$has_children = $this->List_model->retrieve(array('group'=>'has_children'));
		$pets = $this->List_model->retrieve(array('group'=>'pets'));
		$education = $this->List_model->retrieve(array('group'=>'education_level'));
		$specialty = $this->List_model->retrieve(array('group'=>'specialty'));
		$employment = $this->List_model->retrieve(array('group'=>'employment_stat'));
		$tv_genre = $this->List_model->retrieve(array('group'=>'tv_genre'));
		$movie_genre = $this->List_model->retrieve(array('group'=>'movie_genre'));
		$music_genre = $this->List_model->retrieve(array('group'=>'music_genre'));
		$book_genre = $this->List_model->retrieve(array('group'=>'book_genre'));
		$hobbies = $this->List_model->retrieve(array('group'=>'hobbies'));
		$drinking_habit = $this->List_model->retrieve(array('group'=>'drinking_habit'));
		$smoking_habit = $this->List_model->retrieve(array('group'=>'smoking_habit'));
		$social_behaviou = $this->List_model->retrieve(array('group'=>'social_behaviou'));
		$i_enjoy = $this->List_model->retrieve(array('group'=>'i_enjoy'));
		$political_views = $this->List_model->retrieve(array('group'=>'political_view'));
		$religion = $this->List_model->retrieve(array('group'=>'religion'));
		$attracted_to = $this->List_model->retrieve(array('group'=>'attracted_to'));
		$data['languages'] = $languages;
		$data['interested_in'] = $interested_in;
		$data['ethnicities'] = $ethnicities;
		$data['nationalities'] = $nationalities;
		$data['eye_colors'] = $eye_colors;
		$data['hair_colors'] = $hair_colors;
		$data['body_arts'] = $body_arts;
		$data['marital_status'] = $marital_status;
		$data['has_children'] = $has_children;
		$data['pets'] = $pets;
		$data['education'] = $education;
		$data['specialty'] = $specialty;
		$data['employment'] = $employment;
		$data['tv_genre'] = $tv_genre;
		$data['movie_genre'] = $movie_genre;
		$data['music_genre'] = $music_genre;
		$data['book_genre'] = $book_genre;
		$data['hobbies'] = $hobbies;
		$data['drinking_habit'] = $drinking_habit;
		$data['smoking_habit'] = $smoking_habit;
		$data['social_behaviou'] = $social_behaviou;
		$data['i_enjoy'] = $i_enjoy;
		$data['political_views'] = $political_views;
		$data['religion'] = $religion;
		$data['looking_for'] = $attracted_to;
		echo json_encode(array('title'=>'Edit Profile','html'=>$this->load->view('pages/editprofile',$data,true)));		
	}
	function messages(){
		$uid = $this->session->userdata('logged_uid');
		$message_threads = $this->Usermessages_model->getMessageThreads($uid);
		//print_r( array_unique($message_threads));
		
		echo json_encode(array('title'=>'Messages','html'=>$this->load->view('pages/messages', array('message_threads' => $message_threads),true)));		
	}
	function photos(){
		echo json_encode(array('title'=>'Photos','html'=>$this->load->view('pages/photos',null,true)));		
	}
	function random_member(){
		echo json_encode(array('title'=>'Random Member','html'=>$this->load->view('pages/random_member_wrapper',null,true)));		
	} 
	function account_settings(){
		$uid = $this->session->userdata('logged_uid');
		$user = $this->User_model->retrieve(array('uid' => $uid));
		echo json_encode(array('title'=>'Account Settings','html'=>$this->load->view('pages/account_settings', $user[0],true)));		
	}
	function view_profile($uid){
		$logged_uid = $this->session->userdata('logged_uid');
		$user_data= $this->User_model->retrieve(array('uid'=>$uid));
		$page_data['user_data'] = $user_data[0];
		$page_data['user_photo'] = $user_data[0]['profile_pic'];
		$page_data['logged_user_uid'] = $this->session->userdata('logged_uid'); 
		$check_followed = $this->Userfollow_model->retrieve(array('from'=>$logged_uid ,'to'=>$uid));
		if($check_followed)
			$page_data['is_followed'] = true;
		else
			$page_data['is_followed'] = false;
		//print_r($page_data);
		echo json_encode(array('title'=>$user_data[0]['firstname'].' '.$user_data[0]['lastname'],'html'=>$this->load->view('pages/member_profile',$page_data,true)));		
	}
	function activities(){
		$page_data['notifications'] = $this->getNotifications();
		echo json_encode(array('title'=>'Activities','html'=>$this->load->view('pages/activities',$page_data,true)));		
	}
	function my_photos(){
		$uid = $this->session->userdata('logged_uid');
		$user_photos = $this->Userphoto_model->retrieve(array('uid' => $uid));
		echo json_encode(array('title'=>'My Photos','html'=>$this->load->view('pages/myphotos',array('my_photos' => $user_photos),true)));		
	}
	function user_profile($uid){
		$user_profile = $this->Userprofile_model->retrieve(array('uid'=>$uid));
		$user_appearance = $this->Userappearance_model->get_appearance($uid);
		$user_situation = $this->Usersituation_model->get_situation($uid);
		$user_eduemploy = $this->Usereduemployment_model->get_eduemployment($uid);
		$user_leisure = $this->Userleisure_model->get_leisure($uid);
		$user_personality = $this->Userpersonality_model->get_personality($uid);
		$user_views = $this->Userviews_model->get_views($uid);
		$user_looking = $this->Userlooking_model->get_looking($uid);
		$user_photos = $this->Userphoto_model->retrieve(array('uid'=>$uid));
		
		$page_data['user_photos'] = $user_photos;
		$page_data['user_profile'] = $user_profile[0];
		
		if($user_appearance != false){
			$bodyart = array();
			if(!empty($user_appearance['bodyart']))
				foreach($user_appearance['bodyart'] as $row){
					$bodyart[] = $row['name'];
				}
			$user_appearance['bodyart'] = $bodyart;
			$page_data['user_appearance'] = $user_appearance;
		}
		if($user_situation != false){
			$pets = array();
			if(!empty($user_situation['pets']))	
				foreach($user_situation['pets'] as $rows){
					$pets[] = $rows['name'];
				}
			$user_situation['pets'] = $pets;
			$page_data['user_situation'] = $user_situation;
		}
		if($user_eduemploy != false){
			$page_data['user_eduemploy'] = $user_eduemploy;
			}
		if($user_leisure != false){
			$leisure_grouped = array();
			foreach($user_leisure as $leisure){
				$leisure_grouped[$leisure['group']][] = $leisure['name'];
			} 
			$page_data['user_leisure'] = $leisure_grouped;
		}
		if($user_personality != false){
			$enjoy = array();
			if(!empty($user_personality['enjoy']))
				foreach($user_personality['enjoy'] as $rows){
					$enjoy[] = $rows['name'];
				}
			$user_personality['enjoy'] = $enjoy;
			$page_data['user_personality'] = $user_personality;
		}
		if($user_views != false){
			$page_data['user_views'] = $user_views;
		}
		if($user_looking != false){
			$look = array();
			if(!empty($user_looking['looking']))
				foreach($user_looking['looking'] as $rows){
					$look[] = $rows['name'];
				}
			$user_looking['looking'] = $look;
			$page_data['user_looking'] = $user_looking;
		}
		$this->load->view('pages/user_profile', $page_data);
	}
	function follower($uid){
		$this->load->model('Userfollow_model');
		$followers = $this->Userfollow_model->get_following_members($uid);
		//print_r($followers);
		$this->load->view('pages/follower', array('follow' => $followers));
	}
	function following($uid){
		$this->load->model('Userfollow_model');
		$followed_members = $this->Userfollow_model->get_followed_members($uid);
		$this->load->view('pages/following', array('follow' => $followed_members));
	}
	private function getNotifications(){
		$this->load->library('session');	
		$this->load->model('Notification_model');
		$this->load->model('Userphoto_model');
		$this->load->model('User_model');
		$uid = $this->session->userdata('logged_uid');
		$notifications = $this->Notification_model->retrieve(array('to_uid' => $uid), array('field' => 'date','value' => 'desc'));
		$new_notification = array();
		foreach($notifications as $notification){
			$user = $this->User_model->retrieve(array('uid'=>$notification['from_uid']));
			if($notification['type'] == 'follow'){
				$notification['notification_string'] = '<div class="mCard" style="background-color: #eaae62" data-id="'.$notification['id'].'">							
															<div class="profile-pic" data-uid="'.$user[0]['uid'].'">
																<img src="'.$user[0]['profile_pic'].'"> 							
															</div> 							
															<div class="profile-name" data-uid="'.$user[0]['uid'].'">
																<span class="full-name">'.$user[0]['firstname'].' '.$user[0]['lastname'].'</span>
															</div> 							
															<div class="info">followed you.</div> 							
															<div style="clear:both"></div> 											  
														</div>';
			}else if($notification['type'] == 'photo_like'){
				$photo = $this->Userphoto_model->retrieve(array('reference'=>$notification['pid']));
				$notification['notification_string'] = '<div class="mCard" style="background-color: #91d152" data-id="'.$user[0]['uid'].'">							
															<div class="profile-pic" data-uid="'.$user[0]['uid'].'">
																<img src="'.$user[0]['profile_pic'].'"> 							
															</div> 							
															<div class="profile-name" data-uid="'.$user[0]['uid'].'">
																<span class="full-name">'.$user[0]['firstname'].' '.$user[0]['lastname'].'</span>
															</div> 							
															<div class="info">
																<span>liked your <a href="../../widgets/photo/'.$notification['pid'].'" class="photo_notification">photo.</a></span>
																<a href="../../widgets/photo/'.$notification['pid'].'" class="photo_notification"><img src="https://s3.amazonaws.com/wheewhew/user/'.$photo[0]['uid'].'/photos/'.$photo[0]['filename'].'"></a>
															</div> 							
															<div style="clear:both"></div> 											  
														</div>';
			}else if($notification['type'] == 'photo_comment'){
				$photo = $this->Userphoto_model->retrieve(array('reference'=>$notification['pid']));
				$notification['notification_string'] = '<div class="mCard" style="background-color: #b166a0" data-id="'.$user[0]['uid'].'">							
															<div class="profile-pic" data-uid="'.$user[0]['uid'].'">
																<img src="'.$user[0]['profile_pic'].'"> 							
															</div> 							
															<div class="profile-name" data-uid="'.$user[0]['uid'].'">
																<span class="full-name">'.$user[0]['firstname'].' '.$user[0]['lastname'].'</span>
															</div> 							
															<div class="info">
																<span>commented on your <a href="../../widgets/photo/'.$notification['pid'].'" class="photo_notification">photo.</a></span>
																<a href="../../widgets/photo/'.$notification['pid'].'" class="photo_notification"><img src="https://s3.amazonaws.com/wheewhew/user/'.$photo[0]['uid'].'/photos/'.$photo[0]['filename'].'"></a>
															</div> 							
															<div style="clear:both"></div> 											  
														</div>';
			}else if($notification['type'] == 'interested'){
				if($notification['revealed'] == 0){
					$str = ($user[0]['sex'] == 'm')? 'man says he': 'woman says she';
					$notification['notification_string'] = '<div class="mCard" style="background-color: #dd7499" data-id="'.$notification['id'].'">														
																<div class="admirer" style="position: relative">
																	<span class="full-name" style="max-width: 100%">You got an admirer!</span>
																</div> 							
																<div class="info">
																	<span> A '. $str .' is interested in you.</span>
																</div> 							
																<button class="reveal">Reveal</button>
																<div style="clear:both"></div> 											  
															</div>';
				}else{
					$notification['notification_string'] = '<div class="mCard" style="background-color: #dd7499" data-id="'.$notification['id'].'">							
																<div class="profile-pic" data-uid="'.$user[0]['uid'].'">
																	<img src="'.$user[0]['profile_pic'].'"> 							
																</div> 							
																<div class="profile-name" data-uid="'.$user[0]['uid'].'">
																	<span class="full-name">'.$user[0]['firstname'].' '.$user[0]['lastname'].'</span>
																</div> 							
																<div class="info">admired you.</div> 							
																<div style="clear:both"></div> 											  
															</div>';
				}
			}
			$new_notification[] = $notification;
		}
		return $new_notification;
	}
	function shouts(){
		$this->load->model('Userpost_model');
		$my_uid = $this->session->userdata('logged_uid');
		$me = $this->User_model->retrieve(array('uid'=>$my_uid));
		$me = $me[0];
		$data['me'] = $me;
		if(empty($me['current_longitude']) OR empty($me['current_lattitude']))
			$data['posts'] = array();
		else
			$data['posts'] = $this->Userpost_model->getLast50();
		$this->load->view('pages/shouts',$data);
	}
	function member($username){
		$this->load->model('Userinterest_model');
		$logged_uid = $this->session->userdata('logged_uid');
		$user_data= $this->User_model->retrieve(array('xmpp_user'=>$username));
		$page_data['user_data'] = $user_data[0];
		$page_data['user_photo'] = $user_data[0]['profile_pic'];
		$page_data['logged_user_uid'] = $this->session->userdata('logged_uid'); 
		$interested_in = $this->Userinterest_model->retrieve(array('uid'=>$user_data[0]['uid']));
		$page_data['user_data']['interested_in'] = (is_array($interested_in) && count($interested_in) > 0)? $interested_in[0] : array();
		$this->load->view('pages/member_profile',$page_data);		
	}
	function user_shouts($uid){
		$this->load->model('Userpost_model');
		$shouts = $this->Userpost_model->getLast50(array('user_post.uid'=>$uid));
		foreach($shouts as $shout){
			echo $this->load->view('widgets/stream_entry',$shout);
		}
	}
	function user_photos($uid){
		$user_photos = $this->Userphoto_model->retrieve_largest(array('uid' => $uid));
		$this->load->view('pages/user_photos',array('user_photos' => $user_photos, 'uid'=>$uid));		
	}
	function members(){
		$where = $this->input->post();
		$data['members'] = $this->User_model->member_search(null,50);
		$this->load->view('pages/members',$data);
	}
}