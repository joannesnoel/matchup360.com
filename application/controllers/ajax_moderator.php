<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_Moderator extends CI_Controller {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->model('moderator_model');
		$this->load->model('user_model');
		$this->load->model('userphoto_model');
		$this->load->library('session');
		$this->load->helper('url');
    }
	public function removeUser($uid){
		$this->load->library('s3');
		$all_user_photos = $this->userphoto_model->retrieve_all_dimensions(array('uid' => $uid));
		foreach($all_user_photos as $user_photo){
			S3::deleteObject("wheewhew", "user/".$uid."/photos/".$user_photo['filename']);
		}
		$user_xmpp = $this->user_model->delete_user($uid);
		exec('sudo /usr/sbin/ejabberdctl unregister '.$user_xmpp.' matchup360.com');
		 //sudo ejabberdctl unregister paulnoel matchup360.com
	}
	public function getUsers(){
		$this->load->model('user_model');
		if($_POST['key'] == ''){
			$users = $this->user_model->getAll();
		}else{
			$users = $this->user_model->get_users($_POST);
		}
		echo json_encode($users);
	}
	public function getPhotos(){
		$post = $this->input->post();
		$this->load->model('Userphoto_model');
		$photos = $this->Userphoto_model->retrieve();
		echo json_encode($photos);
	}
	public function deletePhoto($photo_id){
		$this->load->model('Userphoto_model');
		$this->load->model('Photocomment_model');
		$this->load->model('Photolikes_model');
		$this->load->model('Notification_model');
		$photos = $this->Userphoto_model->retrieve_all_dimensions(array('reference' => $photo_id));
		//$photos = $photos[0];
		$photo = $this->Userphoto_model->retrieve(array('pid' => $photo_id), 0);
		$photos[] = $photo[0];
		$this->load->library('s3');
		foreach($photos as $photo){
			S3::deleteObject("wheewhew", "user/".$photo['uid']."/photos/".$photo['filename']);
			$this->Userphoto_model->delete($photo['pid']);
		}
		$this->Photolikes_model->delete(array('reference'=>$photo_id));
		$this->Notification_model->delete(array('pid'=>$photo_id,'type'=>'photo_like'));
		$this->Notification_model->delete(array('pid'=>$photo_id,'type'=>'photo_comment'));
		$this->Photocomment_model->delete_photo_comments($photo_id);
	}
	public function test_xmpp($toJid, $msg){
		include("/var/www/XMPPHP/XMPP.php");
		$conn = new XMPPHP_XMPP(config_item('site_domain'), 5222, 'admin', 'sempron123', 'default', config_item('site_domain'), $printlog=False, $loglevel=LOGGING_INFO);
		$conn->connect();
		$conn->processUntil('session_start');
		$conn->message($toJid.'@matchup360.com/default', $msg);
		$conn->disconnect();
	}
}