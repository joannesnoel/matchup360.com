<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {


	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
		$this->updateLastSeen();
		header('Content-Type: application/json');
    }

	public function updateLastSeen(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model("User_model");
		$data = array('last_seen'=>date("Y-m-d H:i:s"));
		$where = array('uid'=>$uid);
		$this->User_model->update($data,$where);
	}
	public function fb_user_exist(){
		$post = $this->input->post();
		$me = $this->User_model->retrieve(array('fb_userid'=>$post['fb_userid']));
		if(!empty($me)&&(count($me)>0))
			echo json_encode(array('result'=>true,'uid'=>$me[0]['uid']));
		else
			echo json_encode(array('result'=>false));
	}
	public function randomPhoto(){
		$this->load->model('Userphoto_model');
		$this->load->model('Photocomment_model');
		$this->load->model('Photolikes_model');
		$post = $this->input->post();
		$uid = $this->session->userdata('logged_uid');
		$randomPhoto = $this->Userphoto_model->random($post);
		$data = $randomPhoto;
		$data['comments'] = $this->Photocomment_model->retrieve($randomPhoto['reference']);
		$data['is_liked'] = $this->Photolikes_model->is_liked(array('reference'=> $randomPhoto['reference'],
																	'uid' => $uid));
		$data['photo_likes'] = $this->Photolikes_model->get_likes_string($randomPhoto['reference']); 
		if($randomPhoto == false)
			echo json_encode(array('success'=>false,'message'=>'No more photos to view. Come back again later! :)'));
		else{
			$data['success'] = true;
			echo json_encode($data);
		}
	}
	public function randomMember(){
		$this->load->model('User_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Userinterest_model');
		$user = $this->User_model->retrieve(array('uid' => $this->session->userdata('logged_uid'))); 
		$user_interests = $this->Userinterest_model->retrieve(array('uid' =>$user[0]['uid']));
		//print_r($user_interests);
		$where = array();
				
		$data['user'] = $this->User_model->random($where);
		
		//print_r($data['user']);
		$user_photos = $this->Userphoto_model->retrieve_largest(array('uid'=>$data['user']['uid'])); 
		$data['user_photos'] = $user_photos;
		$this->load->view('pages/random_member',$data); 
	}
	public function upload_photo($photo_url = ''){
		header('Content-Type: text/html');
		$demo_mode = false;
		$upload_dir = 'uploads/';
		$allowed_ext = array('jpg','jpeg','png','gif');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$config['max_size']	= '4096';

		$this->load->library('upload', $config);

		if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
			$this->exit_status('Error! Wrong HTTP method!');
		}
		
		if (!$this->upload->do_upload('Filedata') && $photo_url == ''){
			$error = array('error' => $this->upload->display_errors());
			$this->exit_status($error);
		}else{
				$data = array('upload_data' => $this->upload->data());
				$this->load->model('Userphoto_model');
				$this->load->library('s3');
				
				$random_str = random_string('alnum', 12);
				if($photo_url != ''){
					preg_match("/([^\/]+)(?=\.\w+$).([a-z]+)/i", $photo_url, $matches);
					file_put_contents($upload_dir.$matches[1].'.'.$matches[2], file_get_contents($photo_url));
					$file = $upload_dir.$matches[1].'.'.$matches[2];
					$ext = $matches[2];
				}else{
					$file = $data['upload_data']['full_path'];
					$ext = substr($data['upload_data']['file_ext'],1);
				}
				//$exif_data = exif_read_data ($file);
				
				$output_file = "";
				
				switch($ext){
					case 'jpeg':
							$output_file = str_replace('.jpeg','.jpg',strtolower($file));
							rename($file, $output_file);
					break;
					case 'jpg':
							$output_file = $file;
					break;
					case 'png': 
							$output_file = str_replace('.png','.jpg',strtolower($file));
							$input = imagecreatefrompng($file);
							list($width, $height) = getimagesize($file);
							$output = imagecreatetruecolor($width, $height);
							$white = imagecolorallocate($output,  255, 255, 255);
							imagefilledrectangle($output, 0, 0, $width, $height, $white);
							imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
							imagejpeg($output, $output_file);
							unlink($file);
					break;
					case 'gif': 
							$output_file = str_replace('.gif','.jpg',strtolower($file));
							$input = imagecreatefromgif ($file);
							list($width, $height) = getimagesize($file);
							$output = imagecreatetruecolor($width, $height);
							$white = imagecolorallocate($output,  255, 255, 255);
							imagefilledrectangle($output, 0, 0, $width, $height, $white);
							imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
							imagejpeg($output, $output_file);
							unlink($file);
					break;
					default: $this->exit_status('Only '.implode(',',$allowed_ext).' files are allowed!');
				}
				$this->load->library('image_lib');
				$originalImageData = $this->image_lib->get_image_properties($output_file, TRUE); 
				if($photo_url == '' && ($originalImageData['width']<400 OR $originalImageData['height']<300)){
					$this->exit_status('400x300 or bigger photo is required.');
				}else if($photo_url != '' && ($originalImageData['width']<400 OR $originalImageData['height']<300)){
					return 0;
				}
				S3::putObject(S3::inputFile($output_file), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str.".jpg", S3::ACL_PUBLIC_READ);
								
				$pid = $this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
													 'filename'=>$random_str.".jpg",
													 'height'=>$originalImageData['height'],
													 'width'=>$originalImageData['width'],
													 'dimension'=>0
													 ));
				
				if((int)$originalImageData['width'] >= 800){
					
					//Create 800 Copy	
					$config['image_library'] = 'gd2';
					$config['source_image']	= $output_file;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim'] = 'width';
					$config['width'] = 800;
					$config['height'] = 800;
					$config['new_image'] = $random_str.'_800.jpg';

					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					
					$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_800.jpg', TRUE); 
					$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
														 'reference'=>$pid,
														 'filename'=>$random_str."_800.jpg",
														 'height'=>$fileData['height'],
														 'width'=>$fileData['width'],
														 'dimension'=>1
														 ));
					$config = array();
					$this->image_lib->clear();
					S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_800.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_800.jpg", S3::ACL_PUBLIC_READ);
				}
				if((int)$originalImageData['width'] >= 600){
					//Create 600 Copy
					$config['image_library'] = 'gd2';
					$config['source_image']	= $output_file;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim'] = 'width';
					$config['width'] = 600;
					$config['height'] = 600;
					$config['new_image'] = $random_str.'_600.jpg';
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					
					$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_600.jpg', TRUE); 
					$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
														 'reference'=>$pid,
														 'filename'=>$random_str."_600.jpg",
														 'height'=>$fileData['height'],
														 'width'=>$fileData['width'],
														 'dimension'=>2
														 ));
					
					$config = array();
					$this->image_lib->clear();
					S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_600.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_600.jpg", S3::ACL_PUBLIC_READ);
					
				}
				if((int)$originalImageData['width'] >= 400){
					//Create 400 Copy
					$config['image_library'] = 'gd2';
					$config['source_image']	= $output_file;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim'] = 'width';
					$config['width'] = 400;
					$config['height'] = 400;
					$config['new_image'] = $random_str.'_400.jpg';
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					
					$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_400.jpg', TRUE); 
					$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
														 'reference'=>$pid,
														 'filename'=>$random_str."_400.jpg",
														 'height'=>$fileData['height'],
														 'width'=>$fileData['width'],
														 'dimension'=>3
														 ));
					
					$config = array();
					$this->image_lib->clear();
					S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_400.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_400.jpg", S3::ACL_PUBLIC_READ);
					
				}
				
				//Create square
				
				$config['image_library'] = 'gd2';
				$config['source_image']	= '/var/www/uploads/'.$random_str.'_400.jpg';
				$config['new_image'] = $random_str.'_thumb.jpg';
				$config['maintain_ratio'] = FALSE;
				
				$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_400.jpg', TRUE); 
				$excess = (int)$fileData['height'] - 300;
				$config['y_axis'] = floor($excess / 2) - floor($excess * 0.33);
				$config['x_axis'] = 50;
				$config['width'] = 300;
				$config['height'] = 300;
				
				//Load image library and crop
				$this->image_lib->initialize($config);
				$this->image_lib->crop();
				
				S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_thumb.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_thumb.jpg", S3::ACL_PUBLIC_READ);
				
				$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_thumb.jpg', TRUE); 
				$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
													 'reference'=>$pid,
													 'filename'=>$random_str."_thumb.jpg",
													 'height'=>$fileData['height'],
													 'width'=>$fileData['width'],
													 'dimension'=>4
													 ));
				if(file_exists('/var/www/uploads/'.$random_str.'_800.jpg'))									 
					unlink('/var/www/uploads/'.$random_str.'_800.jpg');
				if(file_exists('/var/www/uploads/'.$random_str.'_600.jpg'))	
					unlink('/var/www/uploads/'.$random_str.'_600.jpg');
				if(file_exists('/var/www/uploads/'.$random_str.'_400.jpg'))	
					unlink('/var/www/uploads/'.$random_str.'_400.jpg');
				if(file_exists('/var/www/uploads/'.$random_str.'_thumb.jpg'))	
					unlink('/var/www/uploads/'.$random_str.'_thumb.jpg');
				
				unlink($output_file);
				if($photo_url == '')
					$this->exit_status('Upload success!');
		}
		if($photo_url == '')
			$this->exit_status('Something went wrong with your upload!');
		// Helper functions
	}
	public function deletePhoto($photo_id){
		$this->load->model('Userphoto_model');
		$this->load->model('Photocomment_model');
		$this->load->model('Notification_model');
		$photos = $this->Userphoto_model->retrieve_all_dimensions(array('reference' => $photo_id));
		//$photos = $photos[0];
		$photo = $this->Userphoto_model->retrieve(array('pid' => $photo_id), 0);
		$photos[] = $photo[0];
		if($photos[0]['uid'] == $this->session->userdata('logged_uid')){
			$this->load->library('s3');
			foreach($photos as $photo){
				S3::deleteObject("wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$photo['filename']);
				$this->Userphoto_model->delete($photo['pid']);
			}
			$this->Notification_model->delete(array('pid' => $photo_id));
		}
		$this->Photocomment_model->delete_photo_comments($photo_id);
	}
	public function addBuddy(){
		$data = array(
						'from' => $this->input->post('from_uid'),
						'to' => $this->input->post('to_uid')
					);
		$this->load->model('Userbuddyrequest_model');
		$addBuddy = $this->Userbuddyrequest_model->addBuddy($data);
		if($addBuddy > 0){
			$this->exit_status('success');
		}else{
			$this->exit_status('requested');
		}
	}
	public function acceptBuddy(){
		$data = array(
						'from' => $this->input->post('from_uid'),
						'to' => $this->input->post('to_uid')
					);
		$this->load->model('Userbuddyrequest_model');
		$acceptBuddy = $this->Userbuddyrequest_model->acceptBuddy($data);
		
		if($acceptBuddy == -999 || $acceptBuddy > 0) {
			$this->exit_status('Success');
		}
	}
	public function getUserData(){
		$post = $this->input->post();
		$this->load->model('User_model');
		$user_data = $this->User_model->retrieve(array('uid'=>$post['uid']));
		$user_data = $user_data[0];
		echo json_encode($user_data);
	}
	/*public function fetchInfo() {
		$data = array(
						'uid' => $this->input->post('uid')
					);
					
		$this->load->model('Search_model');
		$info = $this->Search_model->fetchInfo($data);
		
		echo '<img class="user-profile-pic" src="https://s3.amazonaws.com/wheewhew/user/'.$info[0]['uid'].'/photos/'.$info[0]['profile_pic'].'" style="float: left; border: 1px solid #424242;">';
		echo '<p class="user-profile-pic" style="margin: 0; float: left; margin-left: 7px; font: 23pt segoe ui;">';
		echo $info[0]['firstname'] . " " . $info[0]['lastname'];
		echo '</p>';
		
		
	}*/
	
	private function get_extension($file_name){
		$ext = explode('.', $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	private function exit_status($str){
		echo json_encode(array('status'=>$str));
		exit;
	}
	public function get_photos(){
		$this->load->library('session');
		$this->load->model('Userphoto_model');
		$user_photos = $this->Userphoto_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		echo json_encode($user_photos);
	}
	public function create_user(){
		$this->load->helper(array('form', 'url', 'string'));
		$this->load->library(array('form_validation','encrypt','session'));
		
		$this->form_validation->set_rules('firstname', 'Firstname', 'required');
		$this->form_validation->set_rules('lastname', 'Lastname', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|matches[email-verify]|callback_email_check');
		$this->form_validation->set_rules('email-verify', 'Email Confirmation', 'valid_email|required');
		$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[8]|matches[pwd-verify]');
		$this->form_validation->set_rules('pwd-verify', 'Password Confirmation', 'required');
		$this->form_validation->set_error_delimiters('','');
		
		if ($this->form_validation->run() == FALSE){
			$result = array();
			$result['success'] = false;
			$result['error']['firstname'] = form_error('firstname');
			$result['error']['lastname'] = form_error('lastname');
			$result['error']['email'] = form_error('email');
			$result['error']['email2'] = form_error('email-verify');
			$result['error']['password'] = form_error('pwd');
			echo json_encode($result);
		}else{	
			
			$this->load->model('User_model');
			$confirmation_code = random_string('alnum', 35);
			$this->load->model('Confirmation_model');
			$username = $this->User_model->generate_xmpp_user($this->input->post('firstname', TRUE), $this->input->post('lastname', TRUE));
			$password  = random_string('alnum', 15);
			
			$node = config_item('site_domain');
			exec('sudo /usr/sbin/ejabberdctl register '.$username.' '.$node.' '.$password.' 2>&1',$output,$status);
			$user_data = array(
								'firstname'=> $this->input->post('firstname', TRUE),
								'lastname'=> $this->input->post('lastname', TRUE),
								'email'=> $this->input->post('email', TRUE),
								'password'=>  $this->encrypt->sha1($this->input->post('pwd', TRUE)),
								'xmpp_user'=>$username,
								'username'=>$username,
								'xmpp_password'=>$password,
								'joined_date'=> date("Y-m-d H:i:s"),
								'fb_access_token'=> $this->input->post('fb_access_token',TRUE)
							);
			$referrer = $this->session->userdata('referrer_uid');
			if(!empty($referrer)){
				$user_data['referrer_uid'] = $this->session->userdata('referrer_uid');
				$this->User_model->add_points(50,$user_data['referrer_uid']);
			}
							
			$uid = $this->User_model->insert($user_data);
			$this->session->set_userdata('logged_uid',$uid);
			$confirmation_data = array(
									'uid' => $uid,
									'confirmation_code' => $confirmation_code
								);
			$this->Confirmation_model->insert($confirmation_data);
			$this->load->library('email');
			$this->email->from(config_item('webmaster_email'), config_item('site_name'));
			$this->email->to($this->input->post('email', TRUE)); 
			$this->email->subject('Welcome to Matchup360!');
			$template_data['link'] = 'http://'.config_item('site_domain').'/verify/'.$uid.'/'.$confirmation_code;
			$template_data['firstname'] = $this->input->post('firstname', TRUE);
			$template_data['unsub_link'] = 'http://'.config_item('site_domain').'/unsub/'.$this->input->post('email', TRUE);
			$this->email->message($this->load->view('emails/welcome_email',$template_data,true));	
			$this->email->send();

			$result['success'] = true;
			echo json_encode($result);
		}
	}
	public function email_check($email){
		$this->load->model('User_model');
		$check_email = $this->User_model->check_email($email);
		if($check_email){
			$this->form_validation->set_message('email_check', 'Email is already in use.');
			return false;
		}
		return true;
	}
	public function getUserId(){
		$this->load->library('session');
		$result = $this->db->query('select uid, profile_pic from user_info'); 
		$result = $result->result_array();
		print_r($result);
		echo $this->session->userdata('logged_uid');
	}
	public function set_profile_coordinates(){
		$this->load->helper('url');
		if($this->input->post('action') == 'crop'){
		
			$random_str = random_string('alnum', 12);
			
			copy($this->input->post('orig_photo_url'), '/var/www/uploads/'.$random_str.'.jpg');
			$this->load->library('image_lib');
			$this->load->library('s3');
			
			$config['image_library'] = 'gd2';
			$config['source_image']	= '/var/www/uploads/'.$random_str.'.jpg';
			$config['maintain_ratio'] = FALSE;
			$config['x_axis'] = $this->input->post('x');
			$config['y_axis'] = $this->input->post('y');
			$config['width'] = $this->input->post('w');
			$config['height'] = $this->input->post('h');

			$this->image_lib->initialize($config); 

			if ( ! $this->image_lib->crop())
			{
				echo $this->image_lib->display_errors();
			}
			$this->image_lib->clear();
			
			$config['image_library'] = 'gd2';
			$config['source_image']	= '/var/www/uploads/'.$random_str.'.jpg';
			$config['maintain_ratio'] = TRUE;
			$config['width']	 = 400;
			$config['height']	= 4000;
			
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->load->model('Userphoto_model');
			$profile_photo = $this->Userphoto_model->profile_photo($this->session->userdata('logged_uid'));
			
			if($profile_photo){
				S3::deleteObject("wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$profile_photo);
			}
			S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_profile.jpg", S3::ACL_PUBLIC_READ);
			
			$this->Userphoto_model->set_profile_photo($this->session->userdata('logged_uid'),$random_str.'_profile.jpg');
			
			unlink('/var/www/uploads/'.$random_str.'.jpg');
			if(isset($_POST['requester']) && $_POST['requester']=='my_photos'){
				redirect('my_photos');
			}else if(isset($_POST['requester']) && $_POST['requester']=='canvas'){
				redirect('canvas');
			}else{
				redirect('edit_profile');
			}
		}
	}
	public function create_userInfo(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('hometown_components', 'Hometown', 'required');		
		$this->form_validation->set_rules('sex', 'Sex', 'required');		
		$this->form_validation->set_rules('month', 'Month', 'required');	
		$this->form_validation->set_rules('day', 'Day', 'required');	
		$this->form_validation->set_rules('year', 'Year', 'required');	
		$this->form_validation->set_rules('language', 'Language', 'required');	
		$this->form_validation->set_error_delimiters('','');
		
		if($this->form_validation->run() == FALSE){
			$result = array();
			$result['success'] = false;
			$result['error']['hometown_components'] = form_error('hometown_components');
			$result['error']['sex'] = form_error('sex');
			$result['error']['birthdate'] = form_error('month').form_error('day').form_error('year');
			$result['error']['language'] = form_error('language');
			echo json_encode($result);
		}else{
			$post = $this->input->post();
			$post['hometown_components'] = json_decode($post['hometown_components']);
			$long = $post['hometown_components']->lng;
			$lat  = $post['hometown_components']->lat;
			$this->load->model('Userlocation_model');
			
			$user_info = array(
						'current_longitude' => $long,
						'current_lattitude' => $lat,
						'sex'=> $post['sex'],
						'birthdate'=> $post['year'].'-'.$post['month'].'-'.$post['day']
					);
					
			$this->load->model('Userlanguage_model');
			
			foreach($post['language'] as $key => $language){
				$this->Userlanguage_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
			}
			
			$location_id = 0;
			
			for($i = count($post['hometown_components']->address_components)-1; $i>=0; $i--){
				$location = $post['hometown_components']->address_components[$i];
				$location_check = $this->Userlocation_model->retrieve(array('name'=>$location->long_name,'type'=>$location->types[0]));
				if($location_check == false){
					$location_types = array('locality','administrative_area_level_1','administrative_area_level_2','country');
					if($location->types[0]){
						$data = array(
									'location' => 'hometown',
									'type' => $location->types[0],
									'name' => $location->long_name,
									'parent' => $location_id
								);								
						$location_id = $this->Userlocation_model->insert($data);
					}
				}else{
					$location_id = $location_check[0]->locid;
				}
				switch($location->types[0]){
					case 'administrative_area_level_1' : 
						$user_info['state'] = $location_id;
					break;
					case 'administrative_area_level_2' : 
						$user_info['county'] = $location_id;
					break;
					case 'locality' : 
						$user_info['city'] = $location_id;
					break;
					case 'country' : 
						$user_info['country'] = $location_id;
					break;
				}
			}
			$this->session->set_userdata('user_location', array('lat'=>$lat,'lng'=>$long));
			$this->load->model('User_model');
			$this->User_model->update($user_info, array('uid'=>$this->session->userdata('logged_uid')));
			echo json_encode(array('success'=>true));
		}
	}
	public function find_people(){
		$post = $this->input->post();
		$this->load->model('Search_model');
		$from_location = array(
							'lat'=>$post['lat'],
							'lng'=>$post['lng']
						);
		$age_range[0] = $post['age_from'];
		$age_range[1] = $post['age_to'];
		
		$this->Search_model->search_nearby_people($from_location,$post['radius'],$post['interested_in'], $age_range);
	}
	public function random_members(){
		$this->load->model('User_model');
		$members = $this->User_model->random_members($this->session->userdata('logged_uid'),20);
		echo json_encode($members);
	}
	public function get_members(){
		$criteria = $this->input->post(NULL, true);
		$this->load->model('Search_model');
		$members = $this->Search_model->members($criteria);
		echo json_encode($members);
	}
	public function profileWidget($uid){
		
		$this->load->model('User_model');
		$user = $this->User_model->get_user_details($uid);
		$user = $user[0];
		$page_data['photo_url'] = $user['profile_pic'];
		$page_data['uid'] = $uid;
		$page_data['jabber_address'] = $user['xmpp_user'];
		$page_data['firstname'] = $user['firstname'];
		$page_data['lastname'] = $user['lastname'];
		$page_data['age'] = $user['age'];
		$page_data['sex'] = $user['sex'];
		$page_data['interested_in_men'] = $user['interested_in_men'];
		$page_data['interested_in_women'] = $user['interested_in_women'];
		$page_data['status'] = $user['relationship_status'];
		$page_data['hometown_location'] = $user['hometown_location'];
		$page_data['current_location'] = $user['current_location'];
		$page_data['about_me'] = $user['about_me'];
		$page_data['favorite_quotation'] = $user['favorite_quotation'];
		$page_data['user_photos'] = $this->User_model->get_user_photos($uid);
		$this->load->view("widgets/profile",$page_data);
	}
	public function sendMessage(){
		error_reporting(E_ERROR);
		include("/var/www/XMPPHP/XMPP.php");
		$postData = $this->input->post();
		$this->load->model('User_model');
		$from_user = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$to_user = $this->User_model->retrieve(array('uid'=>$postData['to_uid']));
		
		$from_user = $from_user[0];
		$to_user = $to_user[0];
		
		$message = array();
		
		$msg_insert = array('from_uid'=>$from_user['uid'],'to_uid'=>$to_user['uid'],'message'=>$postData['message'],'sent_date'=>date("Y-m-d H:i:s"));
		if(isset($post['type'])){
			$msg_insert['type'] = $post['type'];
			$message['type'] = $post['type'];
		}
		
		$this->load->model('Usermessages_model');
		$message_id = $this->Usermessages_model->insert($msg_insert);
		
		$message['from_uid'] = $from_user['uid'];
		$message['from_fullname'] = $from_user['firstname'].' '.$from_user['lastname'];
		$message['from_profile_pic'] = $from_user['profile_pic'];
		$message['to_uid'] = $to_user['uid'];
		$message['to_fullname'] = $to_user['firstname'].' '.$to_user['lastname'];
		$message['body'] = $postData['message'];	
		$message['sent_date'] = date("Y-m-d H:i:s");
		$message['msg_id'] = $message_id;		
		$message = json_encode($message);
		
		/** IF OFFLINE **/
		//$to_user = $this->User_model->retrieve(array('uid' => $to_));
		if($this->is_online($to_user['xmpp_user'],false) == FALSE){
			$template_data['user'] = $from_user;
			$template_data['message'] = $postData['message'];
			$this->load->library('email');
			$this->email->from(config_item('webmaster_email'), config_item('site_name'));
			$this->email->to($to_user['email']); 
			$this->email->subject('Message while offline!');
			$this->email->message($this->load->view('emails/message_email',$template_data,true));	
			$this->email->send();
		}
		//print_r(array('from_uid'=>$from_user['uid'],'to_uid'=>$to_user['uid'],'messsage'=>$postData['message'],'msg_date'=>date("Y-m-d H:i:s"))); die();
		// $conn = new XMPPHP_XMPP(config_item('site_domain'), 5222, 'admin', 'sempron123', 'default', config_item('site_domain'), $printlog=False, $loglevel=LOGGING_INFO);
		// $conn->connect();
		// $conn->processUntil('session_start');
		// $conn->message($to_user['xmpp_user'].'@matchup360.com/default', $message, $post['type']);
		// $conn->disconnect();
		echo json_encode(array('status'=>'success','from_jid'=>$from_user['xmpp_user'].'@matchup360.com','to_jid'=>$to_user['xmpp_user'].'@matchup360.com','msg_id'=>$message_id,'message'=>$message));
	}
	function messageReceived(){
		$postData = $this->input->post();
		$this->load->model('Usermessages_model');
		$this->Usermessages_model->update(array('read_date'=>date("Y-m-d H:i:s")),array('mid'=>$postData['msg_id']));
	}
	function followMember(){
		include("/var/www/XMPPHP/XMPP.php");
		error_reporting(E_ERROR);
		$post = $this->input->post();
		$this->load->model('User_model');
		$this->load->model('Notification_model');
		$from_user = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$to_user = $this->User_model->retrieve(array('uid'=>$post['to_uid']));
		$from_user = $from_user[0];
		$to_user = $to_user[0];
		$this->load->model('Userfollow_model');
		$response = array();
		
		$check_duplicate = $this->Userfollow_model->retrieve(array('from'=>$from_user['uid'],'to'=>$to_user['uid']));
		$notification_exist = $this->Notification_model->exist($to_user['uid'], 'follow', $from_user['uid']);
		$notification_string = '';
		if($check_duplicate == false){
			$insert_id = $this->Userfollow_model->insert(array('from'=>$from_user['uid'],'to'=>$to_user['uid'],'request_date'=>date('Y-m-d H:i:s')));
			if(!$notification_exist){
				$this->Notification_model->insert(array('to_uid'=>$to_user['uid'],
													'from_uid'=> $from_user['uid'],
													'date'=>date('Y-m-d H:i:s'), 
													'type' => 'follow',
													'status' => 1));
				$notification_string = '<div class="mCard" style="background-color: #eaae62" data-id="'.$from_user['uid'].'">							
									<div class="profile-pic" data-uid="'.$from_user['uid'].'">
										<img src="'.$from_user['profile_pic'].'"> 							
									</div> 							
									<div class="profile-name" data-uid="'.$from_user['uid'].'">
										<span class="full-name">'.$from_user['firstname'].' '.$from_user['lastname'].'</span>
									</div> 							
									<div class="info">followed you.</div> 							
									<div style="clear:both"></div> 											  
								</div>';
			}
			$response['status'] = 'success';
			$response['user']	= $to_user;
		}else{
			$response['status'] = 'failed';
		}
		
		$check_if_follows_you = $this->Userfollow_model->retrieve(array('from'=>$to_user['uid'],'to'=>$from_user['uid']));

		if(count($check_if_follows_you)){
			exec("sudo ejabberdctl add_rosteritem ".$from_user['xmpp_user']." matchup360.com ".$to_user['xmpp_user']." matchup360.com '' '' both");
			exec("sudo ejabberdctl add_rosteritem ".$to_user['xmpp_user']." matchup360.com ".$from_user['xmpp_user']." matchup360.com '' '' both");
		}else{
			exec("sudo ejabberdctl add_rosteritem ".$from_user['xmpp_user']." matchup360.com ".$to_user['xmpp_user']." matchup360.com '' '' to");
			exec("sudo ejabberdctl add_rosteritem ".$to_user['xmpp_user']." matchup360.com ".$from_user['xmpp_user']." matchup360.com '' '' from");;
		}
		$response['xmpp_user'] = $to_user['xmpp_user'];
		$response['notification_string'] = $notification_string;
		echo json_encode($response);
	}
	function unfollowMember(){
		include("/var/www/XMPPHP/XMPP.php");
		error_reporting(E_ERROR);
		$post = $this->input->post();
		$this->load->model('User_model');
		$from_user = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$to_user = $this->User_model->retrieve(array('uid'=>$post['to_uid']));
		$from_user = $from_user[0];
		$to_user = $to_user[0];
		$this->load->model('Userfollow_model');
		
		$response = array();
		$response['user'] = $to_user;
		
		
		$this->Userfollow_model->delete(array('from'=>$from_user['uid'],'to'=>$to_user['uid']));
		
		$check_if_follows_you = $this->Userfollow_model->retrieve(array('from'=>$to_user['uid'],'to'=>$from_user['uid']));

		if(count($check_if_follows_you)){
			exec("sudo ejabberdctl add_rosteritem ".$from_user['xmpp_user']." matchup360.com ".$to_user['xmpp_user']." matchup360.com '' '' from");
			exec("sudo ejabberdctl add_rosteritem ".$to_user['xmpp_user']." matchup360.com ".$from_user['xmpp_user']." matchup360.com '' '' to");
		}else{
			exec("sudo ejabberdctl add_rosteritem ".$from_user['xmpp_user']." matchup360.com ".$to_user['xmpp_user']." matchup360.com '' '' none");
			exec("sudo ejabberdctl add_rosteritem ".$to_user['xmpp_user']." matchup360.com ".$from_user['xmpp_user']." matchup360.com '' '' none");;
		}
		$response['status'] = 'success';
		echo json_encode($response);
	}
	function getPersonalDescription(){
		$uid = $this->input->post('uid');
		$this->load->model('User_model');
		$this->load->model('Userlanguage_model');
		$this->load->model('Userprofile_model');
		$me = $this->User_model->retrieve(array('uid'=>$uid));
		$me = $me[0];
		$me['languages'] = $this->Userlanguage_model->retrieve(array('uid'=>$uid));
		$me['profile'] = $this->Userprofile_model->retrieve(array('uid'=>$uid));
		$me['profile'] = $me['profile'][0];
		$me['day'] = date('d',strtotime($me['birthdate']));
		$me['month'] = date('m',strtotime($me['birthdate']));
		$me['year'] = date('Y',strtotime($me['birthdate']));
		unset($me['birthdate']);
		//print_r($me);
		echo json_encode($me);

	}
	function updatePersonalDescription(){
		$post = $this->input->post();
		$user_info = array(
			'sex'=> $post['sex'],
			'birthdate'=> $post['year'].'-'.$post['month'].'-'.$post['day']
		);
		if(!empty($post['hometown_components'])){
			$post['hometown_components'] = json_decode($post['hometown_components']);
			$long = $post['hometown_components']->lng;
			$lat  = $post['hometown_components']->lat;
			$this->load->model('Userlocation_model');
			$location_id = 0;
			for($i = count($post['hometown_components']->address_components)-1; $i>=0; $i--){
				$location = $post['hometown_components']->address_components[$i];
				$location_check = $this->Userlocation_model->retrieve(array('name'=>$location->long_name,'type'=>$location->types[0]));
				if($location_check == false){
					$location_types = array('locality','administrative_area_level_1','administrative_area_level_2','country');
					if($location->types[0]){
						$data = array(
									'location' => 'hometown',
									'type' => $location->types[0],
									'name' => $location->long_name,
									'parent' => $location_id
								);								
						$location_id = $this->Userlocation_model->insert($data);
					}
				}else{
					$location_id = $location_check[0]->locid;
				}
				switch($location->types[0]){
					case 'administrative_area_level_1' : 
						$user_info['state'] = $location_id;
					break;
					case 'administrative_area_level_2' : 
						$user_info['county'] = $location_id;
					break;
					case 'locality' : 
						$user_info['city'] = $location_id;
					break;
					case 'country' : 
						$user_info['country'] = $location_id;
					break;
				}
			}
			$this->session->set_userdata('user_location', array('lat'=>$lat,'lng'=>$long));
			$user_info['current_longitude'] = $long;
			$user_info['current_lattitude'] = $lat;
		}
				
		$this->load->model('Userlanguage_model');
		$this->Userlanguage_model->delete(array('uid'=>$this->session->userdata('logged_uid')));
		foreach($post['language'] as $key => $language){
			$this->Userlanguage_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
		}

		$this->load->model('User_model');
		$this->User_model->update($user_info, array('uid'=>$this->session->userdata('logged_uid')));
		
		//if(!empty($post['profile-heading']) OR !empty($post['profile-introduction'])){
			$this->load->model('Userprofile_model');
			$userProfile = $this->Userprofile_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
			//echo $post['profile-introduction']; die();
			if($userProfile){
				$this->Userprofile_model->update(array('heading'=>$post['profile-heading'],'intro'=>$post['profile-introduction']),array('uid'=>$this->session->userdata('logged_uid')));
			}else{
				$this->Userprofile_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'heading'=>$post['profile-heading'],'intro'=>$post['profile-introduction']));
			}
		//}
		
		echo json_encode(array('success'=>true));
	}
	function getInterestedIn(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userinterest_model');
		$interests = $this->Userinterest_model->retrieve(array('uid'=>$uid)); 
		
		$data['interests'] = $interests;
		
		echo json_encode($data);

	}
	function updateInterestedIn(){
		$this->load->model('Userinterest_model');
		$data = $_POST;
		$where = array('uid' => $this->session->userdata('logged_uid'));
		$this->load->model('Userinterest_model');
		$this->Userinterest_model->delete(array('uid'=>$this->session->userdata('logged_uid')));
		foreach($data['interested_in'] as $key => $interest){
			$this->Userinterest_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
		}
		echo json_encode(array('success'=>true));
	}
	function getAppearance(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userappearance_model');
		$me = $this->Userappearance_model->retrieve(array('uid'=>$uid)); 
		$this->load->model('Userbodyart_model');
		$me['body_arts'] =  $this->Userbodyart_model->retrieve(array('uid'=>$uid)); 
		echo json_encode($me);
		//print_r($appearance);
	}
	function updateAppearance(){
		$this->load->model('Userappearance_model');
		$data = $_POST;
		$body_art = $data['body_art'];
		unset($data['body_art']);
		$where = array('uid' => $this->session->userdata('logged_uid'));
		$appearance_check = $this->Userappearance_model->retrieve($where);
		if(!empty($appearance_check)){
			$this->Userappearance_model->update($data, $where);
		}else{
			$data['uid'] = $this->session->userdata('logged_uid');
			$this->Userappearance_model->insert($data);
		}
		$this->load->model('Userbodyart_model');
		$this->Userbodyart_model->delete(array('uid'=>$this->session->userdata('logged_uid')));
		foreach($body_art as $key => $interest){
			$this->Userbodyart_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
		}
		echo json_encode($data);
		//print_r($data);
	}
	function getSituation(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Usersituation_model');
		$me = $this->Usersituation_model->retrieve(array('uid'=>$uid)); 
		$this->load->model('Usertolist_model');
		$this->Usertolist_model->set_table('user_pets');
		$me['pets'] =  $this->Usertolist_model->retrieve(array('uid'=>$uid)); 
		echo json_encode($me);
		//print_r($situation);
	}
	function updateSituation(){
		$this->load->model('Usersituation_model');
		$data = $_POST;
		$pets = $data['pets'];
		unset($data['pets']);
		$where = array('uid' => $this->session->userdata('logged_uid'));
		$situation_check = $this->Usersituation_model->retrieve($where);
		if(!empty($situation_check)){
			$this->Usersituation_model->update($data, $where);
		}else{
			$data['uid'] = $this->session->userdata('logged_uid');
			$this->Usersituation_model->insert($data);
		}
		$this->load->model('Usertolist_model');
		$this->Usertolist_model->set_table('user_pets');
		$this->Usertolist_model->delete(array('uid'=>$this->session->userdata('logged_uid')));
		foreach($pets as $key => $pet){
			$this->Usertolist_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
		}
		echo json_encode($data);
		//print_r($data);
	}
	function getEducationEmployment(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Usereduemployment_model');
		$education = $this->Usereduemployment_model->retrieve(array('uid'=>$uid)); 
		
		echo json_encode($education);
		//print_r($situation);
	}
	function updateEducationEmployment(){
		$this->load->model('Usereduemployment_model');
		$data = $_POST;
		$where = array('uid' => $this->session->userdata('logged_uid'));
		
		$eduemployment_check = $this->Usereduemployment_model->retrieve($where);
		if(!empty($eduemployment_check))
			$this->Usereduemployment_model->update($data, $where);
		else{
			$data['uid'] = $this->session->userdata('logged_uid');
			$this->Usereduemployment_model->insert($data);
		}
		
		echo json_encode($data);
		//print_r($data);
	}
	function getLeisure(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userleisure_model');
		$leisure = $this->Userleisure_model->retrieve(array('uid'=>$uid)); 
		echo json_encode($leisure);
		//print_r($appearance);
	}
	function updateLeisure(){
		$this->load->model('Userleisure_model');
		$data = $_POST;
		$where = array('uid' => $this->session->userdata('logged_uid'));
		$this->load->model('Usertolist_model');
		$this->Usertolist_model->set_table('user_leisure');
		$this->Usertolist_model->delete(array('uid'=>$this->session->userdata('logged_uid')));
		if(!empty($data['tv_genre'])){
			foreach($data['tv_genre'] as $key => $pet){
				$this->Usertolist_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
			}
		}
		if(!empty($data['movie_genre'])){
			foreach($data['movie_genre'] as $key => $pet){
				$this->Usertolist_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
			}
		}
		if(!empty($data['music_genre'])){
			foreach($data['music_genre'] as $key => $pet){
				$this->Usertolist_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
			}
		}
		if(!empty($data['book_genre'])){
			foreach($data['book_genre'] as $key => $pet){
				$this->Usertolist_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
			}
		}
		if(!empty($data['hobbies'])){
			foreach($data['hobbies'] as $key => $pet){
				$this->Usertolist_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
			}
		}
		echo json_encode($data);
		//print_r($data);
	}
	function getPersonality(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userpersonality_model');
		$me = $this->Userpersonality_model->retrieve(array('uid'=>$uid)); 
		$this->load->model('Usertolist_model');
		$this->Usertolist_model->set_table('user_enjoyment');
		$me['i_enjoy'] = $this->Usertolist_model->retrieve(array('uid'=>$uid)); 
		echo json_encode($me);
		//print_r($personality);
	}
	function updatePersonality(){
		$this->load->model('Userpersonality_model');
		$data = $_POST;
		$i_enjoy = $data['i_enjoy'];
		unset($data['i_enjoy']);
		$where = array('uid' => $this->session->userdata('logged_uid'));
		$personality_check = $this->Userpersonality_model->retrieve($where);
		if(!empty($personality_check))
			$this->Userpersonality_model->update($data, $where);
		else{
			$data['uid'] = $this->session->userdata('logged_uid');
			$this->Userpersonality_model->insert($data);
		}
		$this->load->model('Usertolist_model');
		$this->Usertolist_model->set_table('user_enjoyment');
		$this->Usertolist_model->delete(array('uid'=>$this->session->userdata('logged_uid')));
		foreach($i_enjoy as $key => $pet){
			$this->Usertolist_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
		}
		echo json_encode($data);
		//print_r($data);
	}
	function getViews(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userviews_model');
		$views = $this->Userviews_model->retrieve(array('uid'=>$uid)); 
		echo json_encode($views);
		//print_r($views);
	}
	function updateViews(){
		$this->load->model('Userviews_model');
		$data = $_POST;
		$where = array('uid' => $this->session->userdata('logged_uid'));
	
		$views_check = $this->Userviews_model->retrieve($where);
		if(!empty($views_check))
			$this->Userviews_model->update($data, $where);
		else{
			$data['uid'] = $this->session->userdata('logged_uid');
			$this->Userviews_model->insert($data);
		}
		echo json_encode($data);
		//print_r($data);
	}
	function getLookingFor(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userlooking_model');
		$looking = $this->Userlooking_model->retrieve(array('uid'=>$uid)); 
		echo json_encode($looking);
		//print_r($views);
	}
	function updateLookingFor(){
		$this->load->model('Userlooking_model');
		$data = $_POST;
		$where = array('uid' => $this->session->userdata('logged_uid'));
		$this->load->model('Usertolist_model');
		$this->Usertolist_model->set_table('user_looking');
		$this->Usertolist_model->delete(array('uid'=>$this->session->userdata('logged_uid')));
		foreach($data['find-attractive'] as $key => $pet){
			$this->Usertolist_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$key));
		}
		echo json_encode($data);
		//print_r($data);
	}
	function getMessageThread($from_uid){ 
		$this->load->model('Usermessages_model');
		$to_uid = $this->session->userdata('logged_uid');
		$message_thread = $this->Usermessages_model->getThread($from_uid, $to_uid);
		echo json_encode($message_thread);
	}
	function addPhotoComment(){
		$this->load->model('Photocomment_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Notification_model');
		$data = $_POST;
		$uid = $this->session->userdata('logged_uid');
		$user = $this->User_model->retrieve(array('uid'=>$uid));
		$user = $user[0];
		$where = array('uid' => $uid);
		$insert_id = $this->Photocomment_model->insert($data);
		$photo = $this->Userphoto_model->getThumbnail($data['reference']);					   
		if($uid != $photo['uid']){ 
			$this->Notification_model->insert(array('to_uid'=>$photo['uid'],
													'from_uid'=>$uid,
													'date'=>date('Y-m-d H:i:s'), 
													'type'=>'photo_comment',
													'pid' => $data['reference'],
													'status' => 1));
		}
		$notification_string = '<div class="mCard" style="background-color: #b166a0" data-id="'.$user['uid'].'">							
									<div class="profile-pic" data-uid="'.$user['uid'].'">
										<img src="'.$user['profile_pic'].'"> 							
									</div> 							
									<div class="profile-name" data-uid="'.$user['uid'].'">
										<span class="full-name">'.$user['firstname'].' '.$user['lastname'].'</span>
									</div> 							
									<div class="info">
										<span>commented on your <a href="../../widgets/photo/'.$data['reference'].'" class="photo_notification">photo.</a></span>
										<a href="../../widgets/photo/'.$data['reference'].'" class="photo_notification"><img src="https://s3.amazonaws.com/wheewhew/user/'.$photo['uid'].'/photos/'.$photo['filename'].'"></a>
									</div> 							
									<div style="clear:both"></div> 											  
								</div>';
		if($insert_id)
			echo json_encode(array('success'=>true,
								   'comment'=>$data['comment'],
								   'id' => $insert_id,
								   'xmpp_user' => $photo['xmpp_user'],
								   'photo_url' => config_item('s3_bucket_url').$photo['uid'].'/photos/'.$photo['filename'],
								   'notification_string' => $notification_string));
		else
			echo json_encode(array('success'=>false));
		
			
	}
	function deleteComment($id){
		$this->load->model('Photocomment_model');
		$this->Photocomment_model->delete($id);
		
	}
	function generateOTSessionAndToken(){
		error_reporting(0);
		$sessionId = $this->input->post('sessionId');
		require_once '/var/www/OT-SDK/OpenTokSDK.php';
		require_once '/var/www/OT-SDK/OpenTokArchive.php';
		require_once '/var/www/OT-SDK/OpenTokSession.php';
		$apiObj = new OpenTokSDK();
		if(empty($sessionId)){
			$session = $apiObj->create_session($_SERVER["REMOTE_ADDR"], array('p2p.preference' => 'enabled'));
			$sessionId = $session->getSessionId();
		}
		$this->load->model("User_model");
		$user = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$data = array(
					'uid'=>$user[0]['uid'],
					'firstname'=>$user[0]['firstname'],
					'lastname'=>$user[0]['lastname'],
					'profile_pic'=>$user[0]['profile_pic'],
					'sessionId'=>$sessionId
				);
		$token = $apiObj->generate_token($sessionId,RoleConstants::PUBLISHER, null, json_encode($data)); 		
		echo json_encode(array('session_id'=>$sessionId,'token_id'=>$token));
	}
	function getVChatConnection(){
		$post = $this->input->post();
		$this->load->model("Vchatsessions_model");
		$sessionId = $this->Vchatsessions_model->getSessionId($post["to_uid"],$post["from_uid"]);
		$tokenId = $this->Vchatsessions_model->getTokenId($sessionId);
		$this->load->model('User_model');
		$to_user = $this->User_model->retrieve(array('uid'=>$post["to_uid"]));
		echo json_encode(array('sessionId'=>$sessionId,'tokenId'=>$tokenId,'to_jid'=>$to_user[0]['xmpp_user'].'@matchup360.com'));
	}
	function testAjaxPage(){
		$this->load->view('test');
	}
	function updateEmail(){
		$this->load->model('User_model');
		$this->load->model('Confirmation_model');
		$data = $_POST;
		$uid = $this->session->userdata('logged_uid');
		$where = array('uid' => $uid);
		$update_result = $this->User_model->update($data, $where);
		$confirmation_code = random_string('alnum', 35);
		$confirmation_data = array(
								'uid' => $uid,
								'confirmation_code' => $confirmation_code
							);
		$this->Confirmation_model->insert($confirmation_data);
		$this->load->library('email');
		$this->email->from(config_item('webmaster_email'), config_item('site_name'));
		$this->email->to($this->input->post('email', TRUE)); 
		$this->email->subject(config_item('site_name').' Account Updated ');
		$this->email->message('http://'.config_item('site_domain').'/welcome/confirm/'.$uid.'/'.$confirmation_code);	
		$this->email->send();
		echo json_encode(array('status' => $update_result,
							   'message' => $update_result == 1? 'Update complete': 'Something went wrong.',
							   'data' => array('email' => $data['email']))); 
	}
	function updateName(){
		$this->load->model('User_model');
		$data = $_POST;
		$uid = $this->session->userdata('logged_uid');
		$where = array('uid' => $uid);
		$update_result = $this->User_model->update($data, $where);
		echo json_encode(array('status' => $update_result,
							   'message' => $update_result == 1? 'Update complete': 'Something went wrong.',
							   'data' => array('fullname' => $data['firstname'] .' '. $data['lastname']))); 
	}
	function updatePassword(){
		$update_result = 0;
		$message = 'Passwords did not match.';
		if($_POST['password'] == $_POST['password_confirm']){
			if(strlen ($_POST['password']) >= 8){
				$this->load->model('User_model');
				$this->load->library('encrypt');
				$data['password'] = $this->encrypt->sha1($this->input->post('password', TRUE));
				$uid = $this->session->userdata('logged_uid');
				$where = array('uid' => $uid);
				$update_result = $this->User_model->update($data, $where);
			}
			$message = 'Password must be at least 8 characters.';
		}
		echo json_encode(array('status' => $update_result,
							   'message' => $update_result == 1? 'Update complete': $message,
							   'data' => array('password' => '**********')));  
	}
	function like_photo($photo_id){
		require_once '/var/www/FACEBOOK_SDK/facebook.php';
		$app_id = "384283535015016";
		$app_secret = "b61c0395f5caec02a38ced422da2f353";
		$facebook = new Facebook(array(
			'appId' => $app_id,
			'secret' => $app_secret,
			'cookie' => true
		));
		$this->load->model('Notification_model');
		$this->load->model('Photolikes_model');
		$this->load->model('Userphoto_model');
		$this->load->model('User_model');
		$uid = $this->session->userdata('logged_uid');
		$photo = $this->Userphoto_model->getThumbnail($photo_id);
		$user = $this->User_model->retrieve(array('uid'=>$uid));
		$user = $user[0];
		$notification_string = '';
		$notification_exist = $this->Notification_model->exist($photo['uid'], 'photo_like', $photo_id);
		if($uid != $photo['uid'] && !$notification_exist){
			$this->Notification_model->insert(array('to_uid'=>$photo['uid'],
													'from_uid'=>$uid,
													'date'=>date('Y-m-d H:i:s'), 
													'type'=>'photo_like',
													'pid' => $photo_id,
													'status' => 1));
			$notification_string = '<div class="mCard" style="background-color: #91d152" data-id="'.$user['uid'].'">							
									<div class="profile-pic" data-uid="'.$user['uid'].'">
										<img src="'.$user['profile_pic'].'"> 							
									</div> 							
									<div class="profile-name" data-uid="'.$user['uid'].'">
										<span class="full-name">'.$user['firstname'].' '.$user['lastname'].'</span>
									</div> 							
									<div class="info">
										<span>liked your <a href="../../widgets/photo/'.$photo_id.'" class="photo_notification">photo.</a></span>
										<a href="../../widgets/photo/'.$photo_id.'" class="photo_notification"><img src="https://s3.amazonaws.com/wheewhew/user/'.$photo['uid'].'/photos/'.$photo['filename'].'"></a>
									</div> 							
									<div style="clear:both"></div> 											  
								</div>';
			$to_user = $this->User_model->retrieve(array('uid'=>$photo['uid']));
			if($this->is_online($to_user[0]['xmpp_user'],false) == false){
				$token =  array(
					'access_token' => $to_user[0]['fb_access_token']
				);
				$userdata = $facebook->api('/me', 'GET', $token);

				$token_url =    "https://graph.facebook.com/oauth/access_token?" .
								"client_id=" . $app_id .
								"&client_secret=" . $app_secret .
								"&grant_type=client_credentials";
				$app_token = file_get_contents($token_url);
				$app_token = explode('=',$app_token);
				$app_token = $app_token[1];
				/*
				/{recipient_userid}/notifications?
					access_token= … & 
					href= … & 
					template=You have people waiting to play with you, play now!
				*/
				$attachment =  array(
				'access_token' => $app_token,
				'href' => 'photo/'.$photo_id
				);
				if(!empty($user['fb_userid']))
					$attachment['template'] = " @[".$user['fb_userid']."] liked your photo!";
				else
					$attachment['template'] = "Someone liked your photo!";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,'https://graph.facebook.com/'.$userdata['id'].'/notifications');
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $attachment);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output 
				$result = curl_exec($ch);
				curl_close ($ch);
			}
		}
		$this->Photolikes_model->insert(array('reference' => $photo_id,
											  'uid' => $uid,
											  'date' => date("Y-m-d"))); 
		echo json_encode(array('value' => 'Unlike',
								'photo_reference'=>$photo_id,
							   'likes_string' => $this->Photolikes_model->get_likes_string($photo_id),
							   'xmpp_user' => $photo['xmpp_user'],
							   'photo_url' => config_item('s3_bucket_url').$photo['uid'].'/photos/'.$photo['filename'],
							   'notification_string' => $notification_string));
	}
	function unlike_photo($photo_id){
		$this->load->model('Photolikes_model');
		$uid = $this->session->userdata('logged_uid');
		$where = array('reference' => $photo_id,
					  'uid' => $uid);
		$this->Photolikes_model->delete_where($where);
		echo json_encode(array('value' => 'Like',
							   'likes_string' => $this->Photolikes_model->get_likes_string($photo_id)));
	}
	function setNotificationsUnread(){
		$this->load->model('Notification_model');
		$post = $this->input->post();
		$this->Notification_model->set_notification_read($post['uid']);
		
	}
	function importFBPhotos(){
		$post = $this->input->post();
		$photos = json_decode($post['photos']);
		foreach($photos as $photo){
			$this->upload_photo($photo);
		}
		header('Content-Type: application/json');
		echo json_encode(array('status'=>'success'));
	}
	function updateExtendedAccessToken(){
		 require_once("/var/www/FACEBOOK_SDK/facebook.php");

		  $config = array();
		  $config['appId'] = '384283535015016';
		  $config['secret'] = 'b61c0395f5caec02a38ced422da2f353';
		  $config['fileUpload'] = false; // optional

		  $facebook = new Facebook($config);
		  $facebook->setExtendedAccessToken();
		  $new_access_token = $facebook->getAccessToken();
		  
		  $this->load->model('User_model');
		  $this->User_model->update(array('fb_access_token'=>$new_access_token,'fb_userid'=>$facebook->getUser()),array('uid'=>$this->session->userdata('logged_uid')));
	}
	function check_online_users(){
		$result = array();
		$users_array = json_decode($this->input->post('users',true));
		foreach($users_array as $key => $value){
			$user['id'] = $value->id;
			$user['presence'] = (exec("sudo ejabberdctl user-resources ".$value->xmpp_user." matchup360.com"))?true:false;
			$result[] = $user;
		}
		echo json_encode($result);
	}
	function is_online($xmpp_user,$ajax = true){
		$resources = exec("sudo ejabberdctl user-resources ".$xmpp_user." matchup360.com");
		if(!empty($resources)){
			if($ajax){
				echo json_encode(array('result'=>true));
			}else{
				return true;
			}
		}else{
			if($ajax){
				echo json_encode(array('result'=>false));
			}else{
				return false;
			}
		}
	}
	function deleteActivity(){
		$id = $_POST['id'];
		$this->load->model('Notification_model');
		$this->Notification_model->delete(array('id' => $id));
	}
	function emailtest(){
		$this->load->view('emails/message_email');
	}
	function revealAdmirer(){
		$this->load->model('User_model');
		$this->load->model('Notification_model');
		$points = $this->User_model->get_user_points($this->session->userdata('logged_uid'));
		$notification_info = $this->Notification_model->get_notification_info($this->input->post('notification_id'));
		if($points >= 50){
			$this->Notification_model->set_notification_revealed($this->input->post('notification_id'));
			$new_points = $this->User_model->deduct_points(250);
			$notification_string = '<div class="profile-pic" data-uid="'.$notification_info['uid'].'">
										<img src="'.$notification_info['profile_pic'].'"> 							
									</div> 							
									<div class="profile-name" data-uid="'.$notification_info['uid'].'">
										<span class="full-name">'.$notification_info['firstname'].' '.$notification_info['lastname'].'</span>
									</div> 							
									<div class="info">admired you.</div> 							
									<div style="clear:both"></div> '; 
			echo json_encode(array('status' => 1,
								   'data' => $notification_info,
								   'notification_string' => $notification_string,
								   'new_points' => $new_points));
		}else{
			echo json_encode(array('status' => 0));
		}
	}
	function addUserEncounter(){
		$this->load->model('User_model');
		$this->load->model('Userencounter_model');
		$this->load->model('Notification_model');
		$uid = $this->session->userdata('logged_uid');
		$data = array('to_uid' => $this->input->post('random_user_uid'),
					 'from_uid' => $uid,
					 'interested' => $this->input->post('encounter'),
					 'date_added' => date("Y-m-d"));
		$this->Userencounter_model->insert($data);
		$user = $this->User_model->retrieve(array('uid' => $data['to_uid']));
		$me = $this->User_model->retrieve(array('uid' => $uid ));
		$notification_string = '';
		if($data['interested'] == 1){
			$str = ($user[0]['sex'] == 'm')? 'man says he': 'woman says she';
			$notification_id = $this->Notification_model->insert(array('to_uid'=>$this->input->post('random_user_uid'),
													'from_uid'=>$uid,
													'date'=>date('Y-m-d H:i:s'), 
													'type'=>'interested',
													'revealed' => 0,
													'status' => 1));
			$notification_string = '<div class="mCard" style="background-color: #dd7499" data-id="'.$notification_id.'">														
										<div class="admirer" style="position: relative">
											<span class="full-name" style="max-width: 100%">You got an admirer!</span>
										</div> 							
										<div class="info">
											<span> A '. $str .' is interested in you.</span>
										</div> 							
										<button class="reveal">Reveal</button>
										<div style="clear:both"></div> 											  
									</div>';
			if($this->is_online($user[0]['xmpp_user'],false) == FALSE){				
				$template_data['user'] = $me[0];
				$template_data['message'] = 'A '. $str .' is interested in you';
				$template_data['picture'] = ($me[0]['sex'] == 'm')? 'https://www.matchup360.com/assets/img/blank_profile.jpg': 'https://www.matchup360.com/assets/img/blank_profile_female.jpg';
				$template_data['unsub_link'] = 'http://'.config_item('site_domain').'/unsub/'.$this->input->post('email', TRUE);
				$this->load->library('email');
				$this->email->from(config_item('webmaster_email'), config_item('site_name'));
				$this->email->to($user[0]['email']); 
				$this->email->subject('You got a secret admirer!');
				$this->email->message($this->load->view('emails/someone_is_interested_email',$template_data,true));	
				$this->email->send();
				if(!empty($user[0]['fb_access_token'])){
					require_once '/var/www/FACEBOOK_SDK/facebook.php';
					$app_id = "384283535015016";
					$app_secret = "b61c0395f5caec02a38ced422da2f353";
					$facebook = new Facebook(array(
						'appId' => $app_id,
						'secret' => $app_secret,
						'cookie' => true
					));
					
					$token =  array(
						'access_token' => $user[0]['fb_access_token']
					);
					try{
						$userdata = $facebook->api('/me', 'GET', $token);

						$token_url =    "https://graph.facebook.com/oauth/access_token?" .
										"client_id=" . $app_id .
										"&client_secret=" . $app_secret .
										"&grant_type=client_credentials";
						$app_token = file_get_contents($token_url);
						$app_token = explode('=',$app_token);
						$app_token = $app_token[1];
						/*
						/{recipient_userid}/notifications?
							access_token= … & 
							href= … & 
							template=You have people waiting to play with you, play now!
						*/
						$attachment =  array(
						'access_token' => $app_token,
						'href' => 'notification/'.$notification_id
						);
						$attachment['template'] = "You got a secret admirer!";
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL,'https://graph.facebook.com/'.$userdata['id'].'/notifications');
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $attachment);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output 
						$result = curl_exec($ch);
						curl_close ($ch);
					}catch(Exception $e){
					
					}
				}
			}
		}
		echo json_encode(array('xmpp_user' => $user[0]['xmpp_user'],
							   'notification_string' => $notification_string));
	}
	function email_invite(){
		$post = $this->input->post();
		$this->load->model('User_model');
		$this->load->model('Invitation_model');
		$me = $this->User_model->retrieve(array('uid' => $this->session->userdata('logged_uid') ));
		$me = $me[0];
		$this->load->library('email');
		$this->email->from(config_item('webmaster_email'), config_item('site_name'));
		$already_invited = 0;
		$sent = 0;
		foreach($post['contacts'] as $email){
			$invitation_check = $this->Invitation_model->retrieve(array('from_uid'=>$me['uid'],'reference'=>$email));
			if(count($invitation_check)<1){
				$inviteid = $this->Invitation_model->insert(array('from_uid'=>$me['uid'],'reference'=>$email,'type'=>'email','invite_date'=>date('Y-m-d H:i:s')));
				$this->email->to($email); 
				$this->email->subject($me['firstname'].' '.$me['lastname'].' has invited you to Matchup360');
				$template_data['link'] = 'http://'.config_item('site_domain').'/?invite='.$inviteid;
				$template_data['firstname'] = $me['firstname'];
				$template_data['lastname'] = $me['lastname'];
				$template_data['custom_message'] = $post['custom-message'];
				$template_data['unsub_link'] = 'http://'.config_item('site_domain').'/unsub/'.$this->input->post('email', TRUE);
				$this->email->message($this->load->view('emails/invitation_email',$template_data,true));	
				$this->email->send();
				$sent++;
				$this->User_model->add_points(10,$me['uid']);
			}else{
				$already_invited++;
				continue;
			}
		}
		echo json_encode(array('duplicate_invite'=>$already_invited,'sent'=>$sent));
	}
	function fb_invite_multiple(){
		$fb_userids = json_decode($this->input->post('fb_userids'));
		$this->load->model('User_model');
		$this->load->model('Invitation_model');
		$me = $this->User_model->retrieve(array('uid' => $this->session->userdata('logged_uid')));
		$me = $me[0];
		$invited = 0;
		$duplicated = 0;
		foreach($fb_userids as $fb_userid){
			$invitation_check = $this->Invitation_model->retrieve(array('from_uid'=>$me['uid'],'reference'=>$fb_userid));
			if(count($invitation_check)<1){
				$inviteid = $this->Invitation_model->insert(array('from_uid'=>$me['uid'],'reference'=>$fb_userid,'type'=>'facebook','invite_date'=>date('Y-m-d H:i:s')));
				$this->User_model->add_points(10,$me['uid']);
				$invited++;
			}else{
				$duplicated++;
			}
		}
		echo json_encode(array('invited'=>$invited,'duplicated'=>$duplicated));
	}
	function fb_invite(){
		$post = $this->input->post();
		$this->load->model('User_model');
		$this->load->model('Invitation_model');
		$me = $this->User_model->retrieve(array('uid' => $this->session->userdata('logged_uid')));
		$me = $me[0];
		$invitation_check = $this->Invitation_model->retrieve(array('from_uid'=>$me['uid'],'reference'=>$post['fb_uid']));
		if(count($invitation_check)<1){
			$inviteid = $this->Invitation_model->insert(array('from_uid'=>$me['uid'],'reference'=>$post['fb_uid'],'type'=>'facebook','invite_date'=>date('Y-m-d H:i:s')));
			$this->User_model->add_points(10,$me['uid']);
			echo json_encode(array('success'=>true));
		}else{
			echo json_encode(array('success'=>false, 'message'=>'You have already invited this person before'));
		}
	}
	function get_invited_fb_ids(){
		$post = $this->input->post();
		$this->load->model('Invitation_model');
		$invitations = $this->Invitation_model->retrieve(array('from_uid'=>$post['from_uid'],'type'=>$post['type']));
		$fb_ids = array();
		foreach($invitations as $invitation){
			$fb_ids[] = $invitation['reference'];
		}
		echo json_encode($fb_ids);
	}
	function login_with_facebook(){
		require_once("/var/www/FACEBOOK_SDK/facebook.php");
		
		$config = array();
		$config['appId'] = '384283535015016';
		$config['secret'] = 'b61c0395f5caec02a38ced422da2f353';
		$config['fileUpload'] = false; // optional

		$facebook = new Facebook($config);
		$facebook->setExtendedAccessToken();
		$fbid = $facebook->getUser();
		if($fbid){
			$logged_user = $this->User_model->retrieve(array('fb_userid'=>$fbid));
			if(is_array($logged_user)){
				$this->session->set_userdata('logged_uid',$logged_user[0]['uid']);
			}
		}
	}
}