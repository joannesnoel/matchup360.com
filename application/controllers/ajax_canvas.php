<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_canvas extends CI_Controller { 
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('User_model');
		header('Content-Type: application/json');
    }
	public function fb_user_exist(){
		$post = $this->input->post();
		$me = $this->User_model->retrieve(array('fb_userid'=>$post['fb_userid']));
		if($me){
			$this->session->set_userdata('logged_uid',$me[0]['uid']);
			echo json_encode(array('result'=>true));
		}else
			echo json_encode(array('result'=>false));
	}
	public function has_profile_photo(){
		$me = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		if($this->User_model->has_profile_photo($this->session->userdata('logged_uid')))
			echo json_encode(array('result'=>true,'xmpp_user'=>$me[0]['xmpp_user'],'xmpp_pass'=>$me[0]['xmpp_password']));
		else
			echo json_encode(array('result'=>false));
	}
	public function upload_photo($photo_url = ''){
		header('Content-Type: text/html');
		$demo_mode = false;
		$upload_dir = '/var/www/uploads/';
		$allowed_ext = array('jpg','jpeg','png','gif');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$config['max_size']	= '4096';
		$original_filename = '';

		$this->load->library('upload', $config);

		if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
			$this->exit_status('Error! Wrong HTTP method!');
		}
		
		if (!$this->upload->do_upload('Filedata') && $photo_url == ''){
			$error = array('error' => $this->upload->display_errors());
			$this->exit_status($error);
		}else{
				$data = array('upload_data' => $this->upload->data());
				$this->load->model('Userphoto_model');
				$this->load->library('s3');
				
				$random_str = random_string('alnum', 12);
				if($photo_url != ''){
					preg_match("/([^\/]+)(?=\.\w+$).([a-z]+)/i", $photo_url, $matches);
					$original_filename = $matches[1].'.'.$matches[2];
					$check = file_put_contents($upload_dir.$matches[1].'.'.$matches[2], file_get_contents($photo_url));
					$file = $upload_dir.$matches[1].'.'.$matches[2];
					$ext = $matches[2];
				}else{
					$file = $data['upload_data']['full_path'];
					$ext = substr($data['upload_data']['file_ext'],1);
				}
				//$exif_data = exif_read_data ($file);
				
				$output_file = "";
				
				switch($ext){
					case 'jpeg':
							$output_file = str_replace('.jpeg','.jpg',strtolower($file));
							rename($file, $output_file);
					break;
					case 'jpg':
							$output_file = $file;
					break;
					case 'png': 
							$output_file = str_replace('.png','.jpg',strtolower($file));
							$input = imagecreatefrompng($file);
							list($width, $height) = getimagesize($file);
							$output = imagecreatetruecolor($width, $height);
							$white = imagecolorallocate($output,  255, 255, 255);
							imagefilledrectangle($output, 0, 0, $width, $height, $white);
							imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
							imagejpeg($output, $output_file);
							unlink($file);
					break;
					case 'gif': 
							$output_file = str_replace('.gif','.jpg',strtolower($file));
							$input = imagecreatefromgif ($file);
							list($width, $height) = getimagesize($file);
							$output = imagecreatetruecolor($width, $height);
							$white = imagecolorallocate($output,  255, 255, 255);
							imagefilledrectangle($output, 0, 0, $width, $height, $white);
							imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
							imagejpeg($output, $output_file);
							unlink($file);
					break;
					default: $this->exit_status('Only '.implode(',',$allowed_ext).' files are allowed!');
				}
				$this->load->library('image_lib');
				$originalImageData = $this->image_lib->get_image_properties($output_file, TRUE); 
				if($photo_url == '' && ($originalImageData['width']<400 OR $originalImageData['height']<300)){
					$this->exit_status('400x300 or bigger photo is required.');
				}else if($photo_url != '' && ($originalImageData['width']<400 OR $originalImageData['height']<300)){
					return 0;
				}
				S3::putObject(S3::inputFile($output_file), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str.".jpg", S3::ACL_PUBLIC_READ);
								
				$pid = $this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
													 'filename'=>$random_str.".jpg",
													 'height'=>$originalImageData['height'],
													 'width'=>$originalImageData['width'],
													 'dimension'=>0,
													 'original_filename'=>$original_filename
													 ));
				
				if((int)$originalImageData['width'] >= 800){
					
					//Create 800 Copy	
					$config['image_library'] = 'gd2';
					$config['source_image']	= $output_file;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim'] = 'width';
					$config['width'] = 800;
					$config['height'] = 800;
					$config['new_image'] = $random_str.'_800.jpg';

					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					
					$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_800.jpg', TRUE); 
					$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
														 'reference'=>$pid,
														 'filename'=>$random_str."_800.jpg",
														 'height'=>$fileData['height'],
														 'width'=>$fileData['width'],
														 'dimension'=>1
														 ));
					$config = array();
					$this->image_lib->clear();
					S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_800.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_800.jpg", S3::ACL_PUBLIC_READ);
				}
				if((int)$originalImageData['width'] >= 600){
					//Create 600 Copy
					$config['image_library'] = 'gd2';
					$config['source_image']	= $output_file;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim'] = 'width';
					$config['width'] = 600;
					$config['height'] = 600;
					$config['new_image'] = $random_str.'_600.jpg';
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					
					$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_600.jpg', TRUE); 
					$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
														 'reference'=>$pid,
														 'filename'=>$random_str."_600.jpg",
														 'height'=>$fileData['height'],
														 'width'=>$fileData['width'],
														 'dimension'=>2
														 ));
					
					$config = array();
					$this->image_lib->clear();
					S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_600.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_600.jpg", S3::ACL_PUBLIC_READ);
					
				}
				if((int)$originalImageData['width'] >= 400){
					//Create 400 Copy
					$config['image_library'] = 'gd2';
					$config['source_image']	= $output_file;
					$config['maintain_ratio'] = TRUE;
					$config['master_dim'] = 'width';
					$config['width'] = 400;
					$config['height'] = 400;
					$config['new_image'] = $random_str.'_400.jpg';
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					
					$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_400.jpg', TRUE); 
					$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
														 'reference'=>$pid,
														 'filename'=>$random_str."_400.jpg",
														 'height'=>$fileData['height'],
														 'width'=>$fileData['width'],
														 'dimension'=>3
														 ));
					
					$config = array();
					$this->image_lib->clear();
					S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_400.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_400.jpg", S3::ACL_PUBLIC_READ);
					
				}
				
				//Create square
				
				$config['image_library'] = 'gd2';
				$config['source_image']	= '/var/www/uploads/'.$random_str.'_400.jpg';
				$config['new_image'] = $random_str.'_thumb.jpg';
				$config['maintain_ratio'] = FALSE;
				
				$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_400.jpg', TRUE); 
				$excess = (int)$fileData['height'] - 300;
				$config['y_axis'] = floor($excess / 2) - floor($excess * 0.33);
				$config['x_axis'] = 50;
				$config['width'] = 300;
				$config['height'] = 300;
				
				//Load image library and crop
				$this->image_lib->initialize($config);
				$this->image_lib->crop();
				
				S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_thumb.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_thumb.jpg", S3::ACL_PUBLIC_READ);
				
				$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_thumb.jpg', TRUE); 
				$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
													 'reference'=>$pid,
													 'filename'=>$random_str."_thumb.jpg",
													 'height'=>$fileData['height'],
													 'width'=>$fileData['width'],
													 'dimension'=>4
													 ));
				if(file_exists('/var/www/uploads/'.$random_str.'_800.jpg'))									 
					unlink('/var/www/uploads/'.$random_str.'_800.jpg');
				if(file_exists('/var/www/uploads/'.$random_str.'_600.jpg'))	
					unlink('/var/www/uploads/'.$random_str.'_600.jpg');
				if(file_exists('/var/www/uploads/'.$random_str.'_400.jpg'))	
					unlink('/var/www/uploads/'.$random_str.'_400.jpg');
				if(file_exists('/var/www/uploads/'.$random_str.'_thumb.jpg'))	
					unlink('/var/www/uploads/'.$random_str.'_thumb.jpg');
				
				unlink($output_file);
				if($photo_url == '')
					$this->exit_status('Upload success!');
		}
		if($photo_url == '')
			$this->exit_status('Something went wrong with your upload!');
		// Helper functions
	}
	function recent_invitation(){
		$this->load->model('Invitation_model');
		$invitations_from_me = $this->Invitation_model->retrieve(array('from_uid'=>$this->session->userdata('logged_uid')));
		echo json_encode(array('invites'=>$invitations_from_me,'uid'=>$this->session->userdata('logged_uid')));
	}
	function signup(){
		header('Content-type: application/json');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[cpassword]');
		$this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		if ($this->form_validation->run() == FALSE){
			echo json_encode(array('success'=>false,'errors'=>validation_errors()));
		}else{
			set_time_limit(0);
			$this->load->library(array('session','encrypt'));
			$me = json_decode(urldecode($this->input->post('me')));
			$check_user = $this->User_model->retrieve("(fb_userid = '".$me->id."' OR email = '".$this->input->post('email')."')");
			if($check_user){
				$this->session->set_userdata('logged_uid',$check_user[0]['uid']);
				echo json_encode(array('success'=>true,'redirect'=>'https://www.matchup360.com/canvas/chat'));
				exit;
			}
			$user = array(
				'fb_userid'	=>$me->id,
				'firstname'	=>$me->first_name,
				'lastname' 	=>$me->last_name,
				'email'		=>$this->input->post('email')
			);
			if($this->session->userdata('request_ids')){
				$token_url =    "https://graph.facebook.com/oauth/access_token?" .
				"client_id=" . '384283535015016' .
				"&client_secret=" . 'b61c0395f5caec02a38ced422da2f353' .
				"&grant_type=client_credentials";
				$app_token = file_get_contents($token_url);
				$requests = explode(',',urldecode($this->session->userdata('request_ids')));  
				foreach($requests as $request_id) {
					$request_content = json_decode(file_get_contents("https://graph.facebook.com/$request_id?$app_token"), TRUE);
					if(is_array($request_content['data'])){
						extract(json_decode($request_content['data'],true));
						if(!empty($fbinvite)){
							$this->User_model->add_points(40,$fbinvite);
							$user['referrer_uid'] = $fbinvite;
						}
						$deleted = file_get_contents("https://graph.facebook.com/".$request_id."_".$me->fb_userid."?".$app_token."&method=delete"); // Should return true on success
					}
				}
			}
			if(!empty($me->gender))
				$user['sex']	= ($me->gender=='male')?'m':'f';
			$birthdate = explode('/',$me->birthday);
			$user['birthdate'] = $birthdate[2].'-'.$birthdate[0].'-'.$birthdate[1];
			$user['password'] = $this->encrypt->sha1($this->input->post('password'), TRUE);
			$user['joined_date'] = date('Y-m-d H:i:s');
			$user['xmpp_user'] = $this->User_model->generate_xmpp_user($me->first_name, $me->last_name);
			$user['username'] = $user['xmpp_user'];
			$user['xmpp_password']  = $this->input->post('password');
			
			$node = config_item('site_domain');
			exec('sudo /usr/sbin/ejabberdctl register '.$user['xmpp_user'].' '.$node.' '.$user['xmpp_password'].' 2>&1',$output,$status);
			if(!empty($me->current_longitude) && !empty($me->current_lattitude)){
				$user['current_longitude']  = $me->current_longitude;
				$user['current_lattitude']  = $me->current_lattitude;
			}
			$this->load->model('Userlocation_model');		
			$this->load->model('Userlanguage_model');
			$this->load->model('Userlike_model');
			$this->load->model('List_model');
			
			$location_id = 0;
			if(!empty($me->address_components)){
				for($i = count($me->address_components)-1; $i>=0; $i--){
					$location = $me->address_components[$i];
					$location_check = $this->Userlocation_model->retrieve(array('name'=>$location->long_name,'type'=>$location->types[0]));
					if($location_check == false){
						$location_types = array('locality','administrative_area_level_1','administrative_area_level_2','country');
						if($location->types[0]){
							$data = array(
										'location' => 'hometown',
										'type' => $location->types[0],
										'name' => $location->long_name,
										'parent' => $location_id
									);								
							$location_id = $this->Userlocation_model->insert($data);
						}
					}else{
						$location_id = $location_check[0]->locid;
					}
					switch($location->types[0]){
						case 'administrative_area_level_1' : 
							$user['state'] = $location_id;
						break;
						case 'administrative_area_level_2' : 
							$user['county'] = $location_id;
						break;
						case 'locality' : 
							$user['city'] = $location_id;
						break;
						case 'country' : 
							$user['country'] = $location_id;
						break;
					}
				}
			}
			if(!empty($me->work)){
				$ctr = 0;
				foreach($me->work as $work){
					if(empty($work->position->name))
						continue;
					$wid = $this->List_model->retrieve(array('name'=>$work->position->name,'group'=>'work'));
					if(count($wid)<1)
						$wid = $this->List_model->insert(array('name'=>$work->position->name,'group'=>'work'));
					else
						$wid = $wid[0]['id'];
					if($ctr == 0)
						$user['work'] = $wid;
					$ctr++;
				}
			}
			$uid = $this->User_model->insert($user);
			$this->session->set_userdata('logged_uid',$uid);
			if(!empty($me->picture)){
				$upload_dir = 'uploads/';
				$this->load->library('s3');
				$random_str = random_string('alnum', 12);
				preg_match("/([^\/]+)(?=\.\w+$).([a-z]+)/i", $me->picture, $matches);
				file_put_contents($upload_dir.$matches[1].'.'.$matches[2], file_get_contents($me->picture));
				$file = $upload_dir.$matches[1].'.'.$matches[2];
				$ext = $matches[2];
				S3::putObject(S3::inputFile($file), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str.".jpg", S3::ACL_PUBLIC_READ);
				$this->User_model->update(array('profile_pic'=>$random_str.".jpg"),array('uid'=>$uid));
				unlink($file);
			}
			if(!empty($me->interested_in)){
				$this->load->model('Userinterest_model');
				$interested_in['uid'] = $uid;
				if(in_array('male',$me->interested_in))
					$interested_in['men'] = 1;
				if(in_array('female',$me->interested_in))
					$interested_in['women'] = 1;
				$this->Userinterest_model->insert($interested_in);
			}
			if(!empty($me->languages)){
				foreach($me->languages as $language){
					$lid = $this->List_model->retrieve(array('name'=>$language->name,'group'=>'languages'));
					if(count($lid)<1)
						$lid = $this->List_model->insert(array('name'=>$language->name,'group'=>'languages'));
					else
						$lid = $lid[0]['id'];
					$this->Userlanguage_model->insert(array('uid'=>$uid ,'lid'=>$lid));				
				}
			}
			if(!empty($me->likes)){
				foreach($me->likes as $like){
					$lid = $this->List_model->retrieve(array('name'=>$like->name,'group'=>$like->category));
					if(count($lid)<1)
						$lid = $this->List_model->insert(array('name'=>$like->name,'group'=>$like->category));
					else
						$lid = $lid[0]['id'];
					$this->Userlike_model->insert(array('uid'=>$uid ,'lid'=>$lid));				
				}
			}
			if(!empty($me->photos)){
				foreach($me->photos as $photo)
					$this->upload_photo($photo);
			}
			if(!empty($me->bio)){
				$profile['heading'] = $me->bio;
			}
			if(!empty($me->quotes)){
				$profile['intro'] = $me->quotes;
			}
			if(isset($profile)){
				$this->load->model('Userprofile_model');
				$profile['uid'] = $uid;
				$this->Userprofile_model->insert($profile);
			}
			header('Content-type: application/json');
			echo json_encode(array('success'=>true,'redirect'=>'https://www.matchup360.com/canvas/chat'));
		}
	}
	function has_profile_details(){
		$this->load->model('Userappearance_model');
		$me = $this->Userappearance_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		if(!$me)
			echo json_encode(array('result'=>false));
		else
			echo json_encode(array('result'=>true));
	}
	function roulette(){
		$this->load->model('User_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Userinterest_model');
		$user = $this->User_model->retrieve(array('uid' => $this->session->userdata('logged_uid'))); 
		$data['user'] = $this->User_model->random();
		
		//print_r($data['user']);
		$user_photos = $this->Userphoto_model->retrieve_largest(array('uid'=>$data['user']['uid'])); 
		$data['user_photos'] = $user_photos;
		echo json_encode($data); 
	}	
	function update_my_appearance(){
		$this->load->model('Userappearance_model');
		$data = $_POST;
		$where = array('uid' => $this->session->userdata('logged_uid'));
		$appearance_check = $this->Userappearance_model->retrieve($where);
		if(count($appearance_check)>0){
			$this->Userappearance_model->update($data, $where);
		}else{
			$data['uid'] = $this->session->userdata('logged_uid');
			$this->Userappearance_model->insert($data);
		}
		echo json_encode(array('success'=>true));
	}
	function invite_to_room(){
		$roomName = $this->input->post('room');
		$ids = $this->input->post('ids');
		$ids = explode(',',$ids);
		require_once '/var/www/FACEBOOK_SDK/facebook.php';
		$app_id = "384283535015016";
		$app_secret = "b61c0395f5caec02a38ced422da2f353";
		$facebook = new Facebook(array(
			'appId' => $app_id,
			'secret' => $app_secret,
			'cookie' => true
		));
		$uid = $this->session->userdata('logged_uid');
		$user = $this->User_model->retrieve(array('uid'=>$uid));
		$user = $user[0];
		
		$token =  array(
			'access_token' => $this->input->post('access_token')
		);
		$userdata = $facebook->api('/me', 'GET', $token);

		$token_url =    "https://graph.facebook.com/oauth/access_token?" .
						"client_id=" . $app_id .
						"&client_secret=" . $app_secret .
						"&grant_type=client_credentials";
		$app_token = file_get_contents($token_url);
		$app_token = explode('=',$app_token);
		$app_token = $app_token[1];
		$attachment =  array(
		'access_token' => $this->input->post('access_token'),
		'message' => $user['firstname'].' '.$user['lastname'].' invited you to join the #'.$roomName.' chatroom!',
		'name' => 'Join the #'.$roomName.' chatroom!',
		'link' => 'https://apps.facebook.com/matchupthreesixty/?room='.$roomName,
		'description' => 'Matchup360 is a brand new online community in facebook that makes meeting new people easy and fun!',
		'picture'=>'https://www.matchup360.com/assets/img/matchup360-130.jpg'
		);
		foreach($ids as $id){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,'https://graph.facebook.com/'.$id.'/feed');
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $attachment);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output 
			$result = curl_exec($ch);
			curl_close ($ch);
		}
		echo $result;
	}
	function open_room($roomJid){
		$roomJid = urldecode($roomJid);
		$this->load->model('Userchatroom_model');
		$check_room = $this->Userchatroom_model->retrieve(array('uid'=>$this->session->userdata('logged_uid'),'roomJid'=>$roomJid));
		if($check_room == false)
			$this->Userchatroom_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'roomJid'=>$roomJid,'date_opened'=>date('Y-m-d H:i:s')));
		else
			$this->Userchatroom_model->update(array('date_opened'=>date('Y-m-d H:i:s')),array('uid'=>$this->session->userdata('logged_uid'),'roomJid'=>$roomJid));
	}
	function close_room($roomJid){
		$roomJid = urldecode($roomJid);
		$this->load->model('Userchatroom_model');
		$check_room = $this->Userchatroom_model->retrieve(array('uid'=>$this->session->userdata('logged_uid'),'roomJid'=>$roomJid));
		if($check_room == false)
			$this->Userchatroom_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'roomJid'=>$roomJid,'date_closed'=>date('Y-m-d H:i:s')));
		else
			$this->Userchatroom_model->update(array('date_closed'=>date('Y-m-d H:i:s')),array('uid'=>$this->session->userdata('logged_uid'),'roomJid'=>$roomJid));
	}
}