<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facebook extends CI_Controller {
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Userpost_model');
		$this->load->library('session');
		$this->load->helper('url');
		$my_uid = $this->session->userdata('logged_uid');
    }
	function index(){
		$my_uid = $this->session->userdata('logged_uid');
		$me = $this->User_model->retrieve(array('uid'=>$my_uid));
		if($my_uid == '' OR is_array($me) == false){
			$this->signup();
		}else{
			$this->feeds();
		}
	}
	function feeds(){
		$this->load->model('Userpost_model');
		$my_uid = $this->session->userdata('logged_uid');
		$me = $this->User_model->retrieve(array('uid'=>$my_uid));
		$me = $me[0];
		$data['me'] = $me;
		if(empty($me['current_longitude']) OR empty($me['current_lattitude']))
			$data['posts'] = array();
		else
			$data['posts'] = $this->Userpost_model->getLast50();
		$page_data['page_content'] = $this->load->view('pages/shouts',$data,true);
		$page_data['me'] = $me;
		$this->load->view('facebook/main',$page_data);
	}
	function members(){
		$data['members'] = $this->User_model->member_search(null,50);
		$my_uid = $this->session->userdata('logged_uid');
		$me = $this->User_model->retrieve(array('uid'=>$my_uid));
		$me = $me[0];
		$page_data['me'] = $me;
		$page_data['page_content'] = $this->load->view('pages/members',$data, true);
		$this->load->view('facebook/main',$page_data);
	}
	function member_profile($username){
		$this->load->model('Userinterest_model');
		$my_uid = $this->session->userdata('logged_uid');
		$user_data= $this->User_model->retrieve(array('xmpp_user'=>$username));
		$data['user_data'] = $user_data[0];
		$data['user_photo'] = $user_data[0]['profile_pic'];
		$data['logged_user_uid'] = $this->session->userdata('logged_uid'); 
		$interested_in = $this->Userinterest_model->retrieve(array('uid'=>$user_data[0]['uid']));
		$data['user_data']['interested_in'] = (is_array($interested_in) && count($interested_in) > 0)? $interested_in[0] : array();
		$page_data['page_content'] = $this->load->view('pages/member_profile',$data,true);	
		$me = $this->User_model->retrieve(array('uid'=>$my_uid));
		$me = $me[0];
		$page_data['me'] = $me;
		$this->load->view('facebook/main',$page_data);
	}
	function signup(){
		$this->load->model('Userphoto_model');
		$data['photos'] = $this->Userphoto_model->retrieve();
		$this->load->view('facebook/signup',$data);
	}
}