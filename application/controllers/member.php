<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	var $user_data = '';
	var $additional_info = '';
	var $user_photos = '';
	var $profile_pic = '';
	var $page = '';
	var $page_data = '';
	var $page_rendered = 0;

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();	
		$this->load->library(array('form_validation','session'));
		$this->load->model('User_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Userposts_model');
		
    }
	
	private function renderPage(){
		if($this->page_rendered == 0){
			$this->load->view($this->page, $this->page_data);
			$this->page_rendered = 1;
		}else
			$this->page_rendered = 1;
	}

	public function profile($id)
	{
			//echo '---- '.$this->session->userdata('logged_uid');
			//die();
			$user_photo = $this->Userphoto_model->profile_photo($id);
			$user_data= $this->User_model->retrieve($id);
			
			$this->page = 'profile';
			$this->page_data['user_data'] = $user_data;
			$this->page_data['user_photo'] = $user_photo;
			$this->page_data['logged_uid'] = $this->session->userdata('logged_uid');
			$this->page_data['buddy_posts'] = $this->Userposts_model->getProfilePost($id);
			//print_r($this->page_data['buddy_posts']);
			//die();
			$this->form_validation->set_rules('update-status-box', 'Status Box', 'required');
		
			if ($this->form_validation->run() == FALSE)
			{
				$this->page = 'profile';
				$this->renderPage();
			}
			
			else {
			
				$user_post = array(
									'post' => $this->input->post('update-status-box'),
									'poster_uid' => $this->session->userdata('logged_uid'),//$this->input->post('poster_uid'),
									'post_to_uid' => $id
								);
				$this->Userposts_model->insert($user_post);
			}
			
			$this->page = 'profile';
				
			$this->renderPage();		
	}
	/*function testing($uid){
		$this->load->model('Userposts_model');
		$result = $this->Userposts_model->getAllPosts($uid);
		print_r($result);
	}*/
	public function member_profile()
	{
		$this->load->view('pages/member_profile');
		//$this->renderPage();
	}
}