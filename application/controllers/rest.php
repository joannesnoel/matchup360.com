<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rest extends CI_Controller {
	var $uid = '';
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		error_reporting(1);
		$this->load->model('User_model');
		$this->load->library('session');
		$this->load->helper('url');
		$this->uid = $this->session->userdata('logged_uid');
		if($this->uid<1){
			die('You are not logged in.');
		}
    }
	function update_user_data(){
		$data = $this->input->post();
		$success = $this->User_model->update($data,array('uid'=>$this->uid));
		
		if($success)
			echo json_encode(array('status'=>'success'));
		else
			echo json_encode(array('status'=>$this->db->_error_message()));
	}
	function get_user_data(){
		$me = $this->User_model->retrieve(array('uid'=>$this->uid));
		echo json_encode($me[0]);
	}
	function new_message_dialog($uid){
		$user = $this->User_model->retrieve(array('uid'=>$uid));
		$user = $user[0];
		echo $this->load->view('widgets/new_message_dialog',$user);
		//echo json_encode($result);
	}
	function post(){
		$data = $this->input->post();
		if(empty($data['message']))
			die(json_encode(array('status'=>'failed')));
		$data['uid'] = $this->uid;
		$me = $this->User_model->retrieve(array('uid'=>$this->uid));
		$me = $me[0];
		$data['lng'] = $me['current_longitude'];
		$data['lat'] = $me['current_lattitude'];
		$data['location'] = $me['current_location_text'];
		$this->load->model('Userpost_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Tag_model');
		$this->load->model('Posttag_model');
		$attached_photos = json_decode($data['photos']);
		unset($data['photos']);
		$inserted = $this->Userpost_model->insert($data);
		if($inserted){
			preg_match_all('/#([a-z0-9]+)/i',$data['message'],$matches);
			foreach($matches[1] as $tag){
				$tag_id = $this->Tag_model->retrieve(array('name'=>$tag));
				if(!$tag_id)
					$tag_id = $this->Tag_model->insert(array('name'=>$tag));
				$this->Posttag_model->insert(array('post_id'=>$inserted,'tag_id'=>$tag_id));
			}
			foreach($attached_photos as $photo_url){
				$this->upload_photo($photo_url,$inserted);
			}
			$post = $this->Userpost_model->getPost($inserted);
			echo json_encode(array('status'=>'success','the_post'=>$post));
			$receipients = $this->User_model->retrieve_receipients();
			require_once("/var/www/XMPPHP/XMPP.php");
			$conn = new XMPPHP_XMPP(config_item('site_domain'), 5222, 'admin', 'sempron123', 'default', config_item('site_domain'), $printlog=False, $loglevel=LOGGING_INFO);
			$conn->connect();
			$conn->processUntil('session_start');
			if(empty($data['parent_id'])){
				$stanza['type'] = 'newpost';
				$stanza['post'] = $this->load->view('widgets/stream_entry',$post,true);
			}else{
				$stanza['type'] = 'newcomment';
				$stanza['parent_id'] = $data['parent_id'];
				$stanza['post'] = $this->load->view('widgets/comment_entry',$post,true);
			}
			foreach($receipients as $receipient){
				$conn->message($receipient['xmpp_user'].'@matchup360.com', json_encode($stanza), 'headline');
			}
			$conn->disconnect();
		}else
			echo json_encode(array('status'=>'failed'));
	}
	function save_user_photos(){
		$uploaded_photos = json_decode($this->input->post('photos'));
		foreach($uploaded_photos as $photo)
			$this->upload_photo($photo);
		echo json_encode(array('status'=>'success'));
	}
	function delete_post(){
		$this->load->model('Userpost_model');
		$deleted = $this->Userpost_model->delete($this->input->post('post_id'));
		if($deleted)
			echo json_encode(array('status'=>'success'));
		else
			echo json_encode(array('status'=>'failed'));
	}
	function delete_photo(){
		$this->load->model('Userphoto_model');
		$photo_id = $this->input->post('photo_id');
		$photos = $this->Userphoto_model->retrieve_all_dimensions(array('reference' => $photo_id));
		if(empty($photos) || !is_array($photos)){
			echo json_encode(array('status'=>'failed', 'reason'=>'Photo does not exist!'));
		}else if($photos[0]['uid'] != $this->session->userdata('logged_uid')){
			echo json_encode(array('status'=>'failed', 'reason'=>'You do not have the permission to delete this photo!'));
		}else{
			$photo = $this->Userphoto_model->retrieve(array('pid' => $photo_id), 0);
			$photos[] = $photo[0];
			$this->load->library('s3');
			foreach($photos as $photo){
				S3::deleteObject("matchup360", "user/".$photo['uid']."/photos/".$photo['filename']);
				$this->Userphoto_model->delete($photo['pid']);
			}
			echo json_encode(array('status'=>'success'));
		}
	}
	function get_message_threads(){
		$this->load->model('Usermessages_model');
		$threads = $this->Usermessages_model->getMessageThreads($this->uid);
		if(count($threads)>0){
			$html = '';
			foreach($threads as $thread){
				$html .= $this->load->view('widgets/message_thread',$thread,true);
			}
			echo $html;
		}
	}
	function get_conversation($uid){
		$this->load->model('Usermessages_model');
		$messages = $this->Usermessages_model->getThread($this->uid,$uid);
		$html = '';
		$last_msg_from = 0;
		$msg_buffer = '';
		for($i=0; $i<=count($messages); $i++){
			$message = $messages[$i];
			if($last_msg_from == 0){
				$msg_buffer .= '<p>'.$message['message'] . '</p>';
			}else if($last_msg_from != $message['from_uid']){
				$bubble['message'] = $msg_buffer;
				$bubble['from_uid'] = $last_msg_from;
				$bubble['profile_pic'] = $message['profile_pic'];
				$bubble['sent_date'] = $message['sent_date'];
				$bubble['fullname'] = $message['fullname'];
				$bubble['sex'] = $message['sex'];
				$html .= $this->load->view('widgets/message_bubble',$bubble,true);
				$msg_buffer = '<p>'.$message['message'] . '</p>';
			}else{
				$msg_buffer .= '<p>'.$message['message'] . '</p>';
			}
			$last_msg_from = $message['from_uid'];
		}
		echo $html;//print_r($conversation);
	}
	function send_message(){
		$msg = $this->input->post();
		$msg['from_uid'] = $this->session->userdata('logged_uid');
		$msg['sent_date'] = date('Y-m-d H:i:s');
		$this->load->model('Usermessages_model');
		$insert_id = $this->Usermessages_model->insert($msg);
		if($insert_id){
			$to_user = $this->User_model->retrieve(array('uid'=>$msg['to_uid']));
			$from_user = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
			echo json_encode(array('status'=>'success','from_uid'=>$this->session->userdata('logged_uid'),'to_jid'=>$to_user[0]['xmpp_user'].'@matchup360.com','from_photo'=>$from_user[0]['profile_pic'],'from_name'=>$from_user[0]['firstname'] . ' ' . $from_user[0]['lastname']));
		}else
			echo json_encode(array('status'=>'failed'));
	}
	private function upload_photo($photo_url,$post_id = null){
		$allowed_ext = array('jpg','jpeg','png','gif');
		$original_filename = '';
		$this->load->model('Userphoto_model');
		$this->load->library('s3');
		$this->load->helper('string');
		$random_str = random_string('alnum', 12);
		preg_match("/([^\/]+)(?=\.\w+$).([a-z]+)/i", $photo_url, $matches);
		$original_filename = $matches[1].'.'.$matches[2];
		$file = '/var/www/uploads/'.$original_filename;
		$ext = $matches[2];	
		
		$output_file = "";
		
		switch(strtolower($ext)){
			case 'jpeg':
					$output_file = str_replace('.jpeg','.jpg',strtolower($file));
					rename($file, $output_file);
			break;
			case 'jpg':
					$output_file = $file;
			break;
			case 'png': 
					$output_file = str_replace('.png','.jpg',strtolower($file));
					$input = imagecreatefrompng($file);
					list($width, $height) = getimagesize($file);
					$output = imagecreatetruecolor($width, $height);
					$white = imagecolorallocate($output,  255, 255, 255);
					imagefilledrectangle($output, 0, 0, $width, $height, $white);
					imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
					imagejpeg($output, $output_file);
					unlink($file);
			break;
			case 'gif': 
					$output_file = str_replace('.gif','.jpg',strtolower($file));
					$input = imagecreatefromgif ($file);
					list($width, $height) = getimagesize($file);
					$output = imagecreatetruecolor($width, $height);
					$white = imagecolorallocate($output,  255, 255, 255);
					imagefilledrectangle($output, 0, 0, $width, $height, $white);
					imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
					imagejpeg($output, $output_file);
					unlink($file);
			break;
		}
		$this->load->library('image_lib');
		$originalImageData = $this->image_lib->get_image_properties($output_file, TRUE); 

		S3::putObject(S3::inputFile($output_file), "matchup360", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str.".jpg", S3::ACL_PUBLIC_READ);			
		
		$pid = $this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
											 'filename'=>$random_str.".jpg",
											 'height'=>$originalImageData['height'],
											 'width'=>$originalImageData['width'],
											 'dimension'=>0,
											 'original_filename'=>$original_filename,
											 'post_id'=>$post_id
											 ));
		if((int)$originalImageData['width'] >= 800){
			
			//Create 800 Copy	
			$config['image_library'] = 'gd2';
			$config['source_image']	= $output_file;
			$config['maintain_ratio'] = TRUE;
			$config['master_dim'] = 'width';
			$config['width'] = 800;
			$config['height'] = 800;
			$config['new_image'] = $random_str.'_800.jpg';

			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_800.jpg', TRUE); 
			$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
												 'reference'=>$pid,
												 'filename'=>$random_str."_800.jpg",
												 'height'=>$fileData['height'],
												 'width'=>$fileData['width'],
												 'dimension'=>1,
												 'post_id'=>$post_id
												 ));
			$config = array();
			$this->image_lib->clear();
			S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_800.jpg'), "matchup360", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_800.jpg", S3::ACL_PUBLIC_READ);
		}
		if((int)$originalImageData['width'] >= 600){
			//Create 600 Copy
			$config['image_library'] = 'gd2';
			$config['source_image']	= $output_file;
			$config['maintain_ratio'] = TRUE;
			$config['master_dim'] = 'width';
			$config['width'] = 600;
			$config['height'] = 600;
			$config['new_image'] = $random_str.'_600.jpg';
			
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_600.jpg', TRUE); 
			$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
												 'reference'=>$pid,
												 'filename'=>$random_str."_600.jpg",
												 'height'=>$fileData['height'],
												 'width'=>$fileData['width'],
												 'dimension'=>2,
												 'post_id'=>$post_id
												 ));
			
			$config = array();
			$this->image_lib->clear();
			S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_600.jpg'), "matchup360", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_600.jpg", S3::ACL_PUBLIC_READ);
			
		}
		if((int)$originalImageData['width'] >= 400){
			//Create 400 Copy
			$config['image_library'] = 'gd2';
			$config['source_image']	= $output_file;
			$config['maintain_ratio'] = TRUE;
			$config['master_dim'] = 'width';
			$config['width'] = 400;
			$config['height'] = 400;
			$config['new_image'] = $random_str.'_400.jpg';
			
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_400.jpg', TRUE); 
			$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
												 'reference'=>$pid,
												 'filename'=>$random_str."_400.jpg",
												 'height'=>$fileData['height'],
												 'width'=>$fileData['width'],
												 'dimension'=>3,
												 'post_id'=>$post_id
												 ));
			
			$config = array();
			$this->image_lib->clear();
			S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_400.jpg'), "matchup360", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_400.jpg", S3::ACL_PUBLIC_READ);
			
		}
		
		//Create square
		
		$config['image_library'] = 'gd2';
		$config['source_image']	= '/var/www/uploads/'.$random_str.'_400.jpg';
		$config['new_image'] = $random_str.'_thumb.jpg';
		$config['maintain_ratio'] = FALSE;
		
		$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_400.jpg', TRUE); 
		$excess = (int)$fileData['height'] - 300;
		$config['y_axis'] = floor($excess / 2) - floor($excess * 0.33);
		$config['x_axis'] = 50;
		$config['width'] = 300;
		$config['height'] = 300;
		
		//Load image library and crop
		$this->image_lib->initialize($config);
		$this->image_lib->crop();
		
		S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_thumb.jpg'), "matchup360", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_thumb.jpg", S3::ACL_PUBLIC_READ);
		
		$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_thumb.jpg', TRUE); 
		$this->Userphoto_model->insert(array('uid'=>$this->session->userdata('logged_uid'),
											 'reference'=>$pid,
											 'filename'=>$random_str."_thumb.jpg",
											 'height'=>$fileData['height'],
											 'width'=>$fileData['width'],
											 'dimension'=>4,
											 'post_id'=>$post_id
											 ));
		if(file_exists('/var/www/uploads/'.$random_str.'_800.jpg'))									 
			unlink('/var/www/uploads/'.$random_str.'_800.jpg');
		if(file_exists('/var/www/uploads/'.$random_str.'_600.jpg'))	
			unlink('/var/www/uploads/'.$random_str.'_600.jpg');
		if(file_exists('/var/www/uploads/'.$random_str.'_400.jpg'))	
			unlink('/var/www/uploads/'.$random_str.'_400.jpg');
		if(file_exists('/var/www/uploads/'.$random_str.'_thumb.jpg'))	
			unlink('/var/www/uploads/'.$random_str.'_thumb.jpg');
		
		unlink($output_file);
	}
}