<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widgets extends CI_Controller {

	var $user_data = '';
	var $additional_info = '';
	var $user_photos = '';
	var $profile_pic = '';
	var $page = '';
	var $page_data = '';
	var $page_rendered = 0;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Userprofile_model');
		$this->load->model('Userappearance_model');
		$this->load->model('Usersituation_model');
		$this->load->model('Usereduemployment_model');
		$this->load->model('Userleisure_model');
		$this->load->model('Userpersonality_model');
		$this->load->model('Userviews_model');
		$this->load->model('Userlooking_model');
		$this->load->model('List_model');
		$this->load->model('Usermessages_model');
		$this->load->model('Photocomment_model');
		$this->load->model('Photolikes_model');
		$this->load->library('session');
		$this->load->helper('url');
		$uid = $this->session->userdata('logged_uid');
    }
	
	function photo($pid){
		$sizes = 1;
		while(($photo = $this->Userphoto_model->retrieve(array('reference' => $pid),$sizes)) == false){
			$sizes++;
		}
		$uid = $this->session->userdata('logged_uid');
		$where = array('uid'=>$uid);
		$data['user'] = $this->User_model->retrieve($where);
		$data['comments'] = $this->Photocomment_model->retrieve($pid);
		$data['photo'] = $photo[0];
		$data['is_liked'] = $this->Photolikes_model->is_liked(array('reference'=> $pid,
																	'uid' => $uid));
		
		$data['photo_likes'] = $this->Photolikes_model->get_likes($pid); 
		$this->load->view('widgets/photo',$data);
	}
	function mini_profile($xmpp_user){
		$user = $this->User_model->retrieve(array('xmpp_user'=>$xmpp_user));
		$data = $user[0];
		$this->load->view('widgets/mini_profile',$data);
	}
	function hover_profile($xmpp_user){
		$this->load->model('Userinterest_model');
		$user = $this->User_model->retrieve(array('xmpp_user'=>$xmpp_user));
		$data = $user[0];
		$interested_in = $this->Userinterest_model->retrieve(array('uid'=>$data['uid']));
		$data['interested_in'] = $interested_in[0];
		$this->load->view('widgets/mini_profile',$data);
	}
	function set_profile_photo($pid){
		$this->load->helper('form');
		$sizes = 1;
		while(($photo = $this->Userphoto_model->retrieve(array('reference' => $pid),$sizes)) == false){
			$sizes++;
		}
		$data['photo'][0] = $photo[0];
		$photo = $this->Userphoto_model->retrieve(array('pid' => $pid), 0);
		$data['photo'][1] = $photo[0];
		$data['user'] =  $this->session->userdata('logged_uid');
		//echo $pid;
		//print_r($photo);
		$this->load->view('widgets/set_profile_photo',$data);
	}
	function get_credits(){
		$this->load->view('widgets/get_credits');
	}
	function invite_facebook(){
		$this->load->view('widgets/invite_facebook');
	}
	function invite_gmail(){
		$this->load->view('widgets/import_gmail');
	}
	function quick_register(){
		$this->load->view('widgets/quick_register');
	}
	function my_photos(){
		$uid = $this->session->userdata('logged_uid');
		$data['photos'] = $this->Userphoto_model->retrieve_largest(array('uid'=>$uid));
		$this->load->view('widgets/my_photos',$data);
	}
	function my_appearance(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model('Userappearance_model');
		$ethnicities = $this->List_model->retrieve(array('group'=>'ethnicity'));
		$nationalities = $this->List_model->retrieve(array('group'=>'nationality'));
		$eye_colors = $this->List_model->retrieve(array('group'=>'eye_color'));
		$hair_colors = $this->List_model->retrieve(array('group'=>'hair_color'));
		$data['me'] = $this->Userappearance_model->retrieve(array('uid'=>$uid));
		$data['ethnicities'] = $ethnicities;
		$data['nationalities'] = $nationalities;
		$data['eye_colors'] = $eye_colors;
		$data['hair_colors'] = $hair_colors;
		//print_r($data);
		$this->load->view('widgets/my_appearance',$data);
	}
	function update_my_appearance(){
		$this->load->model('Userappearance_model');
		$data = $_POST;
		$where = array('uid' => $this->session->userdata('logged_uid'));
		$appearance_check = $this->Userappearance_model->retrieve($where);
		if(!empty($appearance_check)){
			$this->Userappearance_model->update($data, $where);
		}else{
			$data['uid'] = $this->session->userdata('logged_uid');
			$this->Userappearance_model->insert($data);
		}
	}
	function roulette(){
		$this->load->model('User_model');
		$this->load->model('Userphoto_model');
		$this->load->model('Userinterest_model');
		$user = $this->User_model->retrieve(array('uid' => $this->session->userdata('logged_uid'))); 
		//$user_interests = $this->Userinterest_model->retrieve(array('uid' =>$user[0]['uid']));
		//print_r($user_interests);
		$where = array();
		// foreach($user_interests as $interest){
			// if($interest['lid'] == 2 || //dating men
			   // $interest['lid'] == 4 //relationship with men
			   // ){
				// $men = 1;
			// }
			// if($interest['lid'] == 3 || //dating women
			   // $interest['lid'] == 5 //relationship with women
			   // ){
				// $women = 1;
			// }
		// }
		// if((isset($men) && $men == 1) && (isset($women) && $women == 1))
			// $where = array();
		// else if(isset($men) && $men == 1)
			// $where = array('sex' => 'm');
		// else if(isset($women) && $women == 1)
			// $where = array('sex' => 'f');
		$data['user'] = $this->User_model->random($where);
		
		//print_r($data['user']);
		$user_photos = $this->Userphoto_model->retrieve_largest(array('uid'=>$data['user']['uid'])); 
		$data['user_photos'] = $user_photos;
		$this->load->view('widgets/roulette',$data); 
	}
	function roulette_done(){
		$this->load->view('widgets/roulette_done'); 
	}
}