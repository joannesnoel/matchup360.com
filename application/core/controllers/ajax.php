<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {


	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->library('session');
    }

	public function updateLastSeen(){
		$uid = $this->session->userdata('logged_uid');
		$this->load->model("User_model");
		$data = array('last_seen'=>date("Y-m-d H:i:s"));
		$where = array('uid'=>$uid);
		$this->User_model->update($data,$where);
	}
	
	public function upload_photo(){
		// If you want to ignore the uploaded files,
		// set $demo_mode to true;

		$demo_mode = false;
		$upload_dir = 'uploads/';
		$allowed_ext = array('jpg','jpeg','png','gif');

		if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
			$this->exit_status('Error! Wrong HTTP method!');
		}

		if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){

			$pic = $_FILES['pic'];

			if(!in_array($this->get_extension($pic['name']),$allowed_ext)){
				$this->exit_status('Only '.implode(',',$allowed_ext).' files are allowed!');
			}	

			if($demo_mode){

				// File uploads are ignored. We only log them.

				$line = implode('		', array( date('r'), $_SERVER['REMOTE_ADDR'], $pic['size'], $pic['name']));
				file_put_contents('log.txt', $line.PHP_EOL, FILE_APPEND);

				$this->exit_status('Uploads are ignored in demo mode.');
			}

			// Move the uploaded file from the temporary
			// directory to the uploads folder:

			if(move_uploaded_file($pic['tmp_name'], $upload_dir.$pic['name'])){
				$this->load->library('s3');
				
				$random_str = random_string('alnum', 12);
			
				$file = $upload_dir.$pic['name'];
				
				$exif_data = exif_read_data ($file);
				
				$output_file = "";
				
				switch($this->get_extension($pic['name'])){
					case 'jpeg':
							$output_file = str_replace('.jpeg','.jpg',strtolower($file));
							rename($file, $output_file);
					break;
					case 'jpg':
							$output_file = $file;
					break;
					case 'png': 
							$output_file = str_replace('.png','.jpg',strtolower($file));
							$input = imagecreatefrompng($file);
							list($width, $height) = getimagesize($file);
							$output = imagecreatetruecolor($width, $height);
							$white = imagecolorallocate($output,  255, 255, 255);
							imagefilledrectangle($output, 0, 0, $width, $height, $white);
							imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
							imagejpeg($output, $output_file);
							unlink($file);
					break;
					case 'gif': 
							$output_file = str_replace('.gif','.jpg',strtolower($file));
							$input = imagecreatefromgif ($file);
							list($width, $height) = getimagesize($file);
							$output = imagecreatetruecolor($width, $height);
							$white = imagecolorallocate($output,  255, 255, 255);
							imagefilledrectangle($output, 0, 0, $width, $height, $white);
							imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
							imagejpeg($output, $output_file);
							unlink($file);
					break;
					default: $this->exit_status('Only '.implode(',',$allowed_ext).' files are allowed!');
				}
				
				S3::putObject(S3::inputFile($output_file), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_original.jpg", S3::ACL_PUBLIC_READ);
				//Create 600x600 Copy	
				$config['image_library'] = 'gd2';
				$config['source_image']	= $output_file;
				$config['maintain_ratio'] = TRUE;
				$config['width']	 = 600;
				$config['height']	= 600;
				$config['new_image'] = $random_str.'_600.jpg';

				$this->load->library('image_lib', $config); 

				$this->image_lib->resize();
				$config = array();
				$this->image_lib->clear();
				
				//Create 300x300 Copy
				$config['image_library'] = 'gd2';
				$config['source_image']	= '/var/www/uploads/'.$random_str.'_600.jpg';
				$config['maintain_ratio'] = TRUE;
				$config['width']	 = 300;
				$config['height']	= 300;
				$config['new_image'] = $random_str.'_300.jpg';
				
				$this->image_lib->initialize($config);

				$this->image_lib->resize();
				$config = array();
				$this->image_lib->clear();
				
				//Create 300x225 Cropped Copy from 300x300
				
				$config['image_library'] = 'gd2';
				$config['source_image']	= '/var/www/uploads/'.$random_str.'_300.jpg';
				$config['new_image'] = $random_str.'_300_cropped.jpg';
				$config['maintain_ratio'] = FALSE;
				
				$fileData = $this->image_lib->get_image_properties('/var/www/uploads/'.$random_str.'_300.jpg', TRUE); 
				
				if( $fileData['height'] > 225 ){
					$excess = $fileData['height'] - 225;
					$config['x_axis'] = 0;
					$config['width'] = 0;
					if($excess % 2)
						$offset = $excess / 2 - 1;
					else
						$offset = $excess / 2;
					$config['y_axis'] = $offset;
					$config['height'] = 225;
				}else{
					$excess = 225 - $fileData['height'];
					if($excess % 2)
						$offset = $excess / 2 - 1;
					else
						$offset = $excess / 2;
					$config['y_axis'] = 0-$offset;
					$config['height'] = 225;
				}
				
				//Load image library and crop
				$this->image_lib->initialize($config);
				$this->image_lib->crop();
				$config['maintain_ratio'] = FALSE;
				
				S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_600.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_600.jpg", S3::ACL_PUBLIC_READ);
				S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_300.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_300.jpg", S3::ACL_PUBLIC_READ);
				S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'_300_cropped.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_300_cropped.jpg", S3::ACL_PUBLIC_READ);
				
				unlink($output_file);
				unlink('/var/www/uploads/'.$random_str.'_600.jpg');
				unlink('/var/www/uploads/'.$random_str.'_300.jpg');
				unlink('/var/www/uploads/'.$random_str.'_300_cropped.jpg');
				
				$this->load->model('Userphoto_model');
				$data = array(
								'uid'=> $this->session->userdata('logged_uid'),
								'unique_code'=>$random_str
							);
				
				if(!empty($exif_data['DateTimeOriginal']))
					$data['date_taken'] = $exif_data['DateTimeOriginal'];
				else if(!empty($exif_data['DateTime']))
					$data['date_taken'] = $exif_data['DateTime'];
				
				if(!empty($exif_data['FileDateTime']))
					$data['file_datetime'] = $exif_data['FileDateTime'];
									
				
				$this->Userphoto_model->insert($data);
				
				$this->exit_status('Upload success!');
			}
		}

		$this->exit_status('Something went wrong with your upload!');

		// Helper functions
	}
	
	public function addBuddy(){
		$data = array(
						'from' => $this->input->post('from_uid'),
						'to' => $this->input->post('to_uid')
					);
		$this->load->model('Userbuddyrequest_model');
		$addBuddy = $this->Userbuddyrequest_model->addBuddy($data);
		if($addBuddy > 0){
			$this->exit_status('success');
		}else{
			$this->exit_status('requested');
		}
	}
	
	public function acceptBuddy(){
		$data = array(
						'from' => $this->input->post('from_uid'),
						'to' => $this->input->post('to_uid')
					);
		$this->load->model('Userbuddyrequest_model');
		$acceptBuddy = $this->Userbuddyrequest_model->acceptBuddy($data);
		
		if($acceptBuddy == -999 || $acceptBuddy > 0) {
			$this->exit_status('Success');
		}
	}
	
	public function fetchInfo() {
		$data = array(
						'uid' => $this->input->post('uid')
					);
					
		$this->load->model('Search_model');
		$info = $this->Search_model->fetchInfo($data);
		
		echo '<img class="user-profile-pic" src="https://s3.amazonaws.com/wheewhew/user/'.$info[0]['uid'].'/photos/'.$info[0]['profile_pic'].'" style="float: left; border: 1px solid #424242;">';
		echo '<p class="user-profile-pic" style="margin: 0; float: left; margin-left: 7px; font: 23pt segoe ui;">';
		echo $info[0]['firstname'] . " " . $info[0]['lastname'];
		echo '</p>';
		
		
	}
	
	private function get_extension($file_name){
		$ext = explode('.', $file_name);
		$ext = array_pop($ext);
		return strtolower($ext);
	}
	private function exit_status($str){
		echo json_encode(array('status'=>$str));
		exit;
	}
	public function get_photos(){
		$this->load->library('session');
		$this->load->model('Userphoto_model');
		$user_photos = $this->Userphoto_model->retrieve($this->session->userdata('logged_uid'));
		echo json_encode($user_photos);
	}
	
	public function create_user(){
		$this->load->helper(array('form', 'url', 'string'));
		$this->load->library(array('form_validation','encrypt','session'));
		
		$this->form_validation->set_rules('firstname', 'Firstname', 'required');
		$this->form_validation->set_rules('lastname', 'Lastname', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|matches[email-verify]|callback_email_check');
		$this->form_validation->set_rules('email-verify', 'Email Confirmation', 'valid_email|required');
		$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[8]|matches[pwd-verify]');
		$this->form_validation->set_rules('pwd-verify', 'Password Confirmation', 'required');
		$this->form_validation->set_error_delimiters('','');
		
		if ($this->form_validation->run() == FALSE){
			$result = array();
			$result['success'] = false;
			$result['error']['firstname'] = form_error('firstname');
			$result['error']['lastname'] = form_error('lastname');
			$result['error']['email'] = form_error('email');
			$result['error']['email2'] = form_error('email-verify');
			$result['error']['password'] = form_error('pwd');
			echo json_encode($result);
		}else{	
			
			$this->load->model('User_model');
			$confirmation_code = random_string('alnum', 35);
			$this->load->model('Confirmation_model');
			$username = $this->User_model->generate_xmpp_user($this->input->post('firstname', TRUE), $this->input->post('lastname', TRUE));
			$password  = random_string('alnum', 15);
			
			$node = config_item('site_domain');
			exec('sudo /usr/sbin/ejabberdctl register '.$username.' '.$node.' '.$password.' 2>&1',$output,$status);
			$user_data = array(
								'firstname'=> $this->input->post('firstname', TRUE),
								'lastname'=> $this->input->post('lastname', TRUE),
								'email'=> $this->input->post('email', TRUE),
								'password'=>  $this->encrypt->sha1($this->input->post('pwd', TRUE)),
								'xmpp_user'=>$username,
								'username'=>$username,
								'xmpp_password'=>$password,
								'joined_date'=> date("Y-m-d H:i:s")
							);
			$uid = $this->User_model->insert($user_data);
			$this->session->set_userdata('logged_uid',$uid);
			$confirmation_data = array(
									'uid' => $uid,
									'confirmation_code' => $confirmation_code
								);
			$this->Confirmation_model->insert($confirmation_data);
			$this->load->library('email');
			$this->email->from(config_item('webmaster_email'), config_item('site_name'));
			$this->email->to($this->input->post('email', TRUE)); 
			$this->email->subject(config_item('site_name').' Sign-Up Confirmation Link');
			$this->email->message('http://'.config_item('site_domain').'/welcome/confirm/'.$uid.'/'.$confirmation_code);	
			$this->email->send();

			$result['success'] = true;
			echo json_encode($result);
		}
	}
	
	public function email_check($email){
		$this->load->model('User_model');
		$check_email = $this->User_model->check_email($email);
		if($check_email){
			$this->form_validation->set_message('email_check', 'Email is already in use.');
			return false;
		}
		return true;
	}
	
	public function getUserId(){
		$this->load->library('session');
		$result = $this->db->query('select uid, profile_pic from user_info'); 
		$result = $result->result_array();
		print_r($result);
		echo $this->session->userdata('logged_uid');
	}
	public function set_profile_coordinates(){
		$this->load->helper('url');
		if($this->input->post('action') == 'crop'){
		
			$random_str = random_string('alnum', 12);
			
			copy($this->input->post('orig_photo_url'), '/var/www/uploads/'.$random_str.'.jpg');
		
			$this->load->library('image_lib');
			$this->load->library('s3');
			
			$config['image_library'] = 'gd2';
			$config['source_image']	= '/var/www/uploads/'.$random_str.'.jpg';
			$config['maintain_ratio'] = FALSE;
			$config['x_axis'] = $this->input->post('x');
			$config['y_axis'] = $this->input->post('y');
			$config['width'] = $this->input->post('w');
			$config['height'] = $this->input->post('h');

			$this->image_lib->initialize($config); 

			if ( ! $this->image_lib->crop())
			{
				echo $this->image_lib->display_errors();
			}
			
			$this->image_lib->clear();
			
			$config['image_library'] = 'gd2';
			$config['source_image']	= '/var/www/uploads/'.$random_str.'.jpg';
			$config['maintain_ratio'] = TRUE;
			$config['width']	 = 150;
			$config['height']	= 150;
			
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->load->model('Userphoto_model');
			$profile_photo = $this->Userphoto_model->profile_photo($this->session->userdata('logged_uid'));
			
			if($profile_photo){
				S3::deleteObject("wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$profile_photo);
			}
			
			S3::putObject(S3::inputFile('/var/www/uploads/'.$random_str.'.jpg'), "wheewhew", "user/".$this->session->userdata('logged_uid')."/photos/".$random_str."_profile.jpg", S3::ACL_PUBLIC_READ);
			
			$this->Userphoto_model->set_profile_photo($this->session->userdata('logged_uid'),$random_str.'_profile.jpg');
			
			unlink('/var/www/uploads/'.$random_str.'.jpg');
			echo $random_str.'_profile.jpg'; 
			
			redirect('/dashboard');
			}
		
	}
	
	public function create_userInfo(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('hometown_components', 'Hometown', 'required');		
		$this->form_validation->set_rules('sex', 'Sex', 'required');		
		$this->form_validation->set_rules('birthdate', 'Birthdate', 'required');	
		$this->form_validation->set_rules('language', 'Language', 'required');	
		$this->form_validation->set_error_delimiters('','');
		
		if($this->form_validation->run() == FALSE){
			$result = array();
			$result['success'] = false;
			$result['error']['hometown_components'] = form_error('hometown_components');
			$result['error']['sex'] = form_error('sex');
			$result['error']['birthdate'] = form_error('birthdate');
			$result['error']['language'] = form_error('language');
			echo json_encode($result);
		}else{
			$post = $this->input->post();
			$post['hometown_components'] = json_decode($post['hometown_components']);
			$long = $post['hometown_components']->lng;
			$lat  = $post['hometown_components']->lat;
			$this->load->model('Userlocation_model');
			
			$user_info = array(
						'current_longitude' => $long,
						'current_lattitude' => $lat,
						'sex'=> $post['sex'],
						'birthdate'=> $post['birthdate'],
					);
					
			$this->load->model('Userlanguage_model');
			
			foreach($post['language'] as $language_id){
				$this->Userlanguage_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'lid'=>$language_id));
			}
			
			$location_id = 0;
			
			for($i = count($post['hometown_components']->address_components)-1; $i>=0; $i--){
				$location = $post['hometown_components']->address_components[$i];
				$location_check = $this->Userlocation_model->retrieve(array('name'=>$location->long_name,'type'=>$location->types[0]));
				if($location_check == false){
					$location_types = array('locality','administrative_area_level_1','administrative_area_level_2','country');
					if($location->types[0]){
						$data = array(
									'location' => 'hometown',
									'type' => $location->types[0],
									'name' => $location->long_name,
									'parent' => $location_id
								);								
						$location_id = $this->Userlocation_model->insert($data);
					}
				}else{
					$location_id = $location_check[0]->locid;
				}
				switch($location->types[0]){
					case 'administrative_area_level_1' : 
						$user_info['state'] = $location_id;
					break;
					case 'administrative_area_level_2' : 
						$user_info['county'] = $location_id;
					break;
					case 'locality' : 
						$user_info['city'] = $location_id;
					break;
					case 'country' : 
						$user_info['country'] = $location_id;
					break;
				}
			}
			$this->session->set_userdata('user_location', array('lat'=>$lat,'lng'=>$long));
			$this->load->model('User_model');
			$this->User_model->update($user_info, array('uid'=>$this->session->userdata('logged_uid')));
			echo json_encode(array('success'=>true));
		}
	}
	
	public function createProfile(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('interested_in', 'Interested In', 'required');		
		$this->form_validation->set_rules('heading', 'Profile Heading', 'required');		
		$this->form_validation->set_rules('intro', 'Introduce Yourself', 'required');	
		$this->form_validation->set_error_delimiters('','');
		
		if($this->form_validation->run() == FALSE){
			$result = array();
			$result['success'] = false;
			$result['error']['interested_in'] = form_error('interested_in');
			$result['error']['heading'] = form_error('heading');
			$result['error']['intro'] = form_error('intro');
			echo json_encode($result);
		}else{
			$post = $this->input->post();
			$this->load->model('Userprofile_model');
			
			$user_info = array(
						'heading' => $post['heading'],
						'intro' => $post['intro'],
						'uid' => $this->session->userdata('logged_uid')
					);
					
			$this->load->model('Userinterest_model');
			
			foreach($post['interested_in'] as $interest_id){
				$this->Userinterest_model->insert(array('uid'=>$this->session->userdata('logged_uid'),'interest_id'=>$interest_id));
			}
			
			$this->Userprofile_model->insert($user_info);
			
			echo json_encode(array('success'=>true));
		}
	}
	
	public function find_people(){
		$post = $this->input->post();
		$this->load->model('Search_model');
		$from_location = array(
							'lat'=>$post['lat'],
							'lng'=>$post['lng']
						);
		$age_range[0] = $post['age_from'];
		$age_range[1] = $post['age_to'];
		
		$this->Search_model->search_nearby_people($from_location,$post['radius'],$post['interested_in'], $age_range);
	}
	
	public function random_members(){
		$this->load->model('User_model');
		$members = $this->User_model->random_members($this->session->userdata('logged_uid'),20);
		echo json_encode($members);
	}
	
	public function get_members(){
		$criteria = $this->input->post(NULL, true);
		$this->load->model('Search_model');
		$members = $this->Search_model->members($criteria);	
		echo json_encode($members);
	}
	
	public function profileWidget($uid){
		
		$this->load->model('User_model');
		$user = $this->User_model->get_user_details($uid);
		$user = $user[0];
		$page_data['photo_url'] = $user['profile_pic'];
		$page_data['uid'] = $uid;
		$page_data['jabber_address'] = $user['xmpp_user'];
		$page_data['firstname'] = $user['firstname'];
		$page_data['lastname'] = $user['lastname'];
		$page_data['age'] = $user['age'];
		$page_data['sex'] = $user['sex'];
		$page_data['interested_in_men'] = $user['interested_in_men'];
		$page_data['interested_in_women'] = $user['interested_in_women'];
		$page_data['status'] = $user['relationship_status'];
		$page_data['hometown_location'] = $user['hometown_location'];
		$page_data['current_location'] = $user['current_location'];
		$page_data['about_me'] = $user['about_me'];
		$page_data['favorite_quotation'] = $user['favorite_quotation'];
		$page_data['user_photos'] = $this->User_model->get_user_photos($uid);
		$this->load->view("widgets/profile",$page_data);
	}
	
	public function sendMessage(){
		error_reporting(E_ERROR);
		include("/var/www/XMPPHP/XMPP.php");
		$postData = $this->input->post();
		$this->load->model('User_model');
		$from_user = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$to_user = $this->User_model->retrieve(array('uid'=>$postData['to_uid']));
		
		$from_user = $from_user[0];
		$to_user = $to_user[0];
		
		$message = array();
		
		$msg_insert = array('from_uid'=>$from_user['uid'],'to_uid'=>$to_user['uid'],'message'=>$postData['message'],'sent_date'=>date("Y-m-d H:i:s"));
		if(isset($post['type'])){
			$msg_insert['type'] = $post['type'];
			$message['type'] = $post['type'];
		}
		
		$this->load->model('Usermessages_model');
		$message_id = $this->Usermessages_model->insert($msg_insert);
		
		$message['from_uid'] = $from_user['uid'];
		$message['from_fullname'] = $from_user['firstname'].' '.$from_user['lastname'];
		$message['to_uid'] = $to_user['uid'];
		$message['to_fullname'] = $to_user['firstname'].' '.$to_user['lastname'];
		$message['body'] = $postData['message'];		
		$message['msg_id'] = $message_id;		
		$message = json_encode($message);
		//print_r(array('from_uid'=>$from_user['uid'],'to_uid'=>$to_user['uid'],'messsage'=>$postData['message'],'msg_date'=>date("Y-m-d H:i:s"))); die();
		// $conn = new XMPPHP_XMPP(config_item('site_domain'), 5222, 'admin', 'sempron123', 'default', config_item('site_domain'), $printlog=False, $loglevel=LOGGING_INFO);
		// $conn->connect();
		// $conn->processUntil('session_start');
		// $conn->message($to_user['xmpp_user'].'@matchup360.com/default', $message, $post['type']);
		// $conn->disconnect();
		echo json_encode(array('status'=>'success','from_jid'=>$from_user['xmpp_user'].'@matchup360.com/default','to_jid'=>$to_user['xmpp_user'].'@matchup360.com/default','msg_id'=>$message_id,'message'=>$message));
	}
	function messageReceived(){
		$postData = $this->input->post();
		$this->load->model('Usermessages_model');
		$this->Usermessages_model->update(array('read_date'=>date("Y-m-d H:i:s")),array('mid'=>$postData['msg_id']));
	}
	function followMember(){
		include("/var/www/XMPPHP/XMPP.php");
		error_reporting(E_ERROR);
		$post = $this->input->post();
		$this->load->model('User_model');
		$from_user = $this->User_model->retrieve(array('uid'=>$this->session->userdata('logged_uid')));
		$to_user = $this->User_model->retrieve(array('uid'=>$post['to_uid']));
		$from_user = $from_user[0];
		$to_user = $to_user[0];
		$this->load->model('Userfollow_model');
		$this->Userfollow_model->insert(array('from'=>$from_user['uid'],''=>$to_user['uid']));
		
		$conn = new XMPPHP_XMPP(config_item('site_domain'), 5222, 'admin', 'sempron123', 'default', config_item('site_domain'), $printlog=False, $loglevel=LOGGING_INFO);
		$conn->connect();
		$conn->processUntil('session_start');
		//$conn->message($to_user['xmpp_user'].'@matchup360.com/default', $message, $post['type']);
		$conn->disconnect();
		
		echo json_encode(array('status'=>'success'));
	}	
	function testAjaxPage(){
		$this->load->view('test');
	}
}