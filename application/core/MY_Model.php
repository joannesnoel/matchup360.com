<?php
class MY_Model extends CI_Model { 

	protected $table_name = '';
	
   function __construct(){
		// Call the Model constructor
        parent::__construct();
		$this->load->database();
	}
	public function setTable($table){
		if ($this->db->table_exists($table)){
			$this->table_name = $table;
		}else{
			die("No database table '".$table."'");
		}
	}
	public function insert($data){
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();
	}
	public function update($data,$where){
		$this->db->where($where);
		$this->db->update($this->table_name,$data);
	}
	public function retrieve($where,$limit = null){
		$this->db->where($where);
		if(is_numeric($limit))
			$this->db->limit($limit);
		return $this->db->get($this->table_name)->result_array();
	}
}
?>